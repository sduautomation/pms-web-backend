<?php

return [
    'app_token_lifetime_hours' => env('APP_TOKEN_LIFETIME_HOURS', 12),
    'app_passport_key' => env('APP_PASSPORT_KEY'),
    'encryptor_key' => env('ENCRYPTOR_KEY'),
    'forgot_password_captcha_keyword' => env('FORGOT_PASSWORD_CAPTCHA_KEYWORD'),
    'no_reply_email' => env('NO_REPLY_EMAIL'),
    'user_mailer_postfix' => env('USER_MAILER_POSTFIX'),
    'test_user_username' => env('TEST_USER_USERNAME'),
    'test_user_password' => env('TEST_USER_PASSWORD'),
];
