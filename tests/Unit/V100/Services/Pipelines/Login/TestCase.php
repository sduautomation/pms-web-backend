<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Services\Pipelines\Login;

use Domains\Authentication\V100\Cooker\Attributes\LoginAttribute;
use Domains\Authentication\V100\Cooker\DTO\LoginRequestDTO;
use Domains\Authentication\V100\Cooker\ValueObjects\LoginResultValueObject;
use JetBrains\PhpStorm\Pure;
use Tests\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    /**
     * @return LoginAttribute
     */
    protected function getLoginAttribute(): LoginAttribute
    {
        $credentials = $this->getTestUserCredentials();
        $attribute = new LoginAttribute();
        $attribute->loginRequestDto = new LoginRequestDTO(
            username: $credentials['username'],
            password: $credentials['password'],
            ipAddress: 'auto-testing',
            deviceInfo: 'auto-testing',
        );

        return $attribute;
    }

    /**
     * @param LoginResultValueObject $loginResult
     * @return bool
     */
    protected function isUserVerified(LoginResultValueObject $loginResult): bool
    {
        return $loginResult->isVerified === 1 && $loginResult->result > 0;
    }

    /**
     * @param bool $isVerified
     * @param int $result
     * @return LoginResultValueObject
     */
    #[Pure]
    protected function getLoginResult(bool $isVerified, int $result): LoginResultValueObject
    {
        return new LoginResultValueObject(
            isVerified: $isVerified ? 1 : 0,
            result: $result
        );
    }
}
