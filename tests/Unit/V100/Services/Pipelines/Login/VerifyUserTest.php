<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Services\Pipelines\Login;

use Domains\Authentication\V100\Cooker\Services\Pipelines\Login\VerifyUser;
use Domains\Authentication\V100\Core\Exceptions\AuthenticationException;

/**
 * Class VerifyUserTest.
 */
final class VerifyUserTest extends TestCase
{
    private VerifyUser $pipe;

    protected function setUp(): void
    {
        parent::setUp();

        $this->pipe = $this->app->make(VerifyUser::class);
    }

    /**
     * @throws AuthenticationException
     */
    public function testUserVerified(): void
    {
        $attribute = $this->getLoginAttribute();
        $attribute->loginProcedureResult = $this->getLoginResult(true, 1);

        $this->pipe->handle($attribute, function () {
        });

        $this->assertTrue(true);
    }

    public function testUserNotVerified(): void
    {
        $attribute = $this->getLoginAttribute();
        $attribute->loginProcedureResult = $this->getLoginResult(false, 0);

        $this->expectException(AuthenticationException::class);

        $this->pipe->handle($attribute, function () {
        });
    }
}
