<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Services\Pipelines\Login;

use Domains\Authentication\V100\Cooker\Services\Pipelines\Login\GenerateResponse;
use Domains\Authentication\V100\Core\Exceptions\AuthenticationException;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use Support\Models\Employee;

/**
 * Class GenerateResponseTest.
 */
final class GenerateResponseTest extends TestCase
{
    private GenerateResponse $pipe;

    protected function setUp(): void
    {
        parent::setUp();

        $this->pipe = $this->app->make(GenerateResponse::class);
    }

    /**
     * @throws AuthenticationException
     */
    public function testGenerateResponse(): void
    {
        $attribute = $this->getLoginAttribute();
        $attribute->employee = $this->getTestUser();
        $attribute->loginProcedureResult = $this->getLoginResult(true, 1);
        $attribute->user = $this->authenticateUser($attribute->employee);

        $this->pipe->handle($attribute, function () {
        });

        $isResponseSet = isset($attribute->loginResponseDto);

        $this->assertTrue($isResponseSet);
        $this->assertObjectHasAttribute('token', $attribute->loginResponseDto);
        $this->assertObjectHasAttribute('tokenType', $attribute->loginResponseDto);
        $this->assertObjectHasAttribute('expiresIn', $attribute->loginResponseDto);
        $this->assertObjectHasAttribute('log', $attribute->loginResponseDto);
        $this->assertObjectHasAttribute('access', $attribute->loginResponseDto);
    }

    /**
     * @param Employee $employee
     * @return Authenticatable|null
     */
    private function authenticateUser(Employee $employee): ?Authenticatable
    {
        Auth::login($employee);

        return Auth::user();
    }
}
