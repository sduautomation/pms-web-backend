<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Services\Pipelines\Login;

use Domains\Authentication\V100\Cooker\Services\Pipelines\Login\SetEmployee;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class SetEmployeeTest.
 */
final class SetEmployeeTest extends TestCase
{
    private SetEmployee $pipe;

    protected function setUp(): void
    {
        parent::setUp();

        $this->pipe = $this->app->make(SetEmployee::class);
    }

    public function testSetEmployee(): void
    {
        $attribute = $this->getLoginAttribute();

        $this->pipe->handle($attribute, function () {
        });

        $isEmployeeSet = isset($attribute->employee);
        $this->assertTrue($isEmployeeSet);
    }

    public function testSetEmployeeFailure(): void
    {
        $attribute = $this->getLoginAttribute();
        $attribute->loginRequestDto->username = 'wrong user';

        $this->expectException(ModelNotFoundException::class);

        $this->pipe->handle($attribute, function () {
        });
    }
}
