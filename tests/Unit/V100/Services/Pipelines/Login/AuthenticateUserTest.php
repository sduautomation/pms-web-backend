<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Services\Pipelines\Login;

use Domains\Authentication\V100\Cooker\Services\Pipelines\Login\AuthenticateUser;
use Domains\Authentication\V100\Core\Exceptions\AuthenticationException;

/**
 * Class AuthenticateUserTest.
 */
final class AuthenticateUserTest extends TestCase
{
    private AuthenticateUser $pipe;

    protected function setUp(): void
    {
        parent::setUp();

        $this->pipe = $this->app->make(AuthenticateUser::class);
    }

    /**
     * @throws AuthenticationException
     */
    public function testIsUserAuthenticated(): void
    {
        $attribute = $this->getLoginAttribute();
        $attribute->employee = $this->getTestUser();

        $this->pipe->handle($attribute, function () {
        });

        $isUserSet = isset($attribute->user);
        $this->assertTrue($isUserSet);
    }
}
