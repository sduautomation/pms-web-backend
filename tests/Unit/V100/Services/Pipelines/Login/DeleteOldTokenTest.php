<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Services\Pipelines\Login;

use Domains\Authentication\V100\Cooker\Services\Pipelines\Login\DeleteOldToken;

/**
 * Class DeleteOldTokenTest.
 */
final class DeleteOldTokenTest extends TestCase
{
    private DeleteOldToken $pipe;

    protected function setUp(): void
    {
        parent::setUp();

        $this->pipe = $this->app->make(DeleteOldToken::class);
    }

    public function testOldTokenDeleted(): void
    {
        $attribute = $this->getLoginAttribute();
        $attribute->employee = $this->getTestUser();

        $this->pipe->handle($attribute, function () {
        });

        $this->assertTrue(true);
    }
}
