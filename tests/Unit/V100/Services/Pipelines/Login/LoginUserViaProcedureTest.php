<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Services\Pipelines\Login;

use Domains\Authentication\V100\Cooker\Services\Pipelines\Login\LoginUserViaProcedure;
use Support\Core\Exceptions\JsonApi\HttpException;

/**
 * Class LoginUserViaProcedureTest.
 */
final class LoginUserViaProcedureTest extends TestCase
{
    private LoginUserViaProcedure $pipe;

    protected function setUp(): void
    {
        parent::setUp();

        $this->pipe = $this->app->make(LoginUserViaProcedure::class);
    }

    /**
     * @throws HttpException
     */
    public function testPipeStructure(): void
    {
        $attribute = $this->getLoginAttribute();

        $this->pipe->handle($attribute, function () {
        });

        $isResultDtoSet = isset($attribute->loginProcedureResult);
        $this->assertTrue($isResultDtoSet);
        $this->assertObjectHasAttribute('isVerified', $attribute->loginProcedureResult);
        $this->assertObjectHasAttribute('result', $attribute->loginProcedureResult);
    }

    /**
     * @throws HttpException
     */
    public function testLoginUserSuccessfully(): void
    {
        $attribute = $this->getLoginAttribute();
        $this->pipe->handle($attribute, function () {
        });

        $this->assertTrue($this->isUserVerified($attribute->loginProcedureResult));
    }

    /**
     * @throws HttpException
     */
    public function testLoginUserFailure(): void
    {
        $attribute = $this->getLoginAttribute();
        $attribute->loginRequestDto->password = 'wrong password';

        $this->pipe->handle($attribute, function () {
        });

        $this->assertFalse($this->isUserVerified($attribute->loginProcedureResult));
    }
}
