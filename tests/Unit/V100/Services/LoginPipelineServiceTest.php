<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Services;

use Domains\Authentication\V100\Cooker\DTO\LoginRequestDTO;
use Domains\Authentication\V100\Cooker\Services\LoginPipelineService;
use Domains\Authentication\V100\Resources\LoginResource;
use Tests\TestCase;

/**
 * Class LoginPipelineServiceTest.
 */
final class LoginPipelineServiceTest extends TestCase
{
    private LoginPipelineService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(LoginPipelineService::class);
    }

    public function testLoginPipeline(): void
    {
        $credentials = $this->getTestUserCredentials();
        $loginRequestDto = new LoginRequestDTO(
            username: $credentials['username'],
            password: $credentials['password'],
            ipAddress: 'auto-testing',
            deviceInfo: 'auto-testing',
        );

        /** @var LoginResource $resource */
        $resource = $this->service
            ->setAttributeFromData($loginRequestDto)
            ->handle()
            ->resource();

        $this->assertObjectHasAttribute('token', $resource->resource);
        $this->assertObjectHasAttribute('tokenType', $resource->resource);
        $this->assertObjectHasAttribute('expiresIn', $resource->resource);
        $this->assertObjectHasAttribute('log', $resource->resource);
        $this->assertObjectHasAttribute('access', $resource->resource);
    }
}
