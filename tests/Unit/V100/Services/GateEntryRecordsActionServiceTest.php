<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Services;

use Domains\Administrative\v100\Cooker\Services\GateEntryRecordsActionService;
use Domains\Administrative\v100\Cooker\ValueObjects\GateEntryRecordValueObject;
use Domains\Administrative\v100\Resources\GateEntryRecordResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Tests\TestCase;

/**
 * Class GateEntryRecordsActionServiceTest.
 */
final class GateEntryRecordsActionServiceTest extends TestCase
{
    private const TOURNIQUET_READER_GROUP = 1;
    private const FROM_DATE = '2020-01-01';
    private const DUE_DATE = '2022-01-01';
    private const DEPARTMENT_CODE = '10051';
    private const STUDENT_ID = '180107149';

    private GateEntryRecordsActionService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(GateEntryRecordsActionService::class);
    }

    public function testGetEmployeeRecords(): void
    {
        /**
         * To do Test Gate entry record cases.
         */
    }

    /**
     * @param AnonymousResourceCollection $resourceCollection
     */
    private function assertResourceStructure(AnonymousResourceCollection $resourceCollection): void
    {
        $resourceCollection->collection->each(function (GateEntryRecordResource $resource) {
            /** @var GateEntryRecordValueObject $record */
            $record = $resource->resource;

            $this->assertObjectHasAttribute('fullName', $record);
            $this->assertObjectHasAttribute('userToken', $record);
            $this->assertObjectHasAttribute('accessIdIn', $record);
            $this->assertObjectHasAttribute('accessIdOut', $record);
            $this->assertObjectHasAttribute('cardNumber', $record);
            $this->assertObjectHasAttribute('workTime', $record);
            $this->assertObjectHasAttribute('dayOfWeek', $record);
            $this->assertObjectHasAttribute('accessDateIn', $record);
            $this->assertObjectHasAttribute('accessDateOut', $record);
            $this->assertObjectHasAttribute('accessTimeIn', $record);
            $this->assertObjectHasAttribute('accessTimeOut', $record);
            $this->assertObjectHasAttribute('readerTypeIn', $record);
            $this->assertObjectHasAttribute('readerTypeOut', $record);
            $this->assertObjectHasAttribute('weekDayTitle', $record);
        });
    }
}
