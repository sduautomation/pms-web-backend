<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Profile\Achievements\Services;

use Domains\Profile\V100\Cooker\DTO\ManageInterestDTO;
use Domains\Profile\V100\Cooker\Services\EditInterestPipelineService;
use Domains\Profile\V100\Cooker\Services\EditLanguageSkillPipelineService;
use Domains\Profile\V100\Models\Interest;
use Domains\Profile\V100\Resources\InterestResource;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class EditInterestPipelineServiceTest extends TestCase
{
    use WithFaker;

    private EditInterestPipelineService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(EditLanguageSkillPipelineService::class);
    }

    public function testEdit(): void
    {
        /** @var Interest $interest */
        $interest = Interest::firstOrFail();
        $dto = $this->getDto();

        /** @var InterestResource $resource */
        $resource = $this->service
            ->setLanguageSkill($interest)
            ->setAttributeFromData($dto)
            ->handle()
            ->resource();

        /** @var Interest $interest */
        $interest = $resource->resource;

        $this->assertEquals($dto->interestTitle, $interest->interest_title);
        $this->assertEquals($dto->employeeId, $interest->emp_id);
    }

    private function getDto(): ManageInterestDTO
    {
        $user = $this->getTestUser();

        return new ManageInterestDTO(
            employeeId: $user->emp_id,
            interestTitle: $this->faker->title
        );
    }
}
