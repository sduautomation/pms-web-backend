<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Profile\Achievements\Services;

use Domains\Profile\V100\Cooker\DTO\ManageLanguageDTO;
use Domains\Profile\V100\Cooker\Services\EditLanguageSkillPipelineService;
use Domains\Profile\V100\Models\LanguageSkill;
use Domains\Profile\V100\Resources\LanguageSkillResource;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class EditLanguageSkillPipelineServiceTest extends TestCase
{
    use WithFaker;

    private EditLanguageSkillPipelineService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(EditLanguageSkillPipelineService::class);
    }

    public function testEdit(): void
    {
        /** @var LanguageSkill $project */
        $languageSkill = LanguageSkill::firstOrFail();
        $dto = $this->getDto();

        /** @var LanguageSkillResource $resource */
        $resource = $this->service
            ->setLanguageSkill($languageSkill)
            ->setAttributeFromData($dto)
            ->handle()
            ->resource();

        /** @var LanguageSkill $languageSkill */
        $languageSkill = $resource->resource;

        $this->assertEquals($dto->languageLevelId, $languageSkill->level_id);
        $this->assertEquals($dto->languageCode, $languageSkill->lang_code);
        $this->assertEquals($dto->languageType, $languageSkill->is_native);
        $this->assertEquals($dto->employeeId, $languageSkill->emp_id);
    }

    private function getDto(): ManageLanguageDTO
    {
        $user = $this->getTestUser();

        return new ManageLanguageDTO(
            employeeId: $user->emp_id,
            languageCode: $this->faker->languageCode,
            languageLevelId: $this->faker->numberBetween(0, 6),
            languageType: $this->faker->numberBetween(0, 1)
        );
    }
}
