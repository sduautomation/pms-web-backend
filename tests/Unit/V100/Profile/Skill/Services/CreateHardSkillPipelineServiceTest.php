<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Profile\Biography\Services;

use Domains\Profile\V100\Cooker\DTO\ManageHardSkillDTO;
use Domains\Profile\V100\Cooker\Services\CreateHardSkillPipelineService;
use Domains\Profile\V100\Models\HardSkill;
use Domains\Profile\V100\Resources\HardSkillResource;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class CreateHardSkillPipelineServiceTest extends TestCase
{
    use WithFaker;

    private CreateHardSkillPipelineService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(CreateHardSkillPipelineService::class);
    }

    public function testCreate(): void
    {
        $dto = $this->getDto();

        /** @var HardSkillResource $resource */
        $resource = $this->service
            ->setAttributeFromData($dto)
            ->handle()
            ->resource();

        /** @var HardSkill $hardSkill */
        $hardSkill = $resource->resource;

        $this->assertEquals($dto->hardSkillTitle, $hardSkill->hard_skill_title);
        $this->assertEquals($dto->employeeId, $hardSkill->emp_id);
    }

    private function getDto(): ManageHardSkillDTO
    {
        $user = $this->getTestUser();

        return new ManageHardSkillDTO(
            employeeId: $user->emp_id,
            hardSkillTitle: $this->faker->title,
        );
    }
}
