<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Profile\Skills\Services;

use Domains\Profile\V100\Cooker\DTO\GetSkillsDTO;
use Domains\Profile\V100\Cooker\Services\SkillsPipelineService;
use Domains\Profile\V100\Core\Enums\SkillTypes;
use Domains\Profile\V100\Resources\GetSkillsResource;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use JetBrains\PhpStorm\Pure;
use Tests\TestCase;

final class SkillsPipelineServiceTest extends TestCase
{
    private SkillsPipelineService $service;

    /**
     * @throws BindingResolutionException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->service = Container::getInstance()->make(SkillsPipelineService::class);
    }

    public function testAll(): void
    {
        /** @var GetSkillsResource $resource */
        $resource = $this->service
            ->setAttributeFromData($this->getDto(SkillTypes::ALL))
            ->handle()
            ->resource();

        $resource = $resource->resource;

        $this->assertObjectHasAttribute('hard_skill', $resource);
        $this->assertObjectHasAttribute('interest', $resource);
        $this->assertObjectHasAttribute('language_skill', $resource);
    }

    public function testInterest(): void
    {
        /** @var GetSkillsResource $resource */
        $resource = $this->service
            ->setAttributeFromData($this->getDto(SkillTypes::INTEREST))
            ->handle()
            ->resource();

        $resource = $resource->resource;

        $this->assertObjectHasAttribute('interest', $resource);
    }

    public function testHardSkill(): void
    {
        /** @var GetSkillsResource $resource */
        $resource = $this->service
            ->setAttributeFromData($this->getDto(SkillTypes::HARDSKILL))
            ->handle()
            ->resource();

        $resource = $resource->resource;

        $this->assertObjectHasAttribute('hard_skill', $resource);
    }

    public function testLanguageSkill(): void
    {
        /** @var GetSkillsResource $resource */
        $resource = $this->service
            ->setAttributeFromData($this->getDto(SkillTypes::LANGUAGE))
            ->handle()
            ->resource();

        $resource = $resource->resource;

        $this->assertObjectHasAttribute('language_skill', $resource);
    }

    #[Pure]
    private function getDto(string $type): GetSkillsDTO
    {
        return new GetSkillsDTO(
            type: $type,
            employeeId: $this->getTestUser()->emp_id
        );
    }
}
