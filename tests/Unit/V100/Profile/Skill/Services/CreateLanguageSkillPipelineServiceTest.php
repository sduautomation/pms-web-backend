<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Profile\Biography\Services;

use Domains\Profile\V100\Cooker\DTO\ManageLanguageDTO;
use Domains\Profile\V100\Cooker\Services\CreateLanguagePipelineService;
use Domains\Profile\V100\Models\LanguageSkill;
use Domains\Profile\V100\Resources\LanguageSkillResource;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class CreateLanguageSkillPipelineServiceTest extends TestCase
{
    use WithFaker;

    private CreateLanguagePipelineService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(CreateLanguagePipelineService::class);
    }

    public function testCreate(): void
    {
        $dto = $this->getDto();

        /** @var LanguageSkillResource $resource */
        $resource = $this->service
            ->setAttributeFromData($dto)
            ->handle()
            ->resource();

        /** @var LanguageSkill $languageSkill */
        $languageSkill = $resource->resource;

        $this->assertEquals($dto->languageLevelId, $languageSkill->level_id);
        $this->assertEquals($dto->languageCode, $languageSkill->lang_code);
        $this->assertEquals($dto->languageType, $languageSkill->is_native);
        $this->assertEquals($dto->employeeId, $languageSkill->emp_id);
    }

    private function getDto(): ManageLanguageDTO
    {
        $user = $this->getTestUser();

        return new ManageLanguageDTO(
            employeeId: $user->emp_id,
            languageCode: $this->faker->languageCode,
            languageLevelId: $this->faker->numberBetween(0, 6),
            languageType: $this->faker->numberBetween(0, 1)
        );
    }
}
