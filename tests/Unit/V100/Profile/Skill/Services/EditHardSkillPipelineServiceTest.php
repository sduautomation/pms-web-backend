<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Profile\Achievements\Services;

use Domains\Profile\V100\Cooker\DTO\ManageHardSkillDTO;
use Domains\Profile\V100\Cooker\Services\EditHardSkillPipelineService;
use Domains\Profile\V100\Cooker\Services\EditLanguageSkillPipelineService;
use Domains\Profile\V100\Models\HardSkill;
use Domains\Profile\V100\Resources\HardSkillResource;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class EditHardSkillPipelineServiceTest extends TestCase
{
    use WithFaker;

    private EditHardSkillPipelineService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(EditLanguageSkillPipelineService::class);
    }

    public function testEdit(): void
    {
        /** @var HardSkill $hardSkill */
        $hardSkill = HardSkill::firstOrFail();
        $dto = $this->getDto();

        /** @var HardSkillResource $resource */
        $resource = $this->service
            ->setHardSkill($hardSkill)
            ->setAttributeFromData($dto)
            ->handle()
            ->resource();

        /** @var HardSkill $hardSkill */
        $hardSkill = $resource->resource;

        $this->assertEquals($dto->hardSkillTitle, $hardSkill->hard_skill_title);
        $this->assertEquals($dto->employeeId, $hardSkill->emp_id);
    }

    private function getDto(): ManageHardSkillDTO
    {
        $user = $this->getTestUser();

        return new ManageHardSkillDTO(
            employeeId: $user->emp_id,
            hardSkillTitle: $this->faker->title,
        );
    }
}
