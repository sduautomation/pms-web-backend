<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Profile\Biography\Services;

use Domains\Profile\V100\Cooker\DTO\ManageBiographyDTO;
use Domains\Profile\V100\Cooker\Services\CreateBiographyPipelineService;
use Domains\Profile\V100\Models\Biography;
use Domains\Profile\V100\Resources\BiographyResource;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class CreateBiographyPipelineServiceTest extends TestCase
{
    use WithFaker;

    private CreateBiographyPipelineService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(CreateBiographyPipelineService::class);
    }

    public function testCreate(): void
    {
        $dto = $this->getDto();

        Biography::findOrFail($dto->employeeId)->delete();

        /** @var BiographyResource $resource */
        $resource = $this->service
            ->setAttributeFromData($dto)
            ->handle()
            ->resource();

        /** @var Biography $biography */
        $biography = $resource->resource;

        $this->assertEquals($dto->biography, $biography->autobiography);
        $this->assertEquals($dto->employeeId, $biography->emp_id);
    }

    private function getDto(): ManageBiographyDTO
    {
        $user = $this->getTestUser();

        return new ManageBiographyDTO(
            biography: $this->faker->text,
            employeeId: $user->emp_id,
        );
    }
}
