<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Profile\Biography\Services;

use Domains\Profile\V100\Cooker\DTO\GetBiographiesDTO;
use Domains\Profile\V100\Cooker\Services\BiographiesPipelineService;
use Domains\Profile\V100\Core\Enums\BiographyTypes;
use Domains\Profile\V100\Resources\GetBiographiesResource;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use JetBrains\PhpStorm\Pure;
use Tests\TestCase;

final class BiographiesPipelineServiceTest extends TestCase
{
    private BiographiesPipelineService $service;

    /**
     * @throws BindingResolutionException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->service = Container::getInstance()->make(BiographiesPipelineService::class);
    }

    public function testAll(): void
    {
        /** @var GetBiographiesResource $resource */
        $resource = $this->service
            ->setAttributeFromData($this->getDto(BiographyTypes::ALL))
            ->handle()
            ->resource();

        $resource = $resource->resource;

        $this->assertObjectHasAttribute('children', $resource);
        $this->assertObjectHasAttribute('biography', $resource);
    }

    public function testBiography(): void
    {
        /** @var GetBiographiesResource $resource */
        $resource = $this->service
            ->setAttributeFromData($this->getDto(BiographyTypes::BIOGRAPHY))
            ->handle()
            ->resource();

        $resource = $resource->resource;

        $this->assertObjectHasAttribute('biography', $resource);
    }

    public function testChildren(): void
    {
        /** @var GetBiographiesResource $resource */
        $resource = $this->service
            ->setAttributeFromData($this->getDto(BiographyTypes::CHILDREN))
            ->handle()
            ->resource();

        $resource = $resource->resource;

        $this->assertObjectHasAttribute('children', $resource);
    }

    #[Pure]
    private function getDto(string $type): GetBiographiesDTO
    {
        return new GetBiographiesDTO(
            type: $type
        );
    }
}
