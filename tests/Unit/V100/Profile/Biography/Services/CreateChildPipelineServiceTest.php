<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Profile\Biography\Services;

use Carbon\CarbonImmutable;
use Domains\Profile\V100\Cooker\DTO\ManageChildDTO;
use Domains\Profile\V100\Cooker\Services\CreateChildPipelineService;
use Domains\Profile\V100\Models\Child;
use Domains\Profile\V100\Models\Gender;
use Domains\Profile\V100\Resources\ChildResource;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class CreateChildPipelineServiceTest extends TestCase
{
    use WithFaker;

    private CreateChildPipelineService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(CreateChildPipelineService::class);
    }

    public function testCreate(): void
    {
        $dto = $this->getDto();

        /** @var ChildResource $resource */
        $resource = $this->service
            ->setAttributeFromData($dto)
            ->handle()
            ->resource();

        /** @var Child $child */
        $child = $resource->resource;

        $this->assertEquals($dto->name, $child->name);
        $this->assertEquals($dto->surname, $child->sname);
        $this->assertEquals($dto->genderId, $child->gender_id);
        $this->assertEquals($dto->birthDate, $child->birth_date);
        $this->assertEquals($dto->employeeId, $child->emp_id);
    }

    private function getDto(): ManageChildDTO
    {
        $user = $this->getTestUser();
        /** @var Gender $gender */
        $gender = Gender::firstOrFail();

        return new ManageChildDTO(
            name: $this->faker->firstName,
            surname: $this->faker->lastName,
            genderId: $gender->gender_id,
            birthDate: CarbonImmutable::parse($this->faker->date),
            employeeId: $user->emp_id,
        );
    }
}
