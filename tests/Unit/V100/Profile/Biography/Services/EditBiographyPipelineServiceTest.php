<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Profile\Biography\Services;

use Domains\Profile\V100\Cooker\DTO\ManageBiographyDTO;
use Domains\Profile\V100\Cooker\Services\EditBiographyPipelineService;
use Domains\Profile\V100\Models\Biography;
use Domains\Profile\V100\Resources\BiographyResource;
use Illuminate\Foundation\Testing\WithFaker;
use JetBrains\PhpStorm\Pure;
use Tests\TestCase;

final class EditBiographyPipelineServiceTest extends TestCase
{
    use WithFaker;

    private EditBiographyPipelineService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(EditBiographyPipelineService::class);
    }

    public function testEdit(): void
    {
        $user = $this->getTestUser();
        /** @var Biography $biography */
        $biography = Biography::findOrFail($user->emp_id);

        $dto = $this->getDto($biography->emp_id);

        /** @var BiographyResource $resource */
        $resource = $this->service
            ->setBiography($biography)
            ->setAttributeFromData($dto)
            ->handle()
            ->resource();

        /** @var Biography $biography */
        $biography = $resource->resource;

        $this->assertEquals($dto->biography, $biography->autobiography);
        $this->assertEquals($dto->employeeId, $biography->emp_id);
    }

    #[Pure]
    private function getDto(int $employeeId): ManageBiographyDTO
    {
        return new ManageBiographyDTO(
            biography: $this->faker->text,
            employeeId: $employeeId
        );
    }
}
