<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Profile\Contacts;

use Domains\Profile\V100\Cooker\DTO\GetContactsDTO;
use Domains\Profile\V100\Cooker\Services\ContactsPipelineService;
use Domains\Profile\V100\Resources\GetContactsResource;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use JetBrains\PhpStorm\Pure;
use Tests\TestCase;

final class ContactsPipelineServiceTest extends TestCase
{
    private ContactsPipelineService $service;

    /**
     * @throws BindingResolutionException
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->service = Container::getInstance()->make(ContactsPipelineService::class);
    }

    public function testContacts(): void
    {
        /** @var GetContactsResource $resource */
        $resource = $this->service
            ->setAttributeFromData($this->getDto())
            ->handle()
            ->resource();

        $resource = $resource->resource;

        $this->assertObjectHasAttribute('contacts', $resource);
    }

    #[Pure]
    private function getDto(): GetContactsDTO
    {
        return new GetContactsDTO(
            employeeId: $this->getTestUser()->emp_id
        );
    }
}
