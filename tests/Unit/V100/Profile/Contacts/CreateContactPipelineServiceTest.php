<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Profile\Contacts;

use Domains\Profile\V100\Cooker\DTO\ManageContactDTO;
use Domains\Profile\V100\Cooker\Services\CreateContactPipelineService;
use Domains\Profile\V100\Models\Contact;
use Domains\Profile\V100\Models\ContactType;
use Domains\Profile\V100\Resources\ContactResource;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class CreateContactPipelineServiceTest extends TestCase
{
    use WithFaker;

    private CreateContactPipelineService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(CreateContactPipelineService::class);
    }

    public function testCreate(): void
    {
        $dto = $this->getDto();

        /** @var ContactResource $resource */
        $resource = $this->service
            ->setAttributeFromData($dto)
            ->handle()
            ->resource();

        /** @var Contact $contact */
        $contact = $resource->resource;

        $this->assertDatabaseHas('dbmaster.contacts', [
           'contact_id' => $contact->contact_id,
        ]);

        $this->assertEquals($dto->employeeId, $contact->emp_id);
    }

    private function getDto(): ManageContactDTO
    {
        $user = $this->getTestUser();
        /** @var ContactType $contactType */
        $contactType = ContactType::firstOrFail();

        return new ManageContactDTO(
            employeeId: $user->emp_id,
            typeId: $contactType->type_id,
            contact: $this->faker->numerify('###########')
        );
    }
}
