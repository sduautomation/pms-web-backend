<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Profile\Contacts;

use Domains\Profile\V100\Cooker\DTO\ManageContactDTO;
use Domains\Profile\V100\Cooker\Services\EditContactPipelineService;
use Domains\Profile\V100\Models\Contact;
use Domains\Profile\V100\Models\ContactType;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class EditContactPipelineServiceTest extends TestCase
{
    use WithFaker;

    private EditContactPipelineService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(EditContactPipelineService::class);
    }

    public function testEdit(): void
    {
        /** @var Contact $contact */
        $contact = Contact::firstOrFail();
        $dto = $this->getDto();

        $resource = $this->service
            ->setContact($contact)
            ->setAttributeFromData($dto)
            ->handle()
            ->resource();

        /** @var Contact $contact */
        $contact = $resource->resource;

        $this->assertEquals($dto->employeeId, $contact->emp_id);
    }

    private function getDto(): ManageContactDTO
    {
        $user = $this->getTestUser();
        /** @var ContactType $contactType */
        $contactType = ContactType::firstOrFail();

        return new ManageContactDTO(
            employeeId: $user->emp_id,
            typeId: $contactType->type_id,
            contact: $this->faker->numerify('###########')
        );
    }
}
