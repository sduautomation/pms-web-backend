<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Profile\Achievements\Services;

use Carbon\CarbonImmutable;
use Domains\Profile\V100\Cooker\DTO\ManageProjectDTO;
use Domains\Profile\V100\Cooker\Services\EditProjectPipelineService;
use Domains\Profile\V100\Models\AddableTitle;
use Domains\Profile\V100\Models\Project;
use Domains\Profile\V100\Resources\ProjectResource;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class EditProjectPipelineServiceTest extends TestCase
{
    use WithFaker;

    private EditProjectPipelineService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(EditProjectPipelineService::class);
    }

    public function testEdit(): void
    {
        /** @var Project $project */
        $project = Project::firstOrFail();
        $dto = $this->getDto();

        /** @var ProjectResource $resource */
        $resource = $this->service
            ->setProject($project)
            ->setAttributeFromData($dto)
            ->handle()
            ->resource();

        /** @var Project $project */
        $project = $resource->resource;

        $this->assertEquals($dto->employeeId, $project->emp_id);
        $this->assertEquals($dto->startDate->toDateString(), $project->start_date->toDateString());
        $this->assertEquals($dto->endDate->toDateString(), $project->end_date->toDateString());
    }

    private function getDto(): ManageProjectDTO
    {
        $user = $this->getTestUser();
        /** @var AddableTitle $addableTitle */
        $addableTitle = AddableTitle::firstOrFail();

        return new ManageProjectDTO(
            employeeId: $user->emp_id,
            donorId: $addableTitle->title_id,
            projectName: $this->faker->title,
            positionId: $addableTitle->title_id,
            notes: $this->faker->text,
            startDate: CarbonImmutable::now(),
            endDate: CarbonImmutable::now(),
            ongoing: false
        );
    }
}
