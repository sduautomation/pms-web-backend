<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Profile\Achievements\Services;

use Carbon\CarbonImmutable;
use Domains\Profile\v100\Cooker\DTO\ManageWorkExperienceDTO;
use Domains\Profile\v100\Cooker\Services\CreateWorkExperiencePipelineService;
use Domains\Profile\V100\Models\AddableTitle;
use Domains\Profile\V100\Models\Address;
use Domains\Profile\V100\Models\WorkExperience;
use Domains\Profile\V100\Resources\WorkExperienceResource;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class CreateWorkExperiencePipelineServiceTest extends TestCase
{
    use WithFaker;

    private CreateWorkExperiencePipelineService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(CreateWorkExperiencePipelineService::class);
    }

    public function testCreate(): void
    {
        $dto = $this->getDto();

        /** @var WorkExperienceResource $resource */
        $resource = $this->service
            ->setAttributeFromData($dto)
            ->handle()
            ->resource();

        /** @var WorkExperience $project */
        $workExperience = $resource->resource;

        $this->assertDatabaseHas('dbmaster.cv_emp_job_exp', [
            'ejob_id' => $workExperience->ejob_id,
        ]);

        $this->assertEquals($dto->employeeId, $workExperience->emp_id);
        $this->assertEquals($dto->startDate->toDateString(), $workExperience->start_date->toDateString());
        $this->assertEquals($dto->endDate->toDateString(), $workExperience->end_date->toDateString());
    }

    private function getDto(): ManageWorkExperienceDTO
    {
        $user = $this->getTestUser();
        /** @var AddableTitle $addableTitle */
        $addableTitle = AddableTitle::firstOrFail();
        /** @var Address $address $address */
        $address = Address::firstOrFail();

        return new ManageWorkExperienceDTO(
            employeeId: $user->emp_id,
            jobTitle: $this->faker->title,
            companyId: $addableTitle->title_id,
            addressId: $address->aid,
            startDate: CarbonImmutable::now(),
            endDate: CarbonImmutable::now(),
            present: false
        );
    }
}
