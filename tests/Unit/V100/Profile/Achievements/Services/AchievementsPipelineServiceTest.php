<?php

declare(strict_types=1);

namespace Tests\Unit\V100\Profile\Achievements\Services;

use Domains\Profile\V100\Cooker\DTO\GetAchievementsDTO;
use Domains\Profile\V100\Cooker\Services\AchievementsPipelineService;
use Domains\Profile\V100\Core\Enums\AchievementTypes;
use Domains\Profile\V100\Resources\GetAchievementsResource;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use JetBrains\PhpStorm\Pure;
use Tests\Unit\V100\Services\Pipelines\Login\TestCase;

final class AchievementsPipelineServiceTest extends TestCase
{
    private AchievementsPipelineService $service;

    /**
     * @throws BindingResolutionException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->service = Container::getInstance()->make(AchievementsPipelineService::class);
    }

    public function testAll(): void
    {
        /** @var GetAchievementsResource $resource */
        $resource = $this->service
            ->setAttributeFromData($this->getDto(AchievementTypes::ALL))
            ->handle()
            ->resource();

        $resource = $resource->resource;

        $this->assertObjectHasAttribute('workExperience', $resource);
        $this->assertObjectHasAttribute('projects', $resource);
        $this->assertObjectHasAttribute('awards', $resource);
    }

    public function testWorkExperience(): void
    {
        /** @var GetAchievementsResource $resource */
        $resource = $this->service
            ->setAttributeFromData($this->getDto(AchievementTypes::EXPERIENCE))
            ->handle()
            ->resource();

        $resource = $resource->resource;

        $this->assertObjectHasAttribute('workExperience', $resource);
    }

    public function testProjects(): void
    {
        /** @var GetAchievementsResource $resource */
        $resource = $this->service
            ->setAttributeFromData($this->getDto(AchievementTypes::PROJECTS))
            ->handle()
            ->resource();

        $resource = $resource->resource;

        $this->assertObjectHasAttribute('projects', $resource);
    }

    public function testAwards(): void
    {
        /** @var GetAchievementsResource $resource */
        $resource = $this->service
            ->setAttributeFromData($this->getDto(AchievementTypes::AWARDS))
            ->handle()
            ->resource();

        $resource = $resource->resource;

        $this->assertObjectHasAttribute('awards', $resource);
    }

    #[Pure]
    private function getDto(string $type): GetAchievementsDTO
    {
        return new GetAchievementsDTO(
            employeeId: $this->getTestUser()->emp_id,
            type: $type
        );
    }
}
