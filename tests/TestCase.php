<?php

declare(strict_types=1);

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, TestUser;

    protected function setUp(): void
    {
        parent::setUp();

        if (app()->environment() !== 'testing') {
            dd('Конфиги указаны не правильно, попробуйте почистить кэш');
        }
    }
}
