<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Authentication;

use Tests\TestCase;

/**
 * Class LoginTest.
 */
final class LoginTest extends TestCase
{
    public function testLogin(): void
    {
        $credentials = $this->getTestUserCredentials();

        $response = $this->post(route('v100-auth-login'), [
            'username' => $credentials['username'],
            'password' => $credentials['password'],
        ]);

        $responseData = $response->json();

        $this->assertEquals(
            'User is successfully authenticated',
            $responseData['message']
        );

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'token',
                    'token_type',
                    'expires_in',
                    'log',
                    'access',
                ],
            ]);
    }
}
