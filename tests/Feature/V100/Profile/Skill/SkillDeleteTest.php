<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Skill;

use Domains\Profile\V100\Models\HardSkill;
use Domains\Profile\V100\Models\LanguageSkill;
use Tests\TestCase;

final class SkillDeleteTest extends TestCase
{
    public function testDeleteLanguageSkill(): void
    {
        $token = $this->login();
        /** @var LanguageSkill $project */
        $languageSkill = LanguageSkill::firstOrFail();

        $response = $this->delete(route('v100-profile-skills-languageSkills-delete', [
            'languageSkill' => $languageSkill->lang_k_id,
        ]), [], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
            ]);

        $this->assertDatabaseMissing('dbmaster.lang_know', [
            'lang_k_id' => $languageSkill->lang_k_id,
        ]);
    }

    public function testDeleteHardSkill(): void
    {
        $token = $this->login();
        /** @var HardSkill $project */
        $hardSkill = HardSkill::firstOrFail();

        $response = $this->delete(route('v100-profile-skills-hardSkills-delete', [
            'hardSkill' => $hardSkill->skill_id,
        ]), [], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
            ]);

        $this->assertDatabaseMissing('dbmaster.lang_know', [
            'skill_id' => $hardSkill->skill_id,
        ]);
    }
}
