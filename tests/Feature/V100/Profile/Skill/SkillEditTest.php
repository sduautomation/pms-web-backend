<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Achievements;

use Domains\Profile\V100\Models\HardSkill;
use Domains\Profile\V100\Models\Interest;
use Domains\Profile\V100\Models\LanguageSkill;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class SkillEditTest extends TestCase
{
    use WithFaker;

    public function testEditLanguageSkill(): void
    {
        $token = $this->login();

        /** @var LanguageSkill $languageSkill */
        $languageSkill = LanguageSkill::firstOrFail();

        $response = $this->put(route('v100-profile-skills-languageSkills-edit', ['languageSkill' => $languageSkill->lang_k_id]), [
            'is_native' => $this->faker->numberBetween(0, 1),
            'level_id' => $this->faker->numberBetween(0, 6),
            'lang_code' => $this->faker->languageCode,
        ], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'lang_k_id',
                    'emp_id',
                    'stud_id',
                    'is_native',
                    'level_id',
                    'completed',
                    'web_log',
                    'applicant_id',
                    'lang_code',
                    'language',
                    'language_level',
                ],
            ]);
    }

    public function testEditHardSkill(): void
    {
        $token = $this->login();

        /** @var HardSkill $hardSkill */
        $hardSkill = HardSkill::firstOrFail();

        $response = $this->put(route('v100-profile-skills-hardSkills-edit', ['hardSkill' => $hardSkill->skill_id]), [
            'hard_skill_title' => $this->faker->title,
        ], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'emp_id',
                    'hard_skill',
                ],
            ]);
    }

    public function testEditInterest(): void
    {
        $token = $this->login();

        /** @var Interest $interest */
        $interest = Interest::firstOrFail();

        $response = $this->put(route('v100-profile-skills-languageSkills-edit', ['interest' => $interest->emp_id]), [
            'interest_title' => $this->faker->title,
        ], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'emp_id',
                    'interest',
                ],
            ]);
    }
}
