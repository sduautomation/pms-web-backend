<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Achievements;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class SkillCreateTest extends TestCase
{
    use WithFaker;

    public function testCreateLanguageSkill(): void
    {
        $token = $this->login();

        $response = $this->post(route('v100-profile-skills-languageSkills-create'), [
            'is_native' => $this->faker->numberBetween(0, 1),
            'level_id' => $this->faker->numberBetween(0, 6),
            'lang_code' => $this->faker->languageCode,
        ], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'lang_k_id',
                    'emp_id',
                    'stud_id',
                    'is_native',
                    'level_id',
                    'completed',
                    'web_log',
                    'applicant_id',
                    'lang_code',
                    'language',
                    'language_level',
                ],
            ]);
    }

    public function testCreateHardSkill(): void
    {
        $token = $this->login();

        $response = $this->post(route('v100-profile-skills-hardSkills-create'), [
            'hard_skill_title' => $this->faker->title,
        ], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'emp_id',
                    'hard_skill',
                ],
            ]);
    }
}
