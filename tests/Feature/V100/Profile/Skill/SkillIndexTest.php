<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Skill;

use Domains\Profile\V100\Core\Enums\SkillTypes;
use Tests\TestCase;

final class SkillIndexTest extends TestCase
{
    public function testAll(): void
    {
        $token = $this->login();

        $response = $this->get(route('v100-profile-skills-index', [
            'type' => SkillTypes::ALL,
        ]), [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'language_skill' => [
                        '*' => [
                            'lang_k_id',
                            'emp_id',
                            'stud_id',
                            'is_native',
                            'level_id',
                            'completed',
                            'web_log',
                            'applicant_id',
                            'lang_code',
                            'language',
                            'languageLevel',
                        ],
                    ],
                    'interest' => [
                        '*' => [
                            'emp_id',
                            'interest',
                        ],
                    ],
                    'hard_skill' => [
                        '*' => [
                            'emp_id',
                            'hard_skill',
                        ],
                    ],
                ],
            ]);
    }

    public function testLanguageSkill(): void
    {
        $token = $this->login();
        $response = $this->get(route('v100-profile-skills-index', [
            'type' => SkillTypes::LANGUAGE,
        ]), [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'language_skill' => [
                        '*' => [
                            'lang_k_id',
                            'emp_id',
                            'stud_id',
                            'is_native',
                            'level_id',
                            'completed',
                            'web_log',
                            'applicant_id',
                            'lang_code',
                            'language',
                            'language_level',
                        ],
                    ],
                ],
            ]);
    }

    public function testHardSkill(): void
    {
        $token = $this->login();
        $response = $this->get(route('v100-profile-skills-index', [
            'type' => SkillTypes::HARDSKILL,
        ]), [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'hard_skill' => [
                        '*' => [
                            'emp_id',
                            'hard_skill',
                        ],
                    ],
                ],
            ]);
    }

    public function testInterest()
    {
        $token = $this->login();

        $response = $this->get(route('v100-profile-skills-index', [
            'type' => SkillTypes::INTEREST,
        ]), [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'interest' => [
                        '*' => [
                            'emp_id',
                            'interest',
                        ],
                    ],
                ],
            ]);
    }
}
