<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Achievements;

use Domains\Profile\V100\Core\Enums\AchievementTypes;
use Tests\TestCase;

final class AchievementsIndexTest extends TestCase
{
    public function testAll(): void
    {
        $token = $this->login();

        $response = $this->get(route('v100-profile-achievements-index', [
            'type' => AchievementTypes::ALL,
        ]), [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'work_experience' => [
                        '*' => [
                            'ejob_id',
                            'emp_id',
                            'job_title',
                            'start_date',
                            'end_date',
                            'company',
                            'address_id',
                            'uuid',
                        ],
                    ],
                    'projects' => [
                        '*' => [
                            'project_id',
                            'emp_id',
                            'uuid',
                            'project_name',
                            'position',
                            'notes',
                            'donor',
                            'start_date',
                            'end_date',
                        ],
                    ],
                    'awards' => [
                        '*' => [
                            'cid',
                            'emp_id',
                            'certified_date',
                            'certificate_name',
                            'organization_name',
                            'uuid',
                        ],
                    ],
                ],
            ]);
    }

    public function testWorkExperience(): void
    {
        $token = $this->login();
        $response = $this->get(route('v100-profile-achievements-index', [
            'type' => AchievementTypes::EXPERIENCE,
        ]), [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'work_experience' => [
                        '*' => [
                            'ejob_id',
                            'emp_id',
                            'job_title',
                            'start_date',
                            'end_date',
                            'company',
                            'address_id',
                            'uuid',
                        ],
                    ],
                ],
            ]);
    }

    public function testProjects(): void
    {
        $token = $this->login();
        $response = $this->get(route('v100-profile-achievements-index', [
            'type' => AchievementTypes::PROJECTS,
        ]), [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'projects' => [
                        '*' => [
                            'project_id',
                            'emp_id',
                            'uuid',
                            'project_name',
                            'position',
                            'notes',
                            'donor',
                            'start_date',
                            'end_date',
                        ],
                    ],
                ],
            ]);
    }

    public function testAwards(): void
    {
        $token = $this->login();
        $response = $this->get(route('v100-profile-achievements-index', [
            'type' => AchievementTypes::AWARDS,
        ]), [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'awards' => [
                        '*' => [
                            'cid',
                            'emp_id',
                            'certified_date',
                            'certificate_name',
                            'organization_name',
                            'uuid',
                        ],
                    ],
                ],
            ]);
    }
}
