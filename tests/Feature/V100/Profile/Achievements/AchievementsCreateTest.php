<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Achievements;

use Carbon\CarbonImmutable;
use Domains\Profile\V100\Models\AddableTitle;
use Domains\Profile\V100\Models\Address;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class AchievementsCreateTest extends TestCase
{
    use WithFaker;

    public function testCreateProject(): void
    {
        $token = $this->login();
        /** @var AddableTitle $addableTitle */
        $addableTitle = AddableTitle::firstOrFail();

        $response = $this->post(route('v100-profile-achievements-projects-create'), [
            'donor_id' => $addableTitle->title_id,
            'project_name' => $this->faker->title,
            'position_id' => $addableTitle->title_id,
            'notes' => $this->faker->text,
            'start_date' => CarbonImmutable::now()->toDateString(),
            'end_date' => CarbonImmutable::now()->toDateString(),
            'ongoing' => false,
        ], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'project_id',
                    'emp_id',
                    'uuid',
                    'project_name',
                    'position',
                    'notes',
                    'donor',
                    'start_date',
                    'end_date',
                ],
            ]);
    }

    public function testCreateWorkExperience(): void
    {
        $token = $this->login();
        /** @var AddableTitle $addableTitle */
        $addableTitle = AddableTitle::firstOrFail();
        /** @var Address $address */
        $address = Address::firstOrFail();

        $response = $this->post(route('v100-profile-achievements-experience-create'), [
            'job_title' => $this->faker->title,
            'company_id' => $addableTitle->title_id,
            'address_id' => $address->aid,
            'start_date' => CarbonImmutable::now()->toDateString(),
            'end_date' => CarbonImmutable::now()->toDateString(),
            'present' => false,
        ], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'ejob_id',
                    'emp_id',
                    'job_title',
                    'start_date',
                    'end_date',
                    'company',
                    'address_id',
                    'uuid',
                ],
            ]);
    }

    public function testCreateAward(): void
    {
        $token = $this->login();
        /** @var AddableTitle $addableTitle */
        $addableTitle = AddableTitle::firstOrFail();

        $response = $this->post(route('v100-profile-achievements-awards-create'), [
            'certificate_name_id' => $addableTitle->title_id,
            'organization_name_id' => $addableTitle->title_id,
            'certified_date' => CarbonImmutable::now()->toDateString(),
        ], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'cid',
                    'emp_id',
                    'certified_date',
                    'certificate_name',
                    'organization_name',
                    'uuid',
                ],
            ]);
    }
}
