<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Achievements;

use Carbon\CarbonImmutable;
use Domains\Profile\V100\Models\AddableTitle;
use Domains\Profile\V100\Models\Address;
use Domains\Profile\V100\Models\Award;
use Domains\Profile\V100\Models\Project;
use Domains\Profile\V100\Models\WorkExperience;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class AchievementsEditTest extends TestCase
{
    use WithFaker;

    public function testEditProject(): void
    {
        $token = $this->login();
        /** @var Project $project */
        $project = Project::firstOrFail();
        /** @var AddableTitle $addableTitle */
        $addableTitle = AddableTitle::firstOrFail();

        $response = $this->put(route('v100-profile-achievements-projects-edit', ['project' => $project->project_id]), [
            'donor_id' => $addableTitle->title_id,
            'project_name' => $this->faker->title,
            'position_id' => $addableTitle->title_id,
            'notes' => $this->faker->text,
            'start_date' => CarbonImmutable::now()->toDateString(),
            'end_date' => CarbonImmutable::now()->toDateString(),
            'ongoing' => false,
        ], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'project_id',
                    'emp_id',
                    'uuid',
                    'project_name',
                    'position',
                    'notes',
                    'donor',
                    'start_date',
                    'end_date',
                ],
            ]);
    }

    public function testEditWorkExperience(): void
    {
        $token = $this->login();
        /** @var WorkExperience $workExperience */
        $workExperience = WorkExperience::firstOrFail();
        /** @var AddableTitle $addableTitle */
        $addableTitle = AddableTitle::firstOrFail();
        /** @var Address $address */
        $address = Address::firstOrFail();

        $response = $this->put(route('v100-profile-achievements-experience-edit', ['experience' => $workExperience->ejob_id]), [
            'job_title' => $this->faker->title,
            'company_id' => $addableTitle->title_id,
            'address_id' => $address->aid,
            'start_date' => CarbonImmutable::now()->toDateString(),
            'end_date' => CarbonImmutable::now()->toDateString(),
            'present' => false,
        ], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'ejob_id',
                    'emp_id',
                    'job_title',
                    'start_date',
                    'end_date',
                    'company',
                    'address_id',
                    'uuid',
                ],
            ]);
    }

    public function testEditAward(): void
    {
        $token = $this->login();
        /** @var Award $award */
        $award = Award::firstOrFail();
        /** @var AddableTitle $addableTitle */
        $addableTitle = AddableTitle::firstOrFail();

        $response = $this->put(route('v100-profile-achievements-awards-edit', ['award' => $award->cid]), [
            'certificate_name_id' => $addableTitle->title_id,
            'organization_name_id' => $addableTitle->title_id,
            'certified_date' => CarbonImmutable::now()->toDateString(),
        ], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'cid',
                    'emp_id',
                    'certified_date',
                    'certificate_name',
                    'organization_name',
                    'uuid',
                ],
            ]);
    }
}
