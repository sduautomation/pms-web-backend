<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Achievements;

use Domains\Profile\V100\Models\Award;
use Domains\Profile\V100\Models\Project;
use Domains\Profile\V100\Models\WorkExperience;
use Tests\TestCase;

final class AchievementsDeleteTest extends TestCase
{
    public function testDeleteProject(): void
    {
        $token = $this->login();
        /** @var Project $project */
        $project = Project::firstOrFail();

        $response = $this->delete(route('v100-profile-achievements-projects-delete', [
            'project' => $project->project_id,
        ]), [], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
            ]);

        $this->assertDatabaseMissing('dbmaster.cv_emp_projects', [
            'project_id' => $project->project_id,
        ]);
    }

    public function testDeleteWorkExperience(): void
    {
        $token = $this->login();
        /** @var WorkExperience $workExperience */
        $workExperience = WorkExperience::firstOrFail();

        $response = $this->delete(route('v100-profile-achievements-experience-delete', [
            'experience' => $workExperience->ejob_id,
        ]), [], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
            ]);

        $this->assertDatabaseMissing('dbmaster.cv_emp_job_exp', [
            'ejob_id' => $workExperience->ejob_id,
        ]);
    }

    public function testDeleteAward(): void
    {
        $token = $this->login();
        /** @var Award $award */
        $award = Award::firstOrFail();

        $response = $this->delete(route('v100-profile-achievements-awards-delete', [
            'award' => $award->cid,
        ]), [], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
            ]);

        $this->assertDatabaseMissing('dbmaster.cv_emp_certificates', [
            'cid' => $award->cid,
        ]);
    }
}
