<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Contacts;

use Tests\TestCase;

final class ContactsIndexTest extends TestCase
{
    public function testContacts(): void
    {
        $token = $this->login();
        $response = $this->get(route('v100-profile-contacts-index', [
        ]), [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'contacts' => [
                        '*' => [
                            'contact_id',
                            'emp_id',
                            'type_title',
                            'contact',
                        ],
                    ],
                ],
            ]);
    }
}
