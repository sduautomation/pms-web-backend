<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Contacts;

use Domains\Profile\V100\Models\ContactType;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class ContactsCreateTest extends TestCase
{
    use WithFaker;

    public function testCreateContact(): void
    {
        $token = $this->login();
        /** @var ContactType $contactType */
        $contactType = ContactType::firstOrFail();

        $response = $this->post(route('v100-profile-contacts-create'), [
            'type_id' => $contactType->type_id,
            'contact' => $this->faker->numerify('###########'),
        ], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'contact_id',
                    'emp_id',
                    'type_title',
                    'contact',
                ],
            ]);
    }
}
