<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Contacts;

use Domains\Profile\V100\Models\Contact;
use Domains\Profile\V100\Models\ContactType;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class ContactsEditTest extends TestCase
{
    use WithFaker;

    public function testEditContact(): void
    {
        $token = $this->login();
        /** @var Contact $contact */
        $contact = Contact::firstOrFail();
        /** @var ContactType $contactType */
        $contactType = ContactType::firstOrFail();

        $response = $this->put(route('v100-profile-contacts-edit', ['contact' => $contact->contact_id]), [
            'type_id' => $contactType->type_id,
            'contact' => $this->faker->numerify('###########'),
        ], [
           'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'contact_id',
                    'emp_id',
                    'type_title',
                    'contact',
                ],
            ]);
    }
}
