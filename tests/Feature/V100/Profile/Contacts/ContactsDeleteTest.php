<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Contacts;

use Domains\Profile\V100\Models\Contact;
use Tests\TestCase;

final class ContactsDeleteTest extends TestCase
{
    public function testDeleteContact(): void
    {
        $token = $this->login();
        /** @var Contact $contact */
        $contact = Contact::firstOrFail();

        $response = $this->delete(route('v100-profile-contacts-delete', [
            'contact' => $contact->contact_id,
        ]), [], [
           'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
               'message',
            ]);

        $this->assertDatabaseMissing('dbmaster.contacts', [
            'contact_id' => $contact->contact_id,
        ]);
    }
}
