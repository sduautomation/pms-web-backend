<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Biography;

use Domains\Profile\V100\Models\Biography;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class BiographyEditTest extends TestCase
{
    use WithFaker;

    public function testEdit(): void
    {
        $token = $this->login();
        $user = $this->getTestUser();
        /** @var Biography $biography */
        $biography = Biography::findOrFail($user->emp_id);

        $response = $this->put(route('v100-profile-biographies-edit', ['biography' => $biography->emp_id]), [
            'biography' => $this->faker->text,
        ], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'emp_id',
                    'autobiography',
                ],
            ]);
    }
}
