<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Biography;

use Domains\Profile\V100\Models\Biography;
use Tests\TestCase;

final class BiographyDeleteTest extends TestCase
{
    public function testDelete(): void
    {
        $token = $this->login();
        $user = $this->getTestUser();

        /** @var Biography $biography */
        $biography = Biography::findOrFail($user->emp_id);

        $response = $this->delete(route('v100-profile-biographies-delete', [
            'biography' => $biography->emp_id,
        ]), [], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
            ]);

        $this->assertDatabaseMissing('dbmaster.emp_profile', [
            'emp_id' => $biography->emp_id,
        ]);
    }
}
