<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Biography;

use Domains\Profile\V100\Models\Biography;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class BiographyCreateTest extends TestCase
{
    use WithFaker;

    public function testCreate(): void
    {
        $token = $this->login();
        $user = $this->getTestUser();

        Biography::findOrFail($user->emp_id)->delete();

        $response = $this->post(route('v100-profile-biographies-create'), [
            'biography' => $this->faker->text,
        ], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'emp_id',
                    'autobiography',
                ],
            ]);
    }
}
