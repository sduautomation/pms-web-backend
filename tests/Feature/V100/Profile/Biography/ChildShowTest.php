<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Biography;

use Domains\Profile\V100\Models\Child;
use Tests\TestCase;

final class ChildShowTest extends TestCase
{
    public function testShow(): void
    {
        $token = $this->login();

        /** @var Child $child */
        $child = Child::firstOrFail();

        $response = $this->get(route('v100-profile-children-show', [
            'child' => $child->child_id,
        ]), [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'child_id',
                    'emp_id',
                    'name',
                    'sname',
                    'gender_id',
                    'birth_date',
                    'create_date',
                    'gender',
                ],
            ]);
    }
}
