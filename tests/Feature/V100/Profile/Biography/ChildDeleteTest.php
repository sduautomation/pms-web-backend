<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Biography;

use Domains\Profile\V100\Models\Child;
use Tests\TestCase;

final class ChildDeleteTest extends TestCase
{
    public function testDelete(): void
    {
        $token = $this->login();

        /** @var Child $child */
        $child = Child::firstOrFail();

        $response = $this->delete(route('v100-profile-children-delete', [
            'child' => $child->child_id,
        ]), [], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
            ]);
    }
}
