<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Biography;

use Domains\Profile\V100\Core\Enums\BiographyTypes;
use Tests\TestCase;

final class BiographyIndexTest extends TestCase
{
    public function testAll(): void
    {
        $token = $this->login();

        $response = $this->get(route('v100-profile-biographies-index', [
            'type' => BiographyTypes::ALL,
        ]), [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'children' => [
                        '*' => [
                            'child_id',
                            'emp_id',
                            'name',
                            'sname',
                            'gender_id',
                            'birth_date',
                            'create_date',
                            'gender',
                        ],
                    ],
                    'biography' => [
                        '*' => [
                            'emp_id',
                            'autobiography',
                        ],
                    ],
                ],
            ]);
    }

    public function testChildren(): void
    {
        $token = $this->login();
        $response = $this->get(route('v100-profile-biographies-index', [
            'type' => BiographyTypes::CHILDREN,
        ]), [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'children' => [
                        '*' => [
                            'child_id',
                            'emp_id',
                            'name',
                            'sname',
                            'gender_id',
                            'birth_date',
                            'create_date',
                            'gender',
                        ],
                    ],
                ],
            ]);
    }

    public function testBiography()
    {
        $token = $this->login();

        $response = $this->get(route('v100-profile-biographies-index', [
            'type' => BiographyTypes::BIOGRAPHY,
        ]), [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'biography' => [
                        '*' => [
                            'emp_id',
                            'autobiography',
                        ],
                    ],
                ],
            ]);
    }
}
