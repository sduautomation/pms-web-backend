<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Biography;

use Domains\Profile\V100\Models\Gender;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class ChildCreateTest extends TestCase
{
    use WithFaker;

    public function testCreate(): void
    {
        $token = $this->login();

        /** @var Gender $gender */
        $gender = Gender::firstOrFail();

        $response = $this->post(route('v100-profile-children-create'), [
            'name' => $this->faker->firstName,
            'sname' => $this->faker->lastName,
            'gender_id' => $gender->gender_id,
            'birth_date' => $this->faker->date,
        ], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'child_id',
                    'emp_id',
                    'name',
                    'sname',
                    'gender_id',
                    'birth_date',
                    'create_date',
                    'gender',
                ],
            ]);
    }
}
