<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Biography;

use Domains\Profile\V100\Models\Child;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class ChildEditTest extends TestCase
{
    use WithFaker;

    public function testEdit(): void
    {
        $token = $this->login();

        /** @var Child $child */
        $child = Child::firstOrFail();

        $response = $this->put(route('v100-profile-children-edit', [
            'child' => $child->child_id,
        ]), [
            'name' => $this->faker->firstName,
            'sname' => $this->faker->lastName,
            'gender_id' => $child->gender_id,
            'birth_date' => $this->faker->date,
        ], [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'child_id',
                    'emp_id',
                    'name',
                    'sname',
                    'gender_id',
                    'birth_date',
                    'create_date',
                    'gender',
                ],
            ]);
    }
}
