<?php

declare(strict_types=1);

namespace Tests\Feature\V100\Profile\Biography;

use Domains\Profile\V100\Models\Biography;
use Tests\TestCase;

final class BiographyShowTest extends TestCase
{
    public function testShowBiography(): void
    {
        $token = $this->login();

        /** @var Biography $biography */
        $biography = Biography::firstOrFail();

        $response = $this->get(route('v100-profile-biographies-show', [
            'biography' => $biography->emp_id,
        ]), [
            'Authorization' => $token,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'data' => [
                    'emp_id',
                    'autobiography',
                ],
            ]);
    }
}
