<?php

declare(strict_types=1);

namespace Tests;

use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;

/**
 * Class TestUser.
 */
trait TestUser
{
    /**
     * @return array
     */
    #[ArrayShape(['username' => 'string', 'password' => 'string'])]
    protected function getTestUserCredentials(): array
    {
        $username = (string) config('portal.test_user_username');
        $password = (string) config('portal.test_user_password');

        return [
            'username' => $username,
            'password' => $password,
        ];
    }

    /**
     * @return Employee
     */
    protected function getTestUser(): Employee
    {
        $username = (string) config('portal.test_user_username');

        return Employee::byUsername($username);
    }

    protected function login(): string
    {
        $credentials = $this->getTestUserCredentials();

        $response = $this->post(route('v100-auth-login'), [
            'username' => $credentials['username'],
            'password' => $credentials['password'],
        ]);

        $responseData = $response->json();

        return "Bearer {$responseData['data']['token']}";
    }
}
