<?php

declare(strict_types=1);

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Support\Core\Exceptions\DefaultExceptionParser;
use Throwable;

/**
 * Class Handler.
 */
final class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });

        $this->renderable(
            DefaultExceptionParser::make()->renderable()
        );
    }

    /**
     * Report or log an exception.
     *
     * @param  Throwable $e
     * @return void
     * @throws Throwable
     */
    public function report(Throwable $e): void
    {
        // TODO: reporting to sentry

        parent::report($e);
    }
}
