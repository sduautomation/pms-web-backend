<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

/**
 * Class IndexController.
 */
final class IndexController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api",
     *      operationId="index",
     *      tags={"developers"},
     *      summary="Базовый",
     *      description="Базовый",
     *      @OA\Response(response=200, description="Success"),
     *      @OA\Response(response=400, description="Something went wrong...")
     * )
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json([
            'message' => 'Success',
        ]);
    }
}
