# Запуск проекта
Запускаем в докере.

Запускаем контейнер (Образы соберутся автоматически)
```sh
docker-compose up -d
```

Если MacOS
```sh
DOCKER_DEFAULT_PLATFORM=linux/amd64 docker-compose up -d
DOCKER_DEFAULT_PLATFORM=linux/x86_64 docker-compose up -d
```

Запускаем **bash** контейнера
```sh
docker-compose exec container_name bash
```

Перезапуск контейнера
```sh
docker-compose up -d --force-recreate container_name
```

Остановка и удаление контейнера
```sh
docker-compose stop container_name
docker-compose rm container_name
```

Удаление образа
```
docker images
docker rm image_name|image_id
```

Если собираете удаленно, нужно подключаться к vpn (openconnect)
```sh
docker-compose exec pms bash
openconnect --user=marc 92.47.149.210 -b
```
