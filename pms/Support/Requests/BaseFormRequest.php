<?php

declare(strict_types=1);

namespace Support\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Support\Core\Enums\ErrorCodes;
use Support\Core\Exceptions\NoticeException;

/**
 * Class BaseFormRequest.
 */
abstract class BaseFormRequest extends FormRequest
{
    protected const DEVICE_INFO_PARAMETER = 'HTTP_USER_AGENT';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    abstract public function rules(): array;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    abstract public function authorize(): bool;

    /**
     * Handle a failed validation attempt.
     *
     * @param Validator $validator
     * @return void
     * @throws NoticeException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = implode(', ', $validator->errors()->all());

        throw new NoticeException($errors, ErrorCodes::UNPROCESSABLE_ENTITY);
    }

    /**
     * @return string
     */
    protected function getDeviceInfo(): string
    {
        return (string) $this->server(self::DEVICE_INFO_PARAMETER);
    }
}
