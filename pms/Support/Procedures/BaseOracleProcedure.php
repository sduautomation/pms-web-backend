<?php

declare(strict_types=1);

namespace Support\Procedures;

use Support\Core\Collections\ProcedureParametersCollection;
use Support\Core\Exceptions\ProcedureException;

/**
 * Class BaseOracleProcedure.
 */
abstract class BaseOracleProcedure
{
    protected const DEFAULT_OUT_PARAMETER_VALUE = 0;

    protected const RESULT_PARAMETER = 'pRes';

    /**
     * @param OracleProcedureService $service
     */
    public function __construct(
        protected OracleProcedureService $service
    ) {
    }

    /**
     * @param array $parameters
     * @return ProcedureParametersCollection
     * @throws ProcedureException
     */
    protected function getProcedureParametersCollection(array $parameters): ProcedureParametersCollection
    {
        return ProcedureParametersCollection::make($parameters);
    }
}
