<?php

declare(strict_types=1);

namespace Support\Procedures;

use Illuminate\Support\Facades\DB;
use PDO;
use Support\Cooker\ValueObjects\ProcedureParameterValueObject;
use Support\Core\Collections\ProcedureParametersCollection;
use Support\Core\Enums\DataTypes;
use Support\Core\Exceptions\ProcedureException;
use Throwable;
use Yajra\Pdo\Oci8 as OraclePdo;
use Yajra\Pdo\Oci8\Statement;

/**
 * Class OracleProcedureService.
 */
final class OracleProcedureService
{
    private const DEFAULT_INTEGER_SIZE = -1;

    private OraclePdo $pdo;
    private ProcedureParametersCollection $parameters;

    public function __construct()
    {
        $this->pdo = DB::getPdo();
    }

    /**
     * @return ProcedureParametersCollection
     */
    public function getOutputParameters(): ProcedureParametersCollection
    {
        return $this->parameters
            ->filter(fn (ProcedureParameterValueObject $parameter) => $parameter->isOut === true);
    }

    /**
     * @param string $procedureName
     * @param ProcedureParametersCollection $parameters
     * @throws ProcedureException
     */
    public function execute(string $procedureName, ProcedureParametersCollection $parameters): void
    {
        $this->parameters = $parameters;

        $statement = $this->prepareStatement($procedureName);

        $this->bindParameters($statement);

        try {
            $statement->execute();
        } catch (Throwable $exception) {
            throw new ProcedureException($exception->getMessage());
        }
    }

    /**
     * @param string $procedureName
     * @return Statement
     */
    private function prepareStatement(string $procedureName): Statement
    {
        $parametersQuery = $this->prepareParametersQuery();
        $query = 'begin '.$procedureName.$parametersQuery.' end;';

        return $this->pdo->prepare($query);
    }

    /**
     * @return string
     */
    private function prepareParametersQuery(): string
    {
        $result = '(';

        $this->parameters->each(function (ProcedureParameterValueObject $parameter) use (&$result) {
            $result .= ':'.$parameter->name.',';
        });

        $result = substr($result, 0, -1);

        return $result.');';
    }

    /**
     * @param Statement $statement
     */
    private function bindParameters(Statement $statement): void
    {
        $this->parameters->each(function (ProcedureParameterValueObject $parameter) use ($statement) {
            $statement->bindParam(
                parameter: ':'.$parameter->name,
                variable: $parameter->value,
                dataType: $this->getPdoDataType($parameter->value),
                maxLength: $parameter->size ?? self::DEFAULT_INTEGER_SIZE,
            );
        });
    }

    /**
     * @param mixed $parameterValue
     * @return int
     */
    private function getPdoDataType(mixed $parameterValue): int
    {
        return match (gettype($parameterValue)) {
            DataTypes::INTEGER => PDO::PARAM_INT,
            DataTypes::BOOL => PDO::PARAM_BOOL,
            default => PDO::PARAM_STR,
        };
    }
}
