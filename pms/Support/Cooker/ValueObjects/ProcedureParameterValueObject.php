<?php

declare(strict_types=1);

namespace Support\Cooker\ValueObjects;

/**
 * Class ProcedureParameterValueObject.
 */
final class ProcedureParameterValueObject
{
    /**
     * @param string $name
     * @param mixed $value
     * @param bool $isOut
     * @param int|null $size
     */
    public function __construct(
        public string $name,
        public mixed $value,
        public bool $isOut = false,
        public ?int $size = null
    ) {
    }
}
