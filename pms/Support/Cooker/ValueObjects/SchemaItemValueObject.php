<?php

declare(strict_types=1);

namespace Support\Cooker\ValueObjects;

use Illuminate\Support\Arr;
use Support\Core\Exceptions\ActionsException;

/**
 * Class SchemaItemValueObject.
 */
final class SchemaItemValueObject
{
    /** @psalm-readonly */
    public ?string $actionClass;

    /** @psalm-readonly */
    public ?string $stateFactoryClass;

    /**
     * @param array $item
     * @return static
     * @throws ActionsException
     */
    public static function fromArray(array $item): self
    {
        $self = new self();

        $self->validateArrayStructure($item);
        $self->setAttributes($item);

        return $self;
    }

    /**
     * @param array $item
     * @throws ActionsException
     */
    private function validateArrayStructure(array $item): void
    {
        if (! Arr::exists($item, 'action')) {
            throw ActionsException::schemaItemKeyNotExists('action');
        }

        if (! Arr::exists($item, 'factory')) {
            throw ActionsException::schemaItemKeyNotExists('factory');
        }
    }

    /**
     * @param array $item
     */
    private function setAttributes(array $item): void
    {
        $this->actionClass = Arr::get($item, 'action');
        $this->stateFactoryClass = Arr::get($item, 'factory');
    }
}
