<?php

declare(strict_types=1);

namespace Support\Cooker\Handlers;

use Support\Cooker\Mappers\EmployeePermissionMapper;
use Support\Cooker\Services\EmployeePermissionService;
use Support\Core\Exceptions\PermissionException;

final class EmployeeHasPermissionHandler
{
    public function __construct(
        public EmployeePermissionService $service,
    ) {
    }

    /**
     * @throws PermissionException
     */
    public function handle(int $employeeId, string $routeName): bool
    {
        $permissionId = EmployeePermissionMapper::getPermissionId($routeName);
        $permissions = $this->service->getPermissions($employeeId);

        return in_array($permissionId, $permissions->toArray(), true);
    }
}
