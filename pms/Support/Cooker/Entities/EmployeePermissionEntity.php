<?php

declare(strict_types=1);

namespace Support\Cooker\Entities;

final class EmployeePermissionEntity
{
    public function __construct(
        public int $accId,
        public int $accPid,
        public int $accTypeId,
        public int $accLevel,
        public string $title,
        public ?string $modName,
        public int $level,
        public int $openChildren,
        public int $grantedChildren
    ) {
    }

    /**
     * @param array<string, mixed> $item
     */
    public static function fromArray(array $item): self
    {
        return new self(
            accId: (int) $item['acc_id'],
            accPid: (int) $item['acc_pid'],
            accTypeId: (int) $item['acc_type_id'],
            accLevel: (int) $item['acc_level'],
            title: $item['title'],
            modName: $item['mod_name'],
            level: (int) $item['level'],
            openChildren: (int) $item['open_childs'],
            grantedChildren: (int) $item['granted_childs']
        );
    }

    public static function fromObject(object $item): self
    {
        return new self(
            accId: (int) $item->acc_id,
            accPid: (int) $item->acc_pid,
            accTypeId: (int) $item->acc_type_id,
            accLevel: (int) $item->acc_level,
            title: $item->title,
            modName: $item->mod_name,
            level: (int) $item->level,
            openChildren: (int) $item->open_childs,
            grantedChildren: (int) $item->granted_childs
        );
    }
}
