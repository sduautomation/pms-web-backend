<?php

declare(strict_types=1);

namespace Support\Cooker\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Support\Models\Employee;

final class EmployeePermissionService
{
    private const CACHE_PREFIX = 'EmployeePermission';
    private const EMPLOYEE_CACHE_PREFIX = 'Employee';

    public function getPermissions(int $employeeId): Collection
    {
        $cacheKey = $this->getCacheKey($employeeId);
        $employeeCacheKey = $this->getCacheKeyForEmployee($employeeId);

        return Cache::tags([$cacheKey, $employeeCacheKey])
            ->rememberForever($cacheKey, function () use ($employeeId) {
                /** @var Employee $employee */
                $employee = Employee::findOrFail($employeeId);

                return $employee->getPermissions()->pluck('accId');
            });
    }

    private function getCacheKey(int $employeeId): string
    {
        return sprintf(self::CACHE_PREFIX.':%d', $employeeId);
    }

    private function getCacheKeyForEmployee(int $employeeId): string
    {
        return sprintf(self::EMPLOYEE_CACHE_PREFIX.':%d', $employeeId);
    }
}
