<?php

declare(strict_types=1);

namespace Support\Cooker\Services;

use Support\Cooker\ValueObjects\SchemaItemValueObject;
use Support\Core\Collections\ActionsSchemaCollection;
use Support\Core\Exceptions\ActionsException;
use Support\Core\Interfaces\ActionFactoryInterface;
use Support\Core\Interfaces\ActionInterface;
use Support\Core\Interfaces\ActionServiceInterface;

/**
 * Class ActionService.
 */
final class ActionService
{
    private array $schema = [];

    private mixed $attribute;

    /**
     * @param mixed $attribute
     * @return $this
     */
    public function send(mixed $attribute): self
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * @param array $schema
     * @return $this
     */
    public function through(array $schema): self
    {
        $this->schema = $schema;

        return $this;
    }

    /**
     * @param ActionServiceInterface $actionsFacade
     * @return ActionServiceInterface
     * @throws ActionsException
     */
    public function then(ActionServiceInterface $actionsFacade): ActionServiceInterface
    {
        $schemaCollection = $this->getSchemaCollection();

        if ($schemaCollection->isEmpty()) {
            throw ActionsException::schemaIsEmpty();
        }

        /** @var SchemaItemValueObject $schemaItem */
        $schemaItem = $schemaCollection->first();

        while (true) {
            if ($schemaItem->actionClass !== null) {
                /** @var ActionInterface $action */
                $action = new $schemaItem->actionClass;

                $action->run($this->attribute);
            }

            if ($schemaItem->stateFactoryClass === null) {
                break;
            }

            /** @var ActionFactoryInterface $actionFactory */
            $actionFactory = new $schemaItem->stateFactoryClass;
            $nextActionClass = $actionFactory->make($this->attribute);

            /** @var SchemaItemValueObject $schemaItem */
            $schemaItem = $schemaCollection
                ->where('actionClass', $nextActionClass)
                ->firstOrFail();
        }

        return $actionsFacade;
    }

    /**
     * @return ActionsSchemaCollection
     * @throws ActionsException
     */
    private function getSchemaCollection(): ActionsSchemaCollection
    {
        return ActionsSchemaCollection::make($this->schema);
    }
}
