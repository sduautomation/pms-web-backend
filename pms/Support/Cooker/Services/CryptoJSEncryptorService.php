<?php

declare(strict_types=1);

namespace Support\Cooker\Services;

use Illuminate\Support\Arr;
use Support\Core\Exceptions\EncryptorException;
use Support\Core\Interfaces\EncryptorServiceInterface;

/**
 * Class CryptoJSEncryptorService.
 */
final class CryptoJSEncryptorService implements EncryptorServiceInterface
{
    private const PSEUDO_BYTES_LENGTH = 8;
    private const CIPHER_ALGORITHM = 'aes-256-cbc';
    private const OPENSSL_OPTIONS = 1;

    /** @psalm-readonly  */
    private string $encryptorKey;

    public function __construct()
    {
        $this->encryptorKey = (string) config('portal.encryptor_key');
    }

    /**
     * @param string $value
     * @return string
     */
    public function encrypt(string $value): string
    {
        $salt = openssl_random_pseudo_bytes(self::PSEUDO_BYTES_LENGTH);
        $salted = '';
        $dx = '';

        while (strlen($salted) < 48) {
            $dx = md5($dx.$this->encryptorKey.$salt, true);
            $salted .= $dx;
        }

        $key = substr($salted, 0, 32);
        $iv = substr($salted, 32, 16);

        $encrypted_data = openssl_encrypt(
            data: json_encode($value),
            cipher_algo: self::CIPHER_ALGORITHM,
            passphrase: $key,
            options: self::OPENSSL_OPTIONS,
            iv: $iv
        );

        $data = [
            'ct' => base64_encode($encrypted_data),
            'iv' => bin2hex($iv),
            's' => bin2hex($salt),
        ];

        return json_encode($data);
    }

    /**
     * @param string $value
     * @return string
     * @throws EncryptorException
     */
    public function decrypt(string $value): string
    {
        $data = json_decode($value, true);

        if (! is_array($data)) {
            throw EncryptorException::invalidInputWhileDecrypting();
        }

        $salt = hex2bin(Arr::get($data, 's'));
        $ct = base64_decode(Arr::get($data, 'ct'));
        $iv = hex2bin(Arr::get($data, 'iv'));
        $concatenatedPassphrase = $this->encryptorKey.$salt;

        $md5 = [];
        $md5[] = md5($concatenatedPassphrase, true);
        $result = $md5[0];

        for ($i = 1; $i < 3; $i++) {
            $lastPassphrase = $md5[$i - 1];
            $md5[] = md5($lastPassphrase.$concatenatedPassphrase, true);
            $result .= $md5[$i];
        }

        $key = substr($result, 0, 32);

        $data = openssl_decrypt(
            data: $ct,
            cipher_algo: self::CIPHER_ALGORITHM,
            passphrase: $key,
            options: self::OPENSSL_OPTIONS,
            iv: $iv
        );

        return json_decode($data, true);
    }
}
