<?php

declare(strict_types=1);

namespace Support\Cooker\DTO;

final class PaginationDTO
{
    public function __construct(
        public bool $isPagination,
        public int $limit,
        public int $page
    ) {
    }
}
