<?php

declare(strict_types=1);

namespace Support\Cooker\Mappers;

use Support\Core\Enums\EmployeePermissionID;
use Support\Core\Enums\EmployeePermissionRoute;
use Support\Core\Exceptions\PermissionException;

final class EmployeePermissionMapper
{
    public const MAP = [
        EmployeePermissionRoute::STUDENTS_INFORMATION => EmployeePermissionID::STUDENTS_INFORMATION,
        EmployeePermissionRoute::SCHEDULE_MANAGEMENT => EmployeePermissionID::SCHEDULE_MANAGEMENT,
        EmployeePermissionRoute::FILTER_BY_DEPARTMENT_YEAR => EmployeePermissionID::FILTER_BY_DEPARTMENT_YEAR,
        EmployeePermissionRoute::FEEDBACK_RESPONSIBILITIES => EmployeePermissionID::FEEDBACK_RESPONSIBILITIES,
        EmployeePermissionRoute::FEEDBACK => EmployeePermissionID::FEEDBACK,
        EmployeePermissionRoute::RULES_AND_REGULATIONS => EmployeePermissionID::RULES_AND_REGULATIONS,
        EmployeePermissionRoute::FILTER_BY_SPECIALITIES => EmployeePermissionID::FILTER_BY_SPECIALITIES,
        // TODO: add all routes => id, if there is no such route in api.php, then comment it
    ];

    /**
     * @throws PermissionException
     */
    public static function getPermissionId(string $route): int
    {
        if (! isset(self::MAP[$route])) {
            throw PermissionException::noSuchPermission($route);
        }

        return self::MAP[$route];
    }
}
