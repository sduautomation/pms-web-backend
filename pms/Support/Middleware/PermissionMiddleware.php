<?php

declare(strict_types=1);

namespace Support\Middleware;

use Closure;
use Illuminate\Http\Request;
use Support\Cooker\Handlers\EmployeeHasPermissionHandler;
use Support\Core\Exceptions\ForbiddenException;
use Support\Core\Exceptions\PermissionException;
use Support\Core\Interfaces\MiddlewareInterface;
use Support\Models\Employee;

final class PermissionMiddleware implements MiddlewareInterface
{
    public function __construct(
        public EmployeeHasPermissionHandler $handler
    ) {
    }

    /**
     * @throws ForbiddenException
     * @throws PermissionException
     */
    public function handle(Request $request, Closure $next): mixed
    {
        $routeName = $request->route()->getName();
        /** @var Employee $user */
        $user = $request->user();

        if ($this->handler->handle($user->emp_id, $routeName)) {
            return $next($request);
        }

        throw ForbiddenException::hasNotPermission();
    }
}
