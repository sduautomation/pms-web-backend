<?php

declare(strict_types=1);

namespace Support\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Support\Core\Enums\Languages;
use Support\Core\Exceptions\LanguageException;
use Support\Core\Interfaces\MiddlewareInterface;

/**
 * Class LanguageMiddleware.
 */
final class LanguageMiddleware implements MiddlewareInterface
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws LanguageException
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if ($request->headers->has('Content-Language')) {
            $initialLanguage = Str::substr($request->header('Content-Language'), 0, 2);

            if (! in_array($initialLanguage, Languages::ALL, true)) {
                throw LanguageException::notAllowed($initialLanguage);
            }

            App::setLocale($initialLanguage);

            return $next($request);
        }

        App::setLocale(Languages::ENGLISH);

        return $next($request);
    }
}
