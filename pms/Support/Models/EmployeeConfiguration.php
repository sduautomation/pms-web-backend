<?php

declare(strict_types=1);

namespace Support\Models;

use Illuminate\Support\Facades\DB;

final class EmployeeConfiguration
{
    public static function information(string $code): object|null
    {
        return DB::table('dbmaster.cfg_active_period as cfg')
            ->leftJoin('dbmaster.year_conf as ycf', function ($join) {
                $join->on([
                    ['cfg.year', '=', 'ycf.year'],
                    ['cfg.term', '=', 'ycf.term'],
                ]);
            })
            ->select(
                'cfg.year',
                'cfg.term',
                'cfg.period_kod',
                'ycf.absence_limit',
                DB::raw('nvl(ycf.title_en, cfg.year - cfg.term) as title')
            )
            ->where('cfg.cfg_key', $code)
            ->first();
    }
}
