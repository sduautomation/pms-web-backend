<?php

declare(strict_types=1);

namespace Support\Models\Scopes;

use Domains\Authentication\V100\Cooker\ValueObjects\ProfileInfoValueObject;
use Illuminate\Support\Facades\DB;
use Support\Core\Collections\EmployeePermissionsCollection;
use Support\Models\Employee;

/**
 * Trait EmployeeScopes.
 * @mixin Employee
 */
trait EmployeeScopes
{
    /**
     * @param int|string $employee_id
     * @return ProfileInfoValueObject|null
     */
    public static function getEmployeeInformation(int|string $employee_id): ProfileInfoValueObject|null
    {
        $info = DB::table('dbmaster.employee', 'e')
            ->select(
                'e.web_lan as language',
                'e.status as status',
                'e.name as name',
                'e.sname as surname',
                'e.name_native as nativeName',
                'e.surname_native as nativeSurname',
                'e.hname as username',
                'e.wage_rate as wageRate',
                'ei.outer_email as email',
                'ei.birth_date as birthDate',
                'ud.user_ip as lastIP',
                'ud.device_info as lastDevice',
                'ud.last_login_date as lastLogin',
                'ud.is_reliable as isReliable',
                DB::raw('(select d.degree_title_en from dbmaster.degree d where e.degree_id = d.degree_id) as degreeTitle'),
            )
            ->leftJoin('dbmaster.employee_info as ei', 'e.emp_id', '=', 'ei.emp_id')
            ->leftJoin('dbmaster.user_devices as ud', 'e.emp_id', '=', 'ud.emp_id')
            ->where('e.emp_id', '=', $employee_id)
            ->first();

        if (! is_null($info)) {
            return new ProfileInfoValueObject(
                language: $info->language.'-'.strtoupper($info->language),
                status: (int) $info->status,
                name: (string) $info->name,
                surname: (string) $info->surname,
                nativeName: (string) $info->nativename,
                nativeSurname: (string) $info->nativesurname,
                username: (string) $info->username,
                wageRate: (string) $info->wagerate,
                email: (string) $info->email,
                birthDate: (string) $info->birthdate,
                lastIP: (string) $info->lastip,
                lastDevice: (string) $info->lastdevice,
                lastLogin: (string) $info->lastlogin,
                isReliable: (int) $info->isreliable,
                degreeTitle: (string) $info->degreetitle,
                photo: null
            );
        }

        return null;
    }

    /**
     * @param string|int $employee_id
     * @return string|null
     */
    public static function getProfilePhoto(string|int $employee_id): string|null
    {
        $img = DB::table('dbmaster.emp_foto')->where('emp_id', '=', $employee_id)->first();

        if (! is_null($img)) {
            return 'data:image/png;base64,'.base64_encode($img->foto).'';
        }

        return null;
    }

    /**
     * @param string $username
     * @return Employee|null
     */
    public static function scopeByUsername(string $username): Employee|null
    {
        return self::where('hname', '=', $username)
            ->firstOrFail();
    }

    /**
     * @return EmployeePermissionsCollection
     */
    public function getPermissions(): EmployeePermissionsCollection
    {
        $permissions = DB::select(
            'select ai.acc_id, ai.acc_pid, ai.acc_type_id, ai.acc_level, ai.acc_name_en as title, ai.mod_name, level,
              (select count(*) from acc_info c where c.is_active = 1 and c.acc_pid = ai.acc_id and c.acc_level = 2) as open_childs,
              (select count(*) from acc_info c where c.acc_pid = ai.acc_id
               and( exists(select * from gen_acc_info ei where ei.acc_id = c.acc_id and ei.emp_id = :emp_id)
                  or exists(select * from gorev_acc ga, emp_gorev eg, dep_gorev dg, gen_acc_info ei
                      where ei.acc_id= c.acc_id and eg.emp_id = :emp_id and ei.gorev_acc_id = ga.gorev_acc_id  and eg.dep_gorev_id = dg.dep_gorev_id
                      and dg.gorev_id = ga.gorev_id  and eg.status = 1 and (ga.dep_code is null or ga.dep_code = dg.dep_code))
                 )
               ) as granted_childs
              from acc_info ai
              where level > 1 and ai.is_active = 1 and
              (ai.acc_type_id = 7 or ai.acc_level = 2
                        or exists(select * from gen_acc_info ei where ei.acc_id = ai.acc_id and ei.emp_id = :emp_id)
                        or exists(select * from gorev_acc ga, emp_gorev eg, dep_gorev dg, gen_acc_info ei
                             where ei.acc_id= ai.acc_id and eg.emp_id = :emp_id and ei.gorev_acc_id = ga.gorev_acc_id
                             and eg.dep_gorev_id = dg.dep_gorev_id and dg.gorev_id = ga.gorev_id  and eg.status = 1
                             and (ga.dep_code is null or ga.dep_code = dg.dep_code))
              )
              start with ai.acc_id = 1 connect by prior ai.acc_id = ai.acc_pid ORDER SIBLINGS BY ai.weight',
            [
                'emp_id' => $this->emp_id,
            ]
        );

        if (! empty($permissions)) {
            return EmployeePermissionsCollection::make($permissions);
        }

        return EmployeePermissionsCollection::make([]);
    }

    /**
     * @param string $username
     * @return bool
     */
    public static function isValidEmployee(string $username): bool
    {
        return self::where('hname', 'like', '%'.$username.'%')->count() > 0;
    }

    /**
     * @param string $username
     * @return int
     */
    public static function getEmployeeId(string $username): int
    {
        return self::where('hname', 'like', '%'.$username.'%')->first()->emp_id;
    }
}
