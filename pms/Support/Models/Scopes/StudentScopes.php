<?php

declare(strict_types=1);

namespace Support\Models\Scopes;

trait StudentScopes
{
    public static function isValidUsername(string $username): bool
    {
        return self::where('stud_id', 'like', '%'.$username.'%')->count() > 0;
    }
}
