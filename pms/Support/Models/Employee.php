<?php

declare(strict_types=1);

namespace Support\Models;

use Carbon\Carbon;
use Domains\Profile\v100\Models\Award;
use Domains\Profile\V100\Models\Biography;
use Domains\Profile\V100\Models\Child;
use Domains\Profile\V100\Models\HardSkill;
use Domains\Profile\V100\Models\Interest;
use Domains\Profile\V100\Models\LanguageSkill;
use Domains\Profile\V100\Models\Project;
use Domains\Profile\V100\Models\WorkExperience;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Support\Models\Scopes\EmployeeScopes;

/**
 * Class Employee.
 * @property int $emp_id
 * @property string $name
 * @property string $hname
 * @property string $sname
 * @property int $degree_id
 * @property string $ip
 * @property int $status
 * @property string $passw
 * @property string $passw2
 * @property int $state
 * @property string $web_lan
 * @property Carbon|string $pswchdate
 * @property Carbon|string $reg_date
 * @property string $name_native
 * @property string $surname_native
 * @property string $patronymic
 * @property int $old_emp_id
 * @property int $wage_rate
 * @property int $academic_rank
 * @property int $last_login_info
 * @property int $attempt_count
 * @property Carbon|string $attempt_date
 * @property string $country_code
 * @property int $external_id
 * @property-read Biography|null $biography
 * @property-read Child[]|Collection $children
 * @property-read Interest[]|Collection $interest
 * @property-read HardSkill[]|Collection $it
 * @property-read LanguageSkill[]|Collection $languageSkill
 * @property-read WorkExperience[]|Collection $workExperience
 * @property-read Project[]|Collection $projects
 * @property-read Award[]|Collection $awards
 * @method static Employee getEmpInfo(int $employee_id)
 */
final class Employee extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, EmployeeScopes;

    /**
     * @var string
     */
    protected $primaryKey = 'emp_id';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var string
     */
    protected $table = 'dbmaster.employee';

    /**
     * @var string[]
     */
    protected $fillable = [
        'emp_id',
        'name',
        'sname',
        'hname',
        'degree_id',
        'ip',
        'status',
        'passw',
        'passw2',
        'state',
        'web_lan',
        'pswchdate',
        'reg_date',
        'name_native',
        'surname_native',
        'patronymic',
        'old_emp_id',
        'wage_rate',
        'academic_rank',
        'last_login_info',
        'attempt_count',
        'attempt_date',
        'country_code',
        'external_id',
    ];

    /**
     * @var string[]
     */
    protected $hidden = [
        'passw',
        'passw2',
        'web_lan',
        'pswchdate',
        'last_login_info',
        'attempt_count',
        'attempt_date',
        'external_id',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'emp_id' => 'integer',
        'degree_id' => 'integer',
        'status' => 'integer',
        'state' => 'integer',
        'pswchdate' => 'date',
        'reg_date' => 'date',
        'old_emp_id' => 'integer',
        'wage_rate' => 'integer',
        'academic_rank' => 'integer',
        'last_login_info' => 'integer',
        'attempt_count' => 'integer',
        'attempt_date' => 'date',
        'external_id' => 'integer',
    ];

    public function biography(): HasOne
    {
        return $this->hasOne(Biography::class, 'emp_id', 'emp_id');
    }

    public function children() : HasMany
    {
        return $this->hasMany(Child::class, 'emp_id', 'emp_id');
    }

    public function hardSkill(): HasMany
    {
        return $this->hasMany(HardSkill::class, 'emp_id', 'emp_id');
    }

    public function interest(): HasMany
    {
        return $this->hasMany(Interest::class, 'emp_id', 'emp_id');
    }

    public function languageSkill(): HasMany
    {
        return $this->hasMany(LanguageSkill::class, 'emp_id', 'emp_id');
    }

    public function workExperience() : HasMany
    {
        return $this->hasMany(WorkExperience::class, 'emp_id', 'emp_id');
    }

    public function projects() : HasMany
    {
        return $this->hasMany(Project::class, 'emp_id', 'emp_id');
    }

    public function awards() : HasMany
    {
        return $this->hasMany(Award::class, 'emp_id', 'emp_id');
    }

    public static function byUsername(string $username): self|null
    {
        return self::scopeByUsername($username);
    }
}
