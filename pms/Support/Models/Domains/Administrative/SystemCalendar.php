<?php

declare(strict_types=1);

namespace Support\Models\Domains\Administrative;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

final class SystemCalendar extends Model
{
    protected $table = 'dbmaster.academic_calendar';
    protected $primaryKey = 'academic_item_id';

    protected $fillable = [
        'academic_item_id',
        'event_key',
        'event_year',
        'event_sem',
        'event_start',
        'event_stop',
        'status',
    ];

    public static function getEventCalendar(string $year, string $term): Collection
    {
        return DB::table('dbmaster.academic_calendar', 'ac')
            ->leftJoin('dbmaster.academic_event as ae', 'ae.key', '=', 'ac.event_key')
            ->leftJoin('dbmaster.cfg_active_period as c', 'c.cfg_key', '=', 'ae.cfg_period_key')
            ->select(
                'ae.title_en as title',
                DB::raw("to_char(ac.event_start, 'DD.MM.YYYY') as start_date"),
                DB::raw("to_char(ac.event_start, 'HH24:MI') as start_time"),
                DB::raw("to_char(ac.event_stop, 'DD.MM.YYYY') as end_date"),
                DB::raw("to_char(ac.event_stop, 'HH24:MI') as end_time"),
                DB::raw('decode(ac.event_year || ac.event_sem,
                                  c.year || c.term,
                                  ae.is_active,
                                  0) as is_open')
            )
            ->where('ac.event_year', $year)
            ->where('ac.event_sem', $term)
            ->where('ae.is_visible', 1)
            ->orderBy('ac.event_start')
            ->get();
    }

    public static function getCalendarIndividualPermissions(string $year, string $term, int $employeeId): Collection
    {
        return DB::table('dbmaster.academic_event_ex', 'ax')
            ->leftJoin('dbmaster.academic_event as ae', 'ae.key', '=', 'ax.key')
            ->select(
                'ae.title_en as title',
                'ax.key',
                DB::raw("to_char(ax.start_date, 'DD.MM.YYYY') as start_date"),
                DB::raw("to_char(ax.start_date, 'HH24:MI') as start_time"),
                DB::raw("to_char(ax.end_date, 'DD.MM.YYYY') as end_date"),
                'ae.is_blocking',
                DB::raw("to_char(ax.end_date, 'HH24:MI') as end_time"),
                'ax.emp_id',
                'ax.prog_code',
                'ax.dep_code',
                'ax.class',
                'ax.edu_level',
                DB::raw('case when (sysdate between ax.start_date and ax.end_date) and
                                  ax.is_active = 1 then 1
                                  else 0
                                  end as is_open')
            )
            ->whereRaw("(ax.emp_id = $employeeId or (ax.stud_id is NULL and ax.emp_id is NULL))")
            ->where('ax.year', $year)
            ->where('ax.term', $term)
            ->whereRaw("instr(ae.targets, 'E') > 0")
            ->orderBy('ax.start_date')
            ->get();
    }

    public static function getCalendarAssessmentPermissions(string $year, string $term, int $employeeId): Collection
    {
        return DB::table('dbmaster.sobe_assessments', 'sa')
            ->leftJoin('dbmaster.sobe_assess_types as st', 'st.sat_id', '=', 'sa.sat_id')
            ->leftJoin('dbmaster.academic_calendar as ac', function ($join) {
                $join->on([
                    ['ac.event_key', '=', 'st.at_code'],
                    ['ac.event_year', '=', 'st.year'],
                    ['ac.event_sem', '=', 'st.term'],
                ]);
            })
            ->leftJoin('dbmaster.ders_sobe as ds', function ($join) {
                $join->on([
                    ['ds.ders_kod', '=', 'st.ders_kod'],
                    ['ds.year', '=', 'st.year'],
                    ['ds.term', '=', 'st.term'],
                    ['ds.section', '=', 'st.section'],
                ]);
            })
            ->select(
                'st.ders_kod',
                'st.section',
                'st.title',
                DB::raw("to_char(nvl(sa.open_date, ac.event_start), 'DD.MM.YYYY HH24:MI') as start_date"),
                DB::raw("to_char(sa.close_date, 'DD.MM.YYYY HH24:MI') as end_date"),
                DB::raw('case when sysdate between ac.event_start and sa.close_date and
                                  sa.is_open = 1 then 1
                                  else 0
                                  end as is_open'),
            )
            ->where('sa.close_date', '>', Carbon::now())
            ->where('st.year', $year)
            ->where('st.term', $term)
            ->where('ds.emp_id_ent', $employeeId)
            ->orderBy('sa.close_date')
            ->get();
    }
}
