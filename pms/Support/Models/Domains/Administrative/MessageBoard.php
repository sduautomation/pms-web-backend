<?php

declare(strict_types=1);

namespace Support\Models\Domains\Administrative;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

final class MessageBoard extends Model
{
    protected $table = 'dbmaster.mb_threads';
    protected $primaryKey = 'thread_id';

    protected $fillable = [
        'thread_id',
        'title',
        'content',
        'create_date',
        'emp_id',
        'close',
        'view_count',
        'reply_count',
        'last_op_date',
        'last_op_emp',
        'status',
    ];

    public static function getLastMessages(int $messageLimit = 5): Collection
    {
        return DB::table('dbmaster.mb_threads', 't')
            ->leftJoin('dbmaster.employee as e', 'e.emp_id', '=', 't.emp_id')
            ->leftJoin('dbmaster.employee as le', 'le.emp_id', '=', 't.last_op_emp')
            ->select(
                't.thread_id as id',
                't.title',
                't.content',
                't.create_date as createDate',
                't.closed as status',
                'e.hname as username',
                DB::raw("e.name || ' ' || e.sname as fullname")
            //'t.emp_id',
            //'t.view_count',
            //DB::raw("to_char(t.last_op_date, 'RRRR-MM-DD HH24:MI:SS') as last_op_date"),
            //'t.last_op_emp',
            //'t.reply_count',
            //'le.hname as l_uname',
            //DB::raw("le.name || ' ' || le.sname as l_name")
            )
            ->orderByDesc('t.last_op_date')
            ->limit($messageLimit)
            ->get();
    }
}
