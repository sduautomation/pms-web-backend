<?php

declare(strict_types=1);

namespace Support\Models;

use Domains\Administrative\v100\Models\Scopes\GateEntryRecordScopes;
use Domains\Administrative\v100\Models\Scopes\PageResourcesScopes;
use Domains\Administrative\v100\Models\Scopes\RulesAndRegulationsScopes;
use Domains\Administrative\v100\Models\Scopes\StaffListScopes;
use Domains\Administrative\v100\Models\Scopes\SystemCalendarRecordsScopes;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Department.
 * @property int $dep_id
 * @property string $dep_code
 * @property int $year
 * @property int $p_id
 * @property string $title_az
 * @property string $title_en
 * @property string $title_tr
 * @property string $title_short_az
 * @property string $title_short_en
 * @property string $title_short_tr
 * @property string $dep_url
 * @property string $type
 * @property int $son
 */
final class Department extends Model
{
    use GateEntryRecordScopes;
    use PageResourcesScopes;
    use SystemCalendarRecordsScopes;
    use StaffListScopes;
    use RulesAndRegulationsScopes;

    /**
     * @var string
     */
    protected $table = 'dbmaster.departments';

    /**
     * @var string
     */
    protected $primaryKey = 'dep_id';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'dep_id',
        'dep_code',
        'year',
        'p_id',
        'title_az',
        'title_en',
        'title_tr',
        'title_short_az',
        'title_short_en',
        'title_short_tr',
        'dep_url',
        'type',
        'son',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'dep_id' => 'integer',
        'year' => 'integer',
        'p_id' => 'integer',
        'son' => 'integer',
    ];
}
