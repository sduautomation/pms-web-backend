<?php

declare(strict_types=1);

namespace Support\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

final class PermissionModel extends Model
{
    protected $table = 'dbmaster.acc_info';
    protected $primaryKey = 'acc_id';

    public static function getModulePermissions(int $employeeId, int $id): Collection
    {
        return DB::table('dbmaster.acc_info', 'ai')
            ->select(
                'ai.acc_id',
                'ai.acc_pid',
                'ai.acc_type_id',
                'ai.acc_level',
                'ai.acc_name_en as title',
                'ai.mod_name'
            )
//            ->where('level', '>', '1')
            ->where('ai.is_active', 1)
            ->whereIn('ai.acc_type_id', [1, 7])
            ->where(function ($query) use ($employeeId, $id) {
                $query->where('ai.acc_type_id', '7')
                    ->orWhere('ai.acc_level', '2')
                    ->orWhereRaw('exists (select *
                                   from dbmaster.gen_acc_info ei
                                  where ei.acc_id = ai.acc_id
                                    and ei.emp_id = '.$employeeId.')')
                    ->orWhereRaw('exists (select *
                                   from dbmaster.gorev_acc ga, dbmaster.emp_gorev eg, dbmaster.dep_gorev dg, dbmaster.gen_acc_info ei
                                  where ei.acc_id = ai.acc_id
                                    and eg.emp_id = '.$employeeId.'
                                    and ei.gorev_acc_id = ga.gorev_acc_id
                                    and eg.dep_gorev_id = dg.dep_gorev_id
                                    and dg.gorev_id = ga.gorev_id
                                    and eg.status = 1
                                    and (ga.dep_code is null or ga.dep_code = dg.dep_code))');
            })
            ->orderBy('ai.weight')
            ->get();
    }
}
