<?php

declare(strict_types=1);

namespace Support\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Support\Models\Scopes\StudentScopes;

/**
 * Class Student.
 * @property string $stud_id
 * @property string $name
 * @property string $surname
 * @property int $class
 * @property int $status
 * @property string $passw
 * @property string $web_lan
 * @property Carbon|string $last_psw_set_date
 * @property int $emp_id
 * @property string $type
 * @property string $name_native
 * @property string $surname_native
 * @property int $old_stud_id
 * @property string $patronymic
 * @property string $prog_code_reg
 * @property int $prog_year_reg
 * @property int $last_login_info
 * @property int $attempt_count
 * @property Carbon|string $attempt_date
 * @property string $edu_level
 * @property int $entrance_year
 * @property int $study_count
 * @property string $country_code
 */
final class Student extends Model
{
    use StudentScopes;
    /**
     * @var bool
     */
    public $incrementing = false;
    /**
     * @var string
     */
    protected $table = 'dbmaster.students';
    /**
     * @var string
     */
    protected $primaryKey = 'stud_id';
    /**
     * @var string[]
     */
    protected $fillable = [
        'stud_id',
        'name',
        'surname',
        'class',
        'status',
        'passw',
        'web_lan',
        'last_psw_set_date',
        'emp_id',
        'type',
        'name_native',
        'surname_native',
        'old_stud_id',
        'patronymic',
        'prog_code_reg',
        'prog_year_reg',
        'last_login_info',
        'attempt_count',
        'attempt_date',
        'edu_level',
        'entrance_year',
        'study_count',
        'country_code',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'class' => 'integer',
        'status' => 'integer',
        'last_psw_set' => 'date',
        'emp_id' => 'integer',
        'old_sdu_id' => 'integer',
        'prog_year_reg' => 'integer',
        'last_login_info' => 'integer',
        'attempt_count' => 'integer',
        'attempt_date' => 'date',
        'entrance_year' => 'integer',
        'study_count' => 'integer',
    ];
}
