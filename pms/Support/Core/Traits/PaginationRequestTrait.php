<?php

declare(strict_types=1);

namespace Support\Core\Traits;

use Illuminate\Support\Arr;
use Support\Cooker\DTO\PaginationDTO;
use Support\Core\Enums\PaginationValues;
use Support\Requests\BaseFormRequest;

/**
 * @mixin BaseFormRequest
 */
trait PaginationRequestTrait
{
    protected function getPaginationDto(): PaginationDTO
    {
        $validated = $this->validated();

        return new PaginationDTO(
            isPagination: Arr::get($validated, 'pagination', false),
            limit: Arr::get($validated, 'limit', PaginationValues::LIMIT),
            page: Arr::get($validated, 'page', PaginationValues::OFFSET)
        );
    }
}
