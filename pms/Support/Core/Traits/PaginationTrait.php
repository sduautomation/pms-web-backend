<?php

declare(strict_types=1);

namespace Support\Core\Traits;

use Support\Cooker\DTO\PaginationDTO;

trait PaginationTrait
{
    public function __construct(
        protected PaginationDTO $pagination
    ) {
    }

    public function isPagination(): bool
    {
        return $this->pagination->isPagination;
    }

    public function page(): int
    {
        return $this->pagination->page;
    }

    public function limit(): int
    {
        return $this->pagination->limit;
    }
}
