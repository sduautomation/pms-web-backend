<?php

declare(strict_types=1);

namespace Support\Core\Traits;

use Illuminate\Support\Facades\DB;
use Support\Core\Enums\RoleTypes;

trait RoleMapperTrait
{
    private static function getEmployeeRoles(): array|object
    {
        return DB::select(
            'select EMP_GOREV.EMP_ID as emp_id, DG.GOREV_AD_EN as role_name
                   from EMP_GOREV
                   inner join EMPLOYEE E on E.EMP_ID = EMP_GOREV.EMP_ID
                   inner join (select * from DEP_GOREV
                   inner join GOREV G on G.GOREV_ID = DEP_GOREV.GOREV_ID) DG
                   on EMP_GOREV.DEP_GOREV_ID = DG.DEP_GOREV_ID'
        );
    }

    private static function getMappedRoles(): array
    {
        $mappedRoles = [];
        $employeeRoles = self::getEmployeeRoles();

        foreach ($employeeRoles as $item) {
            if (str_contains(strtolower($item->role_name), 'professor') ||
                    str_contains(strtolower($item->role_name), 'lecturer') ||
                    str_contains(strtolower($item->role_name), 'practician')) {
                if (array_key_exists($item->emp_id, $mappedRoles)) {
                    $oldVal = $mappedRoles[$item->emp_id];
                    $mappedRoles[$item->emp_id] = $oldVal.','.RoleTypes::TEACHER;
                } else {
                    $mappedRoles[$item->emp_id] = RoleTypes::TEACHER;
                }
            } elseif (str_contains(strtolower($item->role_name), 'coordinator')) {
                if (array_key_exists($item->emp_id, $mappedRoles)) {
                    $oldVal = $mappedRoles[$item->emp_id];
                    $mappedRoles[$item->emp_id] = $oldVal.','.RoleTypes::COORDINATOR;
                } else {
                    $mappedRoles[$item->emp_id] = RoleTypes::COORDINATOR;
                }
            } elseif (str_contains(strtolower($item->role_name), 'expert')) {
                if (array_key_exists($item->emp_id, $mappedRoles)) {
                    $oldVal = $mappedRoles[$item->emp_id];
                    $mappedRoles[$item->emp_id] = $oldVal.','.RoleTypes::EXPERT;
                } else {
                    $mappedRoles[$item->emp_id] = RoleTypes::EXPERT;
                }
            } else {
                if (array_key_exists($item->emp_id, $mappedRoles)) {
                    $oldVal = $mappedRoles[$item->emp_id];
                    $mappedRoles[$item->emp_id] = $oldVal.','.RoleTypes::ADMINISTRATOR;
                } else {
                    $mappedRoles[$item->emp_id] = RoleTypes::ADMINISTRATOR;
                }
            }
        }

        return $mappedRoles;
    }

    public static function getEmployeeMappedRole(int $employeeId): array
    {
        $mappedRoles = self::getMappedRoles();

        return array_unique(explode(',', $mappedRoles[$employeeId]));
    }
}
