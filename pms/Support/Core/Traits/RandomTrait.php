<?php

declare(strict_types=1);

namespace Support\Core\Traits;

/**
 * Trait RandomTrait.
 */
trait RandomTrait
{
    /**
     * @param int $length
     * @return string
     */
    public function generateRandomString(int $length): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}
