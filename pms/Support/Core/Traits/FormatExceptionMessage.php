<?php

declare(strict_types=1);

namespace Support\Core\Traits;

use Throwable;

trait FormatExceptionMessage
{
    public function formatExceptionMessage(Throwable $exception): string
    {
        return sprintf('%d %s in %s:%s', $exception->getCode(), $exception->getMessage(), $exception->getFile(), $exception->getLine());
    }
}
