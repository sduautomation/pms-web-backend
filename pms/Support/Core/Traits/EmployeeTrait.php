<?php

declare(strict_types=1);

namespace Support\Core\Traits;

use JetBrains\PhpStorm\ArrayShape;
use Support\Models\EmployeeConfiguration;

trait EmployeeTrait
{
    public function getConfigurations($code = 'CARI_IL_SEM'): object|null
    {
        $code = strtoupper($code);

        return EmployeeConfiguration::information(code:$code);
    }

    public function getInterfaceLanguage(): string
    {
        $lang = explode('-', request()->header('Content-Language'));

        return $lang[0];
    }

    #[ArrayShape(['entranceYear' => 'mixed', 'currentPeriod' => 'mixed', 'currentYear' => 'mixed', 'currentTerm' => 'mixed', 'language' => 'mixed'])]
    public function getGeneralParameters($code = null): array
    {
        $user = Auth::user();
        ($code === null) ? $configInfo = $this->getConfigurations('CARI_IL_SEM') : $configInfo = $this->getConfigurations($code);

        $entranceYear = $user->entrance_year;
        $currentYear = $configInfo->year;
        $currentTerm = $configInfo->term;

        $term = (intval($currentTerm) === 3) ? 2 : $currentTerm;
        $currentPeriod = intval($user->class) * 2 - (2 - $term);

        $language = $this->getInterfaceLanguage();

        return [
            'entranceYear' => $entranceYear,
            'currentPeriod' => $currentPeriod,
            'currentYear' => $currentYear,
            'currentTerm' => $currentTerm,
            'language' => $language,
        ];
    }
}
