<?php

declare(strict_types=1);

namespace Support\Core\Traits;

use Illuminate\Support\Facades\App;

/**
 * Trait LanguageTrait.
 */
trait LanguageTrait
{
    /**
     * @return string
     */
    public static function getLocale(): string
    {
        return App::getLocale();
    }
}
