<?php

declare(strict_types=1);

namespace Support\Core\Collections;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use RuntimeException;
use Support\Cooker\ValueObjects\ProcedureParameterValueObject;
use Support\Core\Enums\DataTypes;
use Support\Core\Exceptions\ProcedureException;

/**
 * Class ProcedureParametersCollection.
 */
final class ProcedureParametersCollection extends Collection
{
    /**
     * @param array $items
     * @return static
     * @throws ProcedureException
     */
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            $self->validate($item);

            $name = Arr::get($item, 'name');
            $value = Arr::get($item, 'value');
            $isOut = Arr::get($item, 'is_out', false);
            $size = Arr::get($item, 'size');

            $self->add(new ProcedureParameterValueObject($name, $value, $isOut, $size));
        }

        return $self;
    }

    /**
     * @param mixed $item
     * @return $this
     */
    public function add($item): self
    {
        if (! $item instanceof ProcedureParameterValueObject) {
            throw new RuntimeException('Item must be of the type ProcedureParameterValueObject');
        }

        return parent::add($item);
    }

    /**
     * @param array $item
     * @throws ProcedureException
     */
    private function validate(array $item): void
    {
        if (! Arr::has($item, ['name', 'value'])) {
            throw ProcedureException::incorrectParameterStructure();
        }

        $name = Arr::get($item, 'name');
        $value = Arr::get($item, 'value');
        $isOut = Arr::get($item, 'is_out');
        $size = Arr::get($item, 'size');

        if (gettype($name) !== DataTypes::STRING) {
            throw ProcedureException::incorrectParameterStructure();
        }

        if (! in_array(gettype($value), DataTypes::TYPES)) {
            throw ProcedureException::incorrectParameterType(gettype($value));
        }

        if ($isOut !== null && gettype($isOut) !== DataTypes::BOOL) {
            throw ProcedureException::incorrectParameterStructure();
        }

        if ($size !== null && gettype($size) !== DataTypes::INTEGER) {
            throw ProcedureException::incorrectParameterStructure();
        }
    }
}
