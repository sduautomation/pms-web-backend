<?php

declare(strict_types=1);

namespace Support\Core\Collections;

use Illuminate\Support\Collection;
use RuntimeException;
use Support\Cooker\ValueObjects\SchemaItemValueObject;
use Support\Core\Exceptions\ActionsException;

/**
 * Class ActionsSchemaCollection.
 */
final class ActionsSchemaCollection extends Collection
{
    /**
     * @param array $items
     * @return static
     * @throws ActionsException
     */
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            $schemaItemValueObject = SchemaItemValueObject::fromArray($item);

            $self->add($schemaItemValueObject);
        }

        return $self;
    }

    /**
     * @param mixed $item
     * @return $this
     */
    public function add($item): self
    {
        if (! $item instanceof SchemaItemValueObject) {
            throw new RuntimeException('Item must be of the type SchemaItemValueObject');
        }

        return parent::add($item);
    }
}
