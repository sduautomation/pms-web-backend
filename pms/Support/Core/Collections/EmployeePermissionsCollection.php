<?php

declare(strict_types=1);

namespace Support\Core\Collections;

use Illuminate\Support\Collection;
use RuntimeException;
use Support\Cooker\Entities\EmployeePermissionEntity;

final class EmployeePermissionsCollection extends Collection
{
    /**
     * @param array $items
     * @return static
     */
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            switch (true) {
                case is_array($item):
                    $self->add(EmployeePermissionEntity::fromArray($item));
                    break;
                case is_object($item):
                    $self->add(EmployeePermissionEntity::fromObject($item));
                    break;
            }
        }

        return $self;
    }

    /**
     * @param mixed $item
     * @return $this
     */
    public function add($item): self
    {
        if (! $item instanceof EmployeePermissionEntity) {
            throw new RuntimeException('Item must be of the type EmployeePermissionEntity');
        }

        return parent::add($item);
    }
}
