<?php

declare(strict_types=1);

namespace Support\Core\Exceptions;

use JetBrains\PhpStorm\Pure;

/**
 * Class LanguageException.
 */
final class LanguageException extends BaseException
{
    /**
     * @param string $language
     * @return static
     */
    #[Pure]
 public static function notAllowed(string $language): self
 {
     return new self("This language :$language: is not allowed");
 }
}
