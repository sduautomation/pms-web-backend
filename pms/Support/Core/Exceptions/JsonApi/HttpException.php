<?php

declare(strict_types=1);

namespace Support\Core\Exceptions\JsonApi;

/**
 * Class HttpException.
 */
final class HttpException extends CommonException
{
}
