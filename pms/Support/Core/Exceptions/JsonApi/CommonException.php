<?php

declare(strict_types=1);

namespace Support\Core\Exceptions\JsonApi;

use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Class CommonException.
 */
abstract class CommonException extends BaseException
{
    /**
     * CommonException constructor.
     * @param string $message
     * @param int $httpCode
     * @param Throwable|null $previous
     */
    public function __construct(string $message, $httpCode = Response::HTTP_BAD_REQUEST, Throwable $previous = null)
    {
        $error = new JsonApiError(
            Str::uuid()->toString(),
            (string) $httpCode,
            $message
        );

        $errors = collect([])->add($error);

        parent::__construct($errors, $httpCode, $previous);
    }
}
