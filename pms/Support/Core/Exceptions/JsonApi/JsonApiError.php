<?php

declare(strict_types=1);

namespace Support\Core\Exceptions\JsonApi;

use Illuminate\Contracts\Support\Arrayable;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Class JsonApiError.
 */
final class JsonApiError implements Arrayable
{
    public string $id;

    public string $status;

    public string $detail;

    /**
     * JsonApiError constructor.
     * @param string $id
     * @param string $status
     * @param string $detail
     */
    public function __construct(string $id, string $status, string $detail)
    {
        $this->id = $id;
        $this->status = $status;
        $this->detail = $detail;
    }

    /**
     * @return array
     */
    #[ArrayShape(['id' => 'string', 'status' => 'string', 'detail' => 'string'])]
    public function toArray(): array
    {
        return [
            'id'        =>  $this->id,
            'status'    =>  $this->status,
            'detail'    =>  $this->detail,
        ];
    }
}
