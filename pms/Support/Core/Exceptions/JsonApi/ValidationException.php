<?php

declare(strict_types=1);

namespace Support\Core\Exceptions\JsonApi;

/**
 * Class ValidationException.
 */
final class ValidationException extends CommonException
{
    /**
     * @param string $parameter
     * @return static
     */
    public static function missingParameter(string $parameter): self
    {
        return new self("$parameter is required");
    }
}
