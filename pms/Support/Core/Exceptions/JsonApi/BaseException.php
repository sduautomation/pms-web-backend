<?php

declare(strict_types=1);

namespace Support\Core\Exceptions\JsonApi;

use Exception;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Class BaseException.
 */
abstract class BaseException extends Exception
{
    private Collection $errors;

    /**
     * BaseException constructor.
     * @param Collection $errors
     * @param int $httpCode
     * @param Throwable|null $previous
     */
    public function __construct(Collection $errors, $httpCode = Response::HTTP_BAD_REQUEST, Throwable $previous = null)
    {
        /** @var JsonApiError $firstError */
        $firstError = $errors->first();
        $this->errors = $errors;

        parent::__construct($firstError->detail, $httpCode, $previous);
    }

    /**
     * @return Collection
     */
    public function getErrors(): Collection
    {
        return $this->errors;
    }
}
