<?php

declare(strict_types=1);

namespace Support\Core\Exceptions;

use JetBrains\PhpStorm\Pure;
use Support\Core\Enums\DataTypes;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ProcedureException.
 */
final class ProcedureException extends BaseException
{
    /**
     * @param string $type
     * @return $this
     */
    #[Pure]
 public static function incorrectParameterType(string $type): self
 {
     $dataTypes = implode(',', DataTypes::TYPES);

     return new self("Incorrect parameter type :$type:, must be one of types [$dataTypes]");
 }

    /**
     * @return $this
     */
    #[Pure]
 public static function incorrectParameterStructure(): self
 {
     return new self(
         'Incorrect parameter structure, must be [[{name} => {string}, {value} => {mixed}, {is_out} => {bool}]]',
         Response::HTTP_UNPROCESSABLE_ENTITY
     );
 }
}
