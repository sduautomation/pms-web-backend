<?php

declare(strict_types=1);

namespace Support\Core\Exceptions;

final class PermissionException extends BaseException
{
    public static function noSuchPermission(string $route): self
    {
        return new self("No such permission for route: {$route}");
    }
}
