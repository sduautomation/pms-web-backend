<?php

declare(strict_types=1);

namespace Support\Core\Exceptions;

use JetBrains\PhpStorm\Pure;
use Support\Core\Enums\ErrorCodes;

/**
 * Class ActionsException.
 */
final class ActionsException extends BaseException
{
    /**
     * @param string $key
     * @return static
     */
    #[Pure]
    public static function schemaItemKeyNotExists(string $key): self
    {
        return new self("There is no :$key: in schema item", ErrorCodes::BAD_REQUEST);
    }

    /**
     * @return static
     */
    #[Pure]
    public static function schemaIsEmpty(): self
    {
        return new self('Schema is empty', ErrorCodes::BAD_REQUEST);
    }
}
