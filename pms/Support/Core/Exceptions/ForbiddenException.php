<?php

declare(strict_types=1);

namespace Support\Core\Exceptions;

use Support\Core\Enums\ErrorCodes;

final class ForbiddenException extends BaseException
{
    public static function hasNotPermission(): self
    {
        return new self(
            'You do not have permission to perform this action.',
            ErrorCodes::FORBIDDEN
        );
    }
}
