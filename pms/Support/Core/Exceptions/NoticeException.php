<?php

declare(strict_types=1);

namespace Support\Core\Exceptions;

/**
 * Class NoticeException.
 */
final class NoticeException extends BaseException
{
}
