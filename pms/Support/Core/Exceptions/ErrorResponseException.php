<?php

declare(strict_types=1);

namespace Support\Core\Exceptions;

use Illuminate\Http\Response;

final class ErrorResponseException extends BaseException
{
    protected array $data = [];

    public function __construct(string $message)
    {
        parent::__construct($message);
    }

    /*
     * Pass parameter to support execution
     * @param string $message
     * @param array $data
     * @return DefaultSupportException::class
     */
    public function with(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @return Response
     */
    public function render(): Response
    {
        return response(
            [
                'message' => $this->message,
                'data' => $this->data,
            ],
            400
        );
    }
}
