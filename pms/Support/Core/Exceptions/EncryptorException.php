<?php

declare(strict_types=1);

namespace Support\Core\Exceptions;

use JetBrains\PhpStorm\Pure;
use Support\Core\Enums\ErrorCodes;

/**
 * Class EncryptorException.
 */
final class EncryptorException extends BaseException
{
    /**
     * @return static
     */
    #[Pure]
 public static function invalidInputWhileDecrypting(): self
 {
     return new self('Could not decrypt: invalid input value', ErrorCodes::UNPROCESSABLE_ENTITY);
 }

    /**
     * @return static
     */
    #[Pure]
 public static function inputDoesNotSatisfiesKeyword(string $type): self
 {
     return new self("Input value does not satisfies keyword :$type:", ErrorCodes::UNPROCESSABLE_ENTITY);
 }
}
