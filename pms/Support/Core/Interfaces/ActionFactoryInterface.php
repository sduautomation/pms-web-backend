<?php

declare(strict_types=1);

namespace Support\Core\Interfaces;

/**
 * Interface ActionFactoryInterface.
 */
interface ActionFactoryInterface
{
    /**
     * @param mixed $attribute
     * @return string
     */
    public function make(mixed $attribute): string;
}
