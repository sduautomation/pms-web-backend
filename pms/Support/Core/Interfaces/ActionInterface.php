<?php

declare(strict_types=1);

namespace Support\Core\Interfaces;

/**
 * Interface ActionInterface.
 */
interface ActionInterface
{
    /**
     * @param mixed $attribute
     */
    public function run(mixed $attribute): void;
}
