<?php

declare(strict_types=1);

namespace Support\Core\Interfaces;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Resources\BaseResource;

/**
 * Interface PipelineServiceInterface.
 */
interface PipelineServiceInterface
{
    /**
     * @return array
     */
    public function loadPipes(): array;

    /**
     * @return $this
     */
    public function handle(): self;

    /**
     * @param mixed $data
     * @return $this
     */
    public function setAttributeFromData(mixed $data): self;

    /**
     * @param mixed $attribute
     * @return $this
     */
    public function setAttribute(mixed $attribute): self;

    /**
     * @return BaseResource|AnonymousResourceCollection|array|null
     */
    public function resource(): BaseResource|AnonymousResourceCollection|array|null;
}
