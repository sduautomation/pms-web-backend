<?php

declare(strict_types=1);

namespace Support\Core\Interfaces;

use Closure;
use Illuminate\Http\Request;

/**
 * Interface MiddlewareInterface.
 */
interface MiddlewareInterface
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed;
}
