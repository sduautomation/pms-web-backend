<?php

declare(strict_types=1);

namespace Support\Core\Interfaces;

/**
 * Interface EncryptorServiceInterface.
 */
interface EncryptorServiceInterface
{
    /**
     * @param string $value
     * @return string
     */
    public function encrypt(string $value): string;

    /**
     * @param string $value
     * @return string
     */
    public function decrypt(string $value): string;
}
