<?php

declare(strict_types=1);

namespace Support\Core\Enums;

final class PaginationValues
{
    public const OFFSET = 1;
    public const LIMIT = 10;
}
