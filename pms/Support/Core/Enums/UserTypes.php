<?php

declare(strict_types=1);

namespace Support\Core\Enums;

/**
 * Class UserTypes.
 */
final class UserTypes
{
    public const EMPLOYEE_USER_TYPE = 'employee';
    public const STUDENT_USER_TYPE = 'student';
}
