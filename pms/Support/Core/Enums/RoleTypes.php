<?php

declare(strict_types=1);

namespace Support\Core\Enums;

final class RoleTypes
{
    public const TEACHER = 'teacher';
    public const COORDINATOR = 'coordinator';
    public const EXPERT = 'expert';
    public const ADMINISTRATOR = 'administrator';

    public const ALL = [
        self::TEACHER,
        self::COORDINATOR,
        self::EXPERT,
        self::ADMINISTRATOR,
    ];
}
