<?php

declare(strict_types=1);

namespace Support\Core\Enums;

final class EmployeePermissionRoute
{
    /**
     * TODO: add all routes values, if there is no such rote in api.php, then comment it.
     */
    public const STUDENTS_INFORMATION = '';
    public const SCHEDULE_MANAGEMENT = '';
    public const FILTER_BY_DEPARTMENT_YEAR = '';
    public const FEEDBACK_RESPONSIBILITIES = '';
    public const FEEDBACK = '';
    public const RULES_AND_REGULATIONS = '';
    public const FILTER_BY_SPECIALITIES = '';
    public const ACADEMIC_OPERATIONS = '';
    public const CURRICULUMS_AND_COURSES = '';
    public const ACCESS_TO_DEPARTMENTS = '';
    public const EDUCATIONAL_PROGRAM_MANAGEMENT = '';
    public const QUESTIONNAIRES = '';
    public const CV_INFORMATION = '';
    public const COURSE_REGISTRATION = '';
    public const ELECTRONIC_ATTENDANCE = '';
    public const FORMS_AND_REPORTS = '';
    public const INFORMATION = '';
    public const PROFILE = '';
    public const GRADE_SUBMISSION = '';
    public const GATE_ENTRY_RECORDS = '';
    public const SETTINGS = '';
    public const SERVICES = '';
    public const SECTION_MANAGEMENT = '';
    public const MESSAGES = '';
    public const COURSES = '';
    public const CURRICULA = '';
    public const MESSAGE_BOARD = '';
    public const MAIL = '';
    public const SYSTEM_CALENDAR = '';
    public const REPORTS = '';
    public const BASIC_INFORMATION = '';
    public const DESCRIPTION = '';
    public const CONTENTS = '';
    public const OTHERS = '';
    public const OBJECTIVES = '';
    public const COURSE_CONTENTS = '';
    public const PROGRAM_COMPONENTS = '';
    public const LECTURERS = '';
    public const MODE_OF_EDUCATION = '';
    public const WORK_PLACEMENT = '';
    public const WEEKLY_COURSE_PLAN = '';
    public const LEARNING_OUTCOMES = '';
    public const TEACHING_METHOD = '';
    public const MATERIALS_USER_TEXTBOOK = '';
    public const ASSESSMENT_METHODS_AND_CRITERIA = '';
    public const COURSE_SCHEDULE = '';
    public const ANNOUNCEMENT_MANAGEMENT = '';
    public const LOCK_MANAGEMENT = '';
    public const LOCKING_DEPARTMENTS = '';
    public const EDIT_CURRICULUM_TERM = '';
    public const ADD_REFERENCE = '';
    public const CONSENT_REQUESTS = '';
    public const LANGUAGE_LEVELS = '';
    public const LANGUAGE_PROGRAM_FILTER = '';
    public const ESIGN = '';
    public const DOCUMENTS_TO_SIGN = '';
    public const SYLLABUS = '';
    public const STAFF_LIST = '';
    public const UPDATE_INNER_PHONE = '';
}
