<?php

declare(strict_types=1);

namespace Support\Core\Enums;

/**
 * Class Languages.
 */
final class Languages
{
    public const ENGLISH = 'en';
    public const RUSSIAN = 'ru';
    public const KAZAKH = 'kz';
    public const TURKISH = 'tr';
    public const AZERBAIJAN = 'az';

    public const ALL = [
        self::ENGLISH,
        self::RUSSIAN,
        self::KAZAKH,
        self::TURKISH,
        self::AZERBAIJAN,
    ];
}
