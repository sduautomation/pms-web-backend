<?php

declare(strict_types=1);

namespace Support\Core\Enums;

final class EmployeePermissionID
{
    public const STUDENTS_INFORMATION = 2;
    public const SCHEDULE_MANAGEMENT = 3;
    public const FILTER_BY_DEPARTMENT_YEAR = 10;
    public const FEEDBACK_RESPONSIBILITIES = 13;
    public const FEEDBACK = 14;
    public const RULES_AND_REGULATIONS = 15;
    public const FILTER_BY_SPECIALITIES = 20;
    public const ACADEMIC_OPERATIONS = 42;
    public const CURRICULUMS_AND_COURSES = 45;
    public const ACCESS_TO_DEPARTMENTS = 46;
    public const EDUCATIONAL_PROGRAM_MANAGEMENT = 47;
    public const QUESTIONNAIRES = 50;
    public const CV_INFORMATION = 51;
    public const COURSE_REGISTRATION = 52;
    public const ELECTRONIC_ATTENDANCE = 53;
    public const FORMS_AND_REPORTS = 54;
    public const INFORMATION = 56;
    public const PROFILE = 59;
    public const GRADE_SUBMISSION = 62;
    public const GATE_ENTRY_RECORDS = 63;
    public const SETTINGS = 67;
    public const SERVICES = 68;
    public const SECTION_MANAGEMENT = 69;
    public const MESSAGES = 70;
    public const COURSES = 72;
    public const CURRICULA = 73;
    public const MESSAGE_BOARD = 77;
    public const MAIL = 83;
    public const SYSTEM_CALENDAR = 104;
    public const REPORTS = 107;
    public const BASIC_INFORMATION = 110;
    public const DESCRIPTION = 111;
    public const CONTENTS = 112;
    public const OTHERS = 113;
    public const OBJECTIVES = 114;
    public const COURSE_CONTENTS = 115;
    public const PROGRAM_COMPONENTS = 116;
    public const LECTURERS = 117;
    public const MODE_OF_EDUCATION = 118;
    public const WORK_PLACEMENT = 119;
    public const WEEKLY_COURSE_PLAN = 120;
    public const LEARNING_OUTCOMES = 121;
    public const TEACHING_METHOD = 122;
    public const MATERIALS_USER_TEXTBOOK = 123;
    public const ASSESSMENT_METHODS_AND_CRITERIA = 124;
    public const COURSE_SCHEDULE = 127;
    public const ANNOUNCEMENT_MANAGEMENT = 145;
    public const LOCK_MANAGEMENT = 151;
    public const LOCKING_DEPARTMENTS = 152;
    public const EDIT_CURRICULUM_TERM = 154;
    public const ADD_REFERENCE = 155;
    public const CONSENT_REQUESTS = 158;
    public const LANGUAGE_LEVELS = 159;
    public const LANGUAGE_PROGRAM_FILTER = 160;
    public const ESIGN = 161;
    public const DOCUMENTS_TO_SIGN = 162;
    public const SYLLABUS = 163;
    public const STAFF_LIST = 164;
    public const UPDATE_INNER_PHONE = 165;
}
