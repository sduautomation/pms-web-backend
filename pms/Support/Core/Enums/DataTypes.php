<?php

declare(strict_types=1);

namespace Support\Core\Enums;

/**
 * Class DataTypes.
 */
final class DataTypes
{
    public const INTEGER = 'integer';
    public const BOOL = 'boolean';
    public const FLOAT = 'double';
    public const STRING = 'string';
    public const ARRAY = 'array';

    public const TYPES = [
        self::INTEGER,
        self::BOOL,
        self::FLOAT,
        self::STRING,
        self::ARRAY,
    ];
}
