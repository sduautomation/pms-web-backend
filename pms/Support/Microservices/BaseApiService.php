<?php

declare(strict_types=1);

namespace Support\Microservices;

use Illuminate\Http\Client\PendingRequest;
use Support\Core\Traits\FormatExceptionMessage;
use Support\Core\Traits\ResponseTrait;

abstract class BaseApiService
{
    use ResponseTrait, FormatExceptionMessage;

    public function __construct(
        protected PendingRequest $client,
        protected string $host
    ) {
    }

    abstract public function basePath(): string;

    protected function getUrl(string $path): string
    {
        return $this->host.$this->basePath().$path;
    }
}
