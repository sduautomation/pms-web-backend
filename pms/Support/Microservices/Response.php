<?php

declare(strict_types=1);

namespace Support\Microservices;

use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Client\Response as BaseResponse;
use RuntimeException;

abstract class Response
{
    protected array $data;

    public function __construct(
        protected PromiseInterface|BaseResponse $response
    ) {
        $this->data = (array) $this->response->json();

        if ($this->isEmptyResponse()) {
            throw new RuntimeException('Empty response received');
        }
    }

    final public function isSuccess(): bool
    {
        return $this->response->ok();
    }

    final public function isFail(): bool
    {
        return $this->response->failed();
    }

    final public function isEmptyResponse(): bool
    {
        return empty($this->data);
    }

    public function getMessage(): string
    {
        return $this->data['message'] ?? '';
    }

    final public function getData(): array
    {
        return $this->data;
    }
}
