<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;

/**
 * @property int $id
 * @property int $institution_type
 * @property int $address_id
 * @property string|null $title_en
 * @property string|null $title_tr
 * @property string|null $title_az
 * @property int|null $equ_code
 * @property-read string|null $title
 * @property-read InstitutionType $type
 * @property-read Address $address
 */
final class Institution extends Model
{
    use LanguageTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.institutions';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'institution_type',
        'address_id',
        'title_en',
        'title_tr',
        'title_az',
        'equ_code',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'id' => 'integer',
        'institution_type' => 'integer',
        'address_id' => 'integer',
        'equ_code' => 'integer',
    ];

    public function type(): BelongsTo
    {
        return $this->belongsTo(InstitutionType::class, 'institution_type', 'type_id');
    }

    public function address(): BelongsTo
    {
        return $this->belongsTo(Address::class, 'address_id', 'aid');
    }

    public function getTitleAttribute(): string|null
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->title_az,
            Languages::RUSSIAN, Languages::TURKISH => $this->title_tr,
            default => $this->title_en
        };
    }
}
