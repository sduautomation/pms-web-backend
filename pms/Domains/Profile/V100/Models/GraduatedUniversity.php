<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Carbon\Carbon;
use Domains\Profile\V100\Core\Traits\GraduatedUniversityFillPropertyTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;
use Support\Models\Employee;
use Support\Models\Student;

/**
 * @property int $id
 * @property int $emp_id
 * @property string|null $stud_id
 * @property string $level_code
 * @property string|null $registration_no
 * @property string|null $diploma_no
 * @property bool|null $distinguished
 * @property Carbon|null $start_date
 * @property Carbon|null $end_date
 * @property Carbon|null $issue_date
 * @property string|null $cipher
 * @property int|null $uni_id
 * @property string|null $uni_en
 * @property string|null $uni_tr
 * @property string|null $uni_az
 * @property string|null $spec_en
 * @property string|null $spec_tr
 * @property string|null $spec_az
 * @property int|null $applicant_id
 *
 * @property-read Employee $employee
 * @property-read Applicant|null $applicant
 * @property-read EducationLevel $educationLevel
 * @property-read Student|null $student
 * @property-read Institution|null $institution
 *
 * @property-read string|null $university
 * @property-read string|null $specialization
 * @property-read bool $in_progress
 *
 * @method static Builder byEmployee(int $employeeId)
 */
final class GraduatedUniversity extends Model
{
    use LanguageTrait, GraduatedUniversityFillPropertyTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.graduated_universities';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'emp_id',
        'stud_id',
        'level_code',
        'registration_no',
        'diploma_no',
        'distinguished',
        'start_date',
        'end_date',
        'issue_date',
        'cipher',
        'uni_id',
        'uni_en',
        'uni_tr',
        'uni_az',
        'spec_en',
        'spec_tr',
        'spec_az',
        'applicant_id',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'id' => 'integer',
        'emp_id' => 'integer',
        'distinguished' => 'boolean',
        'start_date' => 'date',
        'end_date' => 'date',
        'issue_date' => 'date',
        'uni_id' => 'integer',
        'applicant_id' => 'integer',
    ];

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public function applicant(): BelongsTo
    {
        return $this->belongsTo(Applicant::class, 'applicant_id', 'applicant_id');
    }

    public function educationLevel(): BelongsTo
    {
        return $this->belongsTo(EducationLevel::class, 'level_code', 'level_code');
    }

    public function student(): BelongsTo
    {
        return $this->belongsTo(Student::class, 'stud_id', 'stud_id');
    }

    public function institution(): BelongsTo
    {
        return $this->belongsTo(Institution::class, 'uni_id');
    }

    public function getUniversityAttribute(): string|null
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->uni_az,
            Languages::RUSSIAN, Languages::TURKISH => $this->uni_tr,
            default => $this->uni_en
        };
    }

    public function getSpecializationsAttribute(): string|null
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->spec_az,
            Languages::RUSSIAN, Languages::TURKISH => $this->spec_tr,
            default => $this->spec_en
        };
    }

    public function getInProgressAttribute(): bool
    {
        return $this->end_date === null;
    }

    public function scopeByEmployee(Builder $builder, int $employeeId): Builder
    {
        return $builder->where('emp_id', $employeeId);
    }
}
