<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Carbon\Carbon;
use Domains\Profile\V100\Core\Traits\ThesisFillPropertyTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;
use Support\Models\Employee;
use Support\Models\Student;

/**
 * @property int $tid
 * @property int $emp_id
 * @property string|null $reviewer
 * @property string|null $stud_id
 * @property string $student
 * @property string $edu_level
 * @property int|null $begin_year
 * @property int|null $defence_year
 * @property string|null $title_en
 * @property string|null $title_tr
 * @property string|null $title_az
 * @property string|null $uni_en
 * @property string|null $uni_tr
 * @property string|null $uni_az
 * @property int|null $log_id
 * @property Carbon|null $last_modified
 *
 * @property-read string|null $title
 * @property-read string|null $university
 *
 * @property-read Employee $employee
 * @property-read Student|null $studentModel
 * @property-read EducationLevel|null $educationLevel
 *
 * @method static Builder byEmployee(int $employeeId)
 */
final class Thesis extends Model
{
    use LanguageTrait, ThesisFillPropertyTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.thesis_info';

    /**
     * @var string
     */
    protected $primaryKey = 'tid';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'emp_id',
        'reviewer',
        'stud_id',
        'student',
        'edu_level',
        'begin_year',
        'defence_year',
        'title_en',
        'title_tr',
        'title_az',
        'uni_en',
        'uni_tr',
        'uni_az',
        'log_id',
        'last_modified',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'tid' => 'integer',
        'emp_id' => 'integer',
        'begin_year' => 'integer',
        'defence_year' => 'integer',
        'log_id' => 'integer',
        'last_modified' => 'date',
    ];

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public function studentModel(): BelongsTo
    {
        return $this->belongsTo(Student::class, 'stud_id', 'stud_id');
    }

    public function educationLevel(): BelongsTo
    {
        return $this->belongsTo(EducationLevel::class, 'edu_level', 'level_code');
    }

    public function getTitleAttribute(): string|null
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->title_az,
            Languages::RUSSIAN, Languages::TURKISH => $this->title_tr,
            default => $this->title_en
        };
    }

    public function getUniversityAttribute(): string|null
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->uni_az,
            Languages::RUSSIAN, Languages::TURKISH => $this->uni_tr,
            default => $this->uni_en
        };
    }

    public function scopeByEmployee(Builder $builder, int $employeeId): Builder
    {
        return $builder->where('emp_id', $employeeId);
    }
}
