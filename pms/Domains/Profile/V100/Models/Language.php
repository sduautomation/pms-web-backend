<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;

/**
 * @property string $lang_code
 * @property string|null $lang_name_az
 * @property string|null $lang_name_en
 * @property string|null $lang_name_tr
 * @property string|null $lang_full_name
 * @property-read string|null $title
 * @property-read LanguageSkill[]|Collection $language_skills
 */
final class Language extends Model
{
    use LanguageTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.language';

    /**
     * @var string
     */
    protected $primaryKey = 'lang_code';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'lang_code',
        'lang_name_az',
        'lang_name_en',
        'lang_name_tr',
        'lang_full_name',
    ];

    /**
     * @var string[]
     */
    protected $attributes = [
        'title',
    ];

    public function getTitleAttribute(): ?string
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->lang_name_az,
            Languages::TURKISH, Languages::RUSSIAN => $this->lang_name_tr,
            default => $this->lang_name_en
        };
    }

    public function language_skills(): HasMany
    {
        return $this->hasMany(LanguageSkill::class, 'lang_code', 'lang_code');
    }
}
