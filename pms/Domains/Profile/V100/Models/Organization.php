<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Carbon\Carbon;
use Domains\Profile\V100\Core\Traits\OrganizationFillPropertyTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;
use Support\Models\Employee;

/**
 * @property int $org_id
 * @property Carbon|null $membership_date
 * @property int $emp_id
 * @property int|null $org_name_az_id
 * @property int|null $org_name_en_id
 * @property int|null $org_name_tr_id
 * @property string|null $uuid
 *
 * @property-read Employee $employee
 * @property-read AddableTitle|null $name
 * @method static Builder byEmployee(int $employeeId)
 */
final class Organization extends Model
{
    use LanguageTrait, OrganizationFillPropertyTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.cv_emp_organizations';

    /**
     * @var string
     */
    protected $primaryKey = 'org_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'membership_date',
        'emp_id',
        'org_name_az_id',
        'org_name_en_id',
        'org_name_tr_id',
        'uuid',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'org_id' => 'integer',
        'membership_date' => 'date',
        'emp_id' => 'integer',
        'org_name_az_id' => 'integer',
        'org_name_en_id' => 'integer',
        'org_name_tr_id' => 'integer',
    ];

    public function name(): BelongsTo
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->belongsTo(AddableTitle::class, 'org_name_az_id', 'title_id'),
            Languages::RUSSIAN, Languages::TURKISH => $this->belongsTo(AddableTitle::class, 'org_name_tr_id', 'title_id'),
            default => $this->belongsTo(AddableTitle::class, 'org_name_en_id', 'title_id')
        };
    }

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public function scopeByEmployee(Builder $builder, int $employeeId): Builder
    {
        return $builder->where('emp_id', $employeeId);
    }
}
