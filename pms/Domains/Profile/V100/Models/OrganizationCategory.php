<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Illuminate\Database\Eloquent\Model;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;

/**
 * @property string $key
 * @property string|null $title_az
 * @property string|null $title_tr
 * @property string|null $title_en
 * @property-read string|null $title
 */
final class OrganizationCategory extends Model
{
    use LanguageTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.org_category';

    /**
     * @var string
     */
    protected $primaryKey = 'key';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'key',
        'title_az',
        'title_tr',
        'title_en',
    ];

    public function getTitleAttribute(): string|null
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->title_az,
            Languages::RUSSIAN, Languages::TURKISH => $this->title_tr,
            default => $this->title_en
        };
    }
}
