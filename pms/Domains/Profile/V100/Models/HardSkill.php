<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Domains\Profile\V100\Core\Traits\HardSkillFillPropertyTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;
use Support\Models\Employee;

/**
 * @property int $skill_id
 * @property int $emp_id
 * @property string|null $desc_az
 * @property string|null $desc_en
 * @property string|null $desc_tr
 * @property string|null $uuid
 * @property-read string|null $hard_skill_title
 * @property-read Employee $employee
 */
final class HardSkill extends Model
{
    use LanguageTrait, HardSkillFillPropertyTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.cv_emp_it_skills';

    /**
     * @var string
     */
    protected $primaryKey = 'skill_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'skill_id',
        'emp_id',
        'desc_az',
        'desc_en',
        'desc_tr',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'skill_id' => 'integer',
        'emp_id' => 'integer',
    ];

    /**
     * @return string|null
     */
    public function getHardSkillTitleAttribute(): ?string
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->desc_az,
            Languages::TURKISH, Languages::RUSSIAN => $this->desc_tr,
            default => $this->desc_en,
        };
    }

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public static function getHardSkillsByEmpId(int $empId): Collection
    {
        return self::where('emp_id', $empId)->get();
    }
}
