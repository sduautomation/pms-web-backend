<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Carbon\Carbon;
use Domains\Profile\V100\Core\Traits\AwardFillPropertyTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;
use Support\Models\Employee;

/**
 * @property int $cid
 * @property int $emp_id
 * @property string|null $uuid
 * @property string|null $certificate_name_az_id
 * @property string|null $certificate_name_tr_id
 * @property string|null $certificate_name_en_id
 * @property Carbon|null $certified_date
 * @property string|null $org_name_az_id
 * @property string|null $org_name_tr_id
 * @property string|null $org_name_en_id
 * @property-read Employee $employee
 * @property-read AddableTitle $certificate
 * @property-read AddableTitle $organization
 * @property-read string|null $certificate_name
 * @property-read string|null $organization_name
 * @method static where(string $column, mixed $condition)
 */
final class Award extends Model
{
    use LanguageTrait, AwardFillPropertyTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.cv_emp_certificates';

    /**
     * @var string
     */
    protected $primaryKey = 'cid';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'emp_id',
        'uuid',
        'certificate_name_az_id',
        'certificate_name_tr_id',
        'certificate_name_en_id',
        'certified_date',
        'org_name_az_id',
        'org_name_tr_id',
        'org_name_en_id',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'cid' => 'integer',
        'emp_id' => 'integer',
        'certificate_name_az_id' => 'integer',
        'certificate_name_tr_id' => 'integer',
        'certificate_name_en_id' => 'integer',
        'org_name_az_id' => 'integer',
        'org_name_tr_id' => 'integer',
        'org_name_en_id' => 'integer',
    ];

    public function getCertificateNameAttribute(): ?string
    {
        return $this->certificate?->title;
    }

    public function getOrganizationNameAttribute(): ?string
    {
        return $this->organization?->title;
    }

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public function certificate(): BelongsTo
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->belongsTo(AddableTitle::class, 'certificate_name_az_id', 'title_id'),
            Languages::RUSSIAN, Languages::TURKISH => $this->belongsTo(AddableTitle::class, 'certificate_name_tr_id', 'title_id'),
            default => $this->belongsTo(AddableTitle::class, 'certificate_name_en_id', 'title_id')
        };
    }

    public function organization(): BelongsTo
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->belongsTo(AddableTitle::class, 'org_name_az_id', 'title_id'),
            Languages::RUSSIAN, Languages::TURKISH => $this->belongsTo(AddableTitle::class, 'org_name_tr_id', 'title_id'),
            default => $this->belongsTo(AddableTitle::class, 'org_name_en_id', 'title_id')
        };
    }

    public static function getAwardsByEmpId(int $empId): Collection
    {
        return self::where('emp_id', $empId)->get();
    }
}
