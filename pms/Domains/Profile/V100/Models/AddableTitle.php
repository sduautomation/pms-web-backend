<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;

/**
 * @property int $title_id
 * @property string $title
 * @property string $lang_code
 * @property int $type
 * @property-read Project[]|Collection $donorProjects
 * @property-read Project[]|Collection $positionProjects
 * @property-read WorkExperience[]|Collection $workExperience
 * @property-read Award[]|Collection $certificateAwards
 * @property-read Award[]|Collection $organizationAwards
 * @property-read AddableType $addableType
 */
final class AddableTitle extends Model
{
    use LanguageTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.addable_titles';

    /**
     * @var string
     */
    protected $primaryKey = 'title_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'title_id',
        'title',
        'lang_code',
        'type',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'title_id' => 'integer',
        'type' => 'integer',
    ];

    public function donorProjects(): HasMany
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->hasMany(Project::class, 'donor_az_id', 'title_id'),
            Languages::RUSSIAN, Languages::TURKISH => $this->hasMany(Project::class, 'donor_tr_id', 'title_id'),
            default => $this->hasMany(Project::class, 'donor_en_id', 'title_id')
        };
    }

    public function positionProjects(): HasMany
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->hasMany(Project::class, 'position_az_id', 'title_id'),
            Languages::RUSSIAN, Languages::TURKISH => $this->hasMany(Project::class, 'position_tr_id', 'title_id'),
            default => $this->hasMany(Project::class, 'position_en_id', 'title_id')
        };
    }

    public function workExperience(): HasMany
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->hasMany(WorkExperience::class, 'company_az_id', 'title_id'),
            Languages::RUSSIAN, Languages::TURKISH => $this->hasMany(WorkExperience::class, 'company_tr_id', 'title_id'),
            default => $this->hasMany(WorkExperience::class, 'company_en_id', 'title_id')
        };
    }

    public function certificateAwards(): HasMany
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->hasMany(Award::class, 'certificate_name_az_id', 'title_id'),
            Languages::RUSSIAN, Languages::TURKISH => $this->hasMany(Award::class, 'certificate_name_tr_id', 'title_id'),
            default => $this->hasMany(Award::class, 'certificate_name_en_id', 'title_id')
        };
    }

    public function organizationAwards(): HasMany
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->hasMany(Award::class, 'org_name_az_id', 'title_id'),
            Languages::RUSSIAN, Languages::TURKISH => $this->hasMany(Award::class, 'org_name_tr_id', 'title_id'),
            default => $this->hasMany(Award::class, 'org_name_en_id', 'title_id')
        };
    }

    public function addableType(): BelongsTo
    {
        return $this->belongsTo(AddableType::class, 'type', 'type_id');
    }
}
