<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Illuminate\Database\Eloquent\Model;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;

/**
 * @property int $gender_id
 * @property string $gender_title_az
 * @property string $gender_title_en
 * @property string|null $gender_title_tr
 * @property-read string|null $gender
 */
final class Gender extends Model
{
    use LanguageTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.gender';

    /**
     * @var string
     */
    protected $primaryKey = 'gender_id';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'gender_id',
        'gender_title_az',
        'gender_title_en',
        'gender_title_tr',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'gender_id' => 'integer',
    ];

    /**
     * @var string[]
     */
    protected $attributes = [
        'gender',
    ];

    public function getGenderAttribute(): ?string
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->gender_title_az,
            Languages::TURKISH, Languages::RUSSIAN => $this->gender_title_tr,
            default => $this->gender_title_en,
        };
    }
}
