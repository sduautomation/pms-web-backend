<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Domains\Profile\V100\Core\Traits\BiographyFillPropertyTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;
use Support\Models\Employee;

/**
 * @property int $emp_id
 * @property string|null $auto_biog_az
 * @property string|null $auto_biog_en
 * @property string|null $auto_biog_tr
 * @property string|null $research_areas_az
 * @property string|null $research_areas_en
 * @property string|null $research_areas_tr
 * @property-read string|null $autobiography
 * @property-read Employee $employee
 */
final class Biography extends Model
{
    use LanguageTrait, BiographyFillPropertyTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.emp_profile';

    /**
     * @var string
     */
    protected $primaryKey = 'emp_id';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'emp_id',
        'auto_biog_az',
        'auto_biog_en',
        'auto_biog_tr',
        'research_areas_az',
        'research_areas_en',
        'research_areas_tr',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'emp_id' => 'integer',
    ];

    /**
     * @return string|null
     */
    public function getAutobiographyAttribute(): ?string
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->auto_biog_az,
            Languages::TURKISH, Languages::RUSSIAN => $this->auto_biog_tr,
            default => $this->auto_biog_en,
        };
    }

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }
}
