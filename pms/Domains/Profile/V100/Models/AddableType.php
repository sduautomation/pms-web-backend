<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $type_id
 * @property string $type_az
 * @property string $type_en
 * @property string $type_tr
 * @property string|null $type_desc
 * @property int $is_active
 * @property-read AddableTitle[]|Collection $addableTitles
 */
final class AddableType extends Model
{
    /**
     * @var string
     */
    protected $table = 'dbmaster.addable_types';

    /**
     * @var string
     */
    protected $primaryKey = 'type_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'type_id',
        'type_az',
        'type_en',
        'type_tr',
        'type_desc',
        'is_active',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'type_id' => 'integer',
        'is_active' => 'integer',
    ];

    public function addableTitles(): HasMany
    {
        return $this->hasMany(AddableTitle::class, 'type', 'type_id');
    }
}
