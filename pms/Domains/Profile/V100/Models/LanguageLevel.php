<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Illuminate\Database\Eloquent\Model;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;

/**
 * @property int $level_id
 * @property string $level_title_az
 * @property string $level_title_tr
 * @property string $level_title_en
 * @property string $level_code
 * @property-read string|null $title
 */
final class LanguageLevel extends Model
{
    use LanguageTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.lang_know_level';

    /**
     * @var string
     */
    protected $primaryKey = 'level_id';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'level_id',
        'level_title_az',
        'level_title_en',
        'level_title_tr',
        'level_code',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'level_id' => 'integer',
    ];

    /**
     * @var string[]
     */
    protected $attributes = [
        'title',
    ];

    public function getTitleAttribute(): ?string
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->level_title_az,
            Languages::TURKISH, Languages::RUSSIAN => $this->level_title_tr,
            default => $this->level_title_en,
        };
    }
}
