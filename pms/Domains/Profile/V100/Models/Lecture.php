<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Domains\Profile\V100\Core\Traits\LectureFillPropertyTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;
use Support\Models\Employee;

/**
 * @property int $lecture_id
 * @property int $emp_id
 * @property string $level_code
 * @property string|null $lecture_name_az
 * @property string|null $lecture_name_en
 * @property string|null $lecture_name_tr
 * @property string|null $uuid
 *
 * @property-read Employee $employee
 * @property-read EducationLevel|null $educationLevel
 * @property-read string|null $name
 *
 * @method static Builder byEmployee(int $employeeId)
 */
final class Lecture extends Model
{
    use LanguageTrait, LectureFillPropertyTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.cv_emp_lectures';

    /**
     * @var string
     */
    protected $primaryKey = 'lecture_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'emp_id',
        'level_code',
        'lecture_name_az',
        'lecture_name_en',
        'lecture_name_tr',
        'uuid',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'lecture_id' => 'integer',
        'emp_id' => 'integer',
    ];

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public function getNameAttribute(): string
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->lecture_name_az,
            Languages::RUSSIAN, Languages::TURKISH => $this->lecture_name_tr,
            default => $this->lecture_name_en
        };
    }

    public function educationLevel(): BelongsTo
    {
        return $this->belongsTo(EducationLevel::class, 'level_code', 'level_code');
    }

    public function scopeByEmployee(Builder $builder, int $employeeId): Builder
    {
        return $builder->where('emp_id', $employeeId);
    }
}
