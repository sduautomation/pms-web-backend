<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $degree_id
 * @property string|null $degree_title
 */
final class JournalDegree extends Model
{
    /**
     * @var string
     */
    protected $table = 'dbmaster.cv_journal_degree';

    /**
     * @var string
     */
    protected $primaryKey = 'degree_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'degree_title',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'degree_id' => 'integer',
    ];
}
