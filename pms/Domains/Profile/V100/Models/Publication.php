<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Carbon\Carbon;
use Domains\Profile\V100\Core\Traits\PublicationFillPropertyTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;
use Support\Models\Employee;

/**
 * @property int $publication_id
 * @property int $emp_id
 * @property int $publish_type_id
 * @property string|null $url
 * @property string|null $journal_series
 * @property string|null $pages
 * @property int|null $journal_degree_id
 * @property int|null $book_type_id
 * @property Carbon|null $publish_date
 * @property string|null $title_az
 * @property string|null $title_en
 * @property string|null $title_tr
 * @property int|null $journal_az_id
 * @property int|null $journal_en_id
 * @property int|null $journal_tr_id
 * @property int|null $conference_az_id
 * @property int|null $conference_en_id
 * @property int|null $conference_tr_id
 * @property string|null $authors_az
 * @property string|null $authors_en
 * @property string|null $authors_tr
 * @property string|null $publisher_az
 * @property string|null $publisher_en
 * @property string|null $publisher_tr
 * @property int|null $address_id
 * @property string|null $uuid
 *
 * @property-read Address|null $address
 * @property-read BookType|null $bookType
 * @property-read AddableTitle|null $conference
 * @property-read AddableTitle|null $journal
 * @property-read JournalDegree|null $journalDegree
 * @property-read Employee $employee
 *
 * @property-read string|null $title
 * @property-read string|null $authors
 * @property-read string|null $publisher
 *
 * @method static Builder byEmployee(int $employeeId)
 */
final class Publication extends Model
{
    use LanguageTrait, PublicationFillPropertyTrait;

    public const ARTICLE_TYPE = 'article';
    public const CONFERENCE_TYPE = 'conference';
    public const BOOK_TYPE = 'book';

    public const TYPES = [
        self::ARTICLE_TYPE,
        self::CONFERENCE_TYPE,
        self::BOOK_TYPE,
    ];

    /**
     * @var string
     */
    protected $table = 'dbmaster.cv_emp_publications';

    /**
     * @var string
     */
    protected $primaryKey = 'publication_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'emp_id',
        'publish_type_id',
        'url',
        'journal_series',
        'pages',
        'journal_degree_id',
        'book_type_id',
        'publish_date',
        'title_az',
        'title_en',
        'title_tr',
        'journal_az_id',
        'journal_en_id',
        'journal_tr_id',
        'conference_az_id',
        'conference_en_id',
        'conference_tr_id',
        'authors_az',
        'authors_en',
        'authors_tr',
        'publisher_az',
        'publisher_en',
        'publisher_tr',
        'address_id',
        'uuid',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'publication_id' => 'integer',
        'emp_id' => 'integer',
        'publish_type_id' => 'integer',
        'journal_degree_id' => 'integer',
        'book_type_id' => 'integer',
        'publish_date' => 'date',
        'journal_az_id' => 'integer',
        'journal_en_id' => 'integer',
        'journal_tr_id' => 'integer',
        'conference_az_id' => 'integer',
        'conference_en_id' => 'integer',
        'conference_tr_id' => 'integer',
        'address_id' => 'integer',
    ];

    public function getTitleAttribute(): string|null
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->title_az,
            Languages::RUSSIAN, Languages::TURKISH => $this->title_tr,
            default => $this->title_en
        };
    }

    public function getAuthorsAttribute(): string|null
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->authors_az,
            Languages::RUSSIAN, Languages::TURKISH => $this->authors_tr,
            default => $this->authors_en
        };
    }

    public function getPublisherAttribute(): string|null
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->publisher_az,
            Languages::RUSSIAN, Languages::TURKISH => $this->publisher_tr,
            default => $this->publisher_en
        };
    }

    public function address(): BelongsTo
    {
        return $this->belongsTo(Address::class, 'address_id', 'aid');
    }

    public function bookType(): BelongsTo
    {
        return $this->belongsTo(BookType::class, 'book_type_id', 'type_id');
    }

    public function conference(): BelongsTo
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->belongsTo(AddableTitle::class, 'conference_az_id', 'title_id'),
            Languages::RUSSIAN, Languages::TURKISH => $this->belongsTo(AddableTitle::class, 'conference_tr_id', 'title_id'),
            default => $this->belongsTo(AddableTitle::class, 'conference_en_id', 'title_id')
        };
    }

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public function journal(): BelongsTo
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->belongsTo(AddableTitle::class, 'journal_az_id', 'title_id'),
            Languages::RUSSIAN, Languages::TURKISH => $this->belongsTo(AddableTitle::class, 'journal_tr_id', 'title_id'),
            default => $this->belongsTo(AddableTitle::class, 'journal_en_id', 'title_id')
        };
    }

    public function journalDegree(): BelongsTo
    {
        return $this->belongsTo(JournalDegree::class, 'journal_degree_id', 'degree_id');
    }

    public function scopeByEmployee(Builder $builder, int $employeeId): Builder
    {
        return $builder->where('emp_id', $employeeId);
    }
}
