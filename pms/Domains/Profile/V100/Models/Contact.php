<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Models\Employee;

/**
 * @property int $contact_id
 * @property int $emp_id
 * @property int $type_id
 * @property string $contact
 * @property string|null stud_id
 * @property int|null $owner_id
 * @property string|null $note
 * @property int $visible
 * @property int|null $applicant_id
 * @property int|null $log_id
 * @property Carbon|null $confirm_date
 * @property-read Employee $employee
 * @property-read ContactType $contactType
 * @property-read string $type
 */
final class Contact extends Model
{
    /**
     * @var string
     */
    protected $table = 'dbmaster.contacts';

    /**
     * @var string
     */
    protected $primaryKey = 'contact_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'emp_id',
        'type_id',
        'contact',
        'stud_id',
        'owner_id',
        'note',
        'visible',
        'confirm_date',
        'applicant_id',
        'log_id',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'contact_id' => 'integer',
        'emp_id' => 'integer',
        'type_id' => 'integer',
        'owner_id' => 'integer',
        'visible' => 'integer',
        'confirm_date' => 'date',
        'applicant_id' => 'integer',
        'log_id' => 'integer',
    ];

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public function contactType(): BelongsTo
    {
        return $this->belongsTo(ContactType::class, 'type_id', 'type_id');
    }

    public static function getContactsByEmpId(int $empId): Collection
    {
        return self::where('emp_id', $empId)->get();
    }

    public function getTypeAttribute(): ?string
    {
        return $this->contactType?->type_title;
    }
}
