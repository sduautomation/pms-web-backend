<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;
use Support\Models\Employee;

/**
 * @property string|null $stud_id
 * @property int $emp_id
 * @property string $lang_code
 * @property int $level_id
 * @property int $lang_k_id
 * @property int|null $completed
 * @property int|null $web_log
 * @property int|null $applicant_id
 * @property int|null $is_native
 * @property-read Employee $employee
 * @property-read Language $languageFullName
 * @property-read LanguageLevel $languageLevel
 * @property-read string|null $language_title
 * @property-read string|null $language_level_title
 */
final class LanguageSkill extends Model
{
    /**
     * @var string
     */
    protected $table = 'dbmaster.lang_know';

    /**
     * @var string
     */
    protected $primaryKey = 'lang_k_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'lang_k_id',
        'emp_id',
        'level_id',
        'is_native',
        'lang_code',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'lang_k_id' => 'integer',
        'level_id' => 'integer',
        'is_native' => 'integer',
        'emp_id' => 'integer',
        'completed' => 'integer',
        'web_log' => 'integer',
        'applicant_id' => 'integer',
    ];

    public function getLanguageTitleAttribute(): ?string
    {
        return $this->languageFullName?->title;
    }

    public function getLanguageLevelTitleAttribute(): ?string
    {
        return $this->languageLevel?->title;
    }

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public function languageFullName(): BelongsTo
    {
        return $this->belongsTo(Language::class, 'lang_code', 'lang_code');
    }

    public function languageLevel(): BelongsTo
    {
        return $this->belongsTo(LanguageLevel::class, 'level_id', 'level_id');
    }

    public static function getLanguageSkillsByEmpId(int $empId): Collection
    {
        return self::where('emp_id', $empId)->get();
    }
}
