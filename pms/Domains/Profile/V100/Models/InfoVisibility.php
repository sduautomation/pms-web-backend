<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $gen_info_visibility_id
 * @property int $emp_id
 * @property bool $age
 * @property bool $birth_date
 * @property bool $gender
 * @property bool $marital_status
 * @property bool $cell
 * @property bool $job_exp
 * @property bool $edu
 * @property bool $tez
 * @property bool $publish
 * @property bool $projects
 * @property bool $organizations
 * @property bool $mukafat
 * @property bool $dersler
 * @property bool $it_skills
 * @property bool $language
 * @property bool $emp_website
 * @property bool $autobiography
 * @property bool $research_areas
 * @property string|null $uuid
 */
final class InfoVisibility extends Model
{
    /**
     * @var string
     */
    protected $table = 'dbmaster.cv_emp_general_info_visibility';

    /**
     * @var string
     */
    protected $primaryKey = 'gen_info_visibility_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'emp_id',
        'age',
        'birth_date',
        'gender',
        'marital_status',
        'cell',
        'job_exp',
        'edu',
        'tez',
        'publish',
        'projects',
        'organizations',
        'mukafat',
        'dersler',
        'it_skills',
        'language',
        'emp_website',
        'autobiography',
        'research_areas',
        'uuid',
    ];

    //TODO : cast bool

    public static function getInfoVisibilityByEmpId(int $empId): Collection
    {
        return self::where('emp_id', $empId)->get();
    }
}
