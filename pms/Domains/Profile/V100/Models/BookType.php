<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Illuminate\Database\Eloquent\Model;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;

/**
 * @property int $type_id
 * @property string|null $type_name_az
 * @property string|null $type_name_tr
 * @property string|null $type_name_en
 * @property-read string|null $type_name
 */
final class BookType extends Model
{
    use LanguageTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.cv_book_types';

    /**
     * @var string
     */
    protected $primaryKey = 'type_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'type_name_az',
        'type_name_tr',
        'type_name_en',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'type_id' => 'integer',
    ];

    public function getTypeNameAttribute(): string|null
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->type_name_az,
            Languages::RUSSIAN, Languages::TURKISH => $this->type_name_tr,
            default => $this->type_name_en
        };
    }
}
