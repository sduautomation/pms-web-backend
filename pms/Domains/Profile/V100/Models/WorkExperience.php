<?php

declare(strict_types=1);

namespace Domains\Profile\v100\Models;

use Carbon\Carbon;
use Domains\Profile\V100\Core\Traits\WorkExperienceFillPropertyTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;
use Support\Models\Employee;

/**
 * @property int $ejob_id
 * @property int $emp_id
 * @property string|null $uuid
 * @property string|null $job_title_az
 * @property string|null $job_title_tr
 * @property string|null $job_title_en
 * @property int|null $company_az_id
 * @property int|null $company_tr_id
 * @property int|null $company_en_id
 * @property int|null $address_id
 * @property Carbon|null $start_date
 * @property Carbon|null $end_date
 * @property-read string|null $address_title
 * @property-read Employee $employee
 * @property-read Address $address
 * @property-read AddableTitle $company
 * @property-read string|null $job_title
 * @property-read string|null $company_title
 * @method static where(string $column, mixed $condition)
 */
final class WorkExperience extends Model
{
    use LanguageTrait, WorkExperienceFillPropertyTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.cv_emp_job_exp';

    /**
     * @var string
     */
    protected $primaryKey = 'ejob_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'emp_id',
        'uuid',
        'job_title_az',
        'job_title_tr',
        'job_title_en',
        'company_az_id',
        'company_tr_id',
        'company_en_id',
        'address_id',
        'start_date',
        'end_date',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'ejob_id' => 'integer',
        'emp_id' => 'integer',
        'company_az_id' => 'integer',
        'company_tr_id' => 'integer',
        'company_en_id' => 'integer',
        'start_date' => 'date',
        'end_date' => 'date',
        'address_id' => 'integer',
    ];

    public function getJobTitleAttribute(): ?string
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->job_title_az,
            Languages::TURKISH, Languages::RUSSIAN => $this->job_title_tr,
            default => $this->job_title_en,
        };
    }

    public function getCompanyTitleAttribute(): ?string
    {
        return $this->company?->title;
    }

    public function getAddressTitleAttribute() : ?string
    {
        return $this->address?->title;
    }

    public function employee() : BelongsTo
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public function address() : BelongsTo
    {
        return $this->belongsTo(Address::class, 'address_id', 'aid');
    }

    public function company(): BelongsTo
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->belongsTo(AddableTitle::class, 'company_az_id', 'title_id'),
            Languages::RUSSIAN, Languages::TURKISH => $this->belongsTo(AddableTitle::class, 'company_tr_id', 'title_id'),
            default => $this->belongsTo(AddableTitle::class, 'company_en_id', 'title_id')
        };
    }

    public static function getWorkExperienceByEmpId(int $empId): Collection
    {
        return self::where('emp_id', $empId)->get();
    }
}
