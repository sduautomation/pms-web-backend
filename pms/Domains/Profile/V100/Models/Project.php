<?php

declare(strict_types=1);

namespace Domains\Profile\v100\Models;

use Carbon\Carbon;
use Domains\Profile\V100\Core\Traits\ProjectFillPropertyTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;
use Support\Models\Employee;

/**
 * @property int $project_id
 * @property int $emp_id
 * @property string|null $uuid
 * @property string|null $project_name_az
 * @property string|null $project_name_tr
 * @property string|null $project_name_en
 * @property int|null $position_az_id
 * @property int|null $position_tr_id
 * @property int|null $position_en_id
 * @property string|null $notes_az
 * @property string|null $notes_tr
 * @property string|null $notes_en
 * @property int|null $donor_az_id
 * @property int|null $donor_tr_id
 * @property int|null $donor_en_id
 * @property Carbon|null $start_date
 * @property Carbon|null $end_date
 * @property-read Employee $employee
 * @property-read AddableTitle $donor
 * @property-read AddableTitle $position
 * @property-read string|null $project_name
 * @property-read string|null $notes
 * @property-read string|null $donor_title
 * @property-read string|null $position_title
 * @method static where(string $column, mixed $condition)
 * @method static create(mixed $input)
 */
final class Project extends Model
{
    use LanguageTrait, ProjectFillPropertyTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.cv_emp_projects';

    /**
     * @var string
     */
    protected $primaryKey = 'project_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'emp_id',
        'uuid',
        'project_name_az',
        'project_name_tr',
        'project_name_en',
        'position_az_id',
        'position_tr_id',
        'position_en_id',
        'notes_az',
        'notes_tr',
        'notes_en',
        'donor_az_id',
        'donor_tr_id',
        'donor_en_id',
        'start_date',
        'end_date',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'project_id' => 'integer',
        'emp_id' => 'integer',
        'donor_az_id' => 'integer',
        'donor_tr_id' => 'integer',
        'donor_en_id' => 'integer',
        'position_az_id' => 'integer',
        'position_tr_id' => 'integer',
        'position_en_id' => 'integer',
        'start_date' => 'date',
        'end_date' => 'date',
    ];

    public function getDonorTitleAttribute(): ?string
    {
        return $this->donor?->title;
    }

    public function getPositionTitleAttribute(): ?string
    {
        return $this->position?->title;
    }

    public function getProjectNameAttribute(): ?string
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->project_name_az,
            Languages::RUSSIAN, Languages::TURKISH => $this->project_name_tr,
            default => $this->project_name_en
        };
    }

    public function getNotesAttribute(): ?string
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->notes_az,
            Languages::RUSSIAN, Languages::TURKISH => $this->notes_tr,
            default => $this->notes_en
        };
    }

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public function donor(): BelongsTo
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->belongsTo(AddableTitle::class, 'donor_az_id', 'title_id'),
            Languages::RUSSIAN, Languages::TURKISH => $this->belongsTo(AddableTitle::class, 'donor_tr_id', 'title_id'),
            default => $this->belongsTo(AddableTitle::class, 'donor_en_id', 'title_id')
        };
    }

    public function position(): BelongsTo
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->belongsTo(AddableTitle::class, 'position_az_id', 'title_id'),
            Languages::RUSSIAN, Languages::TURKISH => $this->belongsTo(AddableTitle::class, 'position_tr_id', 'title_id'),
            default => $this->belongsTo(AddableTitle::class, 'position_en_id', 'title_id')
        };
    }

    public static function getProjectsByEmpId(int $empId): Collection
    {
        return self::where('emp_id', $empId)->get();
    }
}
