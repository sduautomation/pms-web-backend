<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Domains\Profile\V100\Core\Traits\InterestFillPropertyTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;
use Support\Models\Employee;

/**
 * @property int $emp_id
 * @property string|null $auto_biog_az
 * @property string|null $auto_biog_en
 * @property string|null $auto_biog_tr
 * @property string|null $research_areas_az
 * @property string|null $research_areas_en
 * @property string|null $research_areas_tr
 * @property-read string|null interest_title
 * @property-read Employee $employee
 */
final class Interest extends Model
{
    use LanguageTrait, InterestFillPropertyTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.emp_profile';

    /**
     * @var string
     */
    protected $primaryKey = 'emp_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'emp_id',
        'auto_biog_az',
        'auto_biog_en',
        'auto_biog_tr',
        'research_areas_az',
        'research_areas_en',
        'research_areas_tr',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'emp_id' => 'integer',
    ];

    /**
     * @return string|null
     */
    public function getInterestTitleAttribute(): ?string
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->research_areas_az,
            Languages::TURKISH, Languages::RUSSIAN => $this->research_areas_tr,
            default => $this->research_areas_en,
        };
    }

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public static function getInterestsByEmpId(int $empId): Collection
    {
        return self::where('emp_id', $empId)->get();
    }
}
