<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;

/**
 * @property int $type_id
 * @property string $type_title_az
 * @property string $type_title_tr
 * @property string $type_title_en
 * @property int $weight
 * @property-read Contact[]|Collection $contacts
 * @property-read string $type_title;
 */
final class ContactType extends Model
{
    use LanguageTrait;
    /**
     * @var string
     */
    protected $table = 'dbmaster.contact_types';

    /**
     * @var string
     */
    protected $primaryKey = 'type_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'type_title_az',
        'type_title_tr',
        'type_title_en',
        'weight',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'type_id' => 'integer',
        'weight' => 'integer',
    ];

    public function contacts(): HasMany
    {
        return $this->hasMany(Contact::class, 'type_id', 'type_id');
    }

    public function getTypeTitleAttribute(): string
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->type_title_az,
            Languages::RUSSIAN, Languages::TURKISH => $this->type_title_tr,
            default => $this->type_title_en
        };
    }
}
