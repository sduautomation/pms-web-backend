<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;

/**
 * @property int $type_id
 * @property string|null $title_az
 * @property string|null $title_tr
 * @property string|null $title_en
 * @property string $category
 * @property bool|null $distinguished
 * @property bool|null $is_visible
 * @property-read string|null $title
 * @property-read OrganizationCategory organizationCategory
 */
final class InstitutionType extends Model
{
    use LanguageTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.institution_types';

    /**
     * @var string
     */
    protected $primaryKey = 'type_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'title_az',
        'title_tr',
        'title_en',
        'category',
        'distinguished',
        'is_visible',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'type_id' => 'integer',
        'distinguished' => 'boolean',
        'is_visible' => 'boolean',
    ];

    public function organizationCategory(): BelongsTo
    {
        return $this->belongsTo(OrganizationCategory::class, 'category', 'key');
    }

    public function getTitleAttribute(): string|null
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->title_az,
            Languages::RUSSIAN, Languages::TURKISH => $this->title_tr,
            default => $this->title_en
        };
    }
}
