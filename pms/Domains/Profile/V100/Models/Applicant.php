<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Models\Student;

/**
 * @property int $applicant_id
 * @property string $email
 * @property string|null $psw
 * @property string|null $name
 * @property string|null $surname
 * @property string|null $name_native
 * @property string|null $surname_native
 * @property string|null $patronymic
 * @property int|null $attempt_count
 * @property Carbon|null $attempt_date
 * @property bool|null $is_active
 * @property int|null $last_login_info
 * @property string|null $web_lang
 * @property string|null $applicant_type
 * @property Carbon|null $reg_date
 * @property string|null $stud_id
 *
 * @property-read StudentType|null $applicantType
 * @property-read Language|null $language
 * @property-read Student|null $student
 */
final class Applicant extends Model
{
    /**
     * @var string
     */
    protected $table = 'dbmaster.applicants';

    /**
     * @var string
     */
    protected $primaryKey = 'applicant_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'email',
        'psw',
        'name',
        'surname',
        'name_native',
        'surname_native',
        'patronymic',
        'attempt_count',
        'attempt_date',
        'is_active',
        'last_login_info',
        'web_lang',
        'applicant_type',
        'reg_date',
        'stud_id',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'applicant_id' => 'integer',
        'attempt_count' => 'integer',
        'attempt_date' => 'date',
        'is_active' => 'boolean',
        'last_login_info' => 'integer',
        'reg_date' => 'date',
    ];

    public function applicantType(): BelongsTo
    {
        return $this->belongsTo(StudentType::class, 'applicant_type', 'key');
    }

    public function language(): BelongsTo
    {
        return $this->belongsTo(Language::class, 'web_lang', 'lang_code');
    }

    public function student(): BelongsTo
    {
        return $this->belongsTo(Student::class, 'stud_id', 'stud_id');
    }
}
