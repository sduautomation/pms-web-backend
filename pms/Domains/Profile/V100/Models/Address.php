<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;

/**
 * @property int $aid
 * @property int|null $pid
 * @property int $type
 * @property string $title_az
 * @property string $title_tr
 * @property string $title_en
 * @property string $country_code
 * @property int|null $external_id
 * @property int $is_visible
 * @property Carbon|null $create_date
 * @property-read WorkExperience[]|Collection $workExperience
 * @property-read string|null $title
 */
final class Address extends Model
{
    use LanguageTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.address';

    /**
     * @var string
     */
    protected $primaryKey = 'aid';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'aid',
        'pid',
        'type',
        'title_az',
        'title_tr',
        'title_en',
        'country_code',
        'external_id',
        'is_visible',
        'craete_date',
    ];

    protected $casts = [
        'aid' => 'integer',
        'pid' => 'integer',
        'type' => 'integer',
        'external_id' => 'integer',
        'is_visible' => 'integer',
        'craete_date' => 'date',
    ];

    protected $attributes = [
        'title',
    ];

    public function getTitleAttribute() : ?string
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->title_az,
            Languages::TURKISH, Languages::RUSSIAN => $this->title_tr,
            default => $this->title_en,
        };
    }

    public function workExperience(): HasMany
    {
        return $this->hasMany(WorkExperience::class, 'address_id', 'aid');
    }
}
