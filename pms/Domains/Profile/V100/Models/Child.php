<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Models\Employee;

/**
 * @property int $child_id
 * @property int $emp_id
 * @property string|null $name
 * @property string|null $sname
 * @property int|null $gender_id
 * @property Carbon|null $birth_date
 * @property Carbon|null $create_date
 * @property-read Employee $employee
 * @property-read Gender|null $gender
 * @property-read string|null $gender_title
 */
final class Child extends Model
{
    /**
     * @var string
     */
    protected $table = 'dbmaster.employee_children';

    /**
     * @var string
     */
    protected $primaryKey = 'child_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'emp_id',
        'name',
        'sname',
        'gender_id',
        'birth_date',
        'create_date',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'child_id' => 'integer',
        'emp_id' => 'integer',
        'gender_id' => 'integer',
        'birth_date' => 'date',
        'create_date' => 'date',
    ];

    public function getGenderTitleAttribute(): ?string
    {
        return $this->gender?->gender;
    }

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public function gender(): BelongsTo
    {
        return $this->belongsTo(Gender::class, 'gender_id', 'gender_id');
    }
}
