<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Models;

use Illuminate\Database\Eloquent\Model;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;

/**
 * @property string $level_code
 * @property string|null $title_az
 * @property string|null $title_en
 * @property string|null $title_tr
 * @property int|null $weight
 * @property bool|null $is_active
 *
 * @property-read string|null $title
 */
final class EducationLevel extends Model
{
    use LanguageTrait;

    /**
     * @var string
     */
    protected $table = 'dbmaster.edu_levels';

    /**
     * @var string
     */
    protected $primaryKey = 'level_code';

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'level_code',
        'title_az',
        'title_tr',
        'title_en',
        'weight',
        'is_active',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'weight' => 'integer',
        'is_active' => 'boolean',
    ];

    public function getTitleAttribute(): string|null
    {
        return match ($this->getLocale()) {
            Languages::KAZAKH, Languages::AZERBAIJAN => $this->title_az,
            Languages::RUSSIAN, Languages::TURKISH => $this->title_tr,
            default => $this->title_en
        };
    }
}
