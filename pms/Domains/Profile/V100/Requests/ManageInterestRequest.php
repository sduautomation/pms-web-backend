<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Domains\Profile\V100\Cooker\DTO\ManageInterestDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"interest_title"},
 *     @OA\Property(
 *         property="interest_title",
 *         type="string"
 *      )
 * )
 */
final class ManageInterestRequest extends BaseFormRequest
{
    #[ArrayShape(['interest_title' => 'string'])]
    public function rules(): array
    {
        return [
            'interest_title' => 'required|string',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): ManageInterestDTO
    {
        $validated = $this->validated();

        /** @var Employee $user */
        $user = $this->user();

        return new ManageInterestDTO(
            employeeId: $user->emp_id,
            interestTitle: Arr::get($validated, 'interest_title')
        );
    }
}
