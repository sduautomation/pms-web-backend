<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Carbon\CarbonImmutable;
use Domains\Profile\V100\Cooker\DTO\ManageBookDTO;
use Illuminate\Support\Arr;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"title", "publisher", "book_type_id", "authors", "pages",
 *              "url", "country", "province", "region", "city", "date", "type"},
 *     @OA\Property(
 *          property="title",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="publisher",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="book_type_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="authors",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="pages",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="url",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="country",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="province",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="region",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="city",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="type",
 *          type="string"
 *      )
 * )
 */
final class ManageBookRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'publisher' => 'required|string',
            'book_type_id' => 'required|integer|exists:cv_book_types,type_id',
            'authors' => 'required|string',
            'pages' => 'required|string',
            'url' => 'required|string',
            'country' => 'required|string',
            'province' => 'required|string',
            'region' => 'required|string',
            'city' => 'required|string',
            'date' => 'required|date',
            'type' => 'required|string|in:book',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): ManageBookDTO
    {
        $validated = $this->validated();

        /** @var Employee $user */
        $user = $this->user();

        return new ManageBookDTO(
            title: Arr::get($validated, 'title'),
            publisher: Arr::get($validated, 'publisher'),
            bookTypeId: (int) Arr::get($validated, 'book_type_id'),
            authors: Arr::get($validated, 'authors'),
            pages: Arr::get($validated, 'pages'),
            url: Arr::get($validated, 'url'),
            country: Arr::get($validated, 'country'),
            province: Arr::get($validated, 'province'),
            region: Arr::get($validated, 'region'),
            city: Arr::get($validated, 'city'),
            date: CarbonImmutable::parse(Arr::get($validated, 'date')),
            type: Arr::get($validated, 'type'),
            employeeId: $user->emp_id
        );
    }
}
