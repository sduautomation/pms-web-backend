<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Domains\Profile\V100\Cooker\DTO\ManageHardSkillDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"hard_skill_title"},
 *     @OA\Property(
 *         property="hard_skill_title",
 *         type="string"
 *      )
 * )
 */
final class ManageHardSkillRequest extends BaseFormRequest
{
    #[ArrayShape(['hard_skill_title' => 'string'])]
    public function rules(): array
    {
        return [
            'hard_skill_title' => 'required|string',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): ManageHardSkillDTO
    {
        $validated = $this->validated();

        /** @var Employee $user */
        $user = $this->user();

        return new ManageHardSkillDTO(
            employeeId: $user->emp_id,
            hardSkillTitle: Arr::get($validated, 'hard_skill_title')
        );
    }
}
