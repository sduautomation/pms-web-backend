<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Carbon\CarbonImmutable;
use Domains\Profile\V100\Cooker\DTO\ManageAwardDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"certificate_name_id", "organization_name_id", "certified_date"},
 *     @OA\Property(
 *          property="certificate_name_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="organization_name_id",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="certified_date",
 *          type="string"
 *      )
 * )
 */
final class ManageAwardRequest extends BaseFormRequest
{
    #[ArrayShape(['certificate_name_id' => 'string', 'organization_name_id' => 'string', 'certified_date' => 'string'])]
    public function rules(): array
    {
        return [
            'certificate_name_id' => 'required|int|exists:addable_titles,title_id',
            'organization_name_id' => 'required|int|exists:addable_titles,title_id',
            'certified_date' => 'required|date',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): ManageAwardDTO
    {
        $validated = $this->validated();
        /** @var Employee $user */
        $user = $this->user();

        return new ManageAwardDTO(
            employeeId: $user->emp_id,
            certificateNameId: (int) Arr::get($validated, 'certificate_name_id'),
            organizationNameId:(int) Arr::get($validated, 'organization_name_id'),
            certifiedDate: CarbonImmutable::parse(Arr::get($validated, 'certified_date'))
        );
    }
}
