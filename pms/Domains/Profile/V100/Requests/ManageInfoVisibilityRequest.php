<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Domains\Profile\V100\Cooker\Collections\ContactVisibilityCollection;
use Domains\Profile\V100\Cooker\DTO\ManageInfoVisibilityDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"age", "birth_date", "marital_status", "emp_website", "autobiography"},
 *     @OA\Property(
 *          property="age",
 *          type="boolean"
 *      ),
 *     @OA\Property(
 *          property="birth_date",
 *          type="boolean"
 *      ),
 *      @OA\Property(
 *          property="marital_status",
 *          type="boolean"
 *      ),
 *      @OA\Property(
 *          property="emp_website",
 *          type="boolean"
 *      ),
 *      @OA\Property(
 *          property="autobiography",
 *          type="boolean"
 *      )
 * )
 */
final class ManageInfoVisibilityRequest extends BaseFormRequest
{
    #[ArrayShape(['age' => 'string', 'birth_date' => 'string', 'marital_status' => 'string', 'emp_website' => 'string', 'autobiography' => 'string', 'contacts' => 'string', 'contacts.*.id' => 'string', 'contacts.*.visible' => 'string'])]
    public function rules(): array
    {
        return [
            'age' => 'required|boolean',
            'birth_date' => 'required|boolean',
            'marital_status' => 'required|boolean',
            'emp_website' => 'required|boolean',
            'autobiography' => 'required|boolean',
            'contacts' => 'required|array',
            'contacts.*.id' => 'required|int',
            'contacts.*.visible' => 'required|boolean',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): ManageInfoVisibilityDTO
    {
        $validated = $this->validated();
        /** @var Employee $user */
        $user = $this->user();

        $contacts = ContactVisibilityCollection::make(Arr::get($validated, 'contacts'));

        return new ManageInfoVisibilityDTO(
            employeeId: $user->emp_id,
            age: (bool) Arr::get($validated, 'age'),
            birthDate:  (bool) Arr::get($validated, 'birth_date'),
            maritalStatus: (bool) Arr::get($validated, 'marital_status'),
            empWebsite: (bool) Arr::get($validated, 'emp_website'),
            autobiography: (bool) Arr::get($validated, 'autobiography'),
            contacts: $contacts,
        );
    }
}
