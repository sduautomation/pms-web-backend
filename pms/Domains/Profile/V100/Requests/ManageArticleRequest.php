<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Carbon\CarbonImmutable;
use Domains\Profile\V100\Cooker\DTO\ManageArticleDTO;
use Illuminate\Support\Arr;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"title", "journal_id", "journal_degree_id", "journal_series",
 *              "authors", "pages", "url", "date", "country", "type"},
 *     @OA\Property(
 *          property="title",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="journal_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="journal_degree_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="journal_series",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="authors",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="pages",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="url",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="country",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="type",
 *          type="string"
 *      )
 * )
 */
final class ManageArticleRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'journal_id' => 'required|integer|exists:addable_titles,title_id',
            'journal_degree_id' => 'required|integer|exists:cv_journal_degree,degree_id',
            'journal_series' => 'required|string',
            'authors' => 'required|string',
            'pages' => 'required|string',
            'url' => 'required|string',
            'date' => 'required|date',
            'country' => 'required|string',
            'type' => 'required|string|in:article',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): ManageArticleDTO
    {
        $validated = $this->validated();

        /** @var Employee $user */
        $user = $this->user();

        return new ManageArticleDTO(
            title: Arr::get($validated, 'title'),
            journalId: (int) Arr::get($validated, 'journal_id'),
            journalDegreeId: (int) Arr::get($validated, 'journal_degree_id'),
            journalSeries: Arr::get($validated, 'journal_series'),
            authors: Arr::get($validated, 'authors'),
            pages: Arr::get($validated, 'pages'),
            url: Arr::get($validated, 'url'),
            date: CarbonImmutable::parse(Arr::get($validated, 'date')),
            country: Arr::get($validated, 'country'),
            type: Arr::get($validated, 'type'),
            employeeId: $user->emp_id
        );
    }
}
