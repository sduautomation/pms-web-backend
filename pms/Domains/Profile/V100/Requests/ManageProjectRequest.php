<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Carbon\CarbonImmutable;
use Domains\Profile\V100\Cooker\DTO\ManageProjectDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"donor_id", "project_name", "position_id", "start_date", "ongoing"},
 *     @OA\Property(
 *          property="donor_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="project_name",
 *          type="string"
 *      ),
 *      @OA\Property(
 *          property="position_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="notes",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="start_date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="end_date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="ongoing",
 *          type="boolean"
 *      )
 * )
 */
final class ManageProjectRequest extends BaseFormRequest
{
    #[ArrayShape(['donor_id' => 'string', 'project_name' => 'string', 'position_id' => 'string', 'notes' => 'string', 'start_date' => 'string', 'end_date' => 'string', 'ongoing' => 'string'])]
    public function rules(): array
    {
        return [
            'donor_id' => 'required|int|exists:addable_titles,title_id',
            'project_name' => 'required|string',
            'position_id' => 'required|int|exists:addable_titles,title_id',
            'notes' => 'nullable|string',
            'start_date' => 'required|date',
            'end_date' => 'required_if:ongoing,false|date',
            'ongoing' => 'required|boolean',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): ManageProjectDTO
    {
        $validated = $this->validated();
        /** @var Employee $user */
        $user = $this->user();

        $ongoing = (bool) Arr::get($validated, 'ongoing');
        $startDate = CarbonImmutable::parse(Arr::get($validated, 'start_date'));
        $endDate = $ongoing === false ? CarbonImmutable::parse(Arr::get($validated, 'end_date')) : null;

        return new ManageProjectDTO(
            employeeId: $user->emp_id,
            donorId: (int) Arr::get($validated, 'donor_id'),
            projectName: Arr::get($validated, 'project_name'),
            positionId: (int) Arr::get($validated, 'position_id'),
            notes: Arr::get($validated, 'notes'),
            startDate: $startDate,
            endDate: $endDate,
            ongoing: $ongoing
        );
    }
}
