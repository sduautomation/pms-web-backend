<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Domains\Profile\V100\Cooker\DTO\ManageLanguageDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"language_code,language_level,language_type"},
 *     @OA\Property(
 *         property="language_code",
 *         type="string"
 *      ),
 *     @OA\Property(
 *          property="language_level",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="language_type",
 *          type="integer"
 *      )
 * )
 */
final class ManageLanguageRequest extends BaseFormRequest
{
    #[ArrayShape(['language_code' => 'string', 'language_level' => 'integer', 'language_type' => 'integer'])]
    public function rules(): array
    {
        return [
            'language_code' => 'required|string',
            'language_level' => 'required|integer',
            'language_type' => 'required|integer',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): ManageLanguageDTO
    {
        $validated = $this->validated();

        /** @var Employee $user */
        $user = $this->user();

        return new ManageLanguageDTO(
            employeeId: $user->emp_id,
            languageCode: Arr::get($validated, 'language_code'),
            languageLevelId: (int) Arr::get($validated, 'language_level'),
            languageType: (int) Arr::get($validated, 'language_type')
        );
    }
}
