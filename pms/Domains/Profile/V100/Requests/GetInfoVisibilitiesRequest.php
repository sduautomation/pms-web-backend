<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Domains\Profile\V100\Cooker\DTO\GetInfoVisibilitiesDTO;
use Domains\Profile\V100\Core\Enums\InfoVisibilitiesTypes;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"type"},
 *     @OA\Property(
 *         property="type",
 *         type="string"
 *      )
 * )
 */
final class GetInfoVisibilitiesRequest extends BaseFormRequest
{
    #[ArrayShape(['type' => 'string'])]
    public function rules(): array
    {
        return [
            'type' => 'required|string|in:all,contacts,others',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): GetInfoVisibilitiesDTO
    {
        $validated = $this->validated();
        /** @var Employee $user */
        $user = $this->user();

        return new GetInfoVisibilitiesDTO(
            employeeId: $user->emp_id,
            type: Arr::get($validated, 'type', InfoVisibilitiesTypes::ALL)
        );
    }
}
