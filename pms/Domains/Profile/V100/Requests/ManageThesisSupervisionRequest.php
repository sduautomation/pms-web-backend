<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Domains\Profile\V100\Cooker\DTO\ManageThesisSupervisionDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"level_code", "student", "university", "topic", "begin_year", "defence_year"},
 *     @OA\Property(
 *          property="level_code",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="student",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="university",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="topic",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="begin_year",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="defence_year",
 *          type="integer"
 *      )
 * )
 */
final class ManageThesisSupervisionRequest extends BaseFormRequest
{
    #[ArrayShape(['level_code' => 'string', 'student' => 'string', 'university' => 'string', 'topic' => 'string', 'begin_year' => 'string', 'defence_year' => 'string'])]
    public function rules(): array
    {
        return [
            'level_code' => 'required|string|exists:edu_levels,level_code',
            'student' => 'required|string',
            'university' => 'required|string',
            'topic' => 'required|string',
            'begin_year' => 'required|integer',
            'defence_year' => 'required|integer',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): ManageThesisSupervisionDTO
    {
        $validated = $this->validated();

        /** @var Employee $user */
        $user = $this->user();

        return new ManageThesisSupervisionDTO(
            levelCode: Arr::get($validated, 'level_code'),
            student: Arr::get($validated, 'student'),
            university: Arr::get($validated, 'university'),
            topic: Arr::get($validated, 'topic'),
            beginYear: Arr::get($validated, 'begin_year'),
            defenceYear: Arr::get($validated, 'defence_year'),
            employeeId: $user->emp_id
        );
    }
}
