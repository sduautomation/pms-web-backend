<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Domains\Profile\V100\Cooker\DTO\ManageBiographyDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"biography"},
 *     @OA\Property(
 *         property="biography",
 *         type="string"
 *      )
 * )
 */
final class ManageBiographyRequest extends BaseFormRequest
{
    #[ArrayShape(['biography' => 'string'])]
    public function rules(): array
    {
        return [
            'biography' => 'required|string',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): ManageBiographyDTO
    {
        $validated = $this->validated();

        /** @var Employee $user */
        $user = $this->user();

        return new ManageBiographyDTO(
            biography: Arr::get($validated, 'biography'),
            employeeId: $user->emp_id,
        );
    }
}
