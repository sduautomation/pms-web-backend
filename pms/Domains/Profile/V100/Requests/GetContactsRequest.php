<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Domains\Profile\V100\Cooker\DTO\GetContactsDTO;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *
 * )
 */
final class GetContactsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [

        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): GetContactsDTO
    {
        /** @var Employee $user */
        $user = $this->user();

        return new GetContactsDTO(
            employeeId: $user->emp_id
        );
    }
}
