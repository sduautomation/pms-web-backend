<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Domains\Profile\V100\Cooker\DTO\ManageEducationDTO;
use Illuminate\Support\Arr;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"university", "level_code", "speciality", "city", "cipher", "registration_no",
 *                  "diploma_no", "issue_date", "start_date", "end_date", "continues", "honors_diploma"},
 *     @OA\Property(
 *          property="university",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="level_code",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="speciality",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="city",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="cipher",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="registration_no",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="diploma_no",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="issue_date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="start_date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="end_date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="continues",
 *          type="boolean"
 *      ),
 *     @OA\Property(
 *          property="honors_diploma",
 *          type="boolean"
 *      )
 * )
 */
final class ManageEducationRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'university' => 'required|string',
            'level_code' => 'required|string|exists:edu_levels,level_code',
            'speciality' => 'required|string',
            'city' => 'required|string',
            'cipher' => 'required|string',
            'registration_no' => 'required|string',
            'diploma_no' => 'required|string',
            'issue_date' => 'required|date',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'continues' => 'required|boolean',
            'honors_diploma' => 'required|boolean',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): ManageEducationDTO
    {
        $validated = $this->validated();

        /** @var Employee $user */
        $user = $this->user();

        return new ManageEducationDTO(
            university: Arr::get($validated, 'university'),
            levelCode: Arr::get($validated, 'level_code'),
            speciality: Arr::get($validated, 'speciality'),
            city: Arr::get($validated, 'city'),
            cipher: Arr::get($validated, 'cipher'),
            registrationNo: Arr::get($validated, 'registration_no'),
            diplomaNo: Arr::get($validated, 'diploma_no'),
            issueDate: Arr::get($validated, 'issue_date'),
            startDate: Arr::get($validated, 'start_date'),
            endDate: Arr::get($validated, 'end_date'),
            continues: Arr::get($validated, 'continues'),
            honorsDiploma: Arr::get($validated, 'honors_diploma'),
            employeeId: $user->emp_id
        );
    }
}
