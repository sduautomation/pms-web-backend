<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Domains\Profile\V100\Cooker\DTO\ManageConductedCourseDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"level_code", "course_title"},
 *     @OA\Property(
 *          property="level_code",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="course_title",
 *          type="string"
 *      )
 * )
 */
final class ManageConductedCourseRequest extends BaseFormRequest
{
    #[ArrayShape(['level_code' => 'string', 'course_title' => 'string'])]
    public function rules(): array
    {
        return [
            'level_code' => 'required|string|exists:edu_levels,level_code',
            'course_title' => 'required|string',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): ManageConductedCourseDTO
    {
        $validated = $this->validated();

        /** @var Employee $user */
        $user = $this->user();

        return new ManageConductedCourseDTO(
            levelCode: Arr::get($validated, 'level_code'),
            courseTitle: Arr::get($validated, 'course_title'),
            employeeId: $user->emp_id
        );
    }
}
