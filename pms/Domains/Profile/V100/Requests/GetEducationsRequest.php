<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Domains\Profile\V100\Cooker\DTO\GetEducationsDTO;
use Domains\Profile\V100\Core\Enums\EducationTypes;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"type"},
 *     @OA\Property(
 *          property="type",
 *          type="string"
 *      )
 * )
 */
final class GetEducationsRequest extends BaseFormRequest
{
    #[ArrayShape(['type' => 'array'])]
    public function rules(): array
    {
        return [
            'type' => [
                'required',
                'string',
                Rule::in([
                    EducationTypes::ALL,
                    EducationTypes::EDUCATIONS,
                    EducationTypes::THESIS_SUPERVISIONS,
                    EducationTypes::CONDUCTED_COURSES,
                    EducationTypes::SCIENTIFIC_ORGANIZATIONS,
                    EducationTypes::ARTICLES,
                    EducationTypes::BOOKS,
                    EducationTypes::CONFERENCES,
                ]),
            ],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): GetEducationsDTO
    {
        $validated = $this->validated();
        /** @var Employee $user */
        $user = $this->user();

        return new GetEducationsDTO(
            type: Arr::get($validated, 'type'),
            employeeId: $user->emp_id
        );
    }
}
