<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Domains\Profile\V100\Cooker\DTO\ManageContactDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"type_id", "contact"},
 *     @OA\Property(
 *          property="type_id",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="contact",
 *          type="string"
 *      )
 * )
 */
final class ManageContactRequest extends BaseFormRequest
{
    #[ArrayShape(['type_id' => 'integer', 'contact' => 'string'])]
    public function rules(): array
    {
        return [
            'type_id' => 'required|int|exists:contact_types,type_id',
            'contact' => 'required|string',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): ManageContactDTO
    {
        $validated = $this->validated();
        /** @var Employee $user */
        $user = $this->user();

        return new ManageContactDTO(
            employeeId: $user->emp_id,
            typeId: (int) Arr::get($validated, 'type_id'),
            contact: Arr::get($validated, 'contact')
        );
    }
}
