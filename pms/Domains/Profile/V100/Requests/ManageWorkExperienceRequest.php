<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Carbon\CarbonImmutable;
use Domains\Profile\V100\Cooker\DTO\ManageWorkExperienceDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"job_title", "company_id", "address_id", "start_date", "present"},
 *     @OA\Property(
 *          property="job_title",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="company_id",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="address_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="start_date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="end_date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="present",
 *          type="boolean"
 *      )
 * )
 */
final class ManageWorkExperienceRequest extends BaseFormRequest
{
    #[ArrayShape(['job_title' => 'string', 'company_id' => 'string', 'address_id' => 'string', 'start_date' => 'string', 'end_date' => 'string', 'present' => 'string'])]
    public function rules(): array
    {
        return [
            'job_title' => 'required|string',
            'company_id' => 'required|int|exists:addable_titles,title_id',
            'address_id' => 'required|int|exists:address,aid',
            'start_date' => 'required|date',
            'end_date' => 'required_if:present,false|date',
            'present' => 'required|boolean',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): ManageWorkExperienceDTO
    {
        $validated = $this->validated();
        /** @var Employee $user */
        $user = $this->user();

        $present = (bool) Arr::get($validated, 'present');
        $startDate = CarbonImmutable::parse(Arr::get($validated, 'start_date'));
        $endDate = $present === false ? CarbonImmutable::parse(Arr::get($validated, 'end_date')) : null;

        return new ManageWorkExperienceDTO(
            employeeId: $user->emp_id,
            jobTitle: Arr::get($validated, 'job_title'),
            companyId: (int) Arr::get($validated, 'company_id'),
            addressId: (int) Arr::get($validated, 'address_id'),
            startDate: $startDate,
            endDate: $endDate,
            present: $present
        );
    }
}
