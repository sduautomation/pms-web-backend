<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Carbon\CarbonImmutable;
use Domains\Profile\V100\Cooker\DTO\ManageChildDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"name", "sname", "gender_id", "birth_date"},
 *     @OA\Property(
 *         property="name",
 *         type="string"
 *      ),
 *     @OA\Property(
 *         property="sname",
 *         type="string"
 *      ),
 *     @OA\Property(
 *         property="gender_id",
 *         type="integer"
 *      ),
 *     @OA\Property(
 *         property="birth_date",
 *         type="string"
 *      )
 * )
 */
final class ManageChildRequest extends BaseFormRequest
{
    #[ArrayShape(['name' => 'string', 'sname' => 'string', 'gender_id' => 'string', 'birth_date' => 'string'])]
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'sname' => 'required|string',
            'gender_id' => 'required|integer|exists:gender,gender_id',
            'birth_date' => 'required|date',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): ManageChildDTO
    {
        $validated = $this->validated();
        /** @var Employee $user */
        $user = $this->user();

        return new ManageChildDTO(
            name: Arr::get($validated, 'name'),
            surname: Arr::get($validated, 'sname'),
            genderId: Arr::get($validated, 'gender_id'),
            birthDate: CarbonImmutable::parse(Arr::get($validated, 'birth_date')),
            employeeId: $user->emp_id,
        );
    }
}
