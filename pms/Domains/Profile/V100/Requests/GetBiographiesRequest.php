<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Domains\Profile\V100\Cooker\DTO\GetBiographiesDTO;
use Domains\Profile\V100\Core\Enums\BiographyTypes;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"type"},
 *     @OA\Property(
 *         property="type",
 *         type="string"
 *      )
 * )
 */
final class GetBiographiesRequest extends BaseFormRequest
{
    #[ArrayShape(['type' => 'string'])]
    public function rules(): array
    {
        return [
            'type' => 'required|string|in:all,biography,children',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): GetBiographiesDTO
    {
        $validated = $this->validated();

        return new GetBiographiesDTO(
            type: Arr::get($validated, 'type', BiographyTypes::ALL)
        );
    }
}
