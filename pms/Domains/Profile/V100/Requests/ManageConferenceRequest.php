<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Carbon\CarbonImmutable;
use Domains\Profile\V100\Cooker\DTO\ManageConferenceDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"title", "conference_id", "authors", "pages", "url", "country", "date", "type"},
 *     @OA\Property(
 *          property="title",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="conference_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="authors",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="pages",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="url",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="country",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="type",
 *          type="string"
 *      )
 * )
 */
final class ManageConferenceRequest extends BaseFormRequest
{
    #[ArrayShape(['title' => 'string', 'conference_id' => 'string', 'authors' => 'string', 'pages' => 'string', 'url' => 'string', 'country' => 'string', 'date' => 'string', 'type' => 'string'])]
    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'conference_id' => 'required|integer|exists:addable_titles,title_id',
            'authors' => 'required|string',
            'pages' => 'required|string',
            'url' => 'required|string',
            'country' => 'required|string',
            'date' => 'required|date',
            'type' => 'required|string|in:conference',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): ManageConferenceDTO
    {
        $validated = $this->validated();

        /** @var Employee $user */
        $user = $this->user();

        return new ManageConferenceDTO(
            title: Arr::get($validated, 'title'),
            conferenceId: (int) Arr::get($validated, 'conference_id'),
            authors: Arr::get($validated, 'authors'),
            pages: Arr::get($validated, 'pages'),
            url: Arr::get($validated, 'url'),
            country: Arr::get($validated, 'country'),
            date: CarbonImmutable::parse(Arr::get($validated, 'date')),
            type: Arr::get($validated, 'type'),
            employeeId: $user->emp_id
        );
    }
}
