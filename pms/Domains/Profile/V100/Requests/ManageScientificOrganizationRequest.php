<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Carbon\CarbonImmutable;
use Domains\Profile\V100\Cooker\DTO\ManageScientificOrganizationDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"organization_name_id", "membership_date"},
 *     @OA\Property(
 *          property="organization_name_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="membership_date",
 *          type="string"
 *      )
 * )
 */
final class ManageScientificOrganizationRequest extends BaseFormRequest
{
    #[ArrayShape(['organization_name_id' => 'string', 'membership_date' => 'string'])]
    public function rules(): array
    {
        return [
            'organization_name_id' => 'required|integer|exists:addable_titles,title_id',
            'membership_date' => 'required|date',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): ManageScientificOrganizationDTO
    {
        $validated = $this->validated();

        /** @var Employee $user */
        $user = $this->user();

        return new ManageScientificOrganizationDTO(
            organizationNameId: (int) Arr::get($validated, 'organization_name_id'),
            membershipDate: CarbonImmutable::parse(Arr::get($validated, 'membership_date')),
            employeeId: $user->emp_id
        );
    }
}
