<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Domains\Profile\V100\Cooker\DTO\GetAchievementsDTO;
use Domains\Profile\V100\Core\Enums\AchievementTypes;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"type"},
 *     @OA\Property(
 *         property="type",
 *         type="string"
 *      )
 * )
 */
final class GetAchievementsRequest extends BaseFormRequest
{
    #[ArrayShape(['type' => 'string'])]
    public function rules(): array
    {
        return [
            'type' => 'required|string|in:all,experience,projects,awards',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): GetAchievementsDTO
    {
        $validated = $this->validated();
        /** @var Employee $user */
        $user = $this->user();

        return new GetAchievementsDTO(
            employeeId: $user->emp_id,
            type: Arr::get($validated, 'type', AchievementTypes::ALL)
        );
    }
}
