<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Requests;

use Domains\Profile\V100\Cooker\DTO\GetSkillsDTO;
use Domains\Profile\V100\Core\Enums\SkillTypes;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"type"},
 *     @OA\Property(
 *         property="type",
 *         type="string"
 *      )
 * )
 */
final class GetSkillsRequest extends BaseFormRequest
{
    #[ArrayShape(['type' => 'string'])]
    public function rules(): array
    {
        return [
            'type' => 'required|string|in:all,language_skill,hard_skill,interest',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): GetSkillsDTO
    {
        $validated = $this->validated();

        /** @var Employee $user */
        $user = $this->user();

        return new GetSkillsDTO(
            type: Arr::get($validated, 'type', SkillTypes::LANGUAGE),
            employeeId: $user->emp_id
        );
    }
}
