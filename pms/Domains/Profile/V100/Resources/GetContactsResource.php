<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Cooker\DTO\ContactsResponseDTO;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *    @OA\Property(
 *          property="contacts",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/ContactResource")
 *      )
 * )
 * @mixin ContactsResponseDTO
 */
final class GetContactsResource extends BaseResource
{
    #[ArrayShape(['contacts' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection"])]
    public function getResponseArray(): array
    {
        return [
            'contacts' => ContactResource::collection($this->contacts),
        ];
    }
}
