<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Models\InfoVisibility;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="gen_info_visibility_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="emp_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="age",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="birth_date",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="marital_status",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="personal_website",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="autobiography",
 *          type="integer"
 *      )
 * )
 * @mixin InfoVisibility
 */
final class InfoVisibilityResource extends BaseResource
{
    #[ArrayShape(['gen_info_visibility_id' => 'int', 'emp_id' => 'int', 'age' => 'int', 'birth_date' => 'int', 'marital_status' => 'int', 'personal_website' => 'int', 'autobiography' => 'int'])]
    public function getResponseArray(): array
    {
        return [
            'gen_info_visibility_id' => $this->gen_info_visibility_id,
            'emp_id' => $this->emp_id,
            'age' => $this->age,
            'birth_date' => $this->birth_date,
            'marital_status' => $this->marital_status,
            'personal_website' => $this->emp_website,
            'autobiography' => $this->autobiography,
        ];
    }
}
