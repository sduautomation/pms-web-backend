<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Models\Publication;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="title",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="journal",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="journal_degree",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="journal_series",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="authors",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="pages",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="url",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="country",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="type",
 *          type="string"
 *      )
 * )
 * @mixin Publication
 */
final class ArticleResource extends BaseResource
{
    public function getResponseArray(): array
    {
        return [
            'id' => $this->publication_id,
            'title' => $this->title,
            'journal' => $this->journal?->title,
            'journal_degree' => $this->journalDegree?->degree_title,
            'journal_series' => $this->journal_series,
            'authors' => $this->authors,
            'pages' => $this->pages,
            'url' => $this->url,
            'date' => $this->publish_date?->toDateString(),
            // TODO: Правильно ли я беру country?
            'country' => $this->address?->country_code,
            'type' => Publication::ARTICLE_TYPE,
        ];
    }
}
