<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Cooker\DTO\SkillsResponseDTO;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="language_skill",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/LanguageSkillResource")
 *      ),
 *     @OA\Property(
 *          property="interest",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/InterestResource")
 *      ),
 *      @OA\Property(
 *          property="hard_skill",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/HardSkillResource")
 *      )
 * )
 * @mixin SkillsResponseDTO
 */
final class GetSkillsResource extends BaseResource
{
    #[ArrayShape(['language_skill' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'interest' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection",  'hard_skill' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection"])]
    public function getResponseArray(): array
    {
        return [
            'language_skill' => LanguageSkillResource::collection($this->language),
            'interest' => InterestResource::collection($this->interest),
            'hard_skill' => HardSkillResource::collection($this->hardSkill),
        ];
    }
}
