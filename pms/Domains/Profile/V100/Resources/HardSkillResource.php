<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Models\HardSkill;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="emp_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="hard_skill",
 *          type="string"
 *      )
 * )
 * @mixin HardSkill
 */
final class HardSkillResource extends BaseResource
{
    #[ArrayShape(['emp_id' => 'int', 'hard_skill' => 'null|string'])]
    public function getResponseArray(): array
    {
        return [
            'emp_id' => $this->emp_id,
            'hard_skill' => $this->hard_skill_title,
        ];
    }
}
