<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Cooker\DTO\InfoVisibilitiesResponseDTO;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *    @OA\Property(
 *          property="employeeInfoVisibilities",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/InfoVisibilityResource")
 *      ),
 *    @OA\Property(
 *          property="contacts",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/ContactResource")
 *      )
 * )
 * @mixin InfoVisibilitiesResponseDTO
 */
final class GetInfoVisibilityResource extends BaseResource
{
    #[ArrayShape(['employeeInfoVisibilities' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'contacts' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection"])]
    public function getResponseArray(): array
    {
        return [
            'employeeInfoVisibilities' => InfoVisibilityResource::collection($this->employeeInfoVisibilities),
            'contacts' => ContactResource::collection($this->contacts),
        ];
    }
}
