<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Cooker\Attributes\ManageInfoVisibilityAttribute;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *    @OA\Property(
 *          property="contacts",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/ContactResource")
 *      ),
 *    @OA\Property(
 *          property="infoVisibilities",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/InfoVisibilityResource")
 *      )
 * )
 * @mixin ManageInfoVisibilityAttribute
 */
final class ManageInfoVisibilityResource extends BaseResource
{
    #[ArrayShape(['contacts' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'info_visibilities' => "\Domains\Profile\V100\Resources\InfoVisibilityResource"])]
    public function getResponseArray(): array
    {
        return [
            'contacts' => ContactResource::collection($this->contacts),
            'info_visibilities' => new InfoVisibilityResource($this->infoVisibility),
        ];
    }
}
