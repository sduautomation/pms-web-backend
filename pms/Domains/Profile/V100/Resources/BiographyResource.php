<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Models\Biography;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="emp_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="autobiography",
 *          type="string"
 *      )
 * )
 * @mixin Biography
 */
final class BiographyResource extends BaseResource
{
    #[ArrayShape(['emp_id' => 'int', 'autobiography' => 'null|string'])]
    public function getResponseArray(): array
    {
        return [
            'emp_id' => $this->emp_id,
            'autobiography' => $this->autobiography,
        ];
    }
}
