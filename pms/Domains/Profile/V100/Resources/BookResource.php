<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Models\Publication;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="title",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="publisher",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="book_type",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="authors",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="pages",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="url",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="country",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="province",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="region",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="city",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="type",
 *          type="string"
 *      )
 * )
 * @mixin Publication
 */
final class BookResource extends BaseResource
{
    public function getResponseArray(): array
    {
        return [
            'id' => $this->publication_id,
            'title' => $this->title,
            'publisher' => $this->publisher,
            'book_type' => $this->bookType?->type_name,
            'authors' => $this->authors,
            'pages' => $this->pages,
            'url' => $this->url,
            // TODO: Правильно ли я беру country, province, region, city?
            'country' => $this->address?->country_code,
            'province' => $this->address?->title,
            'region' => $this->address?->title,
            'city' => $this->address?->title,
            'date' => $this->publish_date?->toDateString(),
            'type' => Publication::BOOK_TYPE,
        ];
    }
}
