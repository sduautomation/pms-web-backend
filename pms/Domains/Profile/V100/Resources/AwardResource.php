<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Models\Award;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="cid",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="certified_date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="emp_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="certificate_name",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="org_name",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="uuid",
 *          type="string"
 *      )
 * )
 * @mixin Award
 */
final class AwardResource extends BaseResource
{
    #[ArrayShape(['cid' => 'int', 'emp_id' => 'int', 'certified_date' => "\Carbon\Carbon|null", 'certificate_name' => 'null|string', 'organization_name' => 'null|string', 'uuid' => 'null|string'])]
    public function getResponseArray(): array
    {
        return [
            'cid' => $this->cid,
            'emp_id' => $this->emp_id,
            'certified_date' => $this->certified_date,
            'certificate_name' => $this->certificate_name,
            'organization_name' => $this->organization_name,
            'uuid' => $this->uuid,
        ];
    }
}
