<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Cooker\DTO\BiographiesResponseDTO;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="children",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/ChildResource")
 *      ),
 *     @OA\Property(
 *          property="autobiography",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/BiographyResource")
 *      )
 * )
 * @mixin BiographiesResponseDTO
 */
final class GetBiographiesResource extends BaseResource
{
    #[ArrayShape(['children' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'biography' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection"])]
    public function getResponseArray(): array
    {
        return [
            'children' => ChildResource::collection($this->children),
            'biography' => BiographyResource::collection($this->biography),
        ];
    }
}
