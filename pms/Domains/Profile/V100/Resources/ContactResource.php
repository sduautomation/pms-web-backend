<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Models\Contact;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="contact_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="emp_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="type_title",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="contact",
 *          type="string"
 *      ),
 *      @OA\Property(
 *          property="visible",
 *          type="int"
 *      )
 * )
 * @mixin Contact
 */
final class ContactResource extends BaseResource
{
    #[ArrayShape(['contact_id' => 'int', 'emp_id' => 'int', 'type_title' => 'string', 'contact' => 'string', 'visible' => 'int'])]
    public function getResponseArray(): array
    {
        return [
            'contact_id' => $this->contact_id,
            'emp_id' => $this->emp_id,
            'type_title' => $this->type,
            'contact' => $this->contact,
            'visible' => $this->visible,
        ];
    }
}
