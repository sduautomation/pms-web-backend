<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Models\Thesis;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="student",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="level",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="university",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="topic",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="begin_year",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="defence_year",
 *          type="integer"
 *      )
 * )
 * @mixin Thesis
 */
final class ThesisSupervisionResource extends BaseResource
{
    #[ArrayShape(['id' => 'int', 'student' => 'string', 'level' => 'null|string', 'university' => 'null|string', 'topic' => 'null|string', 'begin_year' => 'int|null', 'defence_year' => 'int|null'])]
    public function getResponseArray(): array
    {
        return [
            'id' => $this->tid,
            'student' => $this->student,
            'level' => $this->educationLevel?->title,
            'university' => $this->university,
            'topic' => $this->title,
            'begin_year' => $this->begin_year,
            'defence_year' => $this->defence_year,
        ];
    }
}
