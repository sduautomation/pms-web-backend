<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Models\WorkExperience;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="ejob_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="emp_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="job_title",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="start_date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="end_date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="company",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="address_id",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="uuid",
 *          type="string"
 *      )
 * )
 * @mixin WorkExperience
 */
final class WorkExperienceResource extends BaseResource
{
    #[ArrayShape(['ejob_id' => 'int', 'emp_id' => 'int', 'job_title' => 'null|string', 'start_date' => "\Carbon\Carbon|null", 'end_date' => "\Carbon\Carbon|null", 'company' => 'null|string', 'address_id' => 'int|null', 'uuid' => 'null|string'])]
    public function getResponseArray(): array
    {
        return [
          'ejob_id' => $this->ejob_id,
          'emp_id' => $this->emp_id,
          'job_title' => $this->job_title,
          'start_date' => $this->start_date,
          'end_date' => $this->end_date,
          'company' => $this->company_title,
          'address_id' => $this->address_id,
          'uuid' => $this->uuid,
        ];
    }
}
