<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Models\Lecture;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="course_title",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="level",
 *          type="string"
 *      )
 * )
 * @mixin Lecture
 */
final class ConductedCourseResource extends BaseResource
{
    #[ArrayShape(['id' => 'int', 'course_title' => 'null|string', 'level' => 'null|string'])]
    public function getResponseArray(): array
    {
        return [
            'id' => $this->lecture_id,
            'course_title' => $this->name,
            'level' => $this->educationLevel?->title,
        ];
    }
}
