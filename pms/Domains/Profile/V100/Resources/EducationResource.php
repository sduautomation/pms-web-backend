<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Models\GraduatedUniversity;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="university",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="level",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="speciality",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="diploma_no",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="registration_no",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="city",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="cipher",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="issue_date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="start_date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="end_date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="continues",
 *          type="boolean"
 *      ),
 *     @OA\Property(
 *          property="honors_diploma",
 *          type="boolean"
 *      )
 * )
 * @mixin GraduatedUniversity
 */
final class EducationResource extends BaseResource
{
    public function getResponseArray(): array
    {
        return [
            'id' => $this->id,
            'university' => $this->university,
            'level' => $this->educationLevel?->title,
            'speciality' => $this->specialization,
            'diploma_no' => $this->diploma_no,
            'registration_no' => $this->registration_no,
            // TODO: Правильно ли я беру city?
            'city' => $this->institution?->address?->title,
            'cipher' => $this->cipher,
            'issue_date' => $this->issue_date?->toDateString(),
            'start_date' => $this->start_date?->toDateString(),
            'end_date' => $this->end_date?->toDateString(),
            // TODO: Правильно ли я беру continues, honors_diploma?
            'continues' => $this->in_progress,
            'honors_diploma' => $this->distinguished,
        ];
    }
}
