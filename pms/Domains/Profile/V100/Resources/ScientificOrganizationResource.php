<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Models\Organization;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="organization",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="year_of_membership",
 *          type="string"
 *      )
 * )
 * @mixin Organization
 */
final class ScientificOrganizationResource extends BaseResource
{
    #[ArrayShape(['id' => 'int', 'organization' => "\Domains\Profile\V100\Models\AddableTitle|null", 'year_of_membership' => 'string'])]
    public function getResponseArray(): array
    {
        return [
            'id' => $this->org_id,
            'organization' => $this->name,
            'year_of_membership' => $this->membership_date?->toDateString(),
        ];
    }
}
