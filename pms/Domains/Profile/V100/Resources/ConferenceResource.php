<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Models\Publication;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="title",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="conference",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="authors",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="pages",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="url",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="country",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="type",
 *          type="string"
 *      )
 * )
 * @mixin Publication
 */
final class ConferenceResource extends BaseResource
{
    #[ArrayShape(['id' => 'int', 'title' => 'null|string', 'conference' => 'string', 'authors' => 'null|string', 'pages' => 'null|string', 'url' => 'null|string', 'country' => 'string', 'date' => 'string', 'type' => 'string'])]
    public function getResponseArray(): array
    {
        return [
            'id' => $this->publication_id,
            'title' => $this->title,
            'conference' => $this->conference?->title,
            'authors' => $this->authors,
            'pages' => $this->pages,
            'url' => $this->url,
            // TODO: Правильно ли я беру country?
            'country' => $this->address?->country_code,
            'date' => $this->publish_date?->toDateString(),
            'type' => Publication::CONFERENCE_TYPE,
        ];
    }
}
