<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Cooker\DTO\AchievementsResponseDTO;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="workExperience",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/WorkExperienceResource")
 *      ),
 *     @OA\Property(
 *          property="project",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/ProjectResource")
 *      ),
 *     @OA\Property(
 *          property="award",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/AwardResource")
 *      )
 * )
 * @mixin AchievementsResponseDTO
 */
final class GetAchievementsResource extends BaseResource
{
    #[ArrayShape(['work_experience' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'projects' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'awards' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection"])]
    public function getResponseArray(): array
    {
        return [
          'work_experience' => WorkExperienceResource::collection($this->workExperience),
          'projects' => ProjectResource::collection($this->projects),
          'awards' => AwardResource::collection($this->awards),
        ];
    }
}
