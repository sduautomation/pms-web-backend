<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Models\Project;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="project_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="emp_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="uuid",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="project_name",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="position",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="notes",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="donor",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="start_date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="end_date",
 *          type="string"
 *      )
 * )
 * @mixin Project
 */
final class ProjectResource extends BaseResource
{
    #[ArrayShape(['project_id' => 'int', 'emp_id' => 'int', 'uuid' => 'null|string', 'project_name' => 'null|string', 'position' => 'null|string', 'notes' => 'null|string', 'donor' => 'null|string', 'start_date' => "\Carbon\Carbon|null", 'end_date' => "\Carbon\Carbon|null"])]
     public function getResponseArray(): array
     {
         return[
            'project_id' => $this->project_id,
            'emp_id' => $this->emp_id,
            'uuid' => $this->uuid,
            'project_name' => $this->project_name,
            'position'=> $this->position_title,
            'notes' => $this->notes,
            'donor' => $this->donor_title,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
        ];
     }
}
