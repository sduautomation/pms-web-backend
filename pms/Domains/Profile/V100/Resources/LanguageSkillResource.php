<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Models\LanguageSkill;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="lang_k_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="emp_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="stud_id",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="is_native",
 *          type="integer"
 *     ),
 *      @OA\Property(
 *          property="level_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="completed",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="web_log",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="applicant_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="lang_code",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="language",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="language_level",
 *          type="string"
 *      )
 * )
 * @mixin LanguageSkill
 */
final class LanguageSkillResource extends BaseResource
{
    #[ArrayShape(['lang_k_id' => 'int', 'emp_id' => 'int',  'stud_id' => 'int', 'is_native' => 'int', 'level_id' => 'int', 'web_log' => 'int|null', 'completed' => 'int|null', 'applicant_id' => 'int|null', 'lang_code' => 'string', 'language' => 'string|null', 'language_level' => 'string|null'])]
    public function getResponseArray(): array
    {
        return [
            'lang_k_id' => $this->lang_k_id,
            'emp_id' => $this->emp_id,
            'stud_id' => $this->stud_id,
            'is_native' => $this->is_native,
            'level_id' => $this->level_id,
            'completed' => $this->completed,
            'web_log' => $this->web_log,
            'applicant_id' => $this->applicant_id,
            'lang_code' => $this->lang_code,

            'language' => $this->language_title,
            'language_level' => $this->language_level_title,
        ];
    }
}
