<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Models\Interest;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="emp_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="interest",
 *          type="string"
 *      )
 * )
 * @mixin Interest
 */
final class InterestResource extends BaseResource
{
    #[ArrayShape(['emp_id' => 'int', 'interest' => 'null|string'])]
    public function getResponseArray(): array
    {
        return [
            'emp_id' => $this->emp_id,
            'interest' => $this->interest_title,
        ];
    }
}
