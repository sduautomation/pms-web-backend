<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Models\Child;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="child_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="emp_id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="name",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="sname",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="birthdate",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="create_date",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="gender",
 *          type="string"
 *      )
 * )
 * @mixin Child
 */
final class ChildResource extends BaseResource
{
    #[ArrayShape(['child_id' => 'int', 'emp_id' => 'int', 'name' => 'null|string', 'sname' => 'null|string', 'gender_id' => 'int|null', 'birth_date' => "\Carbon\Carbon|null", 'create_date' => "\Carbon\Carbon|null", 'gender' => "\Domains\Profile\V100\Models\Gender|null"])]
    public function getResponseArray(): array
    {
        return [
            'child_id' => $this->child_id,
            'emp_id' => $this->emp_id,
            'name' => $this->name,
            'sname' => $this->sname,
            'gender_id' => $this->gender_id,
            'birth_date' => $this->birth_date,
            'create_date' => $this->create_date,
            'gender' => $this->gender_title,
        ];
    }
}
