<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Resources;

use Domains\Profile\V100\Cooker\DTO\EducationsResponseDTO;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="educations",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/EducationResource")
 *      ),
 *     @OA\Property(
 *          property="thesis_supervisions",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/ThesisSupervisionResource")
 *      ),
 *     @OA\Property(
 *          property="recently_conducted_course",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/ConductedCourseResource")
 *      ),
 *     @OA\Property(
 *          property="scientific_organizations",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/ScientificOrganizationResource")
 *      ),
 *     @OA\Property(
 *          property="articles",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/ArticleResource")
 *      ),
 *     @OA\Property(
 *          property="conferences",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/ConferenceResource")
 *      ),
 *     @OA\Property(
 *          property="books",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/BookResource")
 *      )
 * )
 * @mixin EducationsResponseDTO
 */
final class EducationsResource extends BaseResource
{
    #[ArrayShape(['educations' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'thesis_supervisions' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'recently_conducted_course' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'scientific_organizations' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'articles' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'conferences' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'books' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection"])]
    public function getResponseArray(): array
    {
        return [
            'educations' => EducationResource::collection($this->educations),
            'thesis_supervisions' => ThesisSupervisionResource::collection($this->thesisSupervisions),
            'recently_conducted_course' => ConductedCourseResource::collection($this->conductedCourses),
            'scientific_organizations' => ScientificOrganizationResource::collection($this->scientificOrganizations),
            'articles' => ArticleResource::collection($this->articles),
            'conferences' => ConferenceResource::collection($this->conferences),
            'books' => BookResource::collection($this->books),
        ];
    }
}
