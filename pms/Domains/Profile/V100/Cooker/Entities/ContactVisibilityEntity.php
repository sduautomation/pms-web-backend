<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Entities;

use Illuminate\Support\Arr;
use JetBrains\PhpStorm\Pure;

final class ContactVisibilityEntity
{
    public function __construct(
        public int $id,
        public bool $visible
    ) {
    }

    /**
     * @param array<string, mixed> $data
     */
    public static function fromArray(array $data): self
    {
        return new self(
            id: Arr::get($data, 'id'),
            visible: Arr::get($data, 'visible')
        );
    }

    #[Pure]
    public static function fromObject(object $data): self
    {
        return new self(
            id: $data->id,
            visible: $data->visible
        );
    }
}
