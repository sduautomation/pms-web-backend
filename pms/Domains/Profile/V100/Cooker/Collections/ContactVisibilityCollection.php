<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Collections;

use Domains\Profile\V100\Cooker\Entities\ContactVisibilityEntity;
use Illuminate\Support\Collection;
use RuntimeException;

final class ContactVisibilityCollection extends Collection
{
    /**
     * @param array $items
     * @return static
     */
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            switch (true) {
                case is_array($item):
                    $self->add(ContactVisibilityEntity::fromArray($item));
                    break;
                case is_object($item):
                    $self->add(ContactVisibilityEntity::fromObject($item));
                    break;
            }
        }

        return $self;
    }

    /**
     * @param mixed $item
     * @return $this
     */
    public function add($item): self
    {
        if (! $item instanceof ContactVisibilityEntity) {
            throw new RuntimeException('Item must be of the type ContactVisibilityEntity');
        }

        return parent::add($item);
    }
}
