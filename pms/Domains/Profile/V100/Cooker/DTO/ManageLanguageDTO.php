<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

final class ManageLanguageDTO
{
    public function __construct(
        public int $employeeId,
        public string $languageCode,
        public int $languageLevelId,
        public int $languageType
    ) {
    }
}
