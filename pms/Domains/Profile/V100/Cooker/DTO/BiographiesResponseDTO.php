<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

use Illuminate\Database\Eloquent\Collection;

final class BiographiesResponseDTO
{
    /**
     * @param Collection $children
     * @param Collection $biography
     */
    public function __construct(
        public Collection $children,
        public Collection $biography
    ) {
    }
}
