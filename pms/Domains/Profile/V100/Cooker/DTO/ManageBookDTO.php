<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

use Carbon\CarbonImmutable;

final class ManageBookDTO
{
    public function __construct(
        public string $title,
        public string $publisher,
        public int $bookTypeId,
        public string $authors,
        public string $pages,
        public string $url,
        public string $country,
        public string $province,
        public string $region,
        public string $city,
        public CarbonImmutable $date,
        public string $type,
        public int $employeeId
    ) {
    }
}
