<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

use Carbon\CarbonImmutable;

final class ManageConferenceDTO
{
    public function __construct(
        public string $title,
        public int $conferenceId,
        public string $authors,
        public string $pages,
        public string $url,
        public string $country,
        public CarbonImmutable $date,
        public string $type,
        public int $employeeId
    ) {
    }
}
