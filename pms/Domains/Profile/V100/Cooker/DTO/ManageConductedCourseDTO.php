<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

final class ManageConductedCourseDTO
{
    public function __construct(
        public string $levelCode,
        public string $courseTitle,
        public int $employeeId
    ) {
    }
}
