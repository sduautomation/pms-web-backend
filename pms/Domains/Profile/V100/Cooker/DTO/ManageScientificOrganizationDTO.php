<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

use Carbon\CarbonImmutable;

final class ManageScientificOrganizationDTO
{
    public function __construct(
        public int $organizationNameId,
        public CarbonImmutable $membershipDate,
        public int $employeeId
    ) {
    }
}
