<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

use Carbon\CarbonImmutable;

final class ManageChildDTO
{
    public function __construct(
        public string $name,
        public string $surname,
        public int $genderId,
        public CarbonImmutable $birthDate,
        public int $employeeId
    ) {
    }
}
