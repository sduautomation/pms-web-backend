<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

use Carbon\CarbonImmutable;

final class ManageAwardDTO
{
    public function __construct(
        public int $employeeId,
        public int $certificateNameId,
        public int $organizationNameId,
        public CarbonImmutable $certifiedDate
    ) {
    }
}
