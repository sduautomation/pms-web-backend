<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

final class ManageContactDTO
{
    public function __construct(
        public int $employeeId,
        public int $typeId,
        public string $contact
    ) {
    }
}
