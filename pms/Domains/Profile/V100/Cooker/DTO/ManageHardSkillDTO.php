<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

final class ManageHardSkillDTO
{
    public function __construct(
        public int $employeeId,
        public string $hardSkillTitle
    ) {
    }
}
