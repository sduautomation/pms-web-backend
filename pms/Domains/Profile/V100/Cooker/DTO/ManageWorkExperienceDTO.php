<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

use Carbon\CarbonImmutable;

final class ManageWorkExperienceDTO
{
    public function __construct(
        public int $employeeId,
        public string $jobTitle,
        public int $companyId,
        public int $addressId,
        public CarbonImmutable $startDate,
        public ?CarbonImmutable $endDate,
        public bool $present
    ) {
    }
}
