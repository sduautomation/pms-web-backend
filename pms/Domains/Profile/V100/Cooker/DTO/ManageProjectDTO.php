<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

use Carbon\CarbonImmutable;

final class ManageProjectDTO
{
    public function __construct(
        public int $employeeId,
        public int $donorId,
        public string $projectName,
        public int $positionId,
        public string $notes,
        public CarbonImmutable $startDate,
        public ?CarbonImmutable $endDate,
        public bool $ongoing
    ) {
    }
}
