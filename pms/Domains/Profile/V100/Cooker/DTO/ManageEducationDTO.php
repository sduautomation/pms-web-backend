<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

use Carbon\CarbonImmutable;

final class ManageEducationDTO
{
    public function __construct(
        public string $university,
        public string $levelCode,
        public string $speciality,
        public string $city,
        public string $cipher,
        public string $registrationNo,
        public string $diplomaNo,
        public CarbonImmutable $issueDate,
        public CarbonImmutable $startDate,
        public CarbonImmutable $endDate,
        public bool $continues,
        public bool $honorsDiploma,
        public int $employeeId
    ) {
    }
}
