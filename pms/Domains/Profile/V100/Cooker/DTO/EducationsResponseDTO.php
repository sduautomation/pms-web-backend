<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

use Domains\Profile\V100\Models\GraduatedUniversity;
use Domains\Profile\V100\Models\Lecture;
use Domains\Profile\V100\Models\Organization;
use Domains\Profile\V100\Models\Publication;
use Domains\Profile\V100\Models\Thesis;
use Illuminate\Database\Eloquent\Collection;

final class EducationsResponseDTO
{
    /**
     * @param Collection|GraduatedUniversity[] $educations
     * @param Collection|Thesis[] $thesisSupervisions
     * @param Collection|Lecture[] $conductedCourses
     * @param Collection|Organization[] $scientificOrganizations
     * @param Collection|Publication[] $articles
     * @param Collection|Publication[] $conferences
     * @param Collection|Publication[] $books
     */
    public function __construct(
        public Collection|array $educations,
        public Collection|array $thesisSupervisions,
        public Collection|array $conductedCourses,
        public Collection|array $scientificOrganizations,
        public Collection|array $articles,
        public Collection|array $conferences,
        public Collection|array $books
    ) {
    }
}
