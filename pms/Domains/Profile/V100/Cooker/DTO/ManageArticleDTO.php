<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

use Carbon\CarbonImmutable;

final class ManageArticleDTO
{
    public function __construct(
        public string $title,
        public int $journalId,
        public int $journalDegreeId,
        public string $journalSeries,
        public string $authors,
        public string $pages,
        public string $url,
        public CarbonImmutable $date,
        public string $country,
        public string $type,
        public int $employeeId
    ) {
    }
}
