<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

final class GetBiographiesDTO
{
    public function __construct(
        public string $type,
    ) {
    }
}
