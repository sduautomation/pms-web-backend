<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

final class ManageBiographyDTO
{
    public function __construct(
        public string $biography,
        public int $employeeId
    ) {
    }
}
