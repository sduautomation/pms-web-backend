<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

use Illuminate\Database\Eloquent\Collection;

final class SkillsResponseDTO
{
    /**
     * @param Collection $language
     * @param Collection $hardSkill
     * @param Collection $interest
     */
    public function __construct(
        public Collection $language,
        public Collection $hardSkill,
        public Collection $interest
    ) {
    }
}
