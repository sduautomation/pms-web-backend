<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

use Domains\Profile\V100\Cooker\Collections\ContactVisibilityCollection;

final class ManageInfoVisibilityDTO
{
    public function __construct(
        public int $employeeId,
        public bool $age,
        public bool $birthDate,
        public bool $maritalStatus,
        public bool $empWebsite,
        public bool $autobiography,
        public ContactVisibilityCollection $contacts,
    ) {
    }
}
