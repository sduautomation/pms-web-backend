<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

use Illuminate\Database\Eloquent\Collection;

final class AchievementsResponseDTO
{
    /**
     * @param Collection $workExperience
     * @param Collection $projects
     * @param Collection $awards
     */
    public function __construct(
        public Collection $workExperience,
        public Collection $projects,
        public Collection $awards
    ) {
    }
}
