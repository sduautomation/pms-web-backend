<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

final class ManageThesisSupervisionDTO
{
    public function __construct(
        public string $levelCode,
        public string $student,
        public string $university,
        public string $topic,
        public int $beginYear,
        public int $defenceYear,
        public int $employeeId
    ) {
    }
}
