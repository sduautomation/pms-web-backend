<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\DTO;

use Illuminate\Database\Eloquent\Collection;

final class ContactsResponseDTO
{
    /**
     * @param Collection $contacts
     */
    public function __construct(
        public Collection $contacts
    ) {
    }
}
