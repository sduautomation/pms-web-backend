<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\BiographiesResponseDTO;
use Domains\Profile\V100\Cooker\DTO\GetBiographiesDTO;
use Illuminate\Database\Eloquent\Collection;

final class GetBiographiesAttribute
{
    public GetBiographiesDTO $biographiesDto;
    public Collection $children;
    public Collection $biography;
    public BiographiesResponseDTO $responseDto;
}
