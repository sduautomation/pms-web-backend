<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ManageConferenceDTO;
use Domains\Profile\V100\Models\Publication;

final class ManageConferenceAttribute
{
    public ManageConferenceDTO $dto;
    public Publication $conference;
}
