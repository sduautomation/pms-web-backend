<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ManageConductedCourseDTO;
use Domains\Profile\V100\Models\Lecture;

final class ManageConductedCourseAttribute
{
    public ManageConductedCourseDTO $dto;
    public Lecture $conductedCourse;
}
