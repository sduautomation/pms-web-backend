<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ManageInfoVisibilityDTO;
use Domains\Profile\V100\Models\InfoVisibility;
use Illuminate\Database\Eloquent\Collection;

final class ManageInfoVisibilityAttribute
{
    public ManageInfoVisibilityDTO $manageInfoVisibilityDTO;
    public Collection $contacts;
    public InfoVisibility $infoVisibility;
}
