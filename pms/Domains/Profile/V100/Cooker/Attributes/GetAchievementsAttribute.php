<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\AchievementsResponseDTO;
use Domains\Profile\V100\Cooker\DTO\GetAchievementsDTO;
use Illuminate\Database\Eloquent\Collection;

final class GetAchievementsAttribute
{
    public GetAchievementsDTO $requestDto;
    public Collection $workExperience;
    public Collection $projects;
    public Collection $awards;
    public AchievementsResponseDTO $responseDto;
}
