<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\GetSkillsDTO;
use Domains\Profile\V100\Cooker\DTO\SkillsResponseDTO;
use Illuminate\Database\Eloquent\Collection;

final class GetSkillsAttribute
{
    public GetSkillsDTO $skillsDto;
    public Collection $interest;
    public Collection $hardSkill;
    public Collection $language;
    public SkillsResponseDTO $responseDto;
}
