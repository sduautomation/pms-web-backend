<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ManageBookDTO;
use Domains\Profile\V100\Models\Publication;

final class ManageBookAttribute
{
    public ManageBookDTO $dto;
    public Publication $book;
}
