<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\EducationsResponseDTO;
use Domains\Profile\V100\Cooker\DTO\GetEducationsDTO;

final class GetEducationsAttribute
{
    public GetEducationsDTO $requestDto;
    public EducationsResponseDTO $responseDto;
}
