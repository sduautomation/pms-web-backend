<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ContactsResponseDTO;
use Domains\Profile\V100\Cooker\DTO\GetContactsDTO;
use Illuminate\Database\Eloquent\Collection;

final class GetContactsAttribute
{
    public GetContactsDTO $requestDto;
    public Collection $contacts;
    public ContactsResponseDTO $responseDTO;
}
