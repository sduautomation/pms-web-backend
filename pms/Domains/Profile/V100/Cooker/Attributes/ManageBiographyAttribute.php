<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ManageBiographyDTO;
use Domains\Profile\V100\Models\Biography;

final class ManageBiographyAttribute
{
    public ManageBiographyDTO $dto;
    public Biography $biography;
}
