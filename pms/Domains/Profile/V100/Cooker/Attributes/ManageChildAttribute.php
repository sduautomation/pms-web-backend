<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ManageChildDTO;
use Domains\Profile\V100\Models\Child;

final class ManageChildAttribute
{
    public ManageChildDTO $dto;
    public Child $child;
}
