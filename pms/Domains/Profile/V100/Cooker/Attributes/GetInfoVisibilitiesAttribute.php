<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\GetInfoVisibilitiesDTO;
use Domains\Profile\V100\Cooker\DTO\InfoVisibilitiesResponseDTO;
use Illuminate\Database\Eloquent\Collection;

final class GetInfoVisibilitiesAttribute
{
    public GetInfoVisibilitiesDTO $requestDto;
    public Collection $employeeInfoVisibilities;
    public Collection $contacts;
    public InfoVisibilitiesResponseDTO $responseDTO;
}
