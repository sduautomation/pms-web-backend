<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ManageLanguageDTO;
use Domains\Profile\V100\Models\LanguageSkill;

final class ManageLanguageAttribute
{
    public ManageLanguageDTO $manageLanguageDTO;
    public LanguageSkill $languageSkill;
}
