<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ManageThesisSupervisionDTO;
use Domains\Profile\V100\Models\Thesis;

final class ManageThesisSupervisionAttribute
{
    public ManageThesisSupervisionDTO $dto;
    public Thesis $thesis;
}
