<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ManageAwardDTO;
use Domains\Profile\V100\Models\Award;

final class ManageAwardAttribute
{
    public ManageAwardDTO $manageAwardDTO;
    public Award $award;
}
