<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ManageHardSkillDTO;
use Domains\Profile\V100\Models\HardSkill;

final class ManageHardSkillAttribute
{
    public ManageHardSkillDTO $manageHardSkillDTO;
    public HardSkill $hardSkill;
}
