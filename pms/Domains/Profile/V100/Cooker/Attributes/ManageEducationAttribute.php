<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ManageEducationDTO;
use Domains\Profile\V100\Models\GraduatedUniversity;

final class ManageEducationAttribute
{
    public ManageEducationDTO $dto;
    public GraduatedUniversity $education;
}
