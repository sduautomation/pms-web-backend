<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ManageScientificOrganizationDTO;
use Domains\Profile\V100\Models\Organization;

final class ManageScientificOrganizationAttribute
{
    public ManageScientificOrganizationDTO $dto;
    public Organization $organization;
}
