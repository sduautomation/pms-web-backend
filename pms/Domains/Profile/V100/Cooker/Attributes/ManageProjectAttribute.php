<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ManageProjectDTO;
use Domains\Profile\V100\Models\Project;

final class ManageProjectAttribute
{
    public ManageProjectDTO $manageProjectDTO;
    public Project $project;
}
