<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ManageWorkExperienceDTO;
use Domains\Profile\V100\Models\WorkExperience;

final class ManageWorkExperienceAttribute
{
    public ManageWorkExperienceDTO $manageWorkExperienceDTO;
    public WorkExperience $workExperience;
}
