<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ManageArticleDTO;
use Domains\Profile\V100\Models\Publication;

final class ManageArticleAttribute
{
    public ManageArticleDTO $dto;
    public Publication $article;
}
