<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ManageContactDTO;
use Domains\Profile\V100\Models\Contact;

final class ManageContactAttribute
{
    public ManageContactDTO $manageContactDTO;
    public Contact $contact;
}
