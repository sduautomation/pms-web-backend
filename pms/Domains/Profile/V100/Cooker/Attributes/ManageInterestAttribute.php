<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Attributes;

use Domains\Profile\V100\Cooker\DTO\ManageInterestDTO;
use Domains\Profile\V100\Models\Interest;

final class ManageInterestAttribute
{
    public ManageInterestDTO $manageInterestDTO;
    public Interest $interest;
}
