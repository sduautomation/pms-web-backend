<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\GetAchievementsAttribute;
use Domains\Profile\V100\Cooker\DTO\GetAchievementsDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Achievements\Index\SetAwards;
use Domains\Profile\V100\Cooker\Services\Pipelines\Achievements\Index\SetProjects;
use Domains\Profile\V100\Cooker\Services\Pipelines\Achievements\Index\SetResponse;
use Domains\Profile\V100\Cooker\Services\Pipelines\Achievements\Index\SetWorkExperience;
use Domains\Profile\V100\Resources\GetAchievementsResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class AchievementsPipelineService implements PipelineServiceInterface
{
    private GetAchievementsAttribute $attribute;

    public function __construct(
        private PipelineService $service
    ) {
    }

    /**
     * @return string[]
     */
    public function loadPipes(): array
    {
        return [
          SetWorkExperience::class,
          SetProjects::class,
          SetAwards::class,
          SetResponse::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param GetAchievementsDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new GetAchievementsAttribute();
        $attribute->requestDto = $data;

        return $this->setAttribute($attribute);
    }

    /**
     * @param GetAchievementsAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new GetAchievementsResource($this->attribute->responseDto);
    }
}
