<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageBiographyAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageBiographyDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Biography\Edit\EditBiography;
use Domains\Profile\V100\Models\Biography;
use Domains\Profile\V100\Resources\BiographyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class EditBiographyPipelineService implements PipelineServiceInterface
{
    private ManageBiographyAttribute $attribute;
    private Biography $biography;

    public function __construct(
        private PipelineService $service,
    ) {
    }

    public function loadPipes(): array
    {
        return [
            EditBiography::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageBiographyDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageBiographyAttribute();
        $attribute->dto = $data;
        $attribute->biography = $this->biography;

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageBiographyAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function setBiography(Biography $biography): PipelineServiceInterface
    {
        $this->biography = $biography;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new BiographyResource($this->attribute->biography);
    }
}
