<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\GetSkillsAttribute;
use Domains\Profile\V100\Cooker\DTO\GetSkillsDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Skill\Index\SetHardSkill;
use Domains\Profile\V100\Cooker\Services\Pipelines\Skill\Index\SetInterest;
use Domains\Profile\V100\Cooker\Services\Pipelines\Skill\Index\SetLanguageSkill;
use Domains\Profile\V100\Cooker\Services\Pipelines\Skill\Index\SetResponse;
use Domains\Profile\V100\Resources\GetSkillsResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class SkillsPipelineService implements PipelineServiceInterface
{
    private GetSkillsAttribute $attribute;

    public function __construct(
        private PipelineService $service
    ) {
    }

    /**
     * @return string[]
     */
    public function loadPipes(): array
    {
        return [
            SetInterest::class,
            SetHardSkill::class,
            SetLanguageSkill::class,
            SetResponse::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param GetSkillsDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new GetSkillsAttribute();
        $attribute->skillsDto = $data;

        return $this->setAttribute($attribute);
    }

    /**
     * @param GetSkillsAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new GetSkillsResource($this->attribute->responseDto);
    }
}
