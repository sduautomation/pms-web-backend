<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageConductedCourseAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageConductedCourseDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Education\ManageConductedCourse;
use Domains\Profile\V100\Models\Lecture;
use Domains\Profile\V100\Resources\ConductedCourseResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class ManageConductedCoursePipelineService implements PipelineServiceInterface
{
    private ManageConductedCourseAttribute $attribute;
    private Lecture $lecture;

    public function __construct(
        private PipelineService $service
    ) {
    }

    /**
     * @return string[]
     */
    public function loadPipes(): array
    {
        return [
            ManageConductedCourse::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageConductedCourseDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageConductedCourseAttribute();
        $attribute->dto = $data;
        $attribute->conductedCourse = $this->lecture ?? new Lecture();

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageConductedCourseAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function setLecture(Lecture $lecture): PipelineServiceInterface
    {
        $this->lecture = $lecture;

        return $this;
    }

    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new ConductedCourseResource($this->attribute->conductedCourse);
    }
}
