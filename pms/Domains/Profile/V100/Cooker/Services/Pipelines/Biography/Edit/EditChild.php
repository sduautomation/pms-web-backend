<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Biography\Edit;

use Carbon\Carbon;
use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageChildAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageChildDTO;
use Domains\Profile\V100\Models\Child;

final class EditChild
{
    public function handle(ManageChildAttribute $attribute, Closure $next): mixed
    {
        $attribute->child = $this->editChild($attribute->child, $attribute->dto);

        return $next($attribute);
    }

    private function editChild(Child $child, ManageChildDTO $dto): Child
    {
        $child->name = $dto->name;
        $child->sname = $dto->surname;
        $child->gender_id = $dto->genderId;
        $child->birth_date = Carbon::parse($dto->birthDate->toDateString());
        $child->emp_id = $dto->employeeId;

        $child->save();

        return $child->refresh();
    }
}
