<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Biography\Create;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageBiographyAttribute;
use Domains\Profile\V100\Models\Biography;

final class CreateBiography
{
    public function handle(ManageBiographyAttribute $attribute, Closure $next): mixed
    {
        $biography = new Biography();
        $biography->emp_id = $attribute->dto->employeeId;
        $biography->fillAutoBiography($attribute->dto->biography);
        $biography->save();

        $attribute->biography = $biography->refresh();

        return $next($attribute);
    }
}
