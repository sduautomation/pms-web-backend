<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Biography\Edit;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageBiographyAttribute;

final class EditBiography
{
    public function handle(ManageBiographyAttribute $attribute, Closure $next): mixed
    {
        $biography = $attribute->biography;
        $biography->fillAutoBiography($attribute->dto->biography);
        $biography->save();

        $attribute->biography = $biography->refresh();

        return $next($attribute);
    }
}
