<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Biography\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetBiographiesAttribute;
use Domains\Profile\V100\Cooker\DTO\BiographiesResponseDTO;

final class SetResponse
{
    public function handle(GetBiographiesAttribute $attribute, Closure $next): mixed
    {
        $attribute->responseDto = new BiographiesResponseDTO(
            children: $attribute->children,
            biography: $attribute->biography
        );

        return $next($attribute);
    }
}
