<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Biography\Create;

use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageChildAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageChildDTO;
use Domains\Profile\V100\Models\Child;

final class CreateChild
{
    public function handle(ManageChildAttribute $attribute, Closure $next): mixed
    {
        $attribute->child = $this->createChild($attribute->dto);

        return $next($attribute);
    }

    private function createChild(ManageChildDTO $dto): Child
    {
        $child = new Child();
        $child->name = $dto->name;
        $child->sname = $dto->surname;
        $child->birth_date = $dto->birthDate->toDateString();
        $child->create_date = Carbon::parse(CarbonImmutable::now()->toDateString());
        $child->gender_id = $dto->genderId;
        $child->emp_id = $dto->employeeId;

        $child->save();

        return $child->refresh();
    }
}
