<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Biography\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetBiographiesAttribute;
use Domains\Profile\V100\Core\Factories\ChildFactory;

final class SetChildren
{
    public function handle(GetBiographiesAttribute $attribute, Closure $next): mixed
    {
        $attribute->children = ChildFactory::makeChildren($attribute->biographiesDto->type);

        return $next($attribute);
    }
}
