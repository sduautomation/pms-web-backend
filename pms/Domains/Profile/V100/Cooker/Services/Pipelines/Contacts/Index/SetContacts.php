<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Contacts\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetContactsAttribute;
use Domains\Profile\V100\Core\Factories\ContactFactory;

final class SetContacts
{
    public function handle(GetContactsAttribute $attribute, Closure $next): mixed
    {
        $attribute->contacts = ContactFactory::makeContacts(
            empId: $attribute->requestDto->employeeId
        );

        return $next($attribute);
    }
}
