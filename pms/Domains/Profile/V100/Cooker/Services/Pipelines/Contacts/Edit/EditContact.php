<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Contacts\Edit;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageContactAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageContactDTO;
use Domains\Profile\V100\Models\Contact;

final class EditContact
{
    public function handle(ManageContactAttribute $attribute, Closure $next)
    {
        $attribute->contact = $this->editContact($attribute->contact, $attribute->manageContactDTO);

        return $next($attribute);
    }

    public function editContact(Contact $contact, ManageContactDTO $dto): Contact
    {
        $contact->emp_id = $dto->employeeId;
        $contact->type_id = $dto->typeId;
        $contact->contact = $dto->contact;

        $contact->save();

        return $contact->refresh();
    }
}
