<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Contacts\Create;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageContactAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageContactDTO;
use Domains\Profile\V100\Models\Contact;

final class CreateContact
{
    public function handle(ManageContactAttribute $attribute, Closure $next): mixed
    {
        $attribute->contact = $this->createContact($attribute->manageContactDTO);

        return $next($attribute);
    }

    public function createContact(ManageContactDTO $dto): Contact
    {
        $contact = new Contact();
        $contact->emp_id = $dto->employeeId;
        $contact->type_id = $dto->typeId;
        $contact->contact = $dto->contact;

        $contact->save();

        return $contact->refresh();
    }
}
