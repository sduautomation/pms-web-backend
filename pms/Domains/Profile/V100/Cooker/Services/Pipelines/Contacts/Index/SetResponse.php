<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Contacts\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetContactsAttribute;
use Domains\Profile\V100\Cooker\DTO\ContactsResponseDTO;

final class SetResponse
{
    public function handle(GetContactsAttribute $attribute, Closure $next): mixed
    {
        $attribute->responseDTO = new ContactsResponseDTO(
            contacts: $attribute->contacts
        );

        return $next($attribute);
    }
}
