<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Settings\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetInfoVisibilitiesAttribute;
use Domains\Profile\V100\Core\Factories\InfoVisibilityFactories\ContactFactory;

final class SetContacts
{
    public function handle(GetInfoVisibilitiesAttribute $attribute, Closure $next): mixed
    {
        $attribute->contacts = ContactFactory::makeContacts(
            type: $attribute->requestDto->type,
            empId: $attribute->requestDto->employeeId
        );

        return $next($attribute);
    }
}
