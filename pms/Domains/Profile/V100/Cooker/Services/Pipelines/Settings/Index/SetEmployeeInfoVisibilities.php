<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Settings\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetInfoVisibilitiesAttribute;
use Domains\Profile\V100\Core\Factories\InfoVisibilityFactories\InfoVisibilityFactory;

final class SetEmployeeInfoVisibilities
{
    public function handle(GetInfoVisibilitiesAttribute $attribute, Closure $next): mixed
    {
        $attribute->employeeInfoVisibilities = InfoVisibilityFactory::makeMany(
            type: $attribute->requestDto->type,
            empId: $attribute->requestDto->employeeId
        );

        return $next($attribute);
    }
}
