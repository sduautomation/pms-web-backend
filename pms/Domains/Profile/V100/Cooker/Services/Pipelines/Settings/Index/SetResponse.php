<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Settings\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetInfoVisibilitiesAttribute;
use Domains\Profile\V100\Cooker\DTO\InfoVisibilitiesResponseDTO;

final class SetResponse
{
    public function handle(GetInfoVisibilitiesAttribute $attribute, Closure $next): mixed
    {
        $attribute->responseDTO = new InfoVisibilitiesResponseDTO(
            employeeInfoVisibilities: $attribute->employeeInfoVisibilities,
            contacts: $attribute->contacts
        );

        return $next($attribute);
    }
}
