<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Settings\Manage;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageInfoVisibilityAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageInfoVisibilityDTO;
use Domains\Profile\V100\Cooker\Entities\ContactVisibilityEntity;
use Domains\Profile\V100\Models\Contact;
use Illuminate\Database\Eloquent\Collection;

final class EditContacts
{
    public function handle(ManageInfoVisibilityAttribute $attribute, Closure $next): mixed
    {
        $attribute->contacts = $this->updateContact($attribute->manageInfoVisibilityDTO);

        return $next($attribute);
    }

    public function updateContact(ManageInfoVisibilityDTO $dto): Collection
    {
        /** @var Collection|Contact[] $contacts */
        $contacts = Contact::whereIn('contact_id', $dto->contacts->pluck('id'))->get();

        $contacts->each(function (Contact $contact) use ($dto) {
            /** @var ContactVisibilityEntity $contactVisibilityEntity */
            $contactVisibilityEntity = $dto->contacts->where('id', $contact->contact_id)->firstOrFail();

            $contact->visible = $contactVisibilityEntity->visible;
            $contact->save();
        });

        return $contacts;
    }
}
