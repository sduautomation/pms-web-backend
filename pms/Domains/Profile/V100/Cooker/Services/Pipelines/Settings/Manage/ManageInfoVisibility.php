<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Settings\Manage;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageInfoVisibilityAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageInfoVisibilityDTO;
use Domains\Profile\V100\Models\InfoVisibility;

final class ManageInfoVisibility
{
    public function handle(ManageInfoVisibilityAttribute $attribute, Closure $next): mixed
    {
        $attribute->infoVisibility = $this->manageInfoVisibility($attribute->manageInfoVisibilityDTO);

        return $next($attribute);
    }

    public function manageInfoVisibility(ManageInfoVisibilityDTO $dto): InfoVisibility
    {
        $visibility = InfoVisibility::updateOrCreate(
            [
                'emp_id' => $dto->employeeId,
            ],
            [
                'age' => $dto->age,
                'birth_date' => $dto->birthDate,
                'marital_status' => $dto->maritalStatus,
                'emp_website' => $dto->empWebsite,
                'autobiography' => $dto->autobiography,
            ],
        );

        return $visibility->refresh();
    }
}
