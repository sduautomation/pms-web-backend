<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Education;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageScientificOrganizationAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageScientificOrganizationDTO;
use Domains\Profile\V100\Models\Organization;

final class ManageScientificOrganization
{
    public function handle(ManageScientificOrganizationAttribute $attribute, Closure $next): mixed
    {
        $attribute->organization = $this->manageScientificOrganization(
            $attribute->organization ?? new Organization(),
            $attribute->dto
        );

        return $next($attribute);
    }

    private function manageScientificOrganization(Organization $organization, ManageScientificOrganizationDTO $dto): Organization
    {
        $organization->fillOrganizationNameId($dto->organizationNameId);
        $organization->membership_date = $dto->membershipDate->toDateString();
        $organization->emp_id = $dto->employeeId;

        return $organization->refresh();
    }
}
