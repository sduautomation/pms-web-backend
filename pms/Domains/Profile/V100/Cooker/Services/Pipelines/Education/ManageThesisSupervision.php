<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Education;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageThesisSupervisionAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageThesisSupervisionDTO;
use Domains\Profile\V100\Models\Thesis;

final class ManageThesisSupervision
{
    public function handle(ManageThesisSupervisionAttribute $attribute, Closure $next): mixed
    {
        $attribute->thesis = $this->manageThesisSupervision(
            $attribute->thesis ?? new Thesis(),
            $attribute->dto
        );

        return $next($attribute);
    }

    private function manageThesisSupervision(Thesis $thesis, ManageThesisSupervisionDTO $dto): Thesis
    {
        $thesis->edu_level = $dto->levelCode;
        $thesis->student = $dto->student;
        $thesis->fillUniversity($dto->university);
        $thesis->fillTitle($dto->topic);
        $thesis->begin_year = $dto->beginYear;
        $thesis->defence_year = $dto->defenceYear;
        $thesis->emp_id = $dto->employeeId;

        return $thesis->refresh();
    }
}
