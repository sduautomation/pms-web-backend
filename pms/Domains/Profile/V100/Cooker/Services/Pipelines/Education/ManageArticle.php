<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Education;

use Carbon\Carbon;
use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageArticleAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageArticleDTO;
use Domains\Profile\V100\Models\Publication;

final class ManageArticle
{
    public function handle(ManageArticleAttribute $attribute, Closure $next): mixed
    {
        $attribute->article = $this->manageArticle(
            $attribute->article ?? new Publication(),
            $attribute->dto
        );

        return $next($attribute);
    }

    private function manageArticle(Publication $publication, ManageArticleDTO $dto): Publication
    {
        $publication->fillTitle($dto->title);
        $publication->fillJournalId($dto->journalId);
        $publication->journal_degree_id = $dto->journalDegreeId;
        $publication->journal_series = $dto->journalSeries;
        $publication->fillAuthors($dto->authors);
        $publication->pages = $dto->pages;
        $publication->url = $dto->url;
        $publication->publish_date = Carbon::parse($dto->date->toDateString());
        // TODO: Узнать куда сохраняется country
        $publication->emp_id = $dto->employeeId;

        return $publication->refresh();
    }
}
