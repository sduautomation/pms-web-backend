<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Education\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetEducationsAttribute;
use Domains\Profile\V100\Cooker\DTO\GetEducationsDTO;
use Domains\Profile\V100\Core\Factories\ScientificOrganizationFactory;
use Illuminate\Database\Eloquent\Collection;

final class SetScientificOrganizations
{
    public function handle(GetEducationsAttribute $attribute, Closure $next): mixed
    {
        $attribute->responseDto->scientificOrganizations = $this->makeScientificOrganizations($attribute->requestDto);

        return $next($attribute);
    }

    private function makeScientificOrganizations(GetEducationsDTO $requestDto): Collection
    {
        return ScientificOrganizationFactory::makeMany($requestDto->type, $requestDto->employeeId);
    }
}
