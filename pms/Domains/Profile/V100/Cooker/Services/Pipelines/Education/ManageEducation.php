<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Education;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageEducationAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageEducationDTO;
use Domains\Profile\V100\Models\GraduatedUniversity;

final class ManageEducation
{
    public function handle(ManageEducationAttribute $attribute, Closure $next): mixed
    {
        $attribute->education = $this->manageEducation(
            $attribute->education ?? new GraduatedUniversity(),
            $attribute->dto
        );

        return $next($attribute);
    }

    private function manageEducation(GraduatedUniversity $education, ManageEducationDTO $dto): GraduatedUniversity
    {
        $education->fillUniversity($dto->university);
        $education->level_code = $dto->levelCode;
        $education->fillSpeciality($dto->speciality);
        // TODO: Узнать куда сохраняется $dto->city
        $education->cipher = $dto->cipher;
        $education->registration_no = $dto->registrationNo;
        $education->diploma_no = $dto->diplomaNo;
        $education->issue_date = $dto->issueDate->toDateString();
        $education->start_date = $dto->startDate->toDateString();
        $education->end_date = $dto->endDate->toDateString();
        // TODO: Узнать куда сохраняется $dto->continues
        // TODO: Узнать правильно ли сохраняется $dto->honorsDiploma
        $education->distinguished = $dto->honorsDiploma;
        $education->emp_id = $dto->employeeId;

        return $education->refresh();
    }
}
