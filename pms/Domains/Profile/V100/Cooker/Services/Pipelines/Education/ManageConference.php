<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Education;

use Carbon\Carbon;
use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageConferenceAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageConferenceDTO;
use Domains\Profile\V100\Models\Publication;

final class ManageConference
{
    public function handle(ManageConferenceAttribute $attribute, Closure $next): mixed
    {
        $attribute->conference = $this->manageConference(
            $attribute->conference ?? new Publication(),
            $attribute->dto
        );

        return $next($attribute);
    }

    private function manageConference(Publication $publication, ManageConferenceDTO $dto): Publication
    {
        $publication->fillTitle($dto->title);
        $publication->fillConferenceId($dto->conferenceId);
        $publication->fillAuthors($dto->authors);
        $publication->pages = $dto->pages;
        $publication->url = $dto->url;
        // TODO: Узнать куда сохраняется country
        $publication->publish_date = Carbon::parse($dto->date->toDateString());
        $publication->emp_id = $dto->employeeId;

        return $publication->refresh();
    }
}
