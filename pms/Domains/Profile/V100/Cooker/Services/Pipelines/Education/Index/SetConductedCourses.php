<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Education\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetEducationsAttribute;
use Domains\Profile\V100\Cooker\DTO\GetEducationsDTO;
use Domains\Profile\V100\Core\Factories\ConductedCourseFactory;
use Illuminate\Database\Eloquent\Collection;

final class SetConductedCourses
{
    public function handle(GetEducationsAttribute $attribute, Closure $next): mixed
    {
        $attribute->responseDto->conductedCourses = $this->makeConductedCourses($attribute->requestDto);

        return $next($attribute);
    }

    private function makeConductedCourses(GetEducationsDTO $requestDto): Collection
    {
        return ConductedCourseFactory::makeMany($requestDto->type, $requestDto->employeeId);
    }
}
