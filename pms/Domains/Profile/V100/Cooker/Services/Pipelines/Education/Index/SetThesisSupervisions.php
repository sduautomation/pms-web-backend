<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Education\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetEducationsAttribute;
use Domains\Profile\V100\Cooker\DTO\GetEducationsDTO;
use Domains\Profile\V100\Core\Factories\ThesisSupervisionFactory;
use Illuminate\Database\Eloquent\Collection;

final class SetThesisSupervisions
{
    public function handle(GetEducationsAttribute $attribute, Closure $next): mixed
    {
        $attribute->responseDto->thesisSupervisions = $this->makeThesisSupervisions($attribute->requestDto);

        return $next($attribute);
    }

    private function makeThesisSupervisions(GetEducationsDTO $requestDto): Collection
    {
        return ThesisSupervisionFactory::makeMany($requestDto->type, $requestDto->employeeId);
    }
}
