<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Education;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageConductedCourseAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageConductedCourseDTO;
use Domains\Profile\V100\Models\Lecture;

final class ManageConductedCourse
{
    public function handle(ManageConductedCourseAttribute $attribute, Closure $next): mixed
    {
        $attribute->conductedCourse = $this->manageConductedCourse(
            $attribute->conductedCourse ?? new Lecture(),
            $attribute->dto
        );

        return $next($attribute);
    }

    private function manageConductedCourse(Lecture $conductedCourse, ManageConductedCourseDTO $dto): Lecture
    {
        $conductedCourse->level_code = $dto->levelCode;
        $conductedCourse->fillLectureName($dto->courseTitle);
        $conductedCourse->emp_id = $dto->employeeId;

        return $conductedCourse->refresh();
    }
}
