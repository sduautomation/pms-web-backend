<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Education\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetEducationsAttribute;
use Domains\Profile\V100\Cooker\DTO\GetEducationsDTO;
use Domains\Profile\V100\Core\Factories\PublicationFactory;
use Illuminate\Database\Eloquent\Collection;

final class SetPublications
{
    public function handle(GetEducationsAttribute $attribute, Closure $next): mixed
    {
        $publications = $this->makePublications($attribute->requestDto);

        $attribute->responseDto->articles = $publications;
        $attribute->responseDto->books = $publications;
        $attribute->responseDto->conferences = $publications;

        return $next($attribute);
    }

    private function makePublications(GetEducationsDTO $requestDto): Collection
    {
        return PublicationFactory::makeMany($requestDto->type, $requestDto->employeeId);
    }
}
