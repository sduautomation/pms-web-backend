<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Education;

use Carbon\Carbon;
use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageBookAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageBookDTO;
use Domains\Profile\V100\Models\Publication;

final class ManageBook
{
    public function handle(ManageBookAttribute $attribute, Closure $next): mixed
    {
        $attribute->book = $this->manageBook(
            $attribute->book ?? new Publication(),
            $attribute->dto
        );

        return $next($attribute);
    }

    public function manageBook(Publication $publication, ManageBookDTO $dto): Publication
    {
        $publication->fillTitle($dto->title);
        $publication->fillPublisher($dto->publisher);
        $publication->book_type_id = $dto->bookTypeId;
        $publication->fillAuthors($dto->authors);
        $publication->pages = $dto->pages;
        $publication->url = $dto->url;
        // TODO: Узнать куда сохраняется country
        // TODO: Узнать куда сохраняется province
        // TODO: Узнать куда сохраняется region
        // TODO: Узнать куда сохраняется city
        $publication->publish_date = Carbon::parse($dto->date->toDateString());
        $publication->emp_id = $dto->employeeId;

        return $publication->refresh();
    }
}
