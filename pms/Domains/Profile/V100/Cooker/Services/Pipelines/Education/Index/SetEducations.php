<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Education\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetEducationsAttribute;
use Domains\Profile\V100\Cooker\DTO\GetEducationsDTO;
use Domains\Profile\V100\Core\Factories\EducationFactory;
use Illuminate\Database\Eloquent\Collection;

final class SetEducations
{
    public function handle(GetEducationsAttribute $attribute, Closure $next): mixed
    {
        $attribute->responseDto->educations = $this->makeEducations($attribute->requestDto);

        return $next($attribute);
    }

    private function makeEducations(GetEducationsDTO $requestDto): Collection
    {
        return EducationFactory::makeMany($requestDto->type, $requestDto->employeeId);
    }
}
