<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Skill\Edit;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageInterestAttribute;

final class EditInterest
{
    public function handle(ManageInterestAttribute $attribute, Closure $next): mixed
    {
        $interest = $attribute->interest;
        $interest->fillInterestTitle($attribute->manageInterestDTO->interestTitle);
        $interest->save();

        $attribute->interest = $interest->refresh();

        return $next($attribute);
    }
}
