<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Skill\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetSkillsAttribute;
use Domains\Profile\V100\Core\Factories\LanguageSkillFactory;

final class SetLanguageSkill
{
    public function handle(GetSkillsAttribute $attribute, Closure $next): mixed
    {
        $attribute->language = LanguageSkillFactory::makeMany(
            type: $attribute->skillsDto->type,
            empId: $attribute->skillsDto->employeeId
        );

        return $next($attribute);
    }
}
