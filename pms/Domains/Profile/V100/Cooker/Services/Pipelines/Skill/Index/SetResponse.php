<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Skill\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetSkillsAttribute;
use Domains\Profile\V100\Cooker\DTO\SkillsResponseDTO;

final class SetResponse
{
    public function handle(GetSkillsAttribute $attribute, Closure $next): mixed
    {
        $attribute->responseDto = new SkillsResponseDTO(
            language: $attribute->language,
            hardSkill: $attribute->hardSkill,
            interest: $attribute->interest
        );

        return $next($attribute);
    }
}
