<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Skill\Create;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageHardSkillAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageHardSkillDTO;
use Domains\Profile\V100\Models\HardSkill;
use Support\Core\Traits\LanguageTrait;

final class CreateHardSkill
{
    use LanguageTrait;

    public function handle(ManageHardSkillAttribute $attribute, Closure $next): mixed
    {
        $attribute->hardSkill = $this->createHardSkill($attribute->manageHardSkillDTO);

        return $next($attribute);
    }

    private function createHardSkill(ManageHardSkillDTO $dto): HardSkill
    {
        $hardSkill = new HardSkill();
        $hardSkill->emp_id = $dto->employeeId;
        $hardSkill->fillHardSkillTitle($dto->hardSkillTitle);

        $hardSkill->save();

        return $hardSkill->refresh();
    }
}
