<?php

declare(strict_types=1);

namespace Domains\Profile\v100\Cooker\Services\Pipelines\Skill\Create;

use Closure;
use Domains\Profile\v100\Cooker\Attributes\ManageLanguageAttribute;
use Domains\Profile\v100\Cooker\DTO\ManageLanguageDTO;
use Domains\Profile\V100\Models\LanguageSkill;
use Support\Core\Traits\LanguageTrait;

final class CreateLanguageSkill
{
    use LanguageTrait;

    public function handle(ManageLanguageAttribute $attribute, Closure $next): mixed
    {
        $attribute->languageSkill = $this->createProject($attribute->manageLanguageDTO);

        return $next($attribute);
    }

    private function createProject(ManageLanguageDTO $dto): LanguageSkill
    {
        $languageSkill = new LanguageSkill();
        $languageSkill->emp_id = $dto->employeeId;
        $languageSkill->lang_code = $dto->languageCode;
        $languageSkill->level_id = $dto->languageLevelId;
        $languageSkill->is_native = $dto->languageType;

        $languageSkill->save();

        return $languageSkill->refresh();
    }
}
