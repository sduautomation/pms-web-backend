<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Skill\Edit;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageLanguageAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageLanguageDTO;
use Domains\Profile\V100\Models\LanguageSkill;

final class EditLanguageSkill
{
    public function handle(ManageLanguageAttribute $attribute, Closure $next): mixed
    {
        $attribute->languageSkill = $this->editLanguageSkill($attribute->languageSkill, $attribute->manageLanguageDTO);

        return $next($attribute);
    }

    private function editLanguageSkill(LanguageSkill $languageSkill, ManageLanguageDTO $dto): LanguageSkill
    {
        $languageSkill->emp_id = $dto->employeeId;
        $languageSkill->lang_code = $dto->languageCode;
        $languageSkill->level_id = $dto->languageLevelId;
        $languageSkill->is_native = $dto->languageType;

        $languageSkill->save();

        return $languageSkill->refresh();
    }
}
