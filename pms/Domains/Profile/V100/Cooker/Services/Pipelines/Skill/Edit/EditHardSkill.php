<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Skill\Edit;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageHardSkillAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageHardSkillDTO;
use Domains\Profile\V100\Models\HardSkill;

final class EditHardSkill
{
    public function handle(ManageHardSkillAttribute $attribute, Closure $next): mixed
    {
        $attribute->hardSkill = $this->editHardSkill($attribute->hardSkill, $attribute->manageHardSkillDTO);

        return $next($attribute);
    }

    private function editHardSkill(HardSkill $hardSkill, ManageHardSkillDTO $dto): HardSkill
    {
        $hardSkill->emp_id = $dto->employeeId;
        $hardSkill->fillHardSkillTitle($dto->hardSkillTitle);
        $hardSkill->save();

        return $hardSkill->refresh();
    }
}
