<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Skill\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetSkillsAttribute;
use Domains\Profile\V100\Core\Factories\HardSkillFactory;

final class SetHardSkill
{
    public function handle(GetSkillsAttribute $attribute, Closure $next): mixed
    {
        $attribute->hardSkill = HardSkillFactory::makeMany(
            type: $attribute->skillsDto->type,
            empId: $attribute->skillsDto->employeeId
        );

        return $next($attribute);
    }
}
