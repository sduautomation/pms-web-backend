<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Achievements\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetAchievementsAttribute;
use Domains\Profile\V100\Core\Factories\ProjectFactory;

final class SetProjects
{
    public function handle(GetAchievementsAttribute $attribute, Closure $next): mixed
    {
        $attribute->projects = ProjectFactory::makeProjects(
            type: $attribute->requestDto->type,
            empId: $attribute->requestDto->employeeId
        );

        return $next($attribute);
    }
}
