<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Achievements\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetAchievementsAttribute;
use Domains\Profile\V100\Cooker\DTO\AchievementsResponseDTO;

final class SetResponse
{
    public function handle(GetAchievementsAttribute $attribute, Closure $next): mixed
    {
        $attribute->responseDto = new AchievementsResponseDTO(
            workExperience: $attribute->workExperience,
            projects: $attribute->projects,
            awards: $attribute->awards,
        );

        return $next($attribute);
    }
}
