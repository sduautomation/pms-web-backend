<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Achievements\Create;

use Carbon\Carbon;
use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageAwardAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageAwardDTO;
use Domains\Profile\V100\Models\Award;

final class CreateAward
{
    public function handle(ManageAwardAttribute $attribute, Closure $next): mixed
    {
        $attribute->award = $this->createAward($attribute->manageAwardDTO);

        return $next($attribute);
    }

    public function createAward(ManageAwardDTO $dto): Award
    {
        $award = new Award();
        $award->emp_id = $dto->employeeId;
        $award->certified_date = Carbon::parse($dto->certifiedDate->toDateString());

        $award->fillCertificateNameId($dto->certificateNameId);
        $award->fillOrganizationNameId($dto->organizationNameId);

        $award->save();

        return $award->refresh();
    }
}
