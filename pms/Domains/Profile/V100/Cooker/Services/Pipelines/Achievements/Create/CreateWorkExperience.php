<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Achievements\Create;

use Carbon\Carbon;
use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageWorkExperienceAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageWorkExperienceDTO;
use Domains\Profile\V100\Models\WorkExperience;
use Support\Core\Traits\LanguageTrait;

final class CreateWorkExperience
{
    use LanguageTrait;

    public function handle(ManageWorkExperienceAttribute $attribute, Closure $next): mixed
    {
        $attribute->workExperience = $this->createWorkExperience($attribute->manageWorkExperienceDTO);

        return $next($attribute);
    }

    private function createWorkExperience(ManageWorkExperienceDTO $dto): WorkExperience
    {
        $workExperience = new WorkExperience();
        $workExperience->emp_id = $dto->employeeId;
        $workExperience->start_date = Carbon::parse($dto->startDate->toDateString());
        $workExperience->end_date = $dto->present === false ? Carbon::parse($dto->endDate->toDateString()) : null;
        $workExperience->address_id = $dto->addressId;

        $workExperience->fillJobTitle($dto->jobTitle);
        $workExperience->fillCompanyId($dto->companyId);

        $workExperience->save();

        return $workExperience->refresh();
    }
}
