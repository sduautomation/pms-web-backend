<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Achievements\Create;

use Carbon\Carbon;
use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageProjectAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageProjectDTO;
use Domains\Profile\V100\Models\Project;
use Support\Core\Traits\LanguageTrait;

final class CreateProject
{
    use LanguageTrait;

    public function handle(ManageProjectAttribute $attribute, Closure $next): mixed
    {
        $attribute->project = $this->createProject($attribute->manageProjectDTO);

        return $next($attribute);
    }

    private function createProject(ManageProjectDTO $dto): Project
    {
        $project = new Project();
        $project->emp_id = $dto->employeeId;
        $project->start_date = Carbon::parse($dto->startDate->toDateString());
        $project->end_date = $dto->ongoing === false ? Carbon::parse($dto->endDate->toDateString()) : null;

        $project->fillProjectName($dto->projectName);
        $project->fillPositionId($dto->positionId);
        $project->fillDonorId($dto->donorId);
        $project->fillNotes($dto->notes);

        $project->save();

        return $project->refresh();
    }
}
