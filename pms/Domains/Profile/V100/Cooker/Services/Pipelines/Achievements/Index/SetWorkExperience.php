<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Achievements\Index;

use Closure;
use Domains\Profile\V100\Cooker\Attributes\GetAchievementsAttribute;
use Domains\Profile\V100\Core\Factories\WorkExperienceFactory;

final class SetWorkExperience
{
    public function handle(GetAchievementsAttribute $attribute, Closure $next): mixed
    {
        $attribute->workExperience = WorkExperienceFactory::makeCollection(
            type: $attribute->requestDto->type,
            empId: $attribute->requestDto->employeeId
        );

        return $next($attribute);
    }
}
