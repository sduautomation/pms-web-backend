<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services\Pipelines\Achievements\Edit;

use Carbon\Carbon;
use Closure;
use Domains\Profile\V100\Cooker\Attributes\ManageWorkExperienceAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageWorkExperienceDTO;
use Domains\Profile\V100\Models\WorkExperience;

final class EditWorkExperience
{
    public function handle(ManageWorkExperienceAttribute $attribute, Closure $next): mixed
    {
        $attribute->workExperience = $this->editWorkExperience($attribute->workExperience, $attribute->manageWorkExperienceDTO);

        return $next($attribute);
    }

    public function editWorkExperience(WorkExperience $workExperience, ManageWorkExperienceDTO $dto): WorkExperience
    {
        $workExperience->emp_id = $dto->employeeId;
        $workExperience->start_date = Carbon::parse($dto->startDate->toDateString());
        $workExperience->end_date = $dto->present === false ? Carbon::parse($dto->endDate->toDateString()) : null;
        $workExperience->address_id = $dto->addressId;
        $workExperience->fillJobTitle($dto->jobTitle);
        $workExperience->fillCompanyId($dto->companyId);

        $workExperience->save();

        return $workExperience->refresh();
    }
}
