<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageEducationAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageEducationDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Education\ManageEducation;
use Domains\Profile\V100\Models\GraduatedUniversity;
use Domains\Profile\V100\Resources\EducationResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class ManageEducationPipelineService implements PipelineServiceInterface
{
    private ManageEducationAttribute $attribute;
    private GraduatedUniversity $education;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            ManageEducation::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageEducationDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageEducationAttribute();
        $attribute->dto = $data;
        $attribute->education = $this->education ?? new GraduatedUniversity();

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageEducationAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function setEducation(GraduatedUniversity $education): PipelineServiceInterface
    {
        $this->education = $education;

        return $this;
    }

    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new EducationResource($this->attribute->education);
    }
}
