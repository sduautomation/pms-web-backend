<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\GetContactsAttribute;
use Domains\Profile\V100\Cooker\DTO\GetContactsDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Contacts\Index\SetContacts;
use Domains\Profile\V100\Cooker\Services\Pipelines\Contacts\Index\SetResponse;
use Domains\Profile\V100\Resources\GetContactsResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class ContactsPipelineService implements PipelineServiceInterface
{
    private GetContactsAttribute $attribute;

    public function __construct(
        private PipelineService $service
    ) {
    }

    /**
     * @return string[]
     */
    public function loadPipes(): array
    {
        return [
            SetContacts::class,
            SetResponse::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param GetContactsDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new GetContactsAttribute();
        $attribute->requestDto = $data;

        return $this->setAttribute($attribute);
    }

    /**
     * @param GetContactsAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new GetContactsResource($this->attribute->responseDTO);
    }
}
