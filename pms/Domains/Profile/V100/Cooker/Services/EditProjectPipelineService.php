<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageProjectAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageProjectDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Achievements\Edit\EditProject;
use Domains\Profile\V100\Models\Project;
use Domains\Profile\V100\Resources\ProjectResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class EditProjectPipelineService implements PipelineServiceInterface
{
    private ManageProjectAttribute $attribute;
    private Project $project;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            EditProject::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageProjectDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageProjectAttribute();
        $attribute->manageProjectDTO = $data;
        $attribute->project = $this->project;

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageProjectAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function setProject(Project $project): PipelineServiceInterface
    {
        $this->project = $project;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new ProjectResource($this->attribute->project);
    }
}
