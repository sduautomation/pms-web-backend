<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageArticleAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageArticleDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Education\ManageArticle;
use Domains\Profile\V100\Models\Publication;
use Domains\Profile\V100\Resources\ArticleResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class ManageArticlePipelineService implements PipelineServiceInterface
{
    private ManageArticleAttribute $attribute;
    private Publication $article;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            ManageArticle::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageArticleDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageArticleAttribute();
        $attribute->dto = $data;
        $attribute->article = $this->article ?? new Publication();

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageArticleAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function setArticle(Publication $article): PipelineServiceInterface
    {
        $this->article = $article;

        return $this;
    }

    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new ArticleResource($this->attribute->article);
    }
}
