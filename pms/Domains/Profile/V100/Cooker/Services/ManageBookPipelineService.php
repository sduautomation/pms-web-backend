<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageBookAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageBookDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Education\ManageBook;
use Domains\Profile\V100\Models\Publication;
use Domains\Profile\V100\Resources\BookResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class ManageBookPipelineService implements PipelineServiceInterface
{
    private ManageBookAttribute $attribute;
    private Publication $book;

    public function __construct(
        private PipelineService $service
    ) {
    }

    /**
     * @return string[]
     */
    public function loadPipes(): array
    {
        return [
            ManageBook::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageBookDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageBookAttribute();
        $attribute->dto = $data;
        $attribute->book = $this->book ?? new Publication();

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageBookAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function setBook(Publication $book): PipelineServiceInterface
    {
        $this->book = $book;

        return $this;
    }

    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new BookResource($this->attribute->book);
    }
}
