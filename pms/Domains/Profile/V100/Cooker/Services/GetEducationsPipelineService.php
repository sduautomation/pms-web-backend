<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\GetEducationsAttribute;
use Domains\Profile\V100\Cooker\DTO\GetEducationsDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Education\Index\SetConductedCourses;
use Domains\Profile\V100\Cooker\Services\Pipelines\Education\Index\SetEducations;
use Domains\Profile\V100\Cooker\Services\Pipelines\Education\Index\SetPublications;
use Domains\Profile\V100\Cooker\Services\Pipelines\Education\Index\SetScientificOrganizations;
use Domains\Profile\V100\Cooker\Services\Pipelines\Education\Index\SetThesisSupervisions;
use Domains\Profile\V100\Resources\EducationsResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class GetEducationsPipelineService implements PipelineServiceInterface
{
    private GetEducationsAttribute $attribute;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            SetEducations::class,
            SetThesisSupervisions::class,
            SetConductedCourses::class,
            SetScientificOrganizations::class,
            SetPublications::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param GetEducationsDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new GetEducationsAttribute();
        $attribute->requestDto = $data;

        return $this->setAttribute($attribute);
    }

    /**
     * @param GetEducationsAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new EducationsResource($this->attribute->responseDto);
    }
}
