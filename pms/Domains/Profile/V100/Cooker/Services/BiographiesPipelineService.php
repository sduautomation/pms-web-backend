<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\GetBiographiesAttribute;
use Domains\Profile\V100\Cooker\DTO\GetBiographiesDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Biography\Index\SetBiography;
use Domains\Profile\V100\Cooker\Services\Pipelines\Biography\Index\SetChildren;
use Domains\Profile\V100\Cooker\Services\Pipelines\Biography\Index\SetResponse;
use Domains\Profile\V100\Resources\GetBiographiesResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class BiographiesPipelineService implements PipelineServiceInterface
{
    private GetBiographiesAttribute $attribute;

    public function __construct(
        private PipelineService $service
    ) {
    }

    /**
     * @return string[]
     */
    public function loadPipes(): array
    {
        return [
            SetChildren::class,
            SetBiography::class,
            SetResponse::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param GetBiographiesDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new GetBiographiesAttribute();
        $attribute->biographiesDto = $data;

        return $this->setAttribute($attribute);
    }

    /**
     * @param GetBiographiesAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new GetBiographiesResource($this->attribute->responseDto);
    }
}
