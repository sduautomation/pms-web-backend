<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageScientificOrganizationAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageScientificOrganizationDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Education\ManageScientificOrganization;
use Domains\Profile\V100\Models\Organization;
use Domains\Profile\V100\Resources\ScientificOrganizationResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class ManageScientificOrganizationPipelineService implements PipelineServiceInterface
{
    private ManageScientificOrganizationAttribute $attribute;
    private Organization $organization;

    public function __construct(
        private PipelineService $service
    ) {
    }

    /**
     * @return string[]
     */
    public function loadPipes(): array
    {
        return [
            ManageScientificOrganization::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageScientificOrganizationDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageScientificOrganizationAttribute();
        $attribute->dto = $data;
        $attribute->organization = $this->organization ?? new Organization();

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageScientificOrganizationAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function setOrganization(Organization $organization): PipelineServiceInterface
    {
        $this->organization = $organization;

        return $this;
    }

    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new ScientificOrganizationResource($this->attribute->organization);
    }
}
