<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageAwardAttribute;
use Domains\Profile\V100\Cooker\Services\Pipelines\Achievements\Edit\EditAward;
use Domains\Profile\V100\Models\Award;
use Domains\Profile\V100\Resources\AwardResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class EditAwardPipelineService implements PipelineServiceInterface
{
    private ManageAwardAttribute $attribute;
    private Award $award;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            EditAward::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageAwardAttribute();
        $attribute->manageAwardDTO = $data;
        $attribute->award = $this->award;

        return $this->setAttribute($attribute);
    }

    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function setAward(Award $award): PipelineServiceInterface
    {
        $this->award = $award;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new AwardResource($this->attribute->award);
    }
}
