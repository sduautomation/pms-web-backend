<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageLanguageAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageLanguageDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Skill\Edit\EditLanguageSkill;
use Domains\Profile\V100\Models\LanguageSkill;
use Domains\Profile\V100\Resources\ProjectResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class EditLanguageSkillPipelineService implements PipelineServiceInterface
{
    private ManageLanguageAttribute $attribute;
    private LanguageSkill $languageSkill;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            EditLanguageSkill::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageLanguageDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageLanguageAttribute();
        $attribute->manageLanguageDTO = $data;
        $attribute->languageSkill = $this->languageSkill;

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageLanguageAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function setLanguageSkill(LanguageSkill $languageSkill): PipelineServiceInterface
    {
        $this->languageSkill = $languageSkill;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new ProjectResource($this->attribute->languageSkill);
    }
}
