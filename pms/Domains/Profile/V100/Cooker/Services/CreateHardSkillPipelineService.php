<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageHardSkillAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageHardSkillDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Skill\Create\CreateHardSkill;
use Domains\Profile\V100\Resources\HardSkillResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class CreateHardSkillPipelineService implements PipelineServiceInterface
{
    private ManageHardSkillAttribute $attribute;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            CreateHardSkill::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageHardSkillDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageHardSkillAttribute();
        $attribute->manageHardSkillDTO = $data;

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageHardSkillAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new HardSkillResource($this->attribute->hardSkill);
    }
}
