<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageWorkExperienceAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageWorkExperienceDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Achievements\Edit\EditWorkExperience;
use Domains\Profile\V100\Models\WorkExperience;
use Domains\Profile\V100\Resources\WorkExperienceResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class EditWorkExperiencePipelineService implements PipelineServiceInterface
{
    private ManageWorkExperienceAttribute $attribute;
    private WorkExperience $workExperience;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            EditWorkExperience::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageWorkExperienceDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageWorkExperienceAttribute();
        $attribute->manageWorkExperienceDTO = $data;
        $attribute->workExperience = $this->workExperience;

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageWorkExperienceAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function setWorkExperience(WorkExperience $workExperience): PipelineServiceInterface
    {
        $this->workExperience = $workExperience;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new WorkExperienceResource($this->attribute->workExperience);
    }
}
