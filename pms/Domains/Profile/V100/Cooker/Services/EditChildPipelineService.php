<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageChildAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageChildDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Biography\Edit\EditChild;
use Domains\Profile\V100\Models\Child;
use Domains\Profile\V100\Resources\ChildResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class EditChildPipelineService implements PipelineServiceInterface
{
    private ManageChildAttribute $attribute;
    private Child $child;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            EditChild::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageChildDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageChildAttribute();
        $attribute->dto = $data;
        $attribute->child = $this->child;

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageChildAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function setChild(Child $child): PipelineServiceInterface
    {
        $this->child = $child;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new ChildResource($this->attribute->child);
    }
}
