<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageConferenceAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageConferenceDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Education\ManageConference;
use Domains\Profile\V100\Models\Publication;
use Domains\Profile\V100\Resources\ConferenceResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class ManageConferencePipelineService implements PipelineServiceInterface
{
    private ManageConferenceAttribute $attribute;
    private Publication $conference;

    public function __construct(
        private PipelineService $service
    ) {
    }

    /**
     * @return string[]
     */
    public function loadPipes(): array
    {
        return [
            ManageConference::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageConferenceDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageConferenceAttribute();
        $attribute->dto = $data;
        $attribute->conference = $this->conference ?? new Publication();

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageConferenceAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function setConference(Publication $conference): PipelineServiceInterface
    {
        $this->conference = $conference;

        return $this;
    }

    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new ConferenceResource($this->attribute->conference);
    }
}
