<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageBiographyAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageBiographyDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Biography\Create\CreateBiography;
use Domains\Profile\V100\Resources\BiographyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class CreateBiographyPipelineService implements PipelineServiceInterface
{
    private ManageBiographyAttribute $attribute;

    public function __construct(
        private PipelineService $service,
    ) {
    }

    public function loadPipes(): array
    {
        return [
            CreateBiography::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageBiographyDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageBiographyAttribute();
        $attribute->dto = $data;

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageBiographyAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new BiographyResource($this->attribute->biography);
    }
}
