<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageWorkExperienceAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageWorkExperienceDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Achievements\Create\CreateWorkExperience;
use Domains\Profile\V100\Resources\WorkExperienceResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class CreateWorkExperiencePipelineService implements PipelineServiceInterface
{
    private ManageWorkExperienceAttribute $attribute;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            CreateWorkExperience::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageWorkExperienceDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageWorkExperienceAttribute();
        $attribute->manageWorkExperienceDTO = $data;

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageWorkExperienceAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new WorkExperienceResource($this->attribute->workExperience);
    }
}
