<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageContactAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageContactDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Contacts\Edit\EditContact;
use Domains\Profile\V100\Models\Contact;
use Domains\Profile\V100\Resources\ContactResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class EditContactPipelineService implements PipelineServiceInterface
{
    private ManageContactAttribute $attribute;
    private Contact $contact;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            EditContact::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageContactDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageContactAttribute();
        $attribute->manageContactDTO = $data;
        $attribute->contact = $this->contact;

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageContactAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function setContact(Contact $contact): PipelineServiceInterface
    {
        $this->contact = $contact;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new ContactResource($this->attribute->contact);
    }
}
