<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageInterestAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageInterestDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Skill\Edit\EditInterest;
use Domains\Profile\V100\Models\Interest;
use Domains\Profile\V100\Resources\InterestResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class EditInterestPipelineService implements PipelineServiceInterface
{
    private ManageInterestAttribute $attribute;
    private Interest $interest;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            EditInterest::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageInterestDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageInterestAttribute();
        $attribute->manageInterestDTO = $data;
        $attribute->interest = $this->interest;

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageInterestAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function setInterest(Interest $interest): PipelineServiceInterface
    {
        $this->interest = $interest;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new InterestResource($this->attribute->interest);
    }
}
