<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageThesisSupervisionAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageThesisSupervisionDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Education\ManageThesisSupervision;
use Domains\Profile\V100\Models\Thesis;
use Domains\Profile\V100\Resources\ThesisSupervisionResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class ManageThesisSupervisionPipelineService implements PipelineServiceInterface
{
    private ManageThesisSupervisionAttribute $attribute;
    private Thesis $thesis;

    public function __construct(
        private PipelineService $service
    ) {
    }

    /**
     * @return string[]
     */
    public function loadPipes(): array
    {
        return [
            ManageThesisSupervision::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageThesisSupervisionDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageThesisSupervisionAttribute();
        $attribute->dto = $data;
        $attribute->thesis = $this->thesis ?? new Thesis();

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageThesisSupervisionAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function setThesis(Thesis $thesis): PipelineServiceInterface
    {
        $this->thesis = $thesis;

        return $this;
    }

    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new ThesisSupervisionResource($this->attribute->thesis);
    }
}
