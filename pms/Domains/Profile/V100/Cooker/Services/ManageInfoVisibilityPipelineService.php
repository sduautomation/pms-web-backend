<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageInfoVisibilityAttribute;
use Domains\Profile\V100\Cooker\Services\Pipelines\Settings\Manage\EditContacts;
use Domains\Profile\V100\Cooker\Services\Pipelines\Settings\Manage\ManageInfoVisibility;
use Domains\Profile\V100\Resources\ManageInfoVisibilityResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class ManageInfoVisibilityPipelineService implements PipelineServiceInterface
{
    private ManageInfoVisibilityAttribute $attribute;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            ManageInfoVisibility::class,
            EditContacts::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageInfoVisibilityAttribute();
        $attribute->manageInfoVisibilityDTO = $data;

        return $this->setAttribute($attribute);
    }

    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new ManageInfoVisibilityResource($this->attribute);
    }
}
