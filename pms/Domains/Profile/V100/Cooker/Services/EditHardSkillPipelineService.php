<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\ManageHardSkillAttribute;
use Domains\Profile\V100\Cooker\DTO\ManageHardSkillDTO;
use Domains\Profile\V100\Cooker\Services\Pipelines\Skill\Edit\EditHardSkill;
use Domains\Profile\V100\Models\HardSkill;
use Domains\Profile\V100\Resources\HardSkillResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class EditHardSkillPipelineService implements PipelineServiceInterface
{
    private ManageHardSkillAttribute $attribute;
    private HardSkill $hardSkill;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            EditHardSkill::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param ManageHardSkillDTO $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ManageHardSkillAttribute();
        $attribute->manageHardSkillDTO = $data;
        $attribute->hardSkill = $this->hardSkill;

        return $this->setAttribute($attribute);
    }

    /**
     * @param ManageHardSkillAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function setHardSkill(HardSkill $hardSkill): PipelineServiceInterface
    {
        $this->hardSkill = $hardSkill;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new HardSkillResource($this->attribute->hardSkill);
    }
}
