<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Cooker\Services;

use Domains\Profile\V100\Cooker\Attributes\GetInfoVisibilitiesAttribute;
use Domains\Profile\V100\Cooker\Services\Pipelines\Settings\Index\SetContacts;
use Domains\Profile\V100\Cooker\Services\Pipelines\Settings\Index\SetEmployeeInfoVisibilities;
use Domains\Profile\V100\Cooker\Services\Pipelines\Settings\Index\SetResponse;
use Domains\Profile\V100\Resources\GetInfoVisibilityResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Models\Employee;
use Support\Resources\BaseResource;

final class InfoVisibilitiesPipelineService implements PipelineServiceInterface
{
    private GetInfoVisibilitiesAttribute $attribute;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            SetEmployeeInfoVisibilities::class,
            SetContacts::class,
            SetResponse::class,
         ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param Employee $data
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new GetInfoVisibilitiesAttribute();
        $attribute->requestDto = $data;

        return $this->setAttribute($attribute);
    }

    /**
     * @param GetInfoVisibilitiesAttribute $attribute
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new GetInfoVisibilityResource($this->attribute->responseDTO);
    }
}
