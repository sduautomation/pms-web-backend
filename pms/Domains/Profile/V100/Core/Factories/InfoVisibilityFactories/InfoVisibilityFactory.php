<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Factories\InfoVisibilityFactories;

use Domains\Profile\V100\Core\Enums\InfoVisibilitiesTypes;
use Domains\Profile\V100\Models\InfoVisibility;
use Illuminate\Database\Eloquent\Collection;

final class InfoVisibilityFactory
{
    public static function makeMany(string $type, int $empId): Collection
    {
        return match ($type) {
            InfoVisibilitiesTypes::CONTACTS => Collection::make(),
            default => InfoVisibility::getInfoVisibilityByEmpId($empId),
        };
    }
}
