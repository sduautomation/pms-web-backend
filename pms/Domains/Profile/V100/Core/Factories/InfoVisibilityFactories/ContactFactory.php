<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Factories\InfoVisibilityFactories;

use Domains\Profile\V100\Core\Enums\InfoVisibilitiesTypes;
use Domains\Profile\V100\Models\Contact;
use Illuminate\Database\Eloquent\Collection;

final class ContactFactory
{
    public static function makeContacts(string $type, int $empId): Collection
    {
        return match ($type) {
            InfoVisibilitiesTypes::OTHERS => Collection::make(),
            default => Contact::getContactsByEmpId($empId),
        };
    }
}
