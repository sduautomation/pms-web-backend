<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Factories;

use Domains\Profile\V100\Core\Enums\EducationTypes;
use Domains\Profile\V100\Models\GraduatedUniversity;
use Illuminate\Database\Eloquent\Collection;

final class EducationFactory
{
    public static function makeMany(string $type, int $employeeId): Collection
    {
        return match ($type) {
            EducationTypes::EDUCATIONS, EducationTypes::ALL => GraduatedUniversity::byEmployee($employeeId)->get(),
            default => Collection::make()
        };
    }
}
