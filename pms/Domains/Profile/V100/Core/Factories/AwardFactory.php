<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Factories;

use Domains\Profile\V100\Core\Enums\AchievementTypes;
use Domains\Profile\v100\Models\Award;
use Illuminate\Database\Eloquent\Collection;

final class AwardFactory
{
    public static function makeAwards(string $type, int $empId): Collection
    {
        return match ($type) {
            AchievementTypes::EXPERIENCE, AchievementTypes::PROJECTS => Collection::make(),
            default => Award::getAwardsByEmpId($empId),
        };
    }
}
