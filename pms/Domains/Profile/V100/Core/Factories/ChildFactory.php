<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Factories;

use Domains\Profile\V100\Core\Enums\BiographyTypes;
use Domains\Profile\V100\Models\Child;
use Illuminate\Database\Eloquent\Collection;

final class ChildFactory
{
    public static function makeChildren(string $type): Collection
    {
        return match ($type) {
            BiographyTypes::BIOGRAPHY => Collection::make(),
            default => Child::all(),
        };
    }
}
