<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Factories;

use Domains\Profile\V100\Core\Enums\AchievementTypes;
use Domains\Profile\V100\Models\Project;
use Illuminate\Database\Eloquent\Collection;

final class ProjectFactory
{
    public static function makeProjects(string $type, int $empId): Collection
    {
        return match ($type) {
            AchievementTypes::EXPERIENCE, AchievementTypes::AWARDS => Collection::make(),
            default => Project::getProjectsByEmpId($empId),
        };
    }
}
