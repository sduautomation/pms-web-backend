<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Factories;

use Domains\Profile\V100\Core\Enums\EducationTypes;
use Domains\Profile\V100\Models\Organization;
use Illuminate\Database\Eloquent\Collection;

final class ScientificOrganizationFactory
{
    public static function makeMany(string $type, int $employeeId): Collection
    {
        return match ($type) {
            EducationTypes::SCIENTIFIC_ORGANIZATIONS, EducationTypes::ALL => Organization::byEmployee($employeeId)->get(),
            default => Collection::make()
        };
    }
}
