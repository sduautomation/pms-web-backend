<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Factories;

use Domains\Profile\V100\Core\Enums\BiographyTypes;
use Domains\Profile\V100\Models\Biography;
use Illuminate\Database\Eloquent\Collection;

final class BiographyFactory
{
    public static function makeBiographies(string $type): Collection
    {
        return match ($type) {
            BiographyTypes::CHILDREN => Collection::make(),
            default => Biography::all(),
        };
    }
}
