<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Factories;

use Domains\Profile\V100\Models\Contact;
use Illuminate\Database\Eloquent\Collection;

final class ContactFactory
{
    public static function makeContacts(int $empId): Collection
    {
        return Contact::getContactsByEmpId($empId);
    }
}
