<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Factories;

use Domains\Profile\V100\Core\Enums\EducationTypes;
use Domains\Profile\V100\Models\Thesis;
use Illuminate\Database\Eloquent\Collection;

final class ThesisSupervisionFactory
{
    public static function makeMany(string $type, int $employeeId): Collection
    {
        return match ($type) {
            EducationTypes::THESIS_SUPERVISIONS, EducationTypes::ALL => Thesis::byEmployee($employeeId)->get(),
            default => Collection::make()
        };
    }
}
