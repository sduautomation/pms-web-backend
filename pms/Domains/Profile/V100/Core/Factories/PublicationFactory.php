<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Factories;

use Domains\Profile\V100\Core\Enums\EducationTypes;
use Domains\Profile\V100\Models\Publication;
use Illuminate\Database\Eloquent\Collection;

final class PublicationFactory
{
    public static function makeMany(string $type, int $employeeId): Collection
    {
        return match ($type) {
            EducationTypes::ALL, EducationTypes::ARTICLES, EducationTypes::BOOKS, EducationTypes::CONFERENCES => Publication::byEmployee($employeeId)->get(),
            default => Collection::make()
        };
    }
}
