<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Factories;

use Domains\Profile\V100\Core\Enums\SkillTypes;
use Domains\Profile\V100\Models\Interest;
use Illuminate\Database\Eloquent\Collection;

final class InterestFactory
{
    public static function makeMany(string $type, int $empId): Collection
    {
        return match ($type) {
            SkillTypes::LANGUAGE, SkillTypes::HARDSKILL => Collection::make(),
            default => Interest::getInterestsByEmpId($empId),
        };
    }
}
