<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Factories;

use Domains\Profile\V100\Core\Enums\SkillTypes;
use Domains\Profile\V100\Models\LanguageSkill;
use Illuminate\Database\Eloquent\Collection;

final class LanguageSkillFactory
{
    public static function makeMany(string $type, int $empId): Collection
    {
        return match ($type) {
            SkillTypes::INTEREST, SkillTypes::HARDSKILL => Collection::make(),
            default => LanguageSkill::getLanguageSkillsByEmpId($empId),
        };
    }
}
