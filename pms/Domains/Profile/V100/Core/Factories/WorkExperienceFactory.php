<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Factories;

use Domains\Profile\V100\Core\Enums\AchievementTypes;
use Domains\Profile\V100\Models\WorkExperience;
use Illuminate\Database\Eloquent\Collection;

final class WorkExperienceFactory
{
    public static function makeCollection(string $type, int $empId): Collection
    {
        return match ($type) {
            AchievementTypes::PROJECTS, AchievementTypes::AWARDS => Collection::make(),
            default => WorkExperience::getWorkExperienceByEmpId($empId),
        };
    }
}
