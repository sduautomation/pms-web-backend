<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Factories;

use Domains\Profile\V100\Core\Enums\EducationTypes;
use Domains\Profile\V100\Models\Lecture;
use Illuminate\Database\Eloquent\Collection;

final class ConductedCourseFactory
{
    public static function makeMany(string $type, int $employeeId): Collection
    {
        return match ($type) {
            EducationTypes::CONDUCTED_COURSES, EducationTypes::ALL => Lecture::byEmployee($employeeId)->get(),
            default => Collection::make()
        };
    }
}
