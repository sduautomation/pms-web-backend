<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Enums;

final class SkillTypes
{
    public const ALL = 'all';
    public const LANGUAGE = 'language';
    public const HARDSKILL = 'hard_skill';
    public const INTEREST = 'interest';

    public const TYPES = [
        self::ALL,
        self::LANGUAGE,
        self::HARDSKILL,
        self::INTEREST,
    ];
}
