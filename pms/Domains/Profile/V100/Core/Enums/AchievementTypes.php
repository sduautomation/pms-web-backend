<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Enums;

final class AchievementTypes
{
    public const ALL = 'all';
    public const EXPERIENCE = 'experience';
    public const PROJECTS = 'projects';
    public const AWARDS = 'awards';

    public const TYPES = [
        self::ALL,
        self::EXPERIENCE,
        self::PROJECTS,
        self::AWARDS,
    ];
}
