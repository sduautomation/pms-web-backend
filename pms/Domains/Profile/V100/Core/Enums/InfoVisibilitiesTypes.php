<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Enums;

final class InfoVisibilitiesTypes
{
    public const ALL = 'all';
    public const CONTACTS = 'contacts';
    public const OTHERS = 'others';

    public const TYPES = [
        self::ALL,
        self::OTHERS,
    ];
}
