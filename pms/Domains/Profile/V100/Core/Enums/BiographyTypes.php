<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Enums;

final class BiographyTypes
{
    public const ALL = 'all';
    public const BIOGRAPHY = 'biography';
    public const CHILDREN = 'children';

    public const TYPES = [
        self::ALL,
        self::BIOGRAPHY,
        self::CHILDREN,
    ];
}
