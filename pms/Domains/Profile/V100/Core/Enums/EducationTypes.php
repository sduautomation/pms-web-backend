<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Enums;

final class EducationTypes
{
    public const ALL = 'all';
    public const EDUCATIONS = 'educations';
    public const THESIS_SUPERVISIONS = 'thesis_supervisions';
    public const CONDUCTED_COURSES = 'recently_conducted_courses';
    public const SCIENTIFIC_ORGANIZATIONS = 'scientific_organizations';
    public const ARTICLES = 'articles';
    public const BOOKS = 'books';
    public const CONFERENCES = 'conferences';
}
