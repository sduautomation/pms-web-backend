<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Traits;

use Domains\Profile\V100\Models\Biography;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;

/**
 * @mixin Biography
 * @mixin LanguageTrait
 */
trait BiographyFillPropertyTrait
{
    public function fillAutoBiography(string $autoBiography): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->auto_biog_az = $autoBiography;
                break;
            case Languages::TURKISH:
            case Languages::RUSSIAN:
                $this->auto_biog_tr = $autoBiography;
                break;
            default:
                $this->auto_biog_en = $autoBiography;
        }
    }
}
