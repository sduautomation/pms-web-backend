<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Traits;

use Domains\Profile\V100\Models\Organization;
use Support\Core\Enums\Languages;

/**
 * @mixin Organization
 */
trait OrganizationFillPropertyTrait
{
    public function fillOrganizationNameId(int $organizationNameId): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->org_name_az_id = $organizationNameId;
                break;
            case Languages::TURKISH:
            case Languages::RUSSIAN:
                $this->org_name_tr_id = $organizationNameId;
                break;
            default:
                $this->org_name_en_id = $organizationNameId;
        }
    }
}
