<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Traits;

use Domains\Profile\V100\Models\Award;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;

/**
 * @mixin Award
 * @mixin LanguageTrait
 */
trait AwardFillPropertyTrait
{
    public function fillCertificateNameId(int $certificateNameId): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->certificate_name_az_id = $certificateNameId;
                break;
            case Languages::RUSSIAN:
            case Languages::TURKISH:
                $this->certificate_name_tr_id = $certificateNameId;
                break;
            default:
                $this->certificate_name_en_id = $certificateNameId;
        }
    }

    public function fillOrganizationNameId(int $organizationNameId): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->org_name_az_id = $organizationNameId;
                break;
            case Languages::RUSSIAN:
            case Languages::TURKISH:
                $this->org_name_tr_id = $organizationNameId;
                break;
            default:
                $this->org_name_en_id = $organizationNameId;
        }
    }
}
