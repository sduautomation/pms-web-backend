<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Traits;

use Domains\Profile\V100\Models\GraduatedUniversity;
use Support\Core\Enums\Languages;

/**
 * @mixin GraduatedUniversity
 */
trait GraduatedUniversityFillPropertyTrait
{
    public function fillUniversity(string $university): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->uni_az = $university;
                break;
            case Languages::TURKISH:
            case Languages::RUSSIAN:
                $this->uni_tr = $university;
                break;
            default:
                $this->uni_en = $university;
        }
    }

    public function fillSpeciality(string $speciality): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->spec_az = $speciality;
                break;
            case Languages::TURKISH:
            case Languages::RUSSIAN:
                $this->spec_tr = $speciality;
                break;
            default:
                $this->spec_en = $speciality;
        }
    }
}
