<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Traits;

use Domains\Profile\V100\Models\Interest;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;

/**
 * @mixin Interest
 * @mixin LanguageTrait
 */
trait InterestFillPropertyTrait
{
    public function fillInterestTitle(string $title): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->research_areas_az = $title;
                break;
            case Languages::RUSSIAN:
            case Languages::TURKISH:
                $this->research_areas_tr = $title;
                break;
            default:
                $this->research_areas_en = $title;
        }
    }
}
