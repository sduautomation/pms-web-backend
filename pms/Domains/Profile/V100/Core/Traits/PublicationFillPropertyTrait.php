<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Traits;

use Domains\Profile\V100\Models\Publication;
use Support\Core\Enums\Languages;

/**
 * @mixin Publication
 */
trait PublicationFillPropertyTrait
{
    public function fillTitle(string $title): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->title_az = $title;
                break;
            case Languages::TURKISH:
            case Languages::RUSSIAN:
                $this->title_tr = $title;
                break;
            default:
                $this->title_en = $title;
        }
    }

    public function fillPublisher(string $publisher): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->publisher_az = $publisher;
                break;
            case Languages::TURKISH:
            case Languages::RUSSIAN:
                $this->publisher_tr = $publisher;
                break;
            default:
                $this->publisher_en = $publisher;
        }
    }

    public function fillAuthors(string $authors): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->authors_az = $authors;
                break;
            case Languages::TURKISH:
            case Languages::RUSSIAN:
                $this->authors_tr = $authors;
                break;
            default:
                $this->authors_en = $authors;
        }
    }

    public function fillJournalId(int $journalId): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->journal_az_id = $journalId;
                break;
            case Languages::TURKISH:
            case Languages::RUSSIAN:
                $this->journal_tr_id = $journalId;
                break;
            default:
                $this->journal_en_id = $journalId;
        }
    }

    public function fillConferenceId(int $conferenceId): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->conference_az_id = $conferenceId;
                break;
            case Languages::TURKISH:
            case Languages::RUSSIAN:
                $this->conference_tr_id = $conferenceId;
                break;
            default:
                $this->conference_en_id = $conferenceId;
        }
    }
}
