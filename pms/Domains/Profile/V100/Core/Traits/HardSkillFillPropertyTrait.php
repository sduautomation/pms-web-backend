<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Traits;

use Domains\Profile\V100\Models\HardSkill;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;

/**
 * @mixin HardSkill
 * @mixin LanguageTrait
 */
trait HardSkillFillPropertyTrait
{
    public function fillHardSkillTitle(string $title): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->desc_az = $title;
                break;
            case Languages::RUSSIAN:
            case Languages::TURKISH:
                $this->desc_tr = $title;
                break;
            default:
                $this->desc_en = $title;
        }
    }
}
