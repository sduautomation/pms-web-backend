<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Traits;

use Domains\Profile\V100\Models\Lecture;
use Support\Core\Enums\Languages;

/**
 * @mixin Lecture
 */
trait LectureFillPropertyTrait
{
    public function fillLectureName(string $lectureName): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->lecture_name_az = $lectureName;
                break;
            case Languages::TURKISH:
            case Languages::RUSSIAN:
                $this->lecture_name_tr = $lectureName;
                break;
            default:
                $this->lecture_name_en = $lectureName;
        }
    }
}
