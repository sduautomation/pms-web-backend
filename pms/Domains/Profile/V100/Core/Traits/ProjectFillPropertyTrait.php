<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Traits;

use Domains\Profile\V100\Models\Project;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;

/**
 * @mixin Project
 * @mixin LanguageTrait
 */
trait ProjectFillPropertyTrait
{
    public function fillProjectName(string $name): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->project_name_az = $name;
                break;
            case Languages::RUSSIAN:
            case Languages::TURKISH:
                $this->project_name_tr = $name;
                break;
            default:
                $this->project_name_en = $name;
        }
    }

    public function fillPositionId(int $positionId): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->position_az_id = $positionId;
                break;
            case Languages::RUSSIAN:
            case Languages::TURKISH:
                $this->position_tr_id = $positionId;
                break;
            default:
                $this->position_en_id = $positionId;
        }
    }

    public function fillNotes(?string $note): void
    {
        if ($note === null) {
            return;
        }

        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->notes_az = $note;
                break;
            case Languages::RUSSIAN:
            case Languages::TURKISH:
                $this->notes_tr = $note;
                break;
            default:
                $this->notes_en = $note;
        }
    }

    public function fillDonorId(int $donorId): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->donor_az_id = $donorId;
                break;
            case Languages::RUSSIAN:
            case Languages::TURKISH:
                $this->donor_tr_id = $donorId;
                break;
            default:
                $this->donor_en_id = $donorId;
        }
    }
}
