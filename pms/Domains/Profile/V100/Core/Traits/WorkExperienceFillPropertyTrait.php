<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Traits;

use Domains\Profile\V100\Models\WorkExperience;
use Support\Core\Enums\Languages;
use Support\Core\Traits\LanguageTrait;

/**
 * @mixin WorkExperience
 * @mixin LanguageTrait
 */
trait WorkExperienceFillPropertyTrait
{
    public function fillJobTitle(string $name): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->job_title_az = $name;
                break;
            case Languages::RUSSIAN:
            case Languages::TURKISH:
                $this->job_title_tr = $name;
                break;
            default:
                $this->job_title_en = $name;
        }
    }

    public function fillCompanyId(int $companyId): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->company_az_id = $companyId;
                break;
            case Languages::RUSSIAN:
            case Languages::TURKISH:
                $this->company_tr_id = $companyId;
                break;
            default:
                $this->company_en_id = $companyId;
        }
    }
}
