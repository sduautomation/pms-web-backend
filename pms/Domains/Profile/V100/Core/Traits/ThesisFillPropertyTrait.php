<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Core\Traits;

use Domains\Profile\V100\Models\Thesis;
use Support\Core\Enums\Languages;

/**
 * @mixin Thesis
 */
trait ThesisFillPropertyTrait
{
    public function fillTitle(string $title): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->title_az = $title;
                break;
            case Languages::TURKISH:
            case Languages::RUSSIAN:
                $this->title_tr = $title;
                break;
            default:
                $this->title_en = $title;
        }
    }

    public function fillUniversity(string $university): void
    {
        switch ($this->getLocale()) {
            case Languages::AZERBAIJAN:
            case Languages::KAZAKH:
                $this->uni_az = $university;
                break;
            case Languages::TURKISH:
            case Languages::RUSSIAN:
                $this->uni_tr = $university;
                break;
            default:
                $this->uni_en = $university;
        }
    }
}
