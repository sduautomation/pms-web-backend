<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Profile\V100\Cooker\Services\InfoVisibilitiesPipelineService;
use Domains\Profile\V100\Cooker\Services\ManageInfoVisibilityPipelineService;
use Domains\Profile\V100\Requests\GetInfoVisibilitiesRequest;
use Domains\Profile\V100\Requests\ManageInfoVisibilityRequest;
use Illuminate\Http\JsonResponse;

final class InfoVisibilitiesController extends Controller
{
    /**
     * @OA\Get(
     *     summary="Get employee info visibilites",
     *     path="/api/V100/profile/settings",
     *     operationId="index",
     *     tags={"profile", "V100"},
     *     description="Get all employee info visibilites",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "type", "in":"query", "type":"string", "required":true, "description":"Info visibility type"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Employee Info Visibilities are successfully returned",
     *          @OA\JsonContent(ref="#/components/schemas/GetInfoVisibilityResource"),
     *     )
     * )
     */
    public function index(GetInfoVisibilitiesRequest $request, InfoVisibilitiesPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Employee Info Visibilities are successfully returned',
            data: $resource
        );
    }

    /**
     * @OA\Post(
     *     summary="Profile - manage employee info visibility",
     *     path="/api/V100/profile/settings",
     *     operationId="manageEmployeeInfoVisibility",
     *     tags={"profile", "settings", "V100"},
     *     description="Profile - manage employee info visibility",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageInfoVisibilityRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Employee info visibility is successfully updated.",
     *          @OA\JsonContent(ref="#/components/schemas/InfoVisibilityResource")
     *     )
     * )
     */
    public function manage(ManageInfoVisibilityRequest $request, ManageInfoVisibilityPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Employee Info Visibilities are successfully updated',
            data: $resource
        );
    }
}
