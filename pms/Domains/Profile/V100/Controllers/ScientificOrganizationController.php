<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Profile\V100\Cooker\Services\ManageScientificOrganizationPipelineService;
use Domains\Profile\V100\Models\Organization;
use Domains\Profile\V100\Requests\ManageScientificOrganizationRequest;
use Domains\Profile\V100\Resources\ScientificOrganizationResource;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class ScientificOrganizationController extends Controller
{
    /**
     * @OA\Post(
     *     summary="Profile - create scientific organization",
     *     path="/api/v100/profile/educations/organizations",
     *     operationId="createScientificOrganization",
     *     tags={"profile", "V100"},
     *     description="Profile - create scientific organization",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageScientificOrganizationRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Scientific organization is successfully created.",
     *          @OA\JsonContent(ref="#/components/schemas/ScientificOrganizationResource")
     *     )
     * )
     */
    public function create(ManageScientificOrganizationRequest $request, ManageScientificOrganizationPipelineService $service): JsonResponse
    {
        /** @var ScientificOrganizationResource $resource */
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Scientific Organization is successfully created.',
            data: $resource,
            code: Response::HTTP_CREATED
        );
    }

    /**
     * @OA\Put(
     *     summary="Profile - edit scientific organization",
     *     path="/api/V100/profile/educations/organizations/{organization_id}",
     *     operationId="editScientificOrganization",
     *     tags={"profile", "V100"},
     *     description="Profile - edit scientific organization",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "organization_id", "in":"query", "type":"integer", "required":true, "description":"Organization ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageScientificOrganizationRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Scientific Organization is successfully updated.",
     *          @OA\JsonContent(ref="#/components/schemas/ScientificOrganizationResource")
     *     )
     * )
     */
    public function edit(Organization $organization, ManageScientificOrganizationRequest $request, ManageScientificOrganizationPipelineService $service): JsonResponse
    {
        /** @var ScientificOrganizationResource $resource */
        $resource = $service
            ->setOrganization($organization)
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Scientific Organization is successfully updated.',
            data: $resource
        );
    }

    /**
     * @OA\Delete(
     *     summary="Profile - delete scientific organization",
     *     path="/api/V100/profile/educations/organizations/{organization_id}",
     *     operationId="deleteScientificOrganization",
     *     tags={"profile", "V100"},
     *     description="Profile - delete scientific organization",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "organization_id", "in":"query", "type":"integer", "required":true, "description":"Organization ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Scientific Organization is successfully deleted.",
     *     )
     * )
     */
    public function delete(Organization $organization): JsonResponse
    {
        $organization->delete();

        return $this->response('Scientific Organization is successfully deleted.');
    }
}
