<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Profile\V100\Cooker\Services\ManageArticlePipelineService;
use Domains\Profile\V100\Models\Publication;
use Domains\Profile\V100\Requests\ManageArticleRequest;
use Domains\Profile\V100\Resources\ArticleResource;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class ArticleController extends Controller
{
    /**
     * @OA\Post(
     *     summary="Profile - create article",
     *     path="/api/v100/profile/educations/articles",
     *     operationId="createArticle",
     *     tags={"profile", "V100"},
     *     description="Profile - create article",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageArticleRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Article is successfully created.",
     *          @OA\JsonContent(ref="#/components/schemas/ArticleResource")
     *     )
     * )
     */
    public function create(ManageArticleRequest $request, ManageArticlePipelineService $service): JsonResponse
    {
        /** @var ArticleResource $resource */
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Article is successfully created.',
            data: $resource,
            code: Response::HTTP_CREATED
        );
    }

    /**
     * @OA\Put(
     *     summary="Profile - edit article",
     *     path="/api/V100/profile/educations/articles/{article_id}",
     *     operationId="editArticle",
     *     tags={"profile", "V100"},
     *     description="Profile - edit article",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "article_id", "in":"query", "type":"integer", "required":true, "description":"Article ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageArticleRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Article is successfully updated.",
     *          @OA\JsonContent(ref="#/components/schemas/ArticleResource")
     *     )
     * )
     */
    public function edit(Publication $article, ManageArticleRequest $request, ManageArticlePipelineService $service): JsonResponse
    {
        /** @var ArticleResource $resource */
        $resource = $service
            ->setArticle($article)
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Article is successfully updated.',
            data: $resource
        );
    }

    /**
     * @OA\Delete(
     *     summary="Profile - delete article",
     *     path="/api/V100/profile/educations/articles/{article_id}",
     *     operationId="deleteArticle",
     *     tags={"profile", "V100"},
     *     description="Profile - delete article",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "article_id", "in":"query", "type":"integer", "required":true, "description":"Article ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Article is successfully deleted.",
     *     )
     * )
     */
    public function delete(Publication $article): JsonResponse
    {
        $article->delete();

        return $this->response('Article is successfully deleted.');
    }
}
