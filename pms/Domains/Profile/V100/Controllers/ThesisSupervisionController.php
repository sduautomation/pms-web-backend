<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Profile\V100\Cooker\Services\ManageThesisSupervisionPipelineService;
use Domains\Profile\V100\Models\Thesis;
use Domains\Profile\V100\Requests\ManageThesisSupervisionRequest;
use Domains\Profile\V100\Resources\ThesisSupervisionResource;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class ThesisSupervisionController extends Controller
{
    /**
     * @OA\Post(
     *     summary="Profile - create thesis supervision",
     *     path="/api/v100/profile/educations/thesis",
     *     operationId="createThesisSupervision",
     *     tags={"profile", "V100"},
     *     description="Profile - create thesis supervision",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageThesisSupervisionRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Thesis supervision is successfully created.",
     *          @OA\JsonContent(ref="#/components/schemas/ThesisSupervisionResource")
     *     )
     * )
     */
    public function create(ManageThesisSupervisionRequest $request, ManageThesisSupervisionPipelineService $service): JsonResponse
    {
        /** @var ThesisSupervisionResource $resource */
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Thesis supervision is successfully created.',
            data: $resource,
            code: Response::HTTP_CREATED
        );
    }

    /**
     * @OA\Put(
     *     summary="Profile - edit thesis supervision",
     *     path="/api/V100/profile/educations/thesis/{thesis_id}",
     *     operationId="editThesisSupervision",
     *     tags={"profile", "V100"},
     *     description="Profile - edit thesis supervision",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "thesis_id", "in":"query", "type":"integer", "required":true, "description":"Thesis ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageThesisSupervisionRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Thesis supervision is successfully updated.",
     *          @OA\JsonContent(ref="#/components/schemas/ThesisSupervisionResource")
     *     )
     * )
     */
    public function edit(Thesis $thesis, ManageThesisSupervisionRequest $request, ManageThesisSupervisionPipelineService $service): JsonResponse
    {
        /** @var ThesisSupervisionResource $resource */
        $resource = $service
            ->setThesis($thesis)
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Thesis supervision is successfully updated.',
            data: $resource
        );
    }

    /**
     * @OA\Delete(
     *     summary="Profile - delete thesis supervision",
     *     path="/api/V100/profile/educations/thesis/{thesis_id}",
     *     operationId="deleteThesisSupervision",
     *     tags={"profile", "V100"},
     *     description="Profile - delete thesis supervision",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "thesis_id", "in":"query", "type":"integer", "required":true, "description":"Thesis ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Thesis supervision is successfully deleted.",
     *     )
     * )
     */
    public function delete(Thesis $thesis): JsonResponse
    {
        $thesis->delete();

        return $this->response('Thesis supervision is successfully deleted.');
    }
}
