<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Profile\V100\Cooker\Services\ManageBookPipelineService;
use Domains\Profile\V100\Models\Publication;
use Domains\Profile\V100\Requests\ManageBookRequest;
use Domains\Profile\V100\Resources\BookResource;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class BookController extends Controller
{
    /**
     * @OA\Post(
     *     summary="Profile - create book",
     *     path="/api/v100/profile/educations/books",
     *     operationId="createBook",
     *     tags={"profile", "V100"},
     *     description="Profile - create book",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageBookRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Book is successfully created.",
     *          @OA\JsonContent(ref="#/components/schemas/BookResource")
     *     )
     * )
     */
    public function create(ManageBookRequest $request, ManageBookPipelineService $service): JsonResponse
    {
        /** @var BookResource $resource */
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Book is successfully created.',
            data: $resource,
            code: Response::HTTP_CREATED
        );
    }

    /**
     * @OA\Put(
     *     summary="Profile - edit book",
     *     path="/api/V100/profile/educations/books/{book_id}",
     *     operationId="editBook",
     *     tags={"profile", "V100"},
     *     description="Profile - edit book",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "book_id", "in":"query", "type":"integer", "required":true, "description":"Book ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageBookRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Book is successfully updated.",
     *          @OA\JsonContent(ref="#/components/schemas/BookResource")
     *     )
     * )
     */
    public function edit(Publication $book, ManageBookRequest $request, ManageBookPipelineService $service): JsonResponse
    {
        /** @var BookResource $resource */
        $resource = $service
            ->setBook($book)
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Book is successfully updated.',
            data: $resource
        );
    }

    /**
     * @OA\Delete(
     *     summary="Profile - delete book",
     *     path="/api/V100/profile/educations/books/{book_id}",
     *     operationId="deleteBook",
     *     tags={"profile", "V100"},
     *     description="Profile - delete book",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "book_id", "in":"query", "type":"integer", "required":true, "description":"Book ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Book is successfully deleted.",
     *     )
     * )
     */
    public function delete(Publication $book): JsonResponse
    {
        $book->delete();

        return $this->response('Book is successfully deleted.');
    }
}
