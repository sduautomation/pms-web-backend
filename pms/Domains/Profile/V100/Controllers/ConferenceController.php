<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Profile\V100\Cooker\Services\ManageConferencePipelineService;
use Domains\Profile\V100\Models\Publication;
use Domains\Profile\V100\Requests\ManageConferenceRequest;
use Domains\Profile\V100\Resources\ConferenceResource;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class ConferenceController extends Controller
{
    /**
     * @OA\Post(
     *     summary="Profile - create conference",
     *     path="/api/v100/profile/educations/conferences",
     *     operationId="createConference",
     *     tags={"profile", "V100"},
     *     description="Profile - create conference",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageConferenceRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Conference is successfully created.",
     *          @OA\JsonContent(ref="#/components/schemas/ConferenceResource")
     *     )
     * )
     */
    public function create(ManageConferenceRequest $request, ManageConferencePipelineService $service): JsonResponse
    {
        /** @var ConferenceResource $resource */
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Conference is successfully created.',
            data: $resource,
            code: Response::HTTP_CREATED
        );
    }

    /**
     * @OA\Put(
     *     summary="Profile - edit conference",
     *     path="/api/V100/profile/educations/conferences/{conference_id}",
     *     operationId="editConference",
     *     tags={"profile", "V100"},
     *     description="Profile - edit conference",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "conference_id", "in":"query", "type":"integer", "required":true, "description":"Conference ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageConferenceRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Conference is successfully updated.",
     *          @OA\JsonContent(ref="#/components/schemas/ConferenceResource")
     *     )
     * )
     */
    public function edit(Publication $conference, ManageConferenceRequest $request, ManageConferencePipelineService $service): JsonResponse
    {
        /** @var ConferenceResource $resource */
        $resource = $service
            ->setConference($conference)
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Conference is successfully updated.',
            data: $resource
        );
    }

    /**
     * @OA\Delete(
     *     summary="Profile - delete conference",
     *     path="/api/V100/profile/educations/conferences/{conference_id}",
     *     operationId="deleteConference",
     *     tags={"profile", "V100"},
     *     description="Profile - delete conference",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "conference_id", "in":"query", "type":"integer", "required":true, "description":"Conference ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Conference is successfully deleted.",
     *     )
     * )
     */
    public function delete(Publication $conference): JsonResponse
    {
        $conference->delete();

        return $this->response('Conference is successfully deleted.');
    }
}
