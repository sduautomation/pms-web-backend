<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Profile\V100\Cooker\Services\BiographiesPipelineService;
use Domains\Profile\V100\Cooker\Services\CreateBiographyPipelineService;
use Domains\Profile\V100\Cooker\Services\CreateChildPipelineService;
use Domains\Profile\V100\Cooker\Services\EditBiographyPipelineService;
use Domains\Profile\V100\Cooker\Services\EditChildPipelineService;
use Domains\Profile\V100\Models\Biography;
use Domains\Profile\V100\Models\Child;
use Domains\Profile\V100\Requests\GetBiographiesRequest;
use Domains\Profile\V100\Requests\ManageBiographyRequest;
use Domains\Profile\V100\Requests\ManageChildRequest;
use Domains\Profile\V100\Resources\BiographyResource;
use Domains\Profile\V100\Resources\ChildResource;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class BiographyController extends Controller
{
    /**
     * @OA\Get(
     *     summary="Get biographies",
     *     path="/api/v100/profile/biographies",
     *     operationId="index",
     *     tags={"profile", "v100"},
     *     description="Get all biographies",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "type", "in":"query", "type":"string", "required":true, "description":"Biography type"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Biographies are successfully returned",
     *          @OA\JsonContent(ref="#/components/schemas/GetBiographiesResource"),
     *     ),
     * )
     */
    public function index(GetBiographiesRequest $request, BiographiesPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Biographies are successfully returned',
            data: $resource
        );
    }

    /**
     * @OA\Post(
     *     summary="Profile - create biography",
     *     path="/api/v100/profile/biographies",
     *     operationId="createBiography",
     *     tags={"profile", "biography", "v100"},
     *     description="Profile - create biography",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageBiographyRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Biography is successfully created.",
     *          @OA\JsonContent(ref="#/components/schemas/BiographyResource"),
     *     ),
     * )
     */
    public function createBiography(ManageBiographyRequest $request, CreateBiographyPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Biography is successfully created.',
            data: $resource,
            code: Response::HTTP_CREATED
        );
    }

    /**
     * @OA\Put(
     *     summary="Profile - edit biography",
     *     path="/api/v100/profile/biographies/{employee_id}",
     *     operationId="editBiography",
     *     tags={"profile", "biography", "v100"},
     *     description="Profile - edit biography",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "employee_id", "in":"query", "type":"string", "required":true, "description":"Employee ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageBiographyRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Biography is successfully updated.",
     *          @OA\JsonContent(ref="#/components/schemas/BiographyResource"),
     *     ),
     * )
     */
    public function editBiography(Biography $biography, ManageBiographyRequest $request, EditBiographyPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setBiography($biography)
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Biography is successfully updated.',
            data: $resource
        );
    }

    /**
     * @OA\Delete(
     *     summary="Profile - delete biography",
     *     path="/api/v100/profile/biographies/{employee_id}",
     *     operationId="deleteBiography",
     *     tags={"profile", "biography", "v100"},
     *     description="Profile - delete biography",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "employee_id", "in":"query", "type":"string", "required":true, "description":"Employee ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Biography is successfully deleted."
     *     )
     * )
     */
    public function deleteBiography(Biography $biography): JsonResponse
    {
        $biography->delete();

        return $this->response(
            message: 'Biography is successfully deleted.'
        );
    }

    /**
     * @OA\Get(
     *     summary="Profile - show biography",
     *     path="/api/v100/profile/biographies/{employee_id}",
     *     operationId="showBiography",
     *     tags={"profile", "biography", "v100"},
     *     description="Profile - show biography",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "employee_id", "in":"query", "type":"string", "required":true, "description":"Employee ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Biography is successfully returned.",
     *          @OA\JsonContent(ref="#/components/schemas/BiographyResource"),
     *     )
     * )
     */
    public function showBiography(Biography $biography): JsonResponse
    {
        return $this->response(
            message: 'Biography is successfully returned.',
            data: new BiographyResource($biography),
        );
    }

    /**
     * @OA\Post(
     *     summary="Profile - create child",
     *     path="/api/v100/profile/children",
     *     operationId="createChild",
     *     tags={"profile", "biography", "v100"},
     *     description="Profile - create child",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageChildRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Child is successfully created.",
     *          @OA\JsonContent(ref="#/components/schemas/ChildResource"),
     *     ),
     * )
     */
    public function createChild(ManageChildRequest $request, CreateChildPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Child is successfully created.',
            data: $resource,
            code: Response::HTTP_CREATED
        );
    }

    /**
     * @OA\Put(
     *     summary="Profile - edit child",
     *     path="/api/v100/profile/children/{child_id}",
     *     operationId="editChild",
     *     tags={"profile", "biography", "v100"},
     *     description="Profile - edit child",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "child_id", "in":"query", "type":"string", "required":true, "description":"Child ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageChildRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Child is successfully updated.",
     *          @OA\JsonContent(ref="#/components/schemas/ChildResource"),
     *     ),
     * )
     */
    public function editChild(Child $child, ManageChildRequest $request, EditChildPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setChild($child)
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Child is successfully updated.',
            data: $resource
        );
    }

    /**
     * @OA\Delete(
     *     summary="Profile - delete child",
     *     path="/api/v100/profile/children/{child_id}",
     *     operationId="deleteChild",
     *     tags={"profile", "biography", "v100"},
     *     description="Profile - delete child",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "child_id", "in":"query", "type":"string", "required":true, "description":"Child ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Child is successfully deleted."
     *     )
     * )
     */
    public function deleteChild(Child $child): JsonResponse
    {
        $child->delete();

        return $this->response(
            message: 'Child is successfully deleted.'
        );
    }

    /**
     * @OA\Get(
     *     summary="Profile - show child",
     *     path="/api/v100/profile/children/{child_id}",
     *     operationId="showChild",
     *     tags={"profile", "biography", "v100"},
     *     description="Profile - show child",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "child_id", "in":"query", "type":"string", "required":true, "description":"Child ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Child is successfully returned.",
     *          @OA\JsonContent(ref="#/components/schemas/ChildResource"),
     *     )
     * )
     */
    public function showChild(Child $child): JsonResponse
    {
        return $this->response(
            message: 'Child is successfully returned.',
            data: new ChildResource($child)
        );
    }
}
