<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Profile\V100\Cooker\Services\GetEducationsPipelineService;
use Domains\Profile\V100\Cooker\Services\ManageEducationPipelineService;
use Domains\Profile\V100\Models\GraduatedUniversity;
use Domains\Profile\V100\Requests\GetEducationsRequest;
use Domains\Profile\V100\Requests\ManageEducationRequest;
use Domains\Profile\V100\Resources\EducationResource;
use Domains\Profile\V100\Resources\EducationsResource;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class EducationController extends Controller
{
    /**
     * @OA\Get(
     *     summary="Get educations",
     *     path="/api/V100/profile/educations",
     *     operationId="getEducations",
     *     tags={"profile", "V100"},
     *     description="Get educations",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "type", "in":"query", "type":"string", "required":true, "description":"Education type"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Educations are successfully returned.",
     *          @OA\JsonContent(ref="#/components/schemas/EducationsResource"),
     *     )
     * )
     */
    public function index(GetEducationsRequest $request, GetEducationsPipelineService $service): JsonResponse
    {
        /** @var EducationsResource $resource */
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Educations are successfully returned.',
            data: $resource
        );
    }

    /**
     * @OA\Post(
     *     summary="Profile - create education",
     *     path="/api/v100/profile/educations",
     *     operationId="createEducation",
     *     tags={"profile", "V100"},
     *     description="Profile - create education",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageEducationRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Education is successfully created.",
     *          @OA\JsonContent(ref="#/components/schemas/EducationResource")
     *     )
     * )
     */
    public function create(ManageEducationRequest $request, ManageEducationPipelineService $service): JsonResponse
    {
        /** @var EducationResource $resource */
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Education is successfully created.',
            data: $resource,
            code: Response::HTTP_CREATED
        );
    }

    /**
     * @OA\Put(
     *     summary="Profile - edit education",
     *     path="/api/V100/profile/educations/{education_id}",
     *     operationId="editEducation",
     *     tags={"profile", "V100"},
     *     description="Profile - edit education",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "education_id", "in":"query", "type":"integer", "required":true, "description":"Education ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageEducationRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Conducted course is successfully updated.",
     *          @OA\JsonContent(ref="#/components/schemas/EducationResource")
     *     )
     * )
     */
    public function edit(GraduatedUniversity $education, ManageEducationRequest $request, ManageEducationPipelineService $service): JsonResponse
    {
        /** @var EducationResource $resource */
        $resource = $service
            ->setEducation($education)
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Education is successfully updated.',
            data: $resource
        );
    }

    /**
     * @OA\Delete(
     *     summary="Profile - delete education",
     *     path="/api/V100/profile/educations/{education_id}",
     *     operationId="deleteEducation",
     *     tags={"profile", "V100"},
     *     description="Profile - delete education",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "education_id", "in":"query", "type":"integer", "required":true, "description":"Education ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Education is successfully deleted.",
     *     )
     * )
     */
    public function delete(GraduatedUniversity $education): JsonResponse
    {
        $education->delete();

        return $this->response('Education is successfully deleted.');
    }
}
