<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Profile\V100\Cooker\Services\AchievementsPipelineService;
use Domains\Profile\V100\Cooker\Services\CreateAwardPipelineService;
use Domains\Profile\V100\Cooker\Services\CreateProjectPipelineService;
use Domains\Profile\V100\Cooker\Services\CreateWorkExperiencePipelineService;
use Domains\Profile\V100\Cooker\Services\EditAwardPipelineService;
use Domains\Profile\V100\Cooker\Services\EditProjectPipelineService;
use Domains\Profile\V100\Cooker\Services\EditWorkExperiencePipelineService;
use Domains\Profile\V100\Models\Award;
use Domains\Profile\V100\Models\Project;
use Domains\Profile\V100\Models\WorkExperience;
use Domains\Profile\V100\Requests\GetAchievementsRequest;
use Domains\Profile\V100\Requests\ManageAwardRequest;
use Domains\Profile\V100\Requests\ManageProjectRequest;
use Domains\Profile\V100\Requests\ManageWorkExperienceRequest;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class AchievementsController extends Controller
{
    /**
     * @OA\Get(
     *     summary="Get achievements",
     *     path="/api/V100/profile/achievements",
     *     operationId="index",
     *     tags={"profile", "V100"},
     *     description="Get all achievements",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "type", "in":"query", "type":"string", "required":true, "description":"Achievement type"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Achievements are successfully returned",
     *          @OA\JsonContent(ref="#/components/schemas/GetAchievementsResource"),
     *     )
     * )
     */
    public function index(GetAchievementsRequest $request, AchievementsPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Achievements are successfully returned',
            data: $resource
        );
    }

    /**
     * @OA\Post(
     *     summary="Profile - create project",
     *     path="/api/V100/profile/achievements/projects",
     *     operationId="createProject",
     *     tags={"profile", "achievements", "V100"},
     *     description="Profile - create project",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageProjectRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Project is successfully created.",
     *          @OA\JsonContent(ref="#/components/schemas/ProjectResource")
     *     )
     * )
     */
    public function createProject(ManageProjectRequest $request, CreateProjectPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Project is successfully created.',
            data: $resource,
            code: Response::HTTP_CREATED
        );
    }

    /**
     * @OA\Put(
     *     summary="Profile - edit project",
     *     path="/api/V100/profile/achievements/projects/{project_id}",
     *     operationId="editProject",
     *     tags={"profile", "achievements", "V100"},
     *     description="Profile - edit project",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "project_id", "in":"query", "type":"string", "required":true, "description":"Project ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageProjectRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Project is successfully updated.",
     *          @OA\JsonContent(ref="#/components/schemas/ProjectResource")
     *     )
     * )
     */
    public function editProject(Project $project, ManageProjectRequest $request, EditProjectPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setProject($project)
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Project is successfully updated.',
            data: $resource
        );
    }

    /**
     * @OA\Delete(
     *     summary="Profile - delete project",
     *     path="/api/V100/profile/achievements/projects/{project_id}",
     *     operationId="deleteProject",
     *     tags={"profile", "achievements", "V100"},
     *     description="Profile - delete project",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "project_id", "in":"query", "type":"string", "required":true, "description":"Project ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Project is successfully deleted.",
     *          @OA\JsonContent(ref="#/components/schemas/ProjectResource")
     *     )
     * )
     */
    public function deleteProject(Project $project): JsonResponse
    {
        $project->delete();

        return $this->response(
            message: 'Project is successfully deleted.'
        );
    }

    /**
     * @OA\Post(
     *     summary="Profile - create work experience",
     *     path="/api/V100/profile/achievements/experience",
     *     operationId="createWorkExperience",
     *     tags={"profile", "achievements", "V100"},
     *     description="Profile - create work experience",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageWorkExperienceRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Work experience is successfully created.",
     *          @OA\JsonContent(ref="#/components/schemas/WorkExperienceResource")
     *     )
     * )
     */
    public function createWorkExperience(ManageWorkExperienceRequest $request, CreateWorkExperiencePipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Work experience is successfully created',
            data: $resource,
            code: Response::HTTP_CREATED
        );
    }

    /**
     * @OA\Put(
     *     summary="Profile - edit work experience",
     *     path="/api/V100/profile/achievements/experience/{ejob_id}",
     *     operationId="editWorkExperience",
     *     tags={"profile", "achievements", "V100"},
     *     description="Profile - edit work experience",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "ejob_id", "in":"query", "type":"string", "required":true, "description":"Work experience ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageWorkExperienceRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Work experience is successfully updated.",
     *          @OA\JsonContent(ref="#/components/schemas/WorkExperienceResource")
     *     )
     * )
     */
    public function editWorkExperience(WorkExperience $workExperience, ManageWorkExperienceRequest $request, EditWorkExperiencePipelineService $service): JsonResponse
    {
        $resource = $service
            ->setWorkExperience($workExperience)
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Work experience is successfully updated.',
            data: $resource
        );
    }

    /**
     * @OA\Delete(
     *     summary="Profile - delete work experience",
     *     path="/api/V100/profile/achievements/experience/{ejob_id}",
     *     operationId="deleteWorkExperience",
     *     tags={"profile", "achievements", "V100"},
     *     description="Profile - delete work experience",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "ejob_id", "in":"query", "type":"string", "required":true, "description":"Work experience ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Work experience is successfully deleted.",
     *          @OA\JsonContent(ref="#/components/schemas/WorkExperienceResource")
     *     )
     * )
     */
    public function deleteWorkExperience(WorkExperience $workExperience): JsonResponse
    {
        $workExperience->delete();

        return $this->response(
            message: 'Work experience is successfully deleted.'
        );
    }

    /**
     * @OA\Post(
     *     summary="Profile - create award",
     *     path="/api/V100/profile/achievements/awards",
     *     operationId="createAward",
     *     tags={"profile", "achievements", "V100"},
     *     description="Profile - create award",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageAwardRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Award is successfully created.",
     *          @OA\JsonContent(ref="#/components/schemas/AwardResource")
     *     )
     * )
     */
    public function createAward(ManageAwardRequest $request, CreateAwardPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Award is successfully created.',
            data: $resource,
            code: Response::HTTP_CREATED
        );
    }

    /**
     * @OA\Put(
     *     summary="Profile - edit award",
     *     path="/api/V100/profile/achievements/awards/{cid}",
     *     operationId="editAward",
     *     tags={"profile", "achievements", "V100"},
     *     description="Profile - edit award",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "cid", "in":"query", "type":"string", "required":true, "description":"Award ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageAwardRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Award is successfully updated.",
     *          @OA\JsonContent(ref="#/components/schemas/AwardResource")
     *     )
     * )
     */
    public function editAward(Award $award, ManageAwardRequest $request, EditAwardPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAward($award)
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Award is successfully updated.',
            data: $resource
        );
    }

    /**
     * @OA\Delete(
     *     summary="Profile - delete award",
     *     path="/api/V100/profile/achievements/awards/{cid}",
     *     operationId="deleteAward",
     *     tags={"profile", "achievements", "V100"},
     *     description="Profile - delete award",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "cid", "in":"query", "type":"string", "required":true, "description":"Award ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Award is successfully deleted.",
     *          @OA\JsonContent(ref="#/components/schemas/AwardResource")
     *     )
     * )
     */
    public function deleteAward(Award $award): JsonResponse
    {
        $award->delete();

        return $this->response(
            message: 'Award is successfully deleted.'
        );
    }
}
