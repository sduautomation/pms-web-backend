<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Profile\V100\Cooker\Services\ManageConductedCoursePipelineService;
use Domains\Profile\V100\Models\Lecture;
use Domains\Profile\V100\Requests\ManageConductedCourseRequest;
use Domains\Profile\V100\Resources\ConductedCourseResource;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class ConductedCourseController extends Controller
{
    /**
     * @OA\Post(
     *     summary="Profile - create conducted course",
     *     path="/api/v100/profile/educations/courses",
     *     operationId="createConductedCourse",
     *     tags={"profile", "V100"},
     *     description="Profile - create conducted course",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageConductedCourseRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Conducted course is successfully created.",
     *          @OA\JsonContent(ref="#/components/schemas/ConductedCourseResource")
     *     )
     * )
     */
    public function create(ManageConductedCourseRequest $request, ManageConductedCoursePipelineService $service): JsonResponse
    {
        /** @var ConductedCourseResource $resource */
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Conducted course is successfully created.',
            data: $resource,
            code: Response::HTTP_CREATED
        );
    }

    /**
     * @OA\Put(
     *     summary="Profile - edit conducted course",
     *     path="/api/V100/profile/educations/courses/{lecture_id}",
     *     operationId="editConductedCourse",
     *     tags={"profile", "V100"},
     *     description="Profile - edit conducted course",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "lecture_id", "in":"query", "type":"integer", "required":true, "description":"Lecture ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageConductedCourseRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Conducted course is successfully updated.",
     *          @OA\JsonContent(ref="#/components/schemas/ConductedCourseResource")
     *     )
     * )
     */
    public function edit(Lecture $lecture, ManageConductedCourseRequest $request, ManageConductedCoursePipelineService $service): JsonResponse
    {
        /** @var ConductedCourseResource $resource */
        $resource = $service
            ->setLecture($lecture)
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Conducted course is successfully updated.',
            data: $resource
        );
    }

    /**
     * @OA\Delete(
     *     summary="Profile - delete conducted course",
     *     path="/api/V100/profile/educations/courses/{lecture_id}",
     *     operationId="deleteConductedCourse",
     *     tags={"profile", "V100"},
     *     description="Profile - delete conducted course",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "lecture_id", "in":"query", "type":"integer", "required":true, "description":"Lecture ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Conducted course is successfully deleted.",
     *     )
     * )
     */
    public function delete(Lecture $lecture): JsonResponse
    {
        $lecture->delete();

        return $this->response('Conducted course is successfully deleted.');
    }
}
