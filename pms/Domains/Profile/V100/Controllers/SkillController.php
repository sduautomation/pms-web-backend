<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Profile\V100\Cooker\Services\CreateHardSkillPipelineService;
use Domains\Profile\V100\Cooker\Services\CreateLanguagePipelineService;
use Domains\Profile\V100\Cooker\Services\EditHardSkillPipelineService;
use Domains\Profile\V100\Cooker\Services\EditInterestPipelineService;
use Domains\Profile\V100\Cooker\Services\EditLanguageSkillPipelineService;
use Domains\Profile\V100\Cooker\Services\SkillsPipelineService;
use Domains\Profile\V100\Models\HardSkill;
use Domains\Profile\V100\Models\Interest;
use Domains\Profile\V100\Models\LanguageSkill;
use Domains\Profile\V100\Requests\GetSkillsRequest;
use Domains\Profile\V100\Requests\ManageHardSkillRequest;
use Domains\Profile\V100\Requests\ManageInterestRequest;
use Domains\Profile\V100\Requests\ManageLanguageRequest;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class SkillController extends Controller
{
    /**
     * @OA\Get(
     *     summary="Get skills",
     *     path="/api/v100/profile/skills",
     *     operationId="index",
     *     tags={"profile", "v100"},
     *     description="Get all skills",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "type", "in":"query", "type":"string", "required":true, "description":"Skill type"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Skills are successfully returned",
     *          @OA\JsonContent(ref="#/components/schemas/GetSkillsResource"),
     *     ),
     * )
     */
    public function index(GetSkillsRequest $request, SkillsPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Skills are successfully returned',
            data: $resource
        );
    }

    /**
     * @OA\Post(
     *     summary="Profile - create language skill",
     *     path="/api/v100/profile/skills/language",
     *     operationId="createLanguageSkill",
     *     tags={"profile", "skills", "V100"},
     *     description="Profile - create language",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageLanguageRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Language is successfully created.",
     *          @OA\JsonContent(ref="#/components/schemas/LanguageSkillResource")
     *     )
     * )
     */
    public function createLanguageSkill(ManageLanguageRequest $request, CreateLanguagePipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Language is successfully created.',
            data: $resource,
            code: Response::HTTP_CREATED
        );
    }

    /**
     * @OA\Put(
     *     summary="Profile - edit language skill",
     *     path="/api/V100/profile/skills/language/{lang_k_id}",
     *     operationId="editLanguageSkill",
     *     tags={"profile", "skills", "V100"},
     *     description="Profile - edit language skills",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "lang_k_id", "in":"query", "type":"string", "required":true, "description":"Language Skill ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageLanguageRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Language skill is successfully updated.",
     *          @OA\JsonContent(ref="#/components/schemas/LanguageSkillResource")
     *     )
     * )
     */
    public function editLanguageSkill(LanguageSkill $languageSkill, ManageLanguageRequest $request, EditLanguageSkillPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setLanguageSkill($languageSkill)
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Language skill is successfully updated.',
            data: $resource
        );
    }

    /**
     * @OA\Delete(
     *     summary="Profile - delete language skill",
     *     path="/api/V100/profile/skills/language/{lang_k_id}",
     *     operationId="deleteLanguageSkill",
     *     tags={"profile", "skills", "V100"},
     *     description="Profile - delete language skill",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "lang_k_id", "in":"query", "type":"string", "required":true, "description":"Language Skill ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Language skill is successfully deleted.",
     *          @OA\JsonContent(ref="#/components/schemas/LanguageSkillResource")
     *     )
     * )
     */
    public function deleteLanguageSkill(LanguageSkill $languageSkill): JsonResponse
    {
        $languageSkill->delete();

        return $this->response(
            message: 'Language skill is successfully deleted.'
        );
    }

    /**
     * @OA\Post(
     *     summary="Profile - create hard skill",
     *     path="/api/v100/profile/skills/hardSkill",
     *     operationId="createHardSkill",
     *     tags={"profile", "skills", "V100"},
     *     description="Profile - create hard skill",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageHardSkillRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Hard skill is successfully created.",
     *          @OA\JsonContent(ref="#/components/schemas/HardSkillResource")
     *     )
     * )
     */
    public function createHardSkill(ManageHardSkillRequest $request, CreateHardSkillPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Hard skill is successfully created.',
            data: $resource,
            code: Response::HTTP_CREATED
        );
    }

    /**
     * @OA\Put(
     *     summary="Profile - edit hard skill",
     *     path="/api/V100/profile/skills/hardSkills/{skill_id}",
     *     operationId="editHardSkill",
     *     tags={"profile", "skills", "V100"},
     *     description="Profile - edit hard skills",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "lang_k_id", "in":"query", "type":"string", "required":true, "description":"Hard Skill ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageHardSkillRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Hard skill is successfully updated.",
     *          @OA\JsonContent(ref="#/components/schemas/HardSkillResource")
     *     )
     * )
     */
    public function editHardSkill(HardSkill $hardSkill, ManageHardSkillRequest $request, EditHardSkillPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setHardSkill($hardSkill)
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Hard skill is successfully updated.',
            data: $resource
        );
    }

    /**
     * @OA\Delete(
     *     summary="Profile - delete hard skill",
     *     path="/api/V100/profile/skills/hardSkills/{skill}",
     *     operationId="deleteHardSkill",
     *     tags={"profile", "skills", "V100"},
     *     description="Profile - delete hard skill",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "skill_id", "in":"query", "type":"string", "required":true, "description":"Hard Skill ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Hard skill is successfully deleted.",
     *          @OA\JsonContent(ref="#/components/schemas/HardSkillResource")
     *     )
     * )
     */
    public function deleteHardSkill(HardSkill $hardSkill): JsonResponse
    {
        $hardSkill->delete();

        return $this->response(
            message: 'Hard skill is successfully deleted.'
        );
    }

    /**
     * @OA\Put(
     *     summary="Profile - edit interest, no create, no delete",
     *     path="/api/V100/profile/skills/interests/{emp_id}",
     *     operationId="editInterest",
     *     tags={"profile", "skills", "V100"},
     *     description="Profile - edit interest",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "emp_id", "in":"query", "type":"string", "required":true, "description":"Employee ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageInterestRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Interest is successfully updated.",
     *          @OA\JsonContent(ref="#/components/schemas/InterestResource")
     *     )
     * )
     */
    public function editInterest(Interest $interest, ManageInterestRequest $request, EditInterestPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setInterest($interest)
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Interest is successfully updated.',
            data: $resource
        );
    }
}
