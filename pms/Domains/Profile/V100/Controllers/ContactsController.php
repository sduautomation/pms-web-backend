<?php

declare(strict_types=1);

namespace Domains\Profile\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Profile\V100\Cooker\Services\ContactsPipelineService;
use Domains\Profile\V100\Cooker\Services\CreateContactPipelineService;
use Domains\Profile\V100\Cooker\Services\EditContactPipelineService;
use Domains\Profile\V100\Models\Contact;
use Domains\Profile\V100\Requests\GetContactsRequest;
use Domains\Profile\V100\Requests\ManageContactRequest;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class ContactsController extends Controller
{
    /**
     * @OA\Get(
     *     summary="Get contacts",
     *     path="/api/V100/profile/contacts",
     *     operationId="index",
     *     tags={"profile", "V100"},
     *     description="Get all contacts",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Contacts are successfully returned",
     *          @OA\JsonContent(ref="#/components/schemas/GetContactsResource"),
     *     )
     * )
     */
    public function index(GetContactsRequest $request, ContactsPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Contacts are successfully returned',
            data: $resource
        );
    }

    /**
     * @OA\Post(
     *     summary="Profile - create contacts",
     *     path="/api/V100/profile/contacts",
     *     operationId="createContact",
     *     tags={"profile", "contacts", "V100"},
     *     description="Profile - create contact",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageContactRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Contact is successfully created.",
     *          @OA\JsonContent(ref="#/components/schemas/ContactResource")
     *     )
     * )
     */
    public function create(ManageContactRequest $request, CreateContactPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Contacts are successfully created',
            data: $resource,
            code: Response::HTTP_CREATED
        );
    }

    /**
     * @OA\Put(
     *     summary="Profile - edit contact",
     *     path="/api/V100/profile/contacts/{contact_id}",
     *     operationId="editContact",
     *     tags={"profile", "contacts", "V100"},
     *     description="Profile - edit contact",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "contact_id", "in":"query", "type":"string", "required":true, "description":"Contact ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ManageContactRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="Contact is successfully updated.",
     *          @OA\JsonContent(ref="#/components/schemas/ContactResource")
     *     )
     * )
     */
    public function edit(Contact $contact, ManageContactRequest $request, EditContactPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setContact($contact)
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Contact is successfully updated.',
            data: $resource
        );
    }

    /**
     * @OA\Delete(
     *     summary="Profile - delete contact",
     *     path="/api/V100/profile/contacts/{contact_id}",
     *     operationId="deleteAward",
     *     tags={"profile", "contacts", "V100"},
     *     description="Profile - delete contact",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"},
     *      {"name": "contact_id", "in":"query", "type":"string", "required":true, "description":"Contact ID"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Contact is successfully deleted.",
     *          @OA\JsonContent(ref="#/components/schemas/ContactResource")
     *     )
     * )
     */
    public function delete(Contact $contact): JsonResponse
    {
        $contact->delete();

        return $this->response(
            message: 'Contact is successfully deleted'
        );
    }
}
