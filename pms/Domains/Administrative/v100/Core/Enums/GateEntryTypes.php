<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Core\Enums;

final class GateEntryTypes
{
    public const TOURNIQUET = 1;
    public const TECHNOPARK = 3;
    public const SERVER_ROOM = 4;
    public const DORM_BOYS = 5;
    public const DORM_GIRLS = 6;
    public const MEETING_ROOM_ENG = 7;
}
