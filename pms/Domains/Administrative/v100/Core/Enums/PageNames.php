<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Core\Enums;

final class PageNames
{
    public const STAFF_LIST = 'staff-list';
    public const GATE_ENTRY = 'gate-entry';

    public const PAGES = [self::STAFF_LIST, self::GATE_ENTRY];
}
