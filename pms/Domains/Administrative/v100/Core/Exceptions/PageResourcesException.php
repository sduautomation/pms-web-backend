<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Core\Exceptions;

use Support\Core\Exceptions\JsonApi\CommonException;

/**
 * Class PageResourceException.
 */
final class PageResourcesException extends CommonException
{
    /**
     * @param string $page
     * @return static
     */
    public static function incorrectPageName(string $page): self
    {
        return new self(":$page: is not found.");
    }
}
