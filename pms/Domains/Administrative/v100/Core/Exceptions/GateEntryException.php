<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Core\Exceptions;

use Support\Core\Enums\ErrorCodes;
use Support\Core\Exceptions\JsonApi\CommonException;

/**
 * Class GateEntryException.
 */
final class GateEntryException extends CommonException
{
    /**
     * @param string $parameter
     * @return static
     */
    public static function parameterRequired(string $parameter): self
    {
        return new self("Parameter :$parameter: is required.");
    }

    /**
     * @param string $userType
     * @return static
     */
    public static function incorrectUserType(string $userType): self
    {
        return new self("Incorrect user type :$userType:");
    }

    /**
     * @param string $userType
     * @return static
     */
    public static function userNotFound(string $userType): self
    {
        return new self("$userType is not found", ErrorCodes::NOT_FOUND);
    }
}
