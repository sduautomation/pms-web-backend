<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Models\Scopes;

use Domains\Administrative\v100\Cooker\Collections\StaffListCollection;
use Domains\Administrative\v100\Cooker\DTO\StaffListRequestDTO;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

trait StaffListScopes
{
    public static function getStaffListRecords(StaffListRequestDTO $dto): StaffListCollection
    {
        $query = DB::table('employee as e')
            ->leftJoin('employee_info ei', function ($join) {
                $join->on([['ei.emp_id', '=', 'e.emp_id']]);
            })
            ->select(
                'e.hname as emp_id',
                DB::raw("e.name || ' ' || e.sname as fullname"),
                DB::raw("e.name_native || ' ' || e.surname_native as fullname_native"),
                DB::raw("(select
                        listagg(decode(eg.esas_gorev,1, '' || d.title_EN || ' - (' || g.gorev_ad_EN || ')' || '', d.title_EN || ' - (' || g.gorev_ad_EN || ')'), ', ')
                    from emp_gorev eg
                        left outer join dep_gorev dg on dg.dep_gorev_id = eg.dep_gorev_id
                        left outer join gorev g on g.gorev_id = dg.gorev_id
                        left outer join departments d on d.dep_code = dg.dep_code and d.son = 1 where eg.emp_id = e.emp_id and eg.state = e.state
                ) as department_position"),
                DB::raw('(select st.title_EN from state st where st.state_id = e.state) as WORKING_STATUS'),
                DB::raw('(select stat.title_EN from stat where stat.stat_id = e.wage_rate) as STAFF'),
                'ei.outer_email',
                'ei.inner_telephone'
            )
            ->where('e.state', '>', 0);

        self::checkIfNameExists($dto, $query);
        self::checkIfSurnameExists($dto, $query);
        self::checkIfDepartmentExists($dto, $query);
        self::checkIfPositionExists($dto, $query);

        return StaffListCollection::make($query->get());
    }

    private static function checkIfNameExists(StaffListRequestDTO $dto, Builder $query): void
    {
        if (! empty($dto->name)) {
            $query->whereRaw(
                "(upper(e.name) like upper(concat(?, '%')) or upper(e.name_native) like upper(concat(?, '%')))",
                [$dto->name, $dto->name]
            );
        }
    }

    private static function checkIfSurnameExists(StaffListRequestDTO $dto, Builder $query): void
    {
        if (! empty($dto->surname)) {
            $query->whereRaw(
                "(upper(e.sname) like upper(concat(?, '%')) or upper(e.surname_native) like upper(concat(?, '%')))",
                [$dto->surname, $dto->surname]
            );
        }
    }

    private static function checkIfDepartmentExists(StaffListRequestDTO $dto, Builder $query): void
    {
        if (! empty($dto->departmentCode) && $dto->includeSubDepartments) {
            $query->whereRaw(
                'exists(
                select
                    *
                from
                    emp_gorev eg
                left outer join
                    dep_gorev dg on
                        dg.dep_gorev_id = eg.dep_gorev_id
                where
                    eg.emp_id = e.emp_id and
                    eg.state = e.state and
                    exists(
                        select
                            d.dep_code
                        from
                            departments d
                        where
                            d.son = 1 and
                            d.dep_code = dg.dep_code
                            start with d.dep_code like ?
                            connect by prior d.dep_id = d.p_id
                    )
            )',
                [$dto->departmentCode]
            );
        }

        if (! empty($dto->departmentCode) && ! $dto->includeSubDepartments) {
            $query->whereRaw(
                'exists(
                    select
                        *
                    from
                        emp_gorev eg
                    left outer join
                        dep_gorev dg on
                            dg.dep_gorev_id = eg.dep_gorev_id
                    where
                        eg.emp_id = e.emp_id and
                        eg.state = e.state and
                        dg.dep_code like ?
                )',
                [$dto->departmentCode]
            );
        }
    }

    private static function checkIfPositionExists(StaffListRequestDTO $dto, Builder $query): void
    {
        if ($dto->positionId > 0) {
            $query->whereRaw(
                'exists (
                    select
                        *
                    from
                        emp_gorev eg
                    left outer join
                        dep_gorev dg on
                            dg.dep_gorev_id = eg.dep_gorev_id
                    where
                        eg.emp_id = e.emp_id and
                        eg.state = e.state and
                        dg.gorev_id = ?
                )',
                [$dto->positionId]
            );
        }
    }
}
