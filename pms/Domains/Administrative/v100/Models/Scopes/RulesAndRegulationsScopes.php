<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Models\Scopes;

use Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations\RulesAndRegulationsAttachmentsCollection;
use Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations\RulesAndRegulationsCategoriesCollection;
use Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations\RulesAndRegulationsContentsCollection;
use Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations\RulesAndRegulationsRecordsCollection;
use Illuminate\Support\Facades\DB;

trait RulesAndRegulationsScopes
{
    public static function getRulesAndRegulationsDocuments(?string $uuid): RulesAndRegulationsRecordsCollection
    {
        $records = $uuid == null
            ? DB::connection('sqlite')->select(
                'select
                    d.long_id as uuid,
                    d.title,
                    d.description,
                    dc.title as category_name,
                    d.meta,
                    d.created_at,
                    d.updated_at
                from documents d
                inner join document_categories dc on d.category_id = dc.id'
            )

            : DB::connection('sqlite')->select(
                'select
                    d.long_id as uuid,
                    d.title,
                    d.description,
                    dc.title as category_name,
                    d.meta,
                    d.created_at,
                    d.updated_at
                from documents d
                inner join document_categories dc on d.category_id = dc.id
                where d.long_id = :uuid',
                ['uuid' => $uuid]
            );

        return RulesAndRegulationsRecordsCollection::make($records);
    }

    public static function getRulesAndRegulationsCategories(): RulesAndRegulationsCategoriesCollection
    {
        $records = DB::connection('sqlite')->select(
            'select
                        long_id as uuid,
                        title,
                        meta,
                        created_at,
                        updated_at
                    from document_categories'
        );

        return RulesAndRegulationsCategoriesCollection::make($records);
    }

    public static function getRulesAndRegulationsAttachments(string $uuid): RulesAndRegulationsAttachmentsCollection
    {
        $records = DB::connection('sqlite')->select(
            'select da.long_id,
                   da.doc_id,
                   da.language_id,
                   da.version_id,
                   l.long as language_title,
                   r.version as version_title,
                   da.title,
                   da.path,
                   da.extension,
                   da.active,
                   da.created_at,
                   da.updated_at
            from document_attachments da
            inner join documents d on d.id = da.doc_id
            inner join languages l on da.language_id = l.id
            inner join releases r on da.version_id = r.id
            where d.long_id = :uuid',
            ['uuid' => $uuid]
        );

        return RulesAndRegulationsAttachmentsCollection::make($records);
    }

    public static function getRulesAndRegulationsContents(string $uuid): RulesAndRegulationsContentsCollection
    {
        $records = DB::connection('sqlite')->select(
            'select
                    dc.long_id,
                    dc.doc_id,
                    dc.language_id,
                    dc.version_id,
                    l.long as language_title,
                    r.version as version_title,
                    dc.image,
                    dc.content,
                    dc.active,
                    dc.created_at,
                    dc.updated_at
                from document_contents dc
                inner join languages l on dc.language_id = l.id
                inner join releases r on dc.version_id = r.id
                inner join documents d on dc.doc_id = d.id
                where d.long_id = :uuid',
            ['uuid' => $uuid]
        );

        return RulesAndRegulationsContentsCollection::make($records);
    }

    public static function insertRulesAndRegulationsDocument(object $data): int
    {
        DB::connection('sqlite')->insert(
            "insert into documents(long_id, category_id, title, description, meta, label, created_at)
                    values ((select
                            lower(hex(randomblob(4)) || '-' || hex(randomblob(2))
                            || '-' || '4' || substr(hex(randomblob(2)), 2) || '-'
                            || substr('AB89', 1 + (abs(random()) % 4) , 1)  || substr(hex(randomblob(2)), 2) || '-' || hex(randomblob(4)))),
                    (select dc.id
                     from document_categories dc
                     where dc.long_id = :categoryUuid
                    ), :title, :description, :meta, :label, strftime('%Y-%m-%d %H:%M:%S', datetime('now')))",
            ['categoryUuid' => $data->categoryUuid,
                    'title' => $data->title,
                    'description' => $data->description,
                    'meta' => $data->meta,
                    'label' => $data->label, ]
        );

        $documentId = DB::connection('sqlite')->select(
            'select id from documents where title = :title',
            ['title' => $data->title]
        );

        return (int) $documentId[0]->id;
    }

    public static function insertRulesAndRegulationsContent(object $data, int $doc_id): void
    {
        DB::connection('sqlite')->insert(
            "insert into document_contents(long_id, doc_id, language_id, version_id, image, content, active, created_at)
                    values ((select
                            lower(hex( randomblob(4)) || '-' || hex(randomblob(2))
                            || '-' || '4' || substr( hex(randomblob(2)), 2) || '-'
                            || substr('AB89', 1 + (abs(random()) % 4) , 1)  || substr(hex(randomblob(2)), 2) || '-' || hex(randomblob(4)))),
                            :doc_id,
                            (select languages.id from languages where languages.long_id = :languageUuid),
                            (select releases.id from releases where releases.long_id = :versionUuid),
                            :image, :content, :active, strftime('%Y-%m-%d %H:%M:%S', datetime('now')))",
            ['doc_id' => $doc_id,
                'languageUuid' => $data->languageUuid,
                'versionUuid' => $data->versionUuid,
                'image' => $data->image,
                'content' => $data->content,
                'active' => $data->active ? 1 : 0, ]
        );
    }

    public static function insertRulesAndRegulationsAttachment(object $data, int $doc_id): void
    {
        DB::connection('sqlite')->insert(
            "insert into document_attachments(long_id, doc_id, language_id, version_id, title, extension, path, active, created_at)
                    values ((select
                            lower(hex( randomblob(4)) || '-' || hex( randomblob(2))
                            || '-' || '4' || substr( hex( randomblob(2)), 2) || '-'
                            || substr('AB89', 1 + (abs(random()) % 4) , 1)  || substr(hex(randomblob(2)), 2) || '-' || hex(randomblob(4)))),
                            :doc_id,
                            (select l.id from languages l where l.long_id = :languageUuid),
                            (select r.id from releases r where r.long_id = :versionUuid),
                            :title, :extension,
                            'files/rules-and-regulations/documents/attachments/' || (select d.title from documents d where d.id = :doc_id) || '/' || :title,
                            :active, strftime('%Y-%m-%d %H:%M:%S', datetime('now')))",
            ['doc_id' => $doc_id,
                'languageUuid' => $data->languageUuid,
                'versionUuid' => $data->versionUuid,
                'title' => $data->title,
                'extension' => $data->extension,
                'active' => $data->active ? 1 : 0, ]
        );
    }

    public static function deleteRulesAndRegulationsDocument(string $uuid): void
    {
        $doc_id = DB::connection('sqlite')->select(
            'select id from documents where long_id = :uuid',
            ['uuid' => $uuid]
        )[0]->id;

        DB::connection('sqlite')->delete(
            'delete from document_contents where doc_id = :doc_id',
            ['doc_id' => $doc_id]
        );

        DB::connection('sqlite')->delete(
            'delete from document_attachments where doc_id = :doc_id',
            ['doc_id' => $doc_id]
        );

        DB::connection('sqlite')->delete(
            'delete from documents where long_id = :uuid',
            ['uuid' => $uuid]
        );
    }
}
