<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Models\Scopes;

use Domains\Administrative\v100\Cooker\Collections\SystemCalendar\SystemCalendarRecordsAssessmentsCollection;
use Domains\Administrative\v100\Cooker\Collections\SystemCalendar\SystemCalendarRecordsEventsCollection;
use Domains\Administrative\v100\Cooker\Collections\SystemCalendar\SystemCalendarRecordsPermissionsCollection;
use Domains\Administrative\v100\Cooker\Collections\SystemCalendar\SystemCalendarTermsCollection;
use Illuminate\Support\Facades\DB;

trait SystemCalendarRecordsScopes
{
    public static function getSystemCalendarTerms(): SystemCalendarTermsCollection
    {
        $records = DB::select(
            'select
                    yc.YEAR_CONF_ID as id,
                    yc.year as year,
                    yc.term as term,
                    yc.title_en as title
                    from YEAR_CONF yc
                    where exists(select ac.event_key from academic_calendar ac where ac.event_year = yc.year and ac.event_sem = yc.term)
                    order by yc.year desc, yc.term desc'
        );

        return SystemCalendarTermsCollection::make($records);
    }

    public static function getSystemCalendarEventsByDefault(): SystemCalendarRecordsEventsCollection
    {
        $records = DB::select(
            'select ac.event_year as year,
                    ac.event_sem as term,
                    ae.title_en as title,
                    ac.event_start as start_date,
                    ac.event_stop as end_date,
                       decode(ac.event_year||ac.event_sem, c.year||c.term, ae.is_active, 0) as status
                    from ACADEMIC_CALENDAR ac
                         left outer join academic_event ae on ae.key = ac.event_key
                         left outer join cfg_active_period c on c.cfg_key = ae.cfg_period_key
                    where ae.is_visible = 1 and ac.event_stop > sysdate
                    order by ac.event_year, ac.event_sem, ac.event_start'
        );

        return SystemCalendarRecordsEventsCollection::make($records);
    }

    public static function getSystemCalendarEvents(int $year, int $term): SystemCalendarRecordsEventsCollection
    {
        $records = DB::select(
            '
            select ac.event_year as year,
                   ac.event_sem as term,
                   ae.title_en as title,
                   ac.event_start as start_date,
                   ac.event_stop as end_date,
                   decode(ac.event_year||ac.event_sem, c.year||c.term, ae.is_active, 0) as status
            from ACADEMIC_CALENDAR ac
                left outer join academic_event ae on ae.key = ac.event_key
                left outer join cfg_active_period c on c.cfg_key = ae.cfg_period_key
            where ac.event_year = :year and ac.event_sem = :term and ae.is_visible = 1
            order by ac.event_start',
            ['year' => $year, 'term' => $term]
        );

        return SystemCalendarRecordsEventsCollection::make($records);
    }

    public static function getSystemCalendarAssessmentsByDefault(int $emp_id): SystemCalendarRecordsAssessmentsCollection
    {
        $records = DB::select(
            "select st.ders_kod,
                   st.year as year,
                   st.term as term,
                   st.section,
                   st.title as title,
                   to_char(nvl(sa.open_date, ac.event_start), 'DD.MM.YYYY HH24:MI') as start_date,
                   to_char(sa.close_date, 'DD.MM.YYYY HH24:MI') as end_date,
                   case
                       when sysdate between ac.event_start and sa.close_date and
                            sa.is_open = 1 then
                           1
                       else
                           0
                       end as status
                    from SOBE_ASSESSMENTS sa
                             left outer join sobe_assess_types st
                                             on st.sat_id = sa.sat_id
                             left outer join academic_calendar ac
                                             on ac.event_key = st.at_code
                                                 and ac.event_year = st.year
                                                 and ac.event_sem = st.term
                             left outer join ders_sobe ds
                                             on ds.ders_kod = st.ders_kod
                                                 and ds.year = st.year
                                                 and ds.term = st.term
                                                 and ds.section = st.section
                    where sa.close_date > sysdate
                      and ds.emp_id_ent = :empID
                    order by sa.close_date",
            ['empID' => $emp_id]
        );

        return SystemCalendarRecordsAssessmentsCollection::make($records);
    }

    public static function getSystemCalendarAssessments(int $year, int $term, int $empId): SystemCalendarRecordsAssessmentsCollection
    {
        $records = DB::select(
            "select st.ders_kod,
                    st.year as year,
                    st.term as term,
                    st.section,
                    st.title as title,
                    to_char(nvl(sa.open_date, ac.event_start), 'DD.MM.YYYY HH24:MI') as start_date,
                    to_char(sa.close_date, 'DD.MM.YYYY HH24:MI') as end_date,
                   case
                       when sysdate between ac.event_start and sa.close_date and sa.is_open = 1
                           then 1
                       else 0
                       end as status
                    from SOBE_ASSESSMENTS sa
                        left outer join sobe_assess_types st on st.sat_id = sa.sat_id
                        left outer join academic_calendar ac on ac.event_key = st.at_code
                                         and ac.event_year = st.year
                                         and ac.event_sem = st.term
                        left outer join ders_sobe ds on ds.ders_kod = st.ders_kod
                                         and ds.year = st.year
                                         and ds.term = st.term
                                         and ds.section = st.section
                    where sa.close_date > sysdate and st.year = :year and  st.term = :term and ds.emp_id_ent = :empID
                    order by sa.close_date",
            ['year' => $year, 'term' => $term, 'empID' => $empId]
        );

        return SystemCalendarRecordsAssessmentsCollection::make($records);
    }

    public static function getSystemCalendarPermissionsByDefault(int $emp_id): SystemCalendarRecordsPermissionsCollection
    {
        $records = DB::select(
            "select ae.title_en as title,
                   ax.key,
                   ax.year as year,
                   ax.term as term,
                   ax.start_date as start_date,
                   ax.end_date as end_date,
                   ax.entry_year,
                   ae.is_blocking,
                   ax.emp_id,
                   ax.prog_code,
                   ax.dep_code,
                   ax.class as classof,
                   ax.edu_level,
                   case
                       when (sysdate between ax.start_date and ax.end_date) and
                            ax.is_active = 1 then
                           1
                       else
                           0
                       end as status
                    from ACADEMIC_EVENT_EX ax
                             left outer join academic_event ae
                                             on ae.key = ax.key
                    where (ax.emp_id = :empID or (ax.stud_id is NULL and ax.emp_id is NULL))
                      and ax.end_date > sysdate and instr(ae.targets, 'E') > 0
                    order by ax.start_date, ax.event_ex_id desc",
            ['empID' => $emp_id]
        );

        return SystemCalendarRecordsPermissionsCollection::make($records);
    }

    public static function getSystemCalendarPermissions(int $year, int $term, int $empId): SystemCalendarRecordsPermissionsCollection
    {
        $records = DB::select(
            "select ae.title_en as title,
                   ax.key,
                   ax.year as year,
                   ax.term as term,
                   ax.start_date as start_date,
                   ax.end_date as end_date,
                   ax.entry_year,
                   ae.is_blocking,
                   ax.emp_id,
                   ax.prog_code,
                   ax.dep_code,
                   ax.class as classof,
                   ax.edu_level,
                   case
                       when (sysdate between ax.start_date and ax.end_date) and
                            ax.is_active = 1 then
                           1
                       else
                           0
                       end as status
                    from ACADEMIC_EVENT_EX ax
                             left outer join academic_event ae
                                             on ae.key = ax.key
                    where (ax.emp_id = :empID or (ax.stud_id is NULL and ax.emp_id is NULL))
                      and ax.year = :year and ax.term = :term
                      and instr(ae.targets, 'E') > 0
                    order by ax.start_date, ax.event_ex_id desc",
            ['year' => $year, 'term' => $term, 'empID' => $empId]
        );

        return SystemCalendarRecordsPermissionsCollection::make($records);
    }
}
