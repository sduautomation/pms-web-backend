<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Models\Scopes;

use Domains\Administrative\v100\Cooker\Collections\GateEntryRecordsCollection;
use Domains\Administrative\v100\Cooker\DTO\GateEntryRequestDTO;
use Illuminate\Support\Facades\DB;

trait GateEntryStudentRecordsScopes
{
    public static function getStudentGateRecords(GateEntryRequestDTO $dto): GateEntryRecordsCollection
    {
        $sql = "select s.name||' '||s.surname as fullname,
            t.user_token,
            t.access_id_in,
            t.access_id_out,
            t.card_no,
            case
                when t.access_date_out is null or t.access_date_in is null then
                   null
                else
                   (t.access_date_out - t.access_date_in) * 24 * 3600
            end worktime,
            nvl(TO_CHAR(access_date_in, 'D'), 0)-1 dayofweek,
            TO_CHAR(nvl(t.access_date_in, t.access_date_out), 'DD.MM.YYYY') access_date_in,
            TO_CHAR(access_date_out, 'DD.MM.YYYY') access_date_out,
            nvl(TO_CHAR(access_date_in, 'HH24:MI'), '-') access_date_in_h,
            nvl(TO_CHAR(access_date_out, 'HH24:MI'), '-') access_date_out_h,
            (select rg.title_EN from reader_groups@cardsys rg where rg.group_id = t.reader_group_in) as reader_type_in,
            (select rg.title_EN from reader_groups@cardsys rg where rg.group_id = t.reader_group_out) as reader_type_out,
            (select wd.title_EN from dbmaster.week_days wd where wd.week_d_id = nvl(TO_CHAR(nvl(t.access_date_in, t.access_date_out), 'D'), 0) - 1) as week_day_title
        from
            table(dbmaster.cardaccess(null, 'S', to_char(:stud_id), to_date(:from_date, 'YYYY-MM-DD'), to_date(:due_date, 'YYYY-MM-DD') + 1, :reader_group)) t
        left outer join
            dbmaster.students s on
                s.stud_id = t.user_token
        left outer join
            dbmaster.stud_prog sp on
                sp.stud_id = s.stud_id and sp.prog_type = 'M'
        left outer join
            dbmaster.dep_programs dp on
                dp.prog_code = sp.prog_code and dp.son = 1
        where
            t.user_type = 'S' and
            t.user_token = :stud_id
        ORDER BY
            nvl(t.access_date_in, t.access_date_out) DESC,
            FULLNAME";

        $params = [
            'stud_id' => $dto->username,
            'from_date' => $dto->from->toDateString(),
            'due_date' => $dto->due->toDateString(),
            'reader_group' => $dto->entry,
        ];

        return GateEntryRecordsCollection::make(
            DB::select($sql, $params)
        );
    }

    public static function getStudentGateRecordsByDepartments(GateEntryRequestDTO $dto): GateEntryRecordsCollection
    {
        $sql = "select s.name||' '||s.surname as fullname,
            t.user_token,
            t.access_id_in,
            t.access_id_out,
            t.card_no,
            case
                when t.access_date_out is null or t.access_date_in is null then
                   null
                else
                   (t.access_date_out - t.access_date_in) * 24 * 3600
            end worktime,
            nvl(TO_CHAR(access_date_in, 'D'), 0)-1 dayofweek,
            TO_CHAR(nvl(t.access_date_in, t.access_date_out), 'DD.MM.YYYY') access_date_in,
            TO_CHAR(access_date_out, 'DD.MM.YYYY') access_date_out,
            nvl(TO_CHAR(access_date_in, 'HH24:MI'), '-') access_date_in_h,
            nvl(TO_CHAR(access_date_out, 'HH24:MI'), '-') access_date_out_h,
            (select rg.title_EN from reader_groups@cardsys rg where rg.group_id = t.reader_group_in) as reader_type_in,
            (select rg.title_EN from reader_groups@cardsys rg where rg.group_id = t.reader_group_out) as reader_type_out,
            (select wd.title_EN from dbmaster.week_days wd where wd.week_d_id = nvl(TO_CHAR(nvl(t.access_date_in, t.access_date_out), 'D'), 0) - 1) as week_day_title
        from
            table(dbmaster.cardaccess(null, 'S', to_char(:stud_id), to_date(:from_date, 'YYYY-MM-DD'), to_date(:due_date, 'YYYY-MM-DD') + 1, :reader_group)) t
        left outer join
            dbmaster.students s on
                s.stud_id = t.user_token
        left outer join
            dbmaster.stud_prog sp on
                sp.stud_id = s.stud_id and sp.prog_type = 'M'
        left outer join
            dbmaster.dep_programs dp on
                dp.prog_code = sp.prog_code and dp.son = 1
        where
            t.user_type = 'S' and
            t.user_token = :stud_id and
            (dp.dep_code = :department_code or dp.dep_code_f = :department_code)
        ORDER BY
            nvl(t.access_date_in, t.access_date_out) DESC,
            FULLNAME";

        $params = [
            'stud_id' => $dto->username,
            'from_date' => $dto->from->toDateString(),
            'due_date' => $dto->due->toDateString(),
            'reader_group' => $dto->entry,
            'department_code' => $dto->department,
        ];

        return GateEntryRecordsCollection::make(
            DB::select($sql, $params)
        );
    }
}
