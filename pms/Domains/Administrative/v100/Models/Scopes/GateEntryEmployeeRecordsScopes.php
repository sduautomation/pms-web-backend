<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Models\Scopes;

use Domains\Administrative\v100\Cooker\Collections\GateEntryRecordsCollection;
use Domains\Administrative\v100\Cooker\DTO\GateEntryRequestDTO;
use Illuminate\Support\Facades\DB;
use JetBrains\PhpStorm\ArrayShape;

trait GateEntryEmployeeRecordsScopes
{
    /**
     * @param GateEntryRequestDTO $dto
     * @return array
     */
    #[ArrayShape(['from_date' => 'string', 'due_date' => 'string', 'reader_group' => 'int'])]
    private static function getQueryParams(GateEntryRequestDTO $dto): array
    {
        return [
            'from_date' => $dto->from->toDateString(),
            'due_date' => $dto->due->toDateString(),
            'reader_group' => $dto->entry,
        ];
    }

    /**
     * @return GateEntryRecordsCollection
     */
    public static function getEmptyRecords(): GateEntryRecordsCollection
    {
        return GateEntryRecordsCollection::make([]);
    }

    /**
     * @param GateEntryRequestDTO $dto
     * @param int $userId
     * @return GateEntryRecordsCollection
     */
    public static function getOwnEntryRecords(GateEntryRequestDTO $dto, int $userId): GateEntryRecordsCollection
    {
        $sql = "select
            e.name||' '||e.sname as fullname,
            t.user_token,
            t.access_id_in,
            t.access_id_out,
            t.card_no,
            case
               when t.access_date_out is null or t.access_date_in is null then
                   null
               else
                   (t.access_date_out - t.access_date_in) * 24 * 3600
            end worktime,
           nvl(TO_CHAR(access_date_in, 'D'), 0)-1 dayofweek,
           TO_CHAR(nvl(t.access_date_in, t.access_date_out), 'DD.MM.YYYY') access_date_in,
           TO_CHAR(access_date_out, 'DD.MM.YYYY') access_date_out,
           nvl(TO_CHAR(access_date_in, 'HH24:MI'), '-') access_date_in_h,
           nvl(TO_CHAR(access_date_out, 'HH24:MI'), '-') access_date_out_h,
           (select rg.title_EN from reader_groups@cardsys rg where rg.group_id = t.reader_group_in) as reader_type_in,
           (select rg.title_EN from reader_groups@cardsys rg where rg.group_id = t.reader_group_out) as reader_type_out,
           (select wd.title_EN from dbmaster.week_days wd where wd.week_d_id = nvl(TO_CHAR(nvl(t.access_date_in, t.access_date_out), 'D'), 0) - 1) as week_day_title
        from
            table(dbmaster.cardaccess(null, 'P', to_char(:emp_id), to_date(:from_date, 'YYYY-MM-DD'), to_date(:due_date, 'YYYY-MM-DD') + 1, :reader_group)) t
        left outer join
            dbmaster.employee e on e.emp_id = t.user_token
        where
            t.user_type = 'P' and
            t.user_token like :emp_id
        ORDER BY
            nvl(t.access_date_in, t.access_date_out) DESC,
            FULLNAME";

        $params = self::getQueryParams($dto);
        $params['emp_id'] = $userId;

        return GateEntryRecordsCollection::make(
            DB::select($sql, $params)
        );
    }

    /**
     * @param GateEntryRequestDTO $dto
     * @param int $userId
     * @return GateEntryRecordsCollection
     */
    public static function getEmployeesEntryRecordsByDepartments(GateEntryRequestDTO $dto, int $userId): GateEntryRecordsCollection
    {
        $sql = "select
            e.name || ' ' || e.sname as fullname,
            t.user_token,
            t.access_id_in,
            t.access_id_out,
            t.card_no,
            case
                when t.access_date_out is null or t.access_date_in is null then
                    null
                else
                    (t.access_date_out - t.access_date_in) * 24 * 3600
            end worktime,
            nvl(TO_CHAR(access_date_in, 'D'), 0) - 1 dayofweek,
            TO_CHAR(nvl(t.access_date_in, t.access_date_out), 'DD.MM.YYYY') access_date_in,
            TO_CHAR(access_date_out, 'DD.MM.YYYY') access_date_out,
            nvl(TO_CHAR(access_date_in, 'HH24:MI'), '-') access_date_in_h,
            nvl(TO_CHAR(access_date_out, 'HH24:MI'), '-') access_date_out_h,
            (
                select
                    rg.title_EN
                from
                     reader_groups@cardsys rg
                where
                      rg.group_id = t.reader_group_in
            ) as reader_type_in,
            (
                select
                    rg.title_EN
                from
                    reader_groups@cardsys rg
                where
                      rg.group_id = t.reader_group_out
            ) as reader_type_out,
            (
                select
                    wd.title_EN
                from
                    week_days wd
                where
                    wd.week_d_id = nvl(TO_CHAR(nvl(t.access_date_in, t.access_date_out), 'D'), 0) - 1
            ) as week_day_title
        from
             table(cardaccess( null, 'P', null, to_date(:from_date, 'YYYY-MM-DD'), to_date(:due_date, 'YYYY-MM-DD'), :reader_group)) t
        left outer join
            employee e
                on e.emp_id = t.user_token
        where
            t.user_type = 'P'
            and e.emp_id in (
                select
                    t.emp_id
                from
                    table(empaccessemp(:emp_id, 108)) t
                where
                    t.dep_code in (
                        select
                            d.dep_code
                        from
                            departments d
                        where
                            d.son = 1
                            START WITH d.dep_code = :department_code
                            CONNECT BY PRIOR d.dep_id = d.p_id
                    )
            )
            ORDER BY
                nvl(t.access_date_in, t.access_date_out) DESC,
                FULLNAME";

        $params = self::getQueryParams($dto);
        $params['emp_id'] = $userId;
        $params['department_code'] = $dto->department;
        $params['due_date'] = $dto->from->addDay()->toDateString();

        return GateEntryRecordsCollection::make(
            DB::select($sql, $params)
        );
    }

    /**
     * @param GateEntryRequestDTO $dto
     * @return GateEntryRecordsCollection
     */
    public static function getEmployeeEntryRecords(GateEntryRequestDTO $dto, int $userId, int $searchId): GateEntryRecordsCollection
    {
        $sql = "select
            e.name || ' ' || e.sname as fullname,
            t.user_token,
            t.access_id_in,
            t.access_id_out,
            t.card_no,
            case
                when t.access_date_out is null or t.access_date_in is null then
                    null
                else
                    (t.access_date_out - t.access_date_in) * 24 * 3600
            end worktime,
            nvl(TO_CHAR(access_date_in, 'D'), 0) - 1 dayofweek,
            TO_CHAR(nvl(t.access_date_in, t.access_date_out), 'DD.MM.YYYY') access_date_in,
            TO_CHAR(access_date_out, 'DD.MM.YYYY') access_date_out,
            nvl(TO_CHAR(access_date_in, 'HH24:MI'), '-') access_date_in_h,
            nvl(TO_CHAR(access_date_out, 'HH24:MI'), '-') access_date_out_h,
            (
                select
                    rg.title_EN
                from
                     reader_groups@cardsys rg
                where
                      rg.group_id = t.reader_group_in
            ) as reader_type_in,
            (
                select
                    rg.title_EN
                from
                    reader_groups@cardsys rg
                where
                      rg.group_id = t.reader_group_out
            ) as reader_type_out,
            (
                select
                    wd.title_EN
                from
                    week_days wd
                where
                    wd.week_d_id = nvl(TO_CHAR(nvl(t.access_date_in, t.access_date_out), 'D'), 0) - 1
            ) as week_day_title
        from
             table(cardaccess( null, 'P', null, to_date(:from_date, 'YYYY-MM-DD'), to_date(:due_date, 'YYYY-MM-DD'), :reader_group)) t
        left outer join
            employee e
                on e.emp_id = t.user_token
        where
            t.user_type = 'P'
            and e.emp_id in (
                select
                    t.emp_id
                from
                    table(empaccessemp(:emp_id, 108)) t
                where
                    t.dep_code in (
                        select
                            d.dep_code
                        from
                            departments d
                        where
                            t.emp_id = :search_id and
                            d.son = 1
                            START WITH d.dep_code = :department_code
                            CONNECT BY PRIOR d.dep_id = d.p_id
                    )
            )
            ORDER BY
                nvl(t.access_date_in, t.access_date_out) DESC,
                FULLNAME";
        $params = self::getQueryParams($dto);
        $params['department_code'] = $dto->department;
        $params['emp_id'] = $userId;
        $params['search_id'] = $searchId;

        return GateEntryRecordsCollection::make(
            DB::select($sql, $params)
        );
    }
}
