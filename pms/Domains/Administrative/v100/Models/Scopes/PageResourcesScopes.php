<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Models\Scopes;

use Domains\Administrative\v100\Cooker\Collections\PageResources\GateEntryReaderGroupsCollection;
use Domains\Administrative\v100\Cooker\Collections\PageResources\PageResourcesDepartmentsCollection;
use Domains\Administrative\v100\Cooker\Collections\PageResources\StaffListPositionsCollection;
use Illuminate\Support\Facades\DB;
use Support\Models\Department;

/**
 * Trait PageResourcesScopes .
 * @mixin Department
 */
trait PageResourcesScopes
{
    public static function getStaffListDepartments(): PageResourcesDepartmentsCollection
    {
        $records = DB::select(
            "select
                    d.title_en as title,
                    d.dep_id as id,
                    d.dep_code as code,
                    level
                    from departments d
                        left outer join cfg_active_period c
                    on c.cfg_key = 'STUD_REG_IL'
                    where d.year = c.year
                    start with d.p_id is NULL
                          connect by prior d.dep_id = d.p_id
                          ORDER SIBLINGS BY d.title_en"
        );

        return PageResourcesDepartmentsCollection::make($records);
    }

    public static function getStaffListPositions(): StaffListPositionsCollection
    {
        $records = DB::select(
            'select
                    g.gorev_ad_en as title,
                    g.gorev_id as id
                    from GOREV g
                    where exists(
                        select dg.gorev_id
                        from emp_gorev eg
                            left outer join dep_gorev dg on dg.dep_gorev_id = eg.dep_gorev_id
                        where dg.gorev_id = g.gorev_id and eg.status > 0)
                    order by title'
        );

        return StaffListPositionsCollection::make($records);
    }

    public static function getGateEntryEmployeeDepartments(int $empId): PageResourcesDepartmentsCollection
    {
        $records = DB::select(
            "select
                    d.title_en as title,
                    d.dep_id as id,
                    d.dep_code as code,
                    level
                from dbmaster.departments d
                         left outer join table(empaccessbirim(:empId, 108)) t on t.dep_code = d.dep_code
                         left outer join cfg_active_period c on c.cfg_key = 'STUD_REG_IL'
                where d.year = c.year
                  and t.dep_code is not NULL start
                    with d.p_id is null connect by prior d.dep_id = d.p_id
                ORDER SIBLINGS BY d.title_en",
            ['empId' => $empId]
        );

        return PageResourcesDepartmentsCollection::make($records);
    }

    public static function getGateEntryStudentsDepartments(): PageResourcesDepartmentsCollection
    {
        $records = DB::select(
            "select
                    d.title_en as title,
                    d.dep_id as id,
                    d.dep_code as code,
                    level
                    from departments d
                    left outer join cfg_active_period c on c.cfg_key = 'STUD_REG_IL'
                    where d.year = c.year
                      and d.type in('A', 'F') start with d.type = 'F' connect by prior d.dep_id = d.p_id
                    ORDER SIBLINGS BY d.title_en
            "
        );

        return PageResourcesDepartmentsCollection::make($records);
    }

    public static function getGateEntryReaderGroups(): GateEntryReaderGroupsCollection
    {
        $records = DB::select(
            "select
                        t.equ_code as id,
                        listagg(t.title_en, ', ') as title
                    from READER_GROUPS@CARDSYS t
                    where t.is_visible = 1
                      and t.is_active = 1
                    group by t.equ_code order by 2
                    "
        );

        return GateEntryReaderGroupsCollection::make($records);
    }
}
