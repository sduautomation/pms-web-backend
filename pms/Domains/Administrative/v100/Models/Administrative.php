<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Models;

use Domains\Administrative\v100\Models\Scopes\GateEntryEmployeeRecordsScopes;
use Domains\Administrative\v100\Models\Scopes\GateEntryStudentRecordsScopes;
use Illuminate\Database\Eloquent\Model;

final class Administrative extends Model
{
    use GateEntryStudentRecordsScopes;
    use GateEntryEmployeeRecordsScopes;
}
