<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Requests;

use Carbon\Carbon;
use Domains\Administrative\v100\Cooker\DTO\GateEntryRequestDTO;
use Domains\Administrative\v100\Core\Enums\GateEntryTypes;
use Exception;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Core\Enums\UserTypes;
use Support\Core\Exceptions\ErrorResponseException;
use Support\Requests\BaseFormRequest;

/**
 * Class GateEntryRecordsRequest.
 * @OA\Schema(
 *     required={"user_type", "due_date", "reader_group"},
 *      @OA\Property(
 *         property="user_type",
 *         type="string"
 *      ),
 *     @OA\Property(
 *         property="from_date",
 *         type="string"
 *      ),
 *     @OA\Property(
 *         property="due_date",
 *         type="string"
 *      ),
 *     @OA\Property(
 *         property="search_id",
 *         type="string"
 *      ),
 *     @OA\Property(
 *         property="reader_group",
 *         type="integer"
 *      ),
 *     @OA\Property(
 *         property="department_code",
 *         type="string"
 *      )
 * )
 */
final class GateEntryRecordsRequest extends BaseFormRequest
{
    /**
     * @return string[]
     */
    #[ArrayShape(['user_type' => 'string', 'reader_group' => 'string', 'department_code' => 'string', 'username' => 'string', 'from_date' => 'string', 'due_date' => 'string', 'mailer' => 'string'])]
    public function rules(): array
    {
        $userTypes = implode(',', [UserTypes::EMPLOYEE_USER_TYPE, UserTypes::STUDENT_USER_TYPE]);
        $entryTypes = implode(',', [GateEntryTypes::TOURNIQUET, GateEntryTypes::TECHNOPARK, GateEntryTypes::SERVER_ROOM, GateEntryTypes::DORM_BOYS, GateEntryTypes::DORM_GIRLS, GateEntryTypes::MEETING_ROOM_ENG]);

        return [
            'user_type' => "required|string|in:$userTypes",
            'reader_group' => "required|integer|in:$entryTypes",
            'username' => 'string|nullable',
            'department_code' => 'string|nullable',
            'from_date' => 'required|date',
            'due_date' => 'date|nullable',
            'mailer' => 'integer|nullable|in:1,0',
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return GateEntryRequestDTO
     * @throws ErrorResponseException
     */
    public function getDto(): GateEntryRequestDTO
    {
        $validated = $this->validated();

        $userType = Arr::get($validated, 'user_type');
        $entry = Arr::get($validated, 'reader_group');
        $department = Arr::get($validated, 'department_code');
        $username = Arr::get($validated, 'username');
        $from_date = Arr::get($validated, 'from_date');
        $due_date = Arr::get($validated, 'due_date');
        $mailer = Arr::get($validated, 'mailer');

        try {
            $from_date = Carbon::parse($from_date);

            if ($due_date !== null) {
                $due_date = Carbon::parse($due_date);
            }
        } catch (Exception $e) {
            throw new ErrorResponseException('Invalid from or due date provided.'.$e->getMessage());
        }

        return new GateEntryRequestDTO(
            userType: $userType,
            username: $username,
            department: $department !== null ? (string) $department : null,
            from: $from_date,
            due: $due_date,
            entry: (int) $entry,
            mailer: $mailer !== null && $mailer == 1
        );
    }
}
