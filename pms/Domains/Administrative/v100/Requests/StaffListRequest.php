<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Requests;

use Domains\Administrative\v100\Cooker\DTO\StaffListRequestDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={},
 *      @OA\Property(
 *         property="name",
 *         type="string"
 *      ),
 *     @OA\Property(
 *         property="surname",
 *         type="string"
 *      ),
 *     @OA\Property(
 *         property="position_id",
 *         type="integer"
 *      ),
 *     @OA\Property(
 *         property="department_code",
 *         type="string"
 *      ),
 *     @OA\Property(
 *         property="sub_departments",
 *         type="boolean"
 *      )
 * )
 */
final class StaffListRequest extends BaseFormRequest
{
    #[ArrayShape(['department_code' => 'string', 'position_id' => 'string', 'name' => 'string', 'surname' => 'string', 'sub_departments' => 'string'])]
    public function rules(): array
    {
        return [
            'name' => 'string|nullable',
            'surname' => 'string|nullable',
            'position_id' => 'integer|nullable',
            'department_code' => 'string|nullable',
            'sub_departments' => 'integer|nullable',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): StaffListRequestDTO
    {
        $validated = $this->validated();

        return new StaffListRequestDTO(
            name: Arr::get($validated, 'name') ?? '',
            surname: Arr::get($validated, 'surname') ?? '',
            positionId: (int) Arr::get($validated, 'position_id') ?? 0,
            departmentCode: Arr::get($validated, 'department_code') ?? '',
            includeSubDepartments: Arr::get($validated, 'sub_departments') == 1
        );
    }
}
