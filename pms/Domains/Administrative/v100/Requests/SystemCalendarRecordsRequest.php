<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Requests;

use Domains\Administrative\v100\Cooker\DTO\SystemCalendarRequest\SystemCalendarRecordsDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Requests\BaseFormRequest;

/**
 * Class SystemCalendarRecordsRequest.
 *  @OA\Schema(
 *     @OA\Property(
 *        property="year",
 *        type="string"
 *     ),
 *     @OA\Property(
 *        property="term",
 *        type="string"
 *     ),
 * )
 */
final class SystemCalendarRecordsRequest extends BaseFormRequest
{
    /**
     * @return string[]
     */
    #[ArrayShape(['year' => 'int', 'term' => 'int'])]
    public function rules(): array
    {
        $startYear = 2015;
        $currentYear = date('Y');

        return [
            'year' => "numeric|min:$startYear|max:$currentYear",
            'term' => 'numeric|min:1|max:3',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return SystemCalendarRecordsDTO
     */
    public function getDto(): SystemCalendarRecordsDTO
    {
        $validated = $this->validated();

        return new SystemCalendarRecordsDTO(
            year: (int) Arr::get($validated, 'year'),
            term: (int) Arr::get($validated, 'term'),
            default: count($validated) == 0,
            emp_id: $this->user()->emp_id
        );
    }
}
