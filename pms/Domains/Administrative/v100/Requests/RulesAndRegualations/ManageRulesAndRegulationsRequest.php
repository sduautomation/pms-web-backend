<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Requests\RulesAndRegualations;

use Domains\Administrative\v100\Cooker\DTO\RulesAndRegulations\RulesAndRegulationsCreateRequestDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Requests\BaseFormRequest;

/**
 * Class ManageRulesAndRegulationsRequest.
 * @OA\Schema(
 *     @OA\Property (
 *         property="document",
 *         type="object",
 *         @OA\Property(property="title", type="string"),
 *         @OA\Property(property="description", type="string"),
 *         @OA\Property(property="categoryUuid", type="string"),
 *         @OA\Property(property="meta", type="string"),
 *         @OA\Property(property="label", type="string")
 *     ),
 *      @OA\Property (
 *         property="contents",
 *         type="array",
 *          @OA\Items(
 *              @OA\Property(property="languageUuid", type="string"),
 *              @OA\Property(property="versionUuid", type="string"),
 *              @OA\Property(property="image", type="string"),
 *              @OA\Property(property="content", type="string"),
 *              @OA\Property(property="active", type="boolean")
 *          )
 *     ),
 *      @OA\Property (
 *         property="attachments",
 *         type="array",
 *          @OA\Items(
 *              @OA\Property(property="languageUuid", type="string"),
 *              @OA\Property(property="versionUuid", type="string"),
 *              @OA\Property(property="title", type="string"),
 *              @OA\Property(property="extension", type="string"),
 *              @OA\Property(property="active", type="boolean")
 *          )
 *     )
 * )
 */
final class ManageRulesAndRegulationsRequest extends BaseFormRequest
{
    #[ArrayShape(['document' => 'string', 'contents' => 'string', 'attachments' => 'string'])]
    public function rules(): array
    {
        return [
            'document' => 'required',
            'contents' => 'required',
            'attachments' => 'required',
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return RulesAndRegulationsCreateRequestDTO
     */
    public function getDto(): RulesAndRegulationsCreateRequestDTO
    {
        $validated = $this->validated();

        return new RulesAndRegulationsCreateRequestDTO(
            document: (object) Arr::get($validated, 'document'),
            contents: Arr::get($validated, 'contents'),
            attachments: Arr::get($validated, 'attachments')
        );
    }
}
