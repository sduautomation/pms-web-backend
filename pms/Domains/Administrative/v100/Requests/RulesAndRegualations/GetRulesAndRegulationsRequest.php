<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Requests\RulesAndRegualations;

use Domains\Administrative\v100\Cooker\DTO\RulesAndRegulations\RulesAndRegulationsRequestDTO;
use JetBrains\PhpStorm\ArrayShape;
use Support\Requests\BaseFormRequest;

/**
 * Class GetRulesAndRegulationsRequest.
 * @OA\Schema(
 *     @OA\Property (
 *         property="uuid",
 *         type="string"
 *     )
 * )
 */
final class GetRulesAndRegulationsRequest extends BaseFormRequest
{
    /**
     * @return string[]
     */
    #[ArrayShape(['uuid' => 'string'])]
    public function rules(): array
    {
        return [
            'uuid' => 'string',
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): RulesAndRegulationsRequestDTO
    {
        return new RulesAndRegulationsRequestDTO(
            uuid: (string) $this->route('uuid')
        );
    }
}
