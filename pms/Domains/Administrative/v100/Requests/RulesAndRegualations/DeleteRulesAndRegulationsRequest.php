<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Requests\RulesAndRegualations;

use JetBrains\PhpStorm\ArrayShape;
use Support\Requests\BaseFormRequest;

/**
 * Class DeleteRulesAndRegulationsRequest.
 * @OA\Schema(
 *     @OA\Property (
 *         property="uuid",
 *         type="string"
 *     )
 * )
 */
final class DeleteRulesAndRegulationsRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    #[ArrayShape(['uuid' => 'string'])]
    public function rules(): array
    {
        return [
           'uuid' => 'string',
       ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getDto(): string
    {
        return (string) request()->route('uuid');
    }
}
