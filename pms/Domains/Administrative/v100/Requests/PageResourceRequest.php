<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Requests;

use Domains\Administrative\v100\Cooker\DTO\PageResources\PageResourceDTO;
use Domains\Administrative\v100\Core\Enums\PageNames;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Requests\BaseFormRequest;

/**
 * Class PageResourceRequest.
 *  @OA\Schema(
 *     required={"page"},
 *     @OA\Property(
 *        property="page",
 *        type="string"
 *     ),
 * )
 */
final class PageResourceRequest extends BaseFormRequest
{
    /**
     * @return string[]
     */
    #[ArrayShape(['page' => 'string'])]
    public function rules(): array
    {
        $pages = implode(',', PageNames::PAGES);

        return [
            'page' => "required|string|in:$pages",
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return PageResourceDTO
     */
    public function getDto(): PageResourceDTO
    {
        $validated = $this->validated();

        return new PageResourceDTO(
            page: Arr::get($validated, 'page'),
            emp_id: $this->user()->emp_id
        );
    }
}
