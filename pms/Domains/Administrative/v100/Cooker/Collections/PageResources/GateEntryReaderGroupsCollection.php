<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Collections\PageResources;

use Domains\Administrative\v100\Cooker\ValueObjects\PageResourceValueObjects\GateEntryTypesValueObject;
use Illuminate\Support\Collection;

final class GateEntryReaderGroupsCollection extends Collection
{
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            $self->add(GateEntryTypesValueObject::fromObject($item));
        }

        return $self;
    }
}
