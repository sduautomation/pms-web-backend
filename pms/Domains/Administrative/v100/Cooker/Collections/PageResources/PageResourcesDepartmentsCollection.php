<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Collections\PageResources;

use Domains\Administrative\v100\Cooker\ValueObjects\PageResourceValueObjects\StaffListDepartmentsValueObject;
use Illuminate\Support\Collection;

final class PageResourcesDepartmentsCollection extends Collection
{
    /**
     * @param array $items
     * @return static
     */
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            $self->add(StaffListDepartmentsValueObject::fromObject($item));
        }

        return $self;
    }
}
