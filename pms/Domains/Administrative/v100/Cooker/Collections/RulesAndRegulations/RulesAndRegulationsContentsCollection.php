<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations;

use Domains\Administrative\v100\Cooker\ValueObjects\RulesAndRegulations\RulesAndRegulationsContentValueObject;
use Illuminate\Support\Collection;

final class RulesAndRegulationsContentsCollection extends Collection
{
    /**
     * @param array $items
     * @return static
     */
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            $self->add(RulesAndRegulationsContentValueObject::fromObject($item));
        }

        return $self;
    }

    /**
     * @param $item
     * @return RulesAndRegulationsContentsCollection
     */
    public function add($item): self
    {
        return parent::add($item);
    }
}
