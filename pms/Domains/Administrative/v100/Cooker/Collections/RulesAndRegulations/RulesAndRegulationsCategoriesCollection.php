<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations;

use Domains\Administrative\v100\Cooker\ValueObjects\RulesAndRegulations\RulesAndRegulationsCategoriesValueObject;
use Illuminate\Support\Collection;

final class RulesAndRegulationsCategoriesCollection extends Collection
{
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            $self->add(RulesAndRegulationsCategoriesValueObject::fromObject($item));
        }

        return $self;
    }
}
