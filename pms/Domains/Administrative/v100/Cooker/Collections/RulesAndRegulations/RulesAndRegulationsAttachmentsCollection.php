<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations;

use Domains\Administrative\v100\Cooker\ValueObjects\RulesAndRegulations\RulesAndRegulationsAttachmentValueObject;
use Illuminate\Support\Collection;
use RuntimeException;

final class RulesAndRegulationsAttachmentsCollection extends Collection
{
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            $self->add(RulesAndRegulationsAttachmentValueObject::fromObject($item));
        }

        return $self;
    }

    /**
     * @param $item
     * @return RulesAndRegulationsAttachmentsCollection
     */
    public function add($item): self
    {
        if (! $item instanceof RulesAndRegulationsAttachmentValueObject) {
            throw new RuntimeException('Item must be of the type RulesAndRegulationsAttachmentValueObject');
        }

        return parent::add($item);
    }
}
