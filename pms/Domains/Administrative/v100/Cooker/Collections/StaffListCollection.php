<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Collections;

use Domains\Administrative\v100\Cooker\Entities\StaffListEntity;
use Illuminate\Support\Collection;
use RuntimeException;

/**
 * Class StaffListCollection.
 */
final class StaffListCollection extends Collection
{
    /**
     * @param array $items
     * @return static
     */
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            switch (true) {
                case is_array($item):
                    $self->add(StaffListEntity::fromArray($item));
                    break;
                case is_object($item):
                    $self->add(StaffListEntity::fromObject($item));
                    break;
            }
        }

        return $self;
    }

    /**
     * @param mixed $item
     * @return $this
     */
    public function add($item): self
    {
        if (! $item instanceof StaffListEntity) {
            throw new RuntimeException('Item must be of the type StaffListEntity');
        }

        return parent::add($item);
    }
}
