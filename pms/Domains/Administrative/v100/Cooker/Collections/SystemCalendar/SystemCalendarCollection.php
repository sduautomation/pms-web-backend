<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Collections\SystemCalendar;

use Domains\Administrative\v100\Cooker\ValueObjects\SystemCalendar\SystemCalendarValueObject;
use Illuminate\Support\Collection;

final class SystemCalendarCollection extends Collection
{
    /**
     * @param array $items
     * @return static
     */
    public static function make($items = []): self
    {
        $self = new self();

        $self->add(SystemCalendarValueObject::fromObject($items[0]));

        return $self;
    }
}
