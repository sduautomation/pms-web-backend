<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Collections\SystemCalendar;

use Domains\Administrative\v100\Cooker\ValueObjects\SystemCalendar\TermsValueObject;
use Illuminate\Support\Collection;

final class SystemCalendarTermsCollection extends Collection
{
    /**
     * @param array $items
     * @return static
     */
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            $self->add(TermsValueObject::fromObject($item));
        }

        return $self;
    }
}
