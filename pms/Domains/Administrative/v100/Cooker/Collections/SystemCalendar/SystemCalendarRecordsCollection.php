<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Collections\SystemCalendar;

use Domains\Administrative\v100\Cooker\ValueObjects\SystemCalendar\SystemCalendarRecordsValueObject;
use Illuminate\Support\Collection;

/**
 * Class SystemCalendarRecordsCollection.
 */
final class SystemCalendarRecordsCollection extends Collection
{
    /**
     * @param array $items
     * @return static
     */
    public static function make($items = []): self
    {
        $self = new self();

        $self->add(SystemCalendarRecordsValueObject::fromArray($items));

        return $self;
    }
}
