<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Collections\SystemCalendar;

use Domains\Administrative\v100\Cooker\ValueObjects\SystemCalendar\PermissionsValueObject;
use Illuminate\Support\Collection;

final class SystemCalendarRecordsPermissionsCollection extends Collection
{
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            $self->add(PermissionsValueObject::fromObject($item));
        }

        return $self;
    }
}
