<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Collections\SystemCalendar;

use Domains\Administrative\v100\Cooker\ValueObjects\SystemCalendar\AssessmentsValueObject;
use Illuminate\Support\Collection;

final class SystemCalendarRecordsAssessmentsCollection extends Collection
{
    /**
     * @param array $items
     * @return static
     */
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            $self->add(AssessmentsValueObject::fromObject($item));
        }

        return $self;
    }
}
