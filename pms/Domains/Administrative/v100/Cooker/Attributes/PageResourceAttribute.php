<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Attributes;

use Domains\Administrative\v100\Cooker\Collections\PageResources\GateEntryReaderGroupsCollection;
use Domains\Administrative\v100\Cooker\Collections\PageResources\PageResourcesDepartmentsCollection;
use Domains\Administrative\v100\Cooker\Collections\PageResources\StaffListPositionsCollection;
use Domains\Administrative\v100\Cooker\DTO\PageResources\PageResourceDTO;

/**
 * Class PageResourceAttribute.
 */
final class PageResourceAttribute
{
    public PageResourceDTO $dto;
    public PageResourcesDepartmentsCollection $staffListDepartments;
    public StaffListPositionsCollection $staffListPositions;
    public PageResourcesDepartmentsCollection $gateEntryEmployeeDepartments;
    public PageResourcesDepartmentsCollection $gateEntryStudentsDepartments;
    public object $gateEntryEmployees;
    public object $gateEntryStudents;
    public GateEntryReaderGroupsCollection $types;
    public object $records;
}
