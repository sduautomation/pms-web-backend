<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Attributes;

use Domains\Administrative\v100\Cooker\Collections\SystemCalendar\SystemCalendarRecordsAssessmentsCollection;
use Domains\Administrative\v100\Cooker\Collections\SystemCalendar\SystemCalendarRecordsEventsCollection;
use Domains\Administrative\v100\Cooker\Collections\SystemCalendar\SystemCalendarRecordsPermissionsCollection;
use Domains\Administrative\v100\Cooker\Collections\SystemCalendar\SystemCalendarTermsCollection;
use Domains\Administrative\v100\Cooker\DTO\SystemCalendarRequest\SystemCalendarRecordsDTO;
use Domains\Administrative\v100\Cooker\DTO\SystemCalendarRequest\SystemCalendarResponseDTO;

/**
 * Class SystemCalendarRecordsAttribute.
 */
final class SystemCalendarRecordsAttribute
{
    public SystemCalendarRecordsDTO $dto;
    public SystemCalendarTermsCollection $terms;

    public SystemCalendarRecordsEventsCollection $recordsEvents;
    public SystemCalendarRecordsPermissionsCollection $recordsPermissions;
    public SystemCalendarRecordsAssessmentsCollection $recordsAssessments;
    public SystemCalendarResponseDTO $records;
}
