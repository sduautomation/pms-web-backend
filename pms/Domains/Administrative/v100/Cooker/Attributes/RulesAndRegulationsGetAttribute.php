<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Attributes;

use Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations\RulesAndRegulationsAttachmentsCollection;
use Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations\RulesAndRegulationsCategoriesCollection;
use Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations\RulesAndRegulationsContentsCollection;
use Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations\RulesAndRegulationsRecordsCollection;
use Domains\Administrative\v100\Cooker\DTO\RulesAndRegulations\RulesAndRegulationsRequestDTO;
use Domains\Administrative\v100\Cooker\DTO\RulesAndRegulations\RulesAndRegulationsResponseDTO;
use Domains\Administrative\v100\Cooker\DTO\RulesAndRegulations\RulesAndRegulationsShowResponseDTO;

final class RulesAndRegulationsGetAttribute
{
    public RulesAndRegulationsRequestDTO $dto;
    public RulesAndRegulationsRecordsCollection $rulesAndRegulationsRecords;
    public RulesAndRegulationsCategoriesCollection $categories;
    public RulesAndRegulationsContentsCollection $contents;
    public RulesAndRegulationsAttachmentsCollection $attachments;
    public RulesAndRegulationsShowResponseDTO $recordsByUuid;
    public RulesAndRegulationsResponseDTO $records;
}
