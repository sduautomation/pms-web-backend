<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Attributes;

use Domains\Administrative\v100\Cooker\Collections\StaffListCollection;
use Domains\Administrative\v100\Cooker\DTO\StaffListRequestDTO;

final class StaffListAttribute
{
    public StaffListRequestDTO $requestDto;
    public StaffListCollection $records;
}
