<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Attributes;

use Domains\Administrative\v100\Cooker\DTO\RulesAndRegulations\RulesAndRegulationsCreateRequestDTO;

final class RulesAndRegulationsAttribute
{
    public RulesAndRegulationsCreateRequestDTO $dto;
    public int $documentId;
    public string $uuid;
}
