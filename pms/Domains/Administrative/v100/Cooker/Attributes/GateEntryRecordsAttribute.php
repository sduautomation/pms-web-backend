<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Attributes;

use Domains\Administrative\v100\Cooker\Collections\GateEntryRecordsCollection;
use Domains\Administrative\v100\Cooker\DTO\GateEntryRequestDTO;

/**
 * Class GateEntryRecordsAttribute.
 */
final class GateEntryRecordsAttribute
{
    public int $userId = 0;
    public int $searchId;
    public GateEntryRequestDTO $dto;
    public GateEntryRecordsCollection $records;
}
