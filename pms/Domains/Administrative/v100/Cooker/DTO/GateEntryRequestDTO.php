<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\DTO;

use Carbon\Carbon;

/**
 * Class GateEntryRequestDTO.
 */
final class GateEntryRequestDTO
{
    public function __construct(
        public string $userType,
        public ?string $username,
        public string|null $department,
        public Carbon $from,
        public Carbon|null $due,
        public int $entry,
        public bool $mailer = false
    ) {
    }
}
