<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\DTO\PageResources;

final class GateEntryPageResourceDataDTO
{
    public function __construct(
        public object $departments,
        public object $types
    ) {
    }
}
