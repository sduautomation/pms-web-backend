<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\DTO\PageResources;

final class GateEntryPageResourceResponseDTO
{
    public function __construct(
        public object $employees,
        public object $students
    ) {
    }
}
