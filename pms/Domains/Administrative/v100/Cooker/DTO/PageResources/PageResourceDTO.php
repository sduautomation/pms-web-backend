<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\DTO\PageResources;

final class PageResourceDTO
{
    /**
     * @param string $page
     */
    public function __construct(
        public string $page,
        public int $emp_id
    ) {
    }
}
