<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\DTO\PageResources;

use Domains\Administrative\v100\Cooker\Collections\PageResources\PageResourcesDepartmentsCollection;
use Domains\Administrative\v100\Cooker\Collections\PageResources\StaffListPositionsCollection;

final class StaffListPageResourcesResponseDTO
{
    public function __construct(
        public PageResourcesDepartmentsCollection $departments,
        public StaffListPositionsCollection $positions,
    ) {
    }
}
