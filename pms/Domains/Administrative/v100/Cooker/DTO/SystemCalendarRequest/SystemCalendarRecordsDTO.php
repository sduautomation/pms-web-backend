<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\DTO\SystemCalendarRequest;

final class SystemCalendarRecordsDTO
{
    public function __construct(
        public int $year,
        public int $term,
        public bool $default,
        public int $emp_id
    ) {
    }
}
