<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\DTO\SystemCalendarRequest;

use Domains\Administrative\v100\Cooker\Collections\SystemCalendar\SystemCalendarTermsCollection;

final class SystemCalendarResponseDTO
{
    public function __construct(
        public SystemCalendarTermsCollection $terms,
        public array $records
    ) {
    }
}
