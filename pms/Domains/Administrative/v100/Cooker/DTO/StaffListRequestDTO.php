<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\DTO;

final class StaffListRequestDTO
{
    public function __construct(
        public ?string $name,
        public ?string $surname,
        public ?int $positionId,
        public ?string $departmentCode,
        public ?bool $includeSubDepartments,
    ) {
    }
}
