<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\DTO\RulesAndRegulations;

final class RulesAndRegulationsCreateRequestDTO
{
    /**
     * @param object $document
     * @param array $contents
     * @param array $attachments
     */
    public function __construct(
        public object $document,
        public array $contents,
        public array $attachments
    ) {
    }
}
