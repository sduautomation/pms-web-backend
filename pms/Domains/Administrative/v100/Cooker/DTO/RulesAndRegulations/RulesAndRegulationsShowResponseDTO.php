<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\DTO\RulesAndRegulations;

use Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations\RulesAndRegulationsAttachmentsCollection;
use Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations\RulesAndRegulationsContentsCollection;
use Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations\RulesAndRegulationsRecordsCollection;

final class RulesAndRegulationsShowResponseDTO
{
    public function __construct(
        public RulesAndRegulationsContentsCollection $contents,
        public RulesAndRegulationsAttachmentsCollection $attachments,
        public RulesAndRegulationsRecordsCollection $rulesAndRegulationsRecords
    ) {
    }
}
