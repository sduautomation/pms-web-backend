<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\DTO\RulesAndRegulations;

use Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations\RulesAndRegulationsCategoriesCollection;
use Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations\RulesAndRegulationsRecordsCollection;

final class RulesAndRegulationsResponseDTO
{
    public function __construct(
        public RulesAndRegulationsCategoriesCollection $categories,
        public RulesAndRegulationsRecordsCollection $records
    ) {
    }
}
