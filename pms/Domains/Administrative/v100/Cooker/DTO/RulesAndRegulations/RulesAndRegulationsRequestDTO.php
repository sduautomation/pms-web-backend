<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\DTO\RulesAndRegulations;

final class RulesAndRegulationsRequestDTO
{
    public function __construct(
        public ?string $uuid
    ) {
    }
}
