<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Entities;

use Illuminate\Support\Arr;
use JetBrains\PhpStorm\Pure;

final class StaffListEntity
{
    public function __construct(
        public ?string $employeeId,
        public ?string $fullName,
        public ?string $fullNameNative,
        public ?string $departmentPosition,
        public ?string $workingStatus,
        public ?string $staff,
        public ?string $outerEmail,
        public ?string $innerTelephone
    ) {
    }

    #[Pure]
    public static function fromObject(object $data): self
    {
        $employeeId = $data->emp_id;
        $fullName = $data->fullname;
        $fullNameNative = $data->fullname_native;
        $departmentPosition = $data->department_position;
        $workingStatus = $data->working_status;
        $staff = $data->staff;
        $outerEmail = $data->outer_email;
        $innerTelephone = $data->inner_telephone;

        return new self(
            employeeId: $employeeId,
            fullName: $fullName,
            fullNameNative: $fullNameNative,
            departmentPosition: $departmentPosition,
            workingStatus: $workingStatus,
            staff: $staff,
            outerEmail: $outerEmail,
            innerTelephone: $innerTelephone
        );
    }

    public static function fromArray(array $data): self
    {
        $employeeId = Arr::get($data, 'emp_id');
        $fullName = Arr::get($data, 'fullname');
        $fullNameNative = Arr::get($data, 'fullname_native');
        $departmentPosition = Arr::get($data, 'department_position');
        $workingStatus = Arr::get($data, 'working_status');
        $staff = Arr::get($data, 'staff');
        $outerEmail = Arr::get($data, 'outer_email');
        $innerTelephone = Arr::get($data, 'inner_telephone');

        return new self(
            employeeId: $employeeId,
            fullName: $fullName,
            fullNameNative: $fullNameNative,
            departmentPosition: $departmentPosition,
            workingStatus: $workingStatus,
            staff: $staff,
            outerEmail: $outerEmail,
            innerTelephone: $innerTelephone
        );
    }
}
