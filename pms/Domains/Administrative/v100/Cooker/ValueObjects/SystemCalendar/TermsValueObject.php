<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\ValueObjects\SystemCalendar;

final class TermsValueObject
{
    /**
     * @param string|null $year
     * @param string|null $term
     */
    public function __construct(
        public ?int $id,
        public ?string $year,
        public ?string $term
    ) {
    }

    /**
     * @param object $data
     * @return static
     */
    public static function fromObject(object $data): self
    {
        return new self(
            id: (int) $data->id,
            year: $data->year,
            term: $data->term
        );
    }
}
