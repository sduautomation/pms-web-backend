<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\ValueObjects\SystemCalendar;

final class PermissionsValueObject
{
    /**
     * @param string|null $year
     * @param string|null $key
     * @param string|null $term
     * @param string|null $title
     * @param string|null $start_date
     * @param string|null $end_date
     * @param string|null $entry_year
     * @param bool|null $is_blocking
     * @param int|null $emp_id
     * @param string|null $prog_code
     * @param string|null $dep_code
     * @param string|null $class_of
     * @param string|null $edu_level
     * @param bool|null $is_open
     */
    public function __construct(
        public ?string $title,
        public ?string $year,
        public ?string $term,
        public ?string $start_date,
        public ?string $end_date,
        public ?bool $is_open,
        public ?string $key,
        public ?string $entry_year,
        public ?bool $is_blocking,
        public mixed $emp_id,
        public mixed $prog_code,
        public mixed $dep_code,
        public mixed $class_of,
        public mixed $edu_level,
    ) {
    }

    /**
     * @param object $data
     * @return static
     */
    public static function fromObject(object $data): self
    {
        return new self(
            title: $data->title,
            year: $data->year,
            term: $data->term,
            start_date: $data->start_date,
            end_date: $data->end_date,
            is_open: $data->status == '1',
            key: $data->key,
            entry_year: $data->entry_year,
            is_blocking: $data->is_blocking == '1',
            emp_id: $data->emp_id,
            prog_code: $data->prog_code,
            dep_code: $data->dep_code,
            class_of: $data->classof,
            edu_level: $data->edu_level
        );
    }
}
