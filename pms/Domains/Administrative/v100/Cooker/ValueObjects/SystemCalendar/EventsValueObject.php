<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\ValueObjects\SystemCalendar;

/**
 * Class EventsValueObject.
 */
final class EventsValueObject
{
    /**
     * @param string|null $year
     * @param string|null $term
     * @param string|null $title
     * @param string|null $start_date
     * @param string|null $end_date
     * @param bool|null $is_open
     */
    public function __construct(
        public ?string $year,
        public ?string $term,
        public ?string $title,
        public ?string $start_date,
        public ?string $end_date,
        public ?bool $is_open,
    ) {
    }

    /**
     * @param object $data
     * @return static
     */
    public static function fromObject(object $data): self
    {
        return new self(
            year: $data->year,
            term: $data->term,
            title: $data->title,
            start_date: $data->start_date,
            end_date: $data->end_date,
            is_open: (bool) $data->status
        );
    }
}
