<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\ValueObjects;

use Illuminate\Support\Arr;

/**
 * Class PageResourceValueObject.
 */
final class PageResourceValueObject
{
    /**
     * @param string|null $departments
     * @param string|null $positions
     */
    public function __construct(
        public ?string $departments,
        public ?string $positions,
    ) {
    }

    /**
     * @param object $data
     * @return static
     */
    public static function fromObject(object $data): self
    {
        return new self(
            departments: $data->departments,
            positions: $data->positions
        );
    }

    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): self
    {
        $departments = Arr::get($data, 'departments');
        $positions = Arr::get($data, 'positions');

        return new self(
            departments: $departments,
            positions: $positions
        );
    }
}
