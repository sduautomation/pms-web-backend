<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\ValueObjects\RulesAndRegulations;

/**
 * Class RulesAndRegulationsRecordsValueObject.
 */
final class RulesAndRegulationsRecordsValueObject
{
    /**
     * @param string|null $uuid
     * @param string|null $title
     * @param string|null $description
     * @param string|null $category_name
     * @param string|null $meta
     * @param string|null $created_at
     * @param string|null $updated_at
     */
    public function __construct(
        public ?string $uuid,
        public ?string $title,
        public ?string $description,
        public ?string $category_name,
        public ?string $meta,
        public ?string $created_at,
        public ?string $updated_at,
    ) {
    }

    /**
     * @param object $data
     * @return static
     */
    public static function fromObject(object $data): self
    {
        return new self(
            uuid: $data->uuid,
            title: $data->title,
            description: $data->description,
            category_name: $data->category_name,
            meta: $data->meta,
            created_at: $data->created_at,
            updated_at: $data->updated_at
        );
    }
}
