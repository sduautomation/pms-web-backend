<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\ValueObjects\RulesAndRegulations;

/**
 * Class RulesAndRegulationsCategoriesValueObject.
 */
final class RulesAndRegulationsCategoriesValueObject
{
    /**
     * @param string|null $uuid
     * @param string|null $title
     * @param string|null $meta
     * @param string|null $created_at
     * @param string|null $updated_at
     */
    public function __construct(
        public ?string $uuid,
        public ?string $title,
        public ?string $meta,
        public ?string $created_at,
        public ?string $updated_at,
    ) {
    }

    /**
     * @param object $data
     * @return static
     */
    public static function fromObject(object $data): self
    {
        return new self(
            uuid: $data->uuid,
            title: $data->title,
            meta: $data->meta,
            created_at: $data->created_at,
            updated_at: $data->updated_at
        );
    }
}
