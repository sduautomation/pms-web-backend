<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\ValueObjects\RulesAndRegulations;

final class RulesAndRegulationsContentValueObject
{
    public function __construct(
        public ?string $uuid,
        public ?string $docid,
        public ?string $langid,
        public ?string $versid,
        public ?string $language_title,
        public ?string $version_title,
        public ?string $image,
        public ?string $content,
        public ?bool $active,
        public ?string $created_at,
        public ?string $updated_at,
    ) {
    }

    public static function fromObject(object $data): self
    {
        return new self(
            uuid: $data->long_id,
            docid: $data->doc_id,
            langid: $data->language_id,
            versid: $data->version_id,
            language_title: $data->language_title,
            version_title: $data->version_title,
            image: $data->image,
            content: $data->content,
            active: $data->active == 1,
            created_at: $data->created_at,
            updated_at: $data->updated_at,
        );
    }
}
