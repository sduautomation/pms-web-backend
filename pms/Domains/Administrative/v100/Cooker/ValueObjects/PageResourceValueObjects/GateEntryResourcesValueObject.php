<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\ValueObjects\PageResourceValueObjects;

use Illuminate\Support\Arr;

final class GateEntryResourcesValueObject
{
    public function __construct(
        public ?object $employees,
        public ?object $students
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            employees: GateEntryEmployeePageResourcesValueObject::fromArray(Arr::get($data, 'employees')),
            students: GateEntryEmployeePageResourcesValueObject::fromArray(Arr::get($data, 'students'))
        );
    }
}
