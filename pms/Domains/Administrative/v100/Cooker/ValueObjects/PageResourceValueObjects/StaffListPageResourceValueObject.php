<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\ValueObjects\PageResourceValueObjects;

use Illuminate\Support\Arr;

final class StaffListPageResourceValueObject
{
    /**
     * @param array|null $departments
     * @param array|null $positions
     */
    public function __construct(
        public ?array $departments,
        public ?array $positions
    ) {
    }

    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): self
    {
        $departments = array_column(Arr::get($data, 'departments'), 'title');
        $positions = array_column(Arr::get($data, 'positions'), 'title');

        return new self(
            departments: $departments,
            positions: $positions
        );
    }
}
