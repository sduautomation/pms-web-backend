<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\ValueObjects\PageResourceValueObjects;

final class StaffListPositionsValueObject
{
    public function __construct(
        public ?int $id,
        public ?string $title
    ) {
    }

    public static function fromObject(object $data): self
    {
        return new self(
            id: (int) $data->id,
            title: $data->title
        );
    }
}
