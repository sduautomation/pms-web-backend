<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\ValueObjects\PageResourceValueObjects;

use Illuminate\Support\Arr;

final class GateEntryEmployeePageResourcesValueObject
{
    /**
     * @param array|null $departments
     * @param array|null $types
     */
    public function __construct(
        public ?array $departments,
        public ?array $types
    ) {
    }

    public static function fromArray(array $data): self
    {
        $departments = array_column(Arr::get($data, 'departments'), 'title');
        $types = array_column(Arr::get($data, 'types'), 'title');

        return new self(
            departments: $departments,
            types: $types
        );
    }
}
