<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\ValueObjects\PageResourceValueObjects;

final class GateEntryDepartmentsValueObject
{
    public function __construct(
        public ?int $id,
        public ?string $title,
        public ?string $code,
        public ?int $level,
    ) {
    }

    public static function fromObject(object $data): self
    {
        return new self(
            id: $data->id,
            title: $data->title,
            code: $data->code,
            level: $data->level
        );
    }
}
