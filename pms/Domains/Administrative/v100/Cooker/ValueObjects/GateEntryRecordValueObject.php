<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\ValueObjects;

use Carbon\CarbonImmutable;
use Illuminate\Support\Arr;

/**
 * Class GateEntryRecordValueObject.
 */
final class GateEntryRecordValueObject
{
    /**
     * @param string|null $fullName
     * @param string|null $userToken
     * @param string|null $accessIdIn
     * @param string|null $accessIdOut
     * @param string|null $cardNumber
     * @param string|null $workTime
     * @param int|null $dayOfWeek
     * @param CarbonImmutable|null $accessDateIn
     * @param CarbonImmutable|null $accessDateOut
     * @param string|null $accessTimeIn
     * @param string|null $accessTimeOut
     * @param string|null $readerTypeIn
     * @param string|null $readerTypeOut
     * @param string|null $weekDayTitle
     */
    public function __construct(
        public ?string $fullName,
        public ?string $userToken,
        public ?string $accessIdIn,
        public ?string $accessIdOut,
        public ?string $cardNumber,
        public ?string $workTime,
        public ?int $dayOfWeek,
        public ?CarbonImmutable $accessDateIn,
        public ?CarbonImmutable $accessDateOut,
        public ?string $accessTimeIn,
        public ?string $accessTimeOut,
        public ?string $readerTypeIn,
        public ?string $readerTypeOut,
        public ?string $weekDayTitle,
    ) {
    }

    /**
     * @param object $data
     * @return $this
     */
    public static function fromObject(object $data): self
    {
        $fullname = $data->fullname ?? null;
        $userToken = $data->user_token ?? null;
        $accessIdIn = $data->access_id_in ?? null;
        $accessIdOut = $data->access_id_out ?? null;
        $cardNumber = $data->card_no ?? null;
        $workTime = $data->worktime ?? null;
        $dayOfWeek = isset($data->dayofweek) ? (int) $data->dayofweek : null;
        $accessDateIn = isset($data->access_date_in) ? CarbonImmutable::parse($data->access_date_in) : null;
        $accessDateOut = isset($data->access_date_out) ? CarbonImmutable::parse($data->access_date_out) : null;
        $accessTimeIn = $data->access_date_in_h ?? null;
        $accessTimeOut = $data->access_date_out_h ?? null;
        $readerTypeIn = $data->reader_type_in ?? null;
        $readerTypeOut = $data->reader_type_out ?? null;
        $weekDayTitle = $data->week_day_title ?? null;

        return new self(
            fullName: $fullname,
            userToken: $userToken,
            accessIdIn: $accessIdIn,
            accessIdOut: $accessIdOut,
            cardNumber: $cardNumber,
            workTime: $workTime,
            dayOfWeek: $dayOfWeek,
            accessDateIn: $accessDateIn,
            accessDateOut: $accessDateOut,
            accessTimeIn: $accessTimeIn,
            accessTimeOut: $accessTimeOut,
            readerTypeIn: $readerTypeIn,
            readerTypeOut: $readerTypeOut,
            weekDayTitle: $weekDayTitle
        );
    }

    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): self
    {
        $fullname = Arr::get($data, 'fullname');
        $userToken = Arr::get($data, 'user_token');
        $accessIdIn = Arr::get($data, 'access_id_in');
        $accessIdOut = Arr::get($data, 'access_id_out');
        $cardNumber = Arr::get($data, 'card_no');
        $workTime = Arr::get($data, 'worktime');
        $dayOfWeek = Arr::get($data, 'dayofweek') !== null ? (int) Arr::get($data, 'dayofweek') : null;
        $accessDateIn = Arr::get($data, 'access_date_in') ? CarbonImmutable::parse(Arr::get($data, 'access_date_in')) : null;
        $accessDateOut = Arr::get($data, 'access_date_out') !== null ? CarbonImmutable::parse(Arr::get($data, 'access_date_out')) : null;
        $accessTimeIn = Arr::get($data, 'access_date_in_h');
        $accessTimeOut = Arr::get($data, 'access_date_out_h');
        $readerTypeIn = Arr::get($data, 'reader_type_in');
        $readerTypeOut = Arr::get($data, 'reader_type_out');
        $weekDayTitle = Arr::get($data, 'week_day_title');

        return new self(
            fullName: $fullname,
            userToken: $userToken,
            accessIdIn: $accessIdIn,
            accessIdOut: $accessIdOut,
            cardNumber: $cardNumber,
            workTime: $workTime,
            dayOfWeek: $dayOfWeek,
            accessDateIn: $accessDateIn,
            accessDateOut: $accessDateOut,
            accessTimeIn: $accessTimeIn,
            accessTimeOut: $accessTimeOut,
            readerTypeIn: $readerTypeIn,
            readerTypeOut: $readerTypeOut,
            weekDayTitle: $weekDayTitle
        );
    }
}
