<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services;

use Domains\Administrative\v100\Cooker\Attributes\GateEntryRecordsAttribute;
use Domains\Administrative\v100\Cooker\DTO\GateEntryRequestDTO;
use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Getters\GetEmployeeRecordsByDepartmentAction;
use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Getters\GetEmployeesRecordsByDepartmentAction;
use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Getters\GetEmptyRecordsAction;
use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Getters\GetOwnEntryRecordsAction;
use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Getters\GetStudentRecordsAction;
use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Getters\GetStudentRecordsByDepartmentAction;
use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Mailer\GateRecordsSendToUserByMailAction;
use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Validations\ValidateEmployeeInformationAction;
use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Validations\ValidateStudentInformationAction;
use Domains\Administrative\v100\Cooker\Services\Factories\GateEntryRecords\EmployeeSearchTypeFactory;
use Domains\Administrative\v100\Cooker\Services\Factories\GateEntryRecords\GateEntryMailerFactory;
use Domains\Administrative\v100\Cooker\Services\Factories\GateEntryRecords\StudentSearchTypeFactory;
use Domains\Administrative\v100\Cooker\Services\Factories\GateEntryRecords\UserTypeFactory;
use Domains\Administrative\v100\Resources\GateEntryRecordResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use Support\Cooker\Services\ActionService;
use Support\Core\Exceptions\ActionsException;
use Support\Core\Interfaces\ActionServiceInterface;
use Support\Resources\BaseResource;

/**
 * Class GateEntryRecordsActionService.
 */
final class GateEntryRecordsActionService implements ActionServiceInterface
{
    private GateEntryRecordsAttribute $attribute;

    /**
     * @param ActionService $service
     */
    public function __construct(
        private ActionService $service
    ) {
    }

    /**
     * @throws ActionsException
     */
    public function handle(): ActionServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadSchema())
            ->then($this);
    }

    public function loadSchema(): array
    {
        return [
            ['action' => null, 'factory' => UserTypeFactory::class],
            ['action' => ValidateStudentInformationAction::class, 'factory' => StudentSearchTypeFactory::class],
            ['action' => GetStudentRecordsByDepartmentAction::class, 'factory' => GateEntryMailerFactory::class],
            ['action' => GetStudentRecordsAction::class, 'factory' => GateEntryMailerFactory::class],
            ['action' => ValidateEmployeeInformationAction::class, 'factory' => EmployeeSearchTypeFactory::class],
            ['action' => GetEmptyRecordsAction::class, 'factory' => null],
            ['action' => GetOwnEntryRecordsAction::class, 'factory' => GateEntryMailerFactory::class],
            ['action' => GetEmployeesRecordsByDepartmentAction::class, 'factory' => GateEntryMailerFactory::class],
            ['action' => GetEmployeeRecordsByDepartmentAction::class, 'factory' => GateEntryMailerFactory::class],
            ['action' => GateRecordsSendToUserByMailAction::class, 'factory' => null],
        ];
    }

    /**
     * @param GateEntryRequestDTO $data
     * @return ActionServiceInterface
     */
    public function setAttributeFromData(mixed $data): ActionServiceInterface
    {
        $user = Auth::user();
        $attribute = new GateEntryRecordsAttribute();
        $attribute->dto = $data;

        if (isset($user->emp_id)) {
            $attribute->userId = $user->emp_id;
        }

        return $this->setAttribute($attribute);
    }

    /**
     * @param GateEntryRecordsAttribute $attribute
     * @return ActionServiceInterface
     */
    public function setAttribute(mixed $attribute): ActionServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * @return BaseResource|AnonymousResourceCollection|array|null
     */
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        if ($this->attribute->dto->mailer) {
            return [];
        }

        return GateEntryRecordResource::collection($this->attribute->records);
    }
}
