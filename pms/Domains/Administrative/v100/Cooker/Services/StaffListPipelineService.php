<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services;

use Domains\Administrative\v100\Cooker\Attributes\StaffListAttribute;
use Domains\Administrative\v100\Cooker\Services\Pipelines\GenerateStaffListAction;
use Domains\Administrative\v100\Cooker\Services\Pipelines\StaffListMinimumOneRequestFieldAction;
use Domains\Administrative\v100\Resources\StaffListResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class StaffListPipelineService implements PipelineServiceInterface
{
    private StaffListAttribute $attribute;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            StaffListMinimumOneRequestFieldAction::class,
            GenerateStaffListAction::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new StaffListAttribute();
        $attribute->requestDto = $data;

        return $this->setAttribute($attribute);
    }

    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return StaffListResource::collection($this->attribute->records);
    }
}
