<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services;

use Domains\Administrative\v100\Cooker\Attributes\PageResourceAttribute;
use Domains\Administrative\v100\Cooker\Services\Actions\PageResources\CollectGateEntryPageResourcesDataAction;
use Domains\Administrative\v100\Cooker\Services\Actions\PageResources\CollectGateEntryPageResourcesRecordsAction;
use Domains\Administrative\v100\Cooker\Services\Actions\PageResources\CollectStaffListPageResourcesAction;
use Domains\Administrative\v100\Cooker\Services\Actions\PageResources\GetGateEntryPageResourcesEmployeesDepartmentsAction;
use Domains\Administrative\v100\Cooker\Services\Actions\PageResources\GetGateEntryPageResourcesReaderGroupsAction;
use Domains\Administrative\v100\Cooker\Services\Actions\PageResources\GetGateEntryPageResourcesStudentsDepartmentsAction;
use Domains\Administrative\v100\Cooker\Services\Actions\PageResources\GetStaffListPageResourcesDepartmentsAction;
use Domains\Administrative\v100\Cooker\Services\Actions\PageResources\GetStaffListPageResourcesPositionsAction;
use Domains\Administrative\v100\Core\Enums\PageNames;
use Domains\Administrative\v100\Resources\PageResourcesResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

/**
 * Class PageResourceService.
 */
final class PageResourcesService implements PipelineServiceInterface
{
    private PageResourceAttribute $attribute;

    public function __construct(
        public PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return match ($this->attribute->dto->page) {
            PageNames::STAFF_LIST => [
                GetStaffListPageResourcesDepartmentsAction::class,
                GetStaffListPageResourcesPositionsAction::class,
                CollectStaffListPageResourcesAction::class,
            ],
            PageNames::GATE_ENTRY => [
                GetGateEntryPageResourcesStudentsDepartmentsAction::class,
                GetGateEntryPageResourcesEmployeesDepartmentsAction::class,
                GetGateEntryPageResourcesReaderGroupsAction::class,
                CollectGateEntryPageResourcesDataAction::class,
                CollectGateEntryPageResourcesRecordsAction::class,
            ],
        };
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new PageResourceAttribute();
        $attribute->dto = $data;

        return $this->setAttribute($attribute);
    }

    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new PageResourcesResource($this->attribute->records);
    }
}
