<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Factories;

use Domains\Administrative\v100\Cooker\Collections\StaffListCollection;
use Domains\Administrative\v100\Cooker\DTO\StaffListRequestDTO;
use Support\Models\Department;

final class StaffListFactory
{
    public static function makeStaffList(StaffListRequestDTO $dto): StaffListCollection
    {
        return Department::getStaffListRecords($dto);
    }
}
