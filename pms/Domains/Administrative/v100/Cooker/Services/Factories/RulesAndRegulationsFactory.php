<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Factories;

use Domains\Administrative\v100\Cooker\Attributes\RulesAndRegulationsAttribute;
use Support\Models\Department;

final class RulesAndRegulationsFactory
{
    public static function makeDocument(RulesAndRegulationsAttribute $attribute): void
    {
        $attribute->documentId = Department::insertRulesAndRegulationsDocument($attribute->dto->document);
    }

    public static function makeContents(array $items, int $documenId): void
    {
        foreach ($items as $data) {
            Department::insertRulesAndRegulationsContent((object) $data, $documenId);
        }
    }

    public static function makeAttachments(array $items, int $documentId): void
    {
        foreach ($items as $data) {
            Department::insertRulesAndRegulationsAttachment((object) $data, $documentId);
        }
    }

    public static function deleteDocument(string $uuid): void
    {
        Department::deleteRulesAndRegulationsDocument($uuid);
    }
}
