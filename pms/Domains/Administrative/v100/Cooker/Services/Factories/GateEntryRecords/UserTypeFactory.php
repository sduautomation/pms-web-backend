<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Factories\GateEntryRecords;

use Domains\Administrative\v100\Cooker\Attributes\GateEntryRecordsAttribute;
use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Validations\ValidateEmployeeInformationAction;
use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Validations\ValidateStudentInformationAction;
use Support\Core\Enums\UserTypes;
use Support\Core\Exceptions\ErrorResponseException;
use Support\Core\Interfaces\ActionFactoryInterface;

/**
 * Class UserTypeFactory.
 */
final class UserTypeFactory implements ActionFactoryInterface
{
    /**
     * @param GateEntryRecordsAttribute $attribute
     * @return string
     * @throws ErrorResponseException
     */
    public function make(mixed $attribute): string
    {
        return match ($attribute->dto->userType) {
            UserTypes::EMPLOYEE_USER_TYPE => ValidateEmployeeInformationAction::class,
            UserTypes::STUDENT_USER_TYPE => ValidateStudentInformationAction::class,
            default => throw new ErrorResponseException('User type not found')
        };
    }
}
