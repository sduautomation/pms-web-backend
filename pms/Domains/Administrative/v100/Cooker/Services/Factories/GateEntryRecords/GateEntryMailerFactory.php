<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Factories\GateEntryRecords;

use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Mailer\GateRecordsSendToUserByMailAction;
use Support\Core\Interfaces\ActionFactoryInterface;

final class GateEntryMailerFactory implements ActionFactoryInterface
{
    /**
     * @param mixed $attribute
     * @return string
     */
    public function make(mixed $attribute): string
    {
        return GateRecordsSendToUserByMailAction::class;
    }
}
