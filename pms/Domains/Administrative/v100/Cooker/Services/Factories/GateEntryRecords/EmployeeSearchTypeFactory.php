<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Factories\GateEntryRecords;

use Domains\Administrative\v100\Cooker\Attributes\GateEntryRecordsAttribute;
use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Getters\GetEmployeeRecordsByDepartmentAction;
use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Getters\GetEmployeesRecordsByDepartmentAction;
use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Getters\GetEmptyRecordsAction;
use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Getters\GetOwnEntryRecordsAction;
use Support\Core\Exceptions\ErrorResponseException;
use Support\Core\Interfaces\ActionFactoryInterface;

/**
 * Class EmployeeSearchTypeFactory.
 */
final class EmployeeSearchTypeFactory implements ActionFactoryInterface
{
    /**
     * @param GateEntryRecordsAttribute $attribute
     * @return string
     * @throws ErrorResponseException
     */
    public function make(mixed $attribute): string
    {
        if ($attribute->dto->department == null && $attribute->dto->username == null) {
            if ($attribute->dto->due == null) {
                $attribute->dto->due = $attribute->dto->from;
            }

            return GetOwnEntryRecordsAction::class;
        }

        if ($attribute->dto->department == null) {
            throw new ErrorResponseException('Department is not provided');
        }

        if ($attribute->dto->department !== null && $attribute->dto->username == null) {
            return GetEmployeesRecordsByDepartmentAction::class;
        }

        if ($attribute->dto->department !== null && $attribute->dto->username !== null) {
            return GetEmployeeRecordsByDepartmentAction::class;
        }

        return GetEmptyRecordsAction::class;
    }
}
