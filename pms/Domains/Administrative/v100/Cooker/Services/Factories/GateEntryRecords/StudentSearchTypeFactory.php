<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Factories\GateEntryRecords;

use Domains\Administrative\v100\Cooker\Attributes\GateEntryRecordsAttribute;
use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Getters\GetStudentRecordsAction;
use Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Getters\GetStudentRecordsByDepartmentAction;
use Support\Core\Interfaces\ActionFactoryInterface;

/**
 * Class StudentSearchTypeFactory.
 */
final class StudentSearchTypeFactory implements ActionFactoryInterface
{
    /**
     * @param mixed|GateEntryRecordsAttribute $attribute
     * @return string
     */
    public function make(mixed $attribute): string
    {
        if ($attribute->dto->department !== null) {
            return GetStudentRecordsByDepartmentAction::class;
        }

        return GetStudentRecordsAction::class;
    }
}
