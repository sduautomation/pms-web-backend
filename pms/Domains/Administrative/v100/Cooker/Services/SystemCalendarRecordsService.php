<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services;

use Domains\Administrative\v100\Cooker\Attributes\SystemCalendarRecordsAttribute;
use Domains\Administrative\v100\Cooker\Services\Actions\SystemCalendarRecords\CollectSystemCalendarAction;
use Domains\Administrative\v100\Cooker\Services\Actions\SystemCalendarRecords\GetByDefault\GetSystemAssessmentsByDefaultAction;
use Domains\Administrative\v100\Cooker\Services\Actions\SystemCalendarRecords\GetByDefault\GetSystemEventsByDefaultAction;
use Domains\Administrative\v100\Cooker\Services\Actions\SystemCalendarRecords\GetByDefault\GetSystemPermissionsByDefaultAction;
use Domains\Administrative\v100\Cooker\Services\Actions\SystemCalendarRecords\GetSystemAssessmentsAction;
use Domains\Administrative\v100\Cooker\Services\Actions\SystemCalendarRecords\GetSystemCalendarTermsAction;
use Domains\Administrative\v100\Cooker\Services\Actions\SystemCalendarRecords\GetSystemEventsAction;
use Domains\Administrative\v100\Cooker\Services\Actions\SystemCalendarRecords\GetSystemPermissionsAction;
use Domains\Administrative\v100\Cooker\Services\Actions\SystemCalendarRecords\ValidYearTermAction;
use Domains\Administrative\v100\Resources\SystemCalendarRecordsResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class SystemCalendarRecordsService implements PipelineServiceInterface
{
    private SystemCalendarRecordsAttribute $attribute;

    /**
     * @param PipelineService $service
     */
    public function __construct(
        public PipelineService $service
    ) {
    }

    /**
     * @return array[]
     */
    public function loadPipes(): array
    {
        return [
            ValidYearTermAction::class,
            GetSystemCalendarTermsAction::class,
            $this->attribute->dto->default ? GetSystemEventsByDefaultAction::class : GetSystemEventsAction::class,
            $this->attribute->dto->default ? GetSystemAssessmentsByDefaultAction::class : GetSystemAssessmentsAction::class,
            $this->attribute->dto->default ? GetSystemPermissionsByDefaultAction::class : GetSystemPermissionsAction::class,
            CollectSystemCalendarAction::class,
        ];
    }

    /**
     * @return PipelineServiceInterface
     */
    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @param mixed $data
     * @return PipelineServiceInterface
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new SystemCalendarRecordsAttribute();
        $attribute->dto = $data;

        return $this->setAttribute($attribute);
    }

    /**
     * @param mixed $attribute
     * @return PipelineServiceInterface
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * @return BaseResource|AnonymousResourceCollection|array|null
     */
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new SystemCalendarRecordsResource($this->attribute->records);
    }
}
