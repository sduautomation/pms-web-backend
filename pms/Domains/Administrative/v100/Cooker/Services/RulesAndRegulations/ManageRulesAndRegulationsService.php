<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\RulesAndRegulations;

use Domains\Administrative\v100\Cooker\Attributes\RulesAndRegulationsAttribute;
use Domains\Administrative\v100\Cooker\DTO\RulesAndRegulations\RulesAndRegulationsCreateRequestDTO;
use Domains\Administrative\v100\Cooker\Services\Actions\RulesAndRegulations\DeleteRulesAndRegulationsDocument;
use Domains\Administrative\v100\Cooker\Services\Actions\RulesAndRegulations\InsertRulesAndRegulationsAttachmentsAction;
use Domains\Administrative\v100\Cooker\Services\Actions\RulesAndRegulations\InsertRulesAndRegulationsContentsAction;
use Domains\Administrative\v100\Cooker\Services\Actions\RulesAndRegulations\InsertRulesAndRegulationsDocumentAction;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class ManageRulesAndRegulationsService implements PipelineServiceInterface
{
    private RulesAndRegulationsAttribute $attribute;

    public function __construct(
        public PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        if (request()->method() == 'DELETE') {
            return [DeleteRulesAndRegulationsDocument::class];
        }

        return [
            InsertRulesAndRegulationsDocumentAction::class,
            InsertRulesAndRegulationsContentsAction::class,
            InsertRulesAndRegulationsAttachmentsAction::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new RulesAndRegulationsAttribute();

        if ($data instanceof RulesAndRegulationsCreateRequestDTO) {
            $attribute->dto = $data;
        } else {
            $attribute->uuid = $data;
        }

        return $this->setAttribute($attribute);
    }

    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return [];
    }
}
