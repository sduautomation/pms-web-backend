<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\RulesAndRegulations;

use Domains\Administrative\v100\Cooker\Attributes\RulesAndRegulationsGetAttribute;
use Domains\Administrative\v100\Cooker\Services\Pipelines\CollectRulesAndRegulationsGetResponse;
use Domains\Administrative\v100\Cooker\Services\Pipelines\CollectRulesAndRegulationsShowResponse;
use Domains\Administrative\v100\Cooker\Services\Pipelines\GetRulesAndRegulationsAttachments;
use Domains\Administrative\v100\Cooker\Services\Pipelines\GetRulesAndRegulationsCategories;
use Domains\Administrative\v100\Cooker\Services\Pipelines\GetRulesAndRegulationsContents;
use Domains\Administrative\v100\Cooker\Services\Pipelines\GetRulesAndRegulationsDocumentsAction;
use Domains\Administrative\v100\Resources\RulesAndRegulationsRecordsResource;
use Domains\Administrative\v100\Resources\RulesAndRegulationsShowResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

/**
 * Class GetRulesAndRegulationsPipelineService.
 */
final class GetRulesAndRegulationsPipelineService implements PipelineServiceInterface
{
    private RulesAndRegulationsGetAttribute $attribute;

    public function __construct(
        public PipelineService $service
    ) {
    }

    /**
     * @return array
     */
    public function loadPipes(): array
    {
        if ($this->attribute->dto->uuid == null) {
            return [
                GetRulesAndRegulationsDocumentsAction::class,
                GetRulesAndRegulationsCategories::class,
                CollectRulesAndRegulationsGetResponse::class,
            ];
        }

        return [
            GetRulesAndRegulationsDocumentsAction::class,
            GetRulesAndRegulationsContents::class,
            GetRulesAndRegulationsAttachments::class,
            CollectRulesAndRegulationsShowResponse::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new RulesAndRegulationsGetAttribute();
        $attribute->dto = $data;

        return $this->setAttribute($attribute);
    }

    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        if ($this->attribute->dto->uuid == null) {
            return new RulesAndRegulationsRecordsResource($this->attribute->records);
        }

        return new RulesAndRegulationsShowResource($this->attribute->recordsByUuid);
    }
}
