<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Actions\PageResources;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\PageResourceAttribute;
use Domains\Administrative\v100\Cooker\DTO\PageResources\StaffListPageResourcesResponseDTO;

final class CollectStaffListPageResourcesAction
{
    public function handle(PageResourceAttribute $attribute, Closure $next): mixed
    {
        $attribute->records = new StaffListPageResourcesResponseDTO(
            departments: $attribute->staffListDepartments,
            positions: $attribute->staffListPositions
        );

        return $next($attribute);
    }
}
