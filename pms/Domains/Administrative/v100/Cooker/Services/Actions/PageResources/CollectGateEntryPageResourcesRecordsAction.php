<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Actions\PageResources;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\PageResourceAttribute;
use Domains\Administrative\v100\Cooker\DTO\PageResources\GateEntryPageResourceResponseDTO;

final class CollectGateEntryPageResourcesRecordsAction
{
    public function handle(PageResourceAttribute $attribute, Closure $next): mixed
    {
        $attribute->records = new GateEntryPageResourceResponseDTO(
            employees: $attribute->gateEntryEmployees,
            students: $attribute->gateEntryStudents
        );

        return $next($attribute);
    }
}
