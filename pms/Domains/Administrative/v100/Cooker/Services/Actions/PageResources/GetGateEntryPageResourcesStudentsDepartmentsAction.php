<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Actions\PageResources;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\PageResourceAttribute;
use Support\Models\Department;

final class GetGateEntryPageResourcesStudentsDepartmentsAction
{
    public function handle(PageResourceAttribute $attribute, Closure $next): mixed
    {
        $attribute->gateEntryStudentsDepartments = Department::getGateEntryStudentsDepartments();

        return $next($attribute);
    }
}
