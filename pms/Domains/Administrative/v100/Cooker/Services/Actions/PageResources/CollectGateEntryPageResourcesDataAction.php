<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Actions\PageResources;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\PageResourceAttribute;
use Domains\Administrative\v100\Cooker\DTO\PageResources\GateEntryPageResourceDataDTO;

final class CollectGateEntryPageResourcesDataAction
{
    public function handle(PageResourceAttribute $attribute, Closure $next): mixed
    {
        $attribute->gateEntryEmployees = new GateEntryPageResourceDataDTO(
            departments: $attribute->gateEntryEmployeeDepartments,
            types: $attribute->types
        );

        $attribute->gateEntryStudents = new GateEntryPageResourceDataDTO(
            departments: $attribute->gateEntryStudentsDepartments,
            types: $attribute->types
        );

        return $next($attribute);
    }
}
