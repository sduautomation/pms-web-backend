<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Actions\RulesAndRegulations;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\RulesAndRegulationsAttribute;
use Domains\Administrative\v100\Cooker\Services\Factories\RulesAndRegulationsFactory;

final class InsertRulesAndRegulationsAttachmentsAction
{
    /**
     * @param RulesAndRegulationsAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public function handle(RulesAndRegulationsAttribute $attribute, Closure $next): mixed
    {
        RulesAndRegulationsFactory::makeAttachments($attribute->dto->attachments, $attribute->documentId);

        return $next($attribute);
    }
}
