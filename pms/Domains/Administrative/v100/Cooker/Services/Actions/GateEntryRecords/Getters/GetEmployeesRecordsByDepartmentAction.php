<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Getters;

use Domains\Administrative\v100\Cooker\Attributes\GateEntryRecordsAttribute;
use Domains\Administrative\v100\Models\Administrative;
use Support\Core\Interfaces\ActionInterface;

final class GetEmployeesRecordsByDepartmentAction implements ActionInterface
{
    /**
     * @param GateEntryRecordsAttribute $attribute
     */
    public function run(mixed $attribute): void
    {
        $attribute->records = Administrative::getEmployeesEntryRecordsByDepartments($attribute->dto, $attribute->userId);
    }
}
