<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Validations;

use Domains\Administrative\v100\Cooker\Attributes\GateEntryRecordsAttribute;
use Support\Core\Exceptions\ErrorResponseException;
use Support\Core\Interfaces\ActionInterface;
use Support\Models\Employee;

final class ValidateEmployeeInformationAction implements ActionInterface
{
    /**
     * @param mixed|GateEntryRecordsAttribute $attribute
     * @throws ErrorResponseException
     */
    public function run(mixed $attribute): void
    {
        if ($attribute->dto->username !== null) {
            $search = Employee::scopeByUsername($attribute->dto->username);

            if ($search !== null) {
                $attribute->searchId = $search->emp_id;
            } else {
                throw new ErrorResponseException('Employee ID (username) is not found');
            }
        }

        if ($attribute->dto->username !== null && $attribute->dto->due == null) {
            throw new ErrorResponseException('Due date is not provided');
        }
    }
}
