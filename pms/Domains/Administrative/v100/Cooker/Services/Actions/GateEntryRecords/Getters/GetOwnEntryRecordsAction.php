<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Getters;

use Domains\Administrative\v100\Cooker\Attributes\GateEntryRecordsAttribute;
use Domains\Administrative\v100\Models\Administrative;
use Support\Core\Interfaces\ActionInterface;

final class GetOwnEntryRecordsAction implements ActionInterface
{
    /**
     * @param mixed|GateEntryRecordsAttribute $attribute
     */
    public function run(mixed $attribute): void
    {
        $attribute->records = Administrative::getOwnEntryRecords($attribute->dto, $attribute->userId);
    }
}
