<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Mailer;

use Domains\Administrative\v100\Cooker\Attributes\GateEntryRecordsAttribute;
use Domains\Administrative\v100\Mail\SendGateRecordsToUser;
use Illuminate\Support\Facades\Mail;
use Support\Core\Interfaces\ActionInterface;
use Support\Models\Employee;

final class GateRecordsSendToUserByMailAction implements ActionInterface
{
    /**
     * @param mixed|GateEntryRecordsAttribute $attribute
     */
    public function run(mixed $attribute): void
    {
        if ($attribute->dto->mailer) {
            $info = Employee::getEmployeeInformation($attribute->userId);

            if ($info !== null) {
                // $info->email
                Mail::to('kurmangazy.kongratbayev@sdu.edu.kz')
                    ->send(new SendGateRecordsToUser(
                        $attribute->records,
                        $attribute->dto
                    ));
            }
        }
    }
}
