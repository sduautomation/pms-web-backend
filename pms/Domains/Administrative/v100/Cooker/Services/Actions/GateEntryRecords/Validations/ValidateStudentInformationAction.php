<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Actions\GateEntryRecords\Validations;

use Domains\Administrative\v100\Cooker\Attributes\GateEntryRecordsAttribute;
use Support\Core\Exceptions\ErrorResponseException;
use Support\Core\Interfaces\ActionInterface;
use Support\Models\Student;

final class ValidateStudentInformationAction implements ActionInterface
{
    /**
     * @param mixed|GateEntryRecordsAttribute $attribute
     * @throws ErrorResponseException
     */
    public function run(mixed $attribute): void
    {
        if ($attribute->dto->username == null || strlen($attribute->dto->username) == 0) {
            throw new ErrorResponseException('Student username (Student ID) is not provided');
        }

        if (! Student::isValidUsername($attribute->dto->username)) {
            throw new ErrorResponseException('Student username (Student ID) is not found');
        }

        if ($attribute->dto->due == null) {
            throw new ErrorResponseException('Due date is not provided');
        }

        if ($attribute->userId == 0) {
            throw new ErrorResponseException('Authentication is failed');
        }
    }
}
