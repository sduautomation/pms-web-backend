<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Actions\SystemCalendarRecords\GetByDefault;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\SystemCalendarRecordsAttribute;
use Support\Models\Department;

final class GetSystemAssessmentsByDefaultAction
{
    /**
     * @param SystemCalendarRecordsAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public function handle(SystemCalendarRecordsAttribute $attribute, Closure $next): mixed
    {
        $attribute->recordsAssessments = Department::getSystemCalendarAssessmentsByDefault($attribute->dto->emp_id);

        return $next($attribute);
    }
}
