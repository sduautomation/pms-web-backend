<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Actions\SystemCalendarRecords;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\SystemCalendarRecordsAttribute;
use Support\Models\Department;

/**
 * Class GetSystemCalendarPermissionsAction.
 */
final class GetSystemPermissionsAction
{
    /**
     * @param SystemCalendarRecordsAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public function handle(SystemCalendarRecordsAttribute $attribute, Closure $next)
    {
        $attribute->recordsPermissions = Department::getSystemCalendarPermissions($attribute->dto->year, $attribute->dto->term, $attribute->dto->emp_id);

        return $next($attribute);
    }
}
