<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Actions\SystemCalendarRecords;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\SystemCalendarRecordsAttribute;
use Support\Models\Department;

final class CollectSystemCalendarRecordsAction
{
    /**
     * @param SystemCalendarRecordsAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public function handle(SystemCalendarRecordsAttribute $attribute, Closure $next)
    {
        $attribute->systemCalendarRecords = Department::collectSystemCalendarRecords($attribute);

        return $next($attribute);
    }
}
