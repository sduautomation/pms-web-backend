<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Actions\SystemCalendarRecords;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\SystemCalendarRecordsAttribute;
use Support\Models\Department;

/**
 * Class GetSystemEventsAction.
 */
final class GetSystemEventsAction
{
    /**
     * @param SystemCalendarRecordsAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public function handle(SystemCalendarRecordsAttribute $attribute, Closure $next)
    {
        $attribute->recordsEvents = Department::getSystemCalendarEvents($attribute->dto->year, $attribute->dto->term);

        return $next($attribute);
    }
}
