<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Actions\SystemCalendarRecords;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\SystemCalendarRecordsAttribute;
use Domains\Administrative\v100\Cooker\DTO\SystemCalendarRequest\SystemCalendarResponseDTO;

/**
 * Class CollectSystemCalendarAction.
 */
final class CollectSystemCalendarAction
{
    /**
     * @param SystemCalendarRecordsAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public function handle(SystemCalendarRecordsAttribute $attribute, Closure $next): mixed
    {
        $attribute->records = new SystemCalendarResponseDTO(
            terms: $attribute->terms,
            records: [
                'events' => $attribute->recordsEvents,
                'permissions' => $attribute->recordsPermissions,
                'assessments' => $attribute->recordsAssessments,
            ]
        );

        return $next($attribute);
    }
}
