<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Actions\SystemCalendarRecords\GetByDefault;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\SystemCalendarRecordsAttribute;
use Support\Models\Department;

final class GetSystemEventsByDefaultAction
{
    public function handle(SystemCalendarRecordsAttribute $attribute, Closure $next)
    {
        $attribute->recordsEvents = Department::getSystemCalendarEventsByDefault();

        return $next($attribute);
    }
}
