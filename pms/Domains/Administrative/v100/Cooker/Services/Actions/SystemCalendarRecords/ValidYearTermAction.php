<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Actions\SystemCalendarRecords;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\SystemCalendarRecordsAttribute;
use Domains\Administrative\v100\Core\Exceptions\ErrorResponseException;

/**
 * Class ValidYearTermAction.
 */
final class ValidYearTermAction
{
    /**
     * @param SystemCalendarRecordsAttribute $attribute
     * @param Closure $next
     * @return mixed
     * @throws ErrorResponseException
     */
    public function handle(SystemCalendarRecordsAttribute $attribute, Closure $next)
    {
        if (! $attribute->dto->default & ($attribute->dto->term == null || $attribute->dto->year == null)) {
            throw new ErrorResponseException('One of the year or term is null (empty value).');
        }

        return $next($attribute);
    }
}
