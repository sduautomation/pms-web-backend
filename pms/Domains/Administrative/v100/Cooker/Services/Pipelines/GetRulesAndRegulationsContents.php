<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Pipelines;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\RulesAndRegulationsGetAttribute;
use Support\Models\Department;

final class GetRulesAndRegulationsContents
{
    /**
     * @param RulesAndRegulationsGetAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public static function handle(RulesAndRegulationsGetAttribute $attribute, Closure $next): mixed
    {
        $attribute->contents = Department::getRulesAndRegulationsContents($attribute->dto->uuid);

        return $next($attribute);
    }
}
