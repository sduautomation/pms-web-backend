<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Pipelines;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\RulesAndRegulationsGetAttribute;
use Domains\Administrative\v100\Cooker\DTO\RulesAndRegulations\RulesAndRegulationsResponseDTO;

final class CollectRulesAndRegulationsGetResponse
{
    public function handle(RulesAndRegulationsGetAttribute $attribute, Closure $next): mixed
    {
        $attribute->records = new RulesAndRegulationsResponseDTO(
            categories: $attribute->categories,
            records: $attribute->rulesAndRegulationsRecords
        );

        return $next($attribute);
    }
}
