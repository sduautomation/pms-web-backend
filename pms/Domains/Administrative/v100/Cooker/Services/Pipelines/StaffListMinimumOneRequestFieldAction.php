<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Pipelines;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\StaffListAttribute;
use Support\Core\Exceptions\ErrorResponseException;

final class StaffListMinimumOneRequestFieldAction
{
    /**
     * @throws ErrorResponseException
     */
    public function handle(StaffListAttribute $attribute, Closure $next): mixed
    {
        if (
            strlen($attribute->requestDto->name) == 0 &&
            strlen($attribute->requestDto->surname) == 0 &&
            strlen($attribute->requestDto->departmentCode) == 0 &&
            $attribute->requestDto->positionId == 0 &&
            ! $attribute->requestDto->includeSubDepartments
        ) {
            throw new ErrorResponseException('Minimum one field must be filled, before search');
        }

        if (strlen($attribute->requestDto->departmentCode) == 0 && $attribute->requestDto->includeSubDepartments) {
            throw new ErrorResponseException('Please, select department!');
        }

        return $next($attribute);
    }
}
