<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Pipelines;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\StaffListAttribute;
use Domains\Administrative\v100\Cooker\Services\Factories\StaffListFactory;

final class GenerateStaffListAction
{
    public function handle(StaffListAttribute $attribute, Closure $next): mixed
    {
        $attribute->records = StaffListFactory::makeStaffList(
            $attribute->requestDto
        );

        return $next($attribute);
    }
}
