<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Pipelines;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\RulesAndRegulationsGetAttribute;
use Support\Models\Department;

final class GetRulesAndRegulationsDocumentsAction
{
    /**
     * @param RulesAndRegulationsGetAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public function handle(RulesAndRegulationsGetAttribute $attribute, Closure $next): mixed
    {
        $attribute->rulesAndRegulationsRecords = Department::getRulesAndRegulationsDocuments($attribute->dto->uuid);

        return $next($attribute);
    }
}
