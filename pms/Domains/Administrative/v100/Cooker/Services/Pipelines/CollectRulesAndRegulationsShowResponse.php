<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Pipelines;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\RulesAndRegulationsGetAttribute;
use Domains\Administrative\v100\Cooker\DTO\RulesAndRegulations\RulesAndRegulationsShowResponseDTO;

final class CollectRulesAndRegulationsShowResponse
{
    public static function handle(RulesAndRegulationsGetAttribute $attribute, Closure $next): mixed
    {
        $attribute->recordsByUuid = new RulesAndRegulationsShowResponseDTO(
            contents: $attribute->contents,
            attachments: $attribute->attachments,
            rulesAndRegulationsRecords: $attribute->rulesAndRegulationsRecords
        );

        return $next($attribute);
    }
}
