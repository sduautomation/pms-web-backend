<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Cooker\Services\Pipelines;

use Closure;
use Domains\Administrative\v100\Cooker\Attributes\RulesAndRegulationsGetAttribute;
use Support\Models\Department;

final class GetRulesAndRegulationsAttachments
{
    /**
     * @param RulesAndRegulationsGetAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public static function handle(RulesAndRegulationsGetAttribute $attribute, Closure $next): mixed
    {
        $attribute->attachments = Department::getRulesAndRegulationsAttachments($attribute->dto->uuid);

        return $next($attribute);
    }
}
