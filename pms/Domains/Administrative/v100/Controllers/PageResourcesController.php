<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Administrative\v100\Cooker\Services\PageResourcesService;
use Domains\Administrative\v100\Requests\PageResourceRequest;
use Illuminate\Http\JsonResponse;

/**
 * Class PageResourcesController.
 */
final class PageResourcesController extends Controller
{
    /**
     * @OA\Post (
     *     summary="Page resources records",
     *     path="/api/v100/administrative/resources",
     *     operationId="pageResourcesRecords",
     *     tags={"administrative", "v100"},
     *     description="Page resources records",
     *     parameters={
     *      {"page": "Page", "in": "header", "type": "string", "required": true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(ref="#/components/schemas/PageResourceRequest")
     *      )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Resources are loaded",
     *          @OA\JsonContent(ref="#/components/schemas/PageResourcesResource")
     *      )
     * )
     */
    public function __invoke(PageResourceRequest $request, PageResourcesService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Resources are loaded',
            data: $resource
        );
    }
}
