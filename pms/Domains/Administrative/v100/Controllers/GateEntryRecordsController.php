<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Administrative\v100\Cooker\Services\GateEntryRecordsActionService;
use Domains\Administrative\v100\Requests\GateEntryRecordsRequest;
use Illuminate\Http\JsonResponse;
use Support\Core\Exceptions\ErrorResponseException;

/**
 * Class GateEntryRecordsController.
 */
final class GateEntryRecordsController extends Controller
{
    /**
     * @OA\Post(
     *     summary="Gate entry records",
     *     path="/api/v100/administrative/gate-entry",
     *     operationId="gateEntryRecords",
     *     tags={"administrative", "v100"},
     *     description="Gate entry records",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/GateEntryRecordsRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Gate entry records was successfully returned",
     *          @OA\JsonContent(ref="#/components/schemas/GateEntryRecordResource"),
     *     ),
     * )
     * @param GateEntryRecordsRequest $request
     * @param GateEntryRecordsActionService $service
     * @return JsonResponse
     * @throws ErrorResponseException
     */
    public function __invoke(GateEntryRecordsRequest $request, GateEntryRecordsActionService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Resources are loaded',
            data: $resource
        );
    }
}
