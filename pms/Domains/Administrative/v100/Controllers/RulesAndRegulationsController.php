<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Administrative\v100\Cooker\Services\RulesAndRegulations\GetRulesAndRegulationsPipelineService;
use Domains\Administrative\v100\Cooker\Services\RulesAndRegulations\ManageRulesAndRegulationsService;
use Domains\Administrative\v100\Requests\RulesAndRegualations\DeleteRulesAndRegulationsRequest;
use Domains\Administrative\v100\Requests\RulesAndRegualations\GetRulesAndRegulationsRequest;
use Domains\Administrative\v100\Requests\RulesAndRegualations\ManageRulesAndRegulationsRequest;
use Illuminate\Http\JsonResponse;

/*
 * Class RulesAndRegulationsController
 */
final class RulesAndRegulationsController extends Controller
{
    /**
     * @OA\Get (
     *     summary="Rules and Regulations resources records",
     *     path="api/v100/administrative/rules-and-regulations",
     *     operationId="rulesAndRegulationsRecords",
     *     tags={"adminitsrative", "v100"},
     *     description="Rules and Regulations records",
     *     parameters={
     *      {"name": "Authorization", "in": "header", "type": "string", "required": true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(ref="#/components/schemas/GetRulesAndRegulationsRequest")
     *      )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Resources are loaded",
     *          @OA\JsonContent(ref="#/components/schemas/RulesAndRegulationsRecordsResource")
     *      )
     * )
     */
    /**
     * @param GetRulesAndRegulationsPipelineService $service
     * @param GetRulesAndRegulationsRequest $request
     * @return JsonResponse
     */
    public function index(GetRulesAndRegulationsPipelineService $service, GetRulesAndRegulationsRequest $request): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Resources are loaded',
            data: $resource
        );
    }

    /**
     * @OA\Get (
     *     summary="Rules and Regulations resources records",
     *     path="api/v100/administrative/rules-and-regulations/:uuid",
     *     operationId="rulesAndRegulationsRecords",
     *     tags={"adminitsrative", "v100"},
     *     description="Rules and Regulations records",
     *     parameters={
     *      {"name": "Authorization", "in": "header", "type": "string", "required": true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(ref="#/components/schemas/GetRulesAndRegulationsRequest")
     *      )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Resources are loaded",
     *          @OA\JsonContent(ref="#/components/schemas/RulesAndRegulationsShowResource")
     *      )
     * )
     */
    /**
     * @param GetRulesAndRegulationsPipelineService $service
     * @param GetRulesAndRegulationsRequest $request
     * @return JsonResponse
     */
    public function show(GetRulesAndRegulationsPipelineService $service, GetRulesAndRegulationsRequest $request): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Resources are loaded',
            data: $resource
        );
    }

    /**
     * @OA\Post (
     *     summary="Rules and Regulations store",
     *     path="api/v100/administrative/rules-and-regulations",
     *     operationId="rulesAndRegulationsRecords",
     *     tags={"adminitsrative", "v100"},
     *     description="Rules and Regulations records",
     *     parameters={
     *      {"name": "Authorization", "in": "header", "type": "string", "required": true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(ref="#/components/schemas/ManageRulesAndRegulationsRequest")
     *      )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Resource is inserted",
     *          @OA\Items()
     *     )
     * )
     */
    /**
     * @param ManageRulesAndRegulationsService $service
     * @param ManageRulesAndRegulationsRequest $request
     * @return JsonResponse
     */
    public function store(ManageRulesAndRegulationsService $service, ManageRulesAndRegulationsRequest $request): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Resource is inserted',
            data: $resource
        );
    }

    /**
     * @OA\Delete (
     *     summary="Rules and Regulations delete",
     *     path="api/v100/administrative/rules-and-regulations/:uuid",
     *     operationId="rulesAndRegulationsRecords",
     *     tags={"adminitsrative", "v100"},
     *     description="Rules and Regulations records",
     *     parameters={
     *      {"name": "Authorization", "in": "header", "type": "string", "required": true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(ref="#/components/schemas/DeleteRulesAndRegulationsRequest")
     *      )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Resource is deleted",
     *          @OA\Items()
     *      )
     * )
     */
    /**
     * @param ManageRulesAndRegulationsService $service
     * @param DeleteRulesAndRegulationsRequest $request
     * @return JsonResponse
     */
    public function delete(ManageRulesAndRegulationsService $service, DeleteRulesAndRegulationsRequest $request): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Resource is deleted',
            data: $resource
        );
    }
}
