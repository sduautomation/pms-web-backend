<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Administrative\v100\Cooker\Services\SystemCalendarRecordsService;
use Domains\Administrative\v100\Requests\SystemCalendarRecordsRequest;
use Illuminate\Http\JsonResponse;

/**
 * Class SystemCalendarRecordsController.
 */
final class SystemCalendarRecordsController extends Controller
{
    /**
     * @OA\Get (
     *     summary="System calendar records",
     *     path="/api/v100/administrative/system-calendar",
     *     operationId="SystemCalendarController",
     *     tags={"administrative", "v100"},
     *     description="System calendar records",
     *     parameters={
     *      {"name": "Authorization", "in": "header", "type": "string", "required": true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(ref="#/components/schemas/SystemCalendarRecordsRequest")
     *      )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Resources are loaded",
     *          @OA\JsonContent(ref="#/components/schemas/SystemCalendarRecordsResource")
     *      )
     * )
     */
    public function __invoke(SystemCalendarRecordsRequest $request, SystemCalendarRecordsService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Resources are loaded',
            data: $resource
        );
    }
}
