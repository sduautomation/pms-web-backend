<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Administrative\v100\Cooker\Services\StaffListPipelineService;
use Domains\Administrative\v100\Requests\StaffListRequest;
use Illuminate\Http\JsonResponse;

final class StaffListController extends Controller
{
    /**
     * @OA\Get(
     *     summary="Staff list records",
     *     path="/api/v100/administrative/staff_list",
     *     operationId="staffListController",
     *     tags={"administrative", "v100"},
     *     description="Staff list records",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/StaffListRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Staff list records was successfully returned",
     *          @OA\JsonContent(ref="#/components/schemas/StaffListResource"),
     *     )
     * )
     */
    public function __invoke(StaffListRequest $request, StaffListPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Resources are loaded',
            data: $resource
        );
    }
}
