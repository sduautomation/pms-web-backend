<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Mail;

use Domains\Administrative\v100\Cooker\Collections\GateEntryRecordsCollection;
use Domains\Administrative\v100\Cooker\DTO\GateEntryRequestDTO;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

final class SendGateRecordsToUser extends Mailable
{
    use Queueable, SerializesModels;

    protected GateEntryRecordsCollection $records;
    protected GateEntryRequestDTO $dto;

    /**
     * SendGateRecordsToUser constructor.
     * @param GateEntryRecordsCollection $records
     * @param GateEntryRequestDTO $dto
     */
    public function __construct(GateEntryRecordsCollection $records, GateEntryRequestDTO $dto)
    {
        $this->records = $records;
        $this->dto = $dto;
    }

    /**
     * @return SendGateRecordsToUser
     */
    public function build(): self
    {
        return $this
            ->view('mail.administrative.gate-entry')
            ->with([
                'records' => $this->records,
                'department' => $this->dto->department,
                'username' => $this->dto->username,
                'from' => $this->dto->from,
                'due' => $this->dto->due,
            ]);
    }
}
