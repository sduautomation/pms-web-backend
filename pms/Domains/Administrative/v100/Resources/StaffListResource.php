<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Resources;

use Domains\Administrative\v100\Cooker\Entities\StaffListEntity;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @mixin StaffListEntity
 * @OA\Schema(
 *     @OA\Property(
 *          property="emp_id",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="full_name",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="full_name_native",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="department_position",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="working_status",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="staff",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="outer_email",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="inner_telephone",
 *          type="string"
 *      )
 * )
 */
final class StaffListResource extends BaseResource
{
    #[ArrayShape(['emp_id' => 'string|null', 'full_name' => 'null|string', 'full_name_native' => 'null|string', 'department_position' => 'null|string', 'working_status' => 'null|string', 'staff' => 'null|string', 'outer_email' => 'null|string', 'inner_telephone' => 'null|string'])]
    public function getResponseArray(): array
    {
        return [
            'emp_id' => $this->employeeId,
            'full_name' => $this->fullName,
            'full_name_native' => $this->fullNameNative,
            'department_position' => $this->departmentPosition,
            'working_status' => $this->workingStatus,
            'staff' => $this->staff,
            'outer_email' => $this->outerEmail,
            'inner_telephone' => $this->innerTelephone,
        ];
    }
}
