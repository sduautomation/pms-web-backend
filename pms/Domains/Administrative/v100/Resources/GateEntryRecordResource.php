<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Resources;

use Domains\Administrative\v100\Cooker\ValueObjects\GateEntryRecordValueObject;
use Support\Resources\BaseResource;

/**
 * Class GateEntryRecordResource.
 * @mixin GateEntryRecordValueObject
 * @OA\Schema(
 *     @OA\Property(
 *          property="full_name",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="user_token",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="access_id_in",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="access_id_out",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="card_number",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="work_time",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="day_of_week",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="access_date_in",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="access_date_out",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="access_time_in",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="access_time_out",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="reader_type_in",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="reader_type_out",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="week_day_title",
 *          type="string"
 *      )
 * )
 */
final class GateEntryRecordResource extends BaseResource
{
    /**
     * @return array
     */
    public function getResponseArray(): array
    {
        return [
            'full_name' => $this->fullName,
            'user_token' => $this->userToken,
            'access_id_in' => $this->accessIdIn,
            'access_id_out' => $this->accessIdOut,
            'card_number' => $this->cardNumber,
            'work_time' => $this->workTime,
            'day_of_week' => $this->dayOfWeek,
            'access_date_in' => $this->accessDateIn?->toDateString(),
            'access_date_out' => $this->accessDateOut?->toDateString(),
            'access_time_in' => $this->accessTimeIn,
            'access_time_out' => $this->accessTimeOut,
            'reader_type_in' => $this->readerTypeIn,
            'reader_type_out' => $this->readerTypeOut,
            'week_day_title' => $this->weekDayTitle,
        ];
    }
}
