<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Resources;

use Domains\Administrative\v100\Cooker\DTO\RulesAndRegulations\RulesAndRegulationsShowResponseDTO;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @mixin RulesAndRegulationsShowResponseDTO
 * @OA\Schema(
 *     @OA\Property(
 *          property="records",
 *          type="object",
 *              @OA\Property(property="uuid", type="string"),
 *              @OA\Property(property="title", type="string"),
 *              @OA\Property(property="description", type="string"),
 *              @OA\Property(property="category_name", type="string"),
 *              @OA\Property(property="meta", type="string"),
 *              @OA\Property(property="created_at", type="string"),
 *              @OA\Property(property="updated_at", type="string")
 *      ),
 *     @OA\Property(
 *          property="contents",
 *          type="array",
 *          @OA\Items (
 *              @OA\Property(property="uuid", type="string"),
 *              @OA\Property(property="docid", type="int"),
 *              @OA\Property(property="langid", type="int"),
 *              @OA\Property(property="verid", type="int"),
 *              @OA\Property(property="language_title", type="string"),
 *              @OA\Property(property="version_title", type="string"),
 *              @OA\Property(property="image", type="string"),
 *              @OA\Property(property="content", type="string"),
 *              @OA\Property(property="active", type="boolean"),
 *              @OA\Property(property="created_at", type="string"),
 *              @OA\Property(property="updated_at", type="string"),
 *          )
 *      ),
 *     @OA\Property(
 *          property="attachments",
 *          type="array",
 *          @OA\Items (
 *              @OA\Property(property="uuid", type="string"),
 *              @OA\Property(property="docid", type="int"),
 *              @OA\Property(property="langid", type="int"),
 *              @OA\Property(property="versid", type="int"),
 *              @OA\Property(property="language_title", type="string"),
 *              @OA\Property(property="version_title", type="string"),
 *              @OA\Property(property="title", type="string"),
 *              @OA\Property(property="path", type="string"),
 *              @OA\Property(property="extension", type="string"),
 *              @OA\Property(property="active", type="boolean"),
 *              @OA\Property(property="created_at", type="string"),
 *              @OA\Property(property="updated_at", type="string"),
 *          )
 *      )
 * )
 */
final class RulesAndRegulationsShowResource extends BaseResource
{
    /**
     * @return array
     */
    #[ArrayShape(['records' => 'mixed', 'contents' => "\Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations\RulesAndRegulationsContentsCollection", 'attachments' => "\Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations\RulesAndRegulationsAttachmentsCollection"])]
    public function getResponseArray(): array
    {
        return [
            'records' => $this->rulesAndRegulationsRecords[0],
            'contents' => $this->contents,
            'attachments' => $this->attachments,
        ];
    }
}
