<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Resources;

use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * Class SystemCalendarRecordsResource.
 * @OA\Schema(
 *     schema="SystemCalendarRecordsResource",
 *     @OA\Property(
 *          property="terms",
 *          type="object",
 *          @OA\Property (property="year", type="string"),
 *          @OA\Property (property="term", type="string"),
 *      ),
 *     @OA\Property(
 *          property="records",
 *          type="object",
 *          @OA\Property (
 *              property="events",
 *              type="object",
 *              @OA\Property (property="title", type="string"),
 *              @OA\Property (property="year", type="string"),
 *              @OA\Property (property="term", type="string"),
 *              @OA\Property (property="start_date", type="string"),
 *              @OA\Property (property="end_date", type="string"),
 *              @OA\Property (property="is_open", type="boolean"),
 *          ),
 *          @OA\Property (
 *              property="permissions",
 *              type="object",
 *              @OA\Property (property="title", type="string"),
 *              @OA\Property (property="start_date", type="string"),
 *              @OA\Property (property="end_date", type="string"),
 *              @OA\Property (property="is_open", type="boolean"),
 *          ),
 *          @OA\Property (
 *              property="assessments",
 *              type="object",
 *              @OA\Property (property="title", type="string"),
 *              @OA\Property (property="year", type="string"),
 *              @OA\Property (property="term", type="string"),
 *              @OA\Property (property="start_date", type="string"),
 *              @OA\Property (property="end_date", type="string"),
 *              @OA\Property (property="is_open", type="boolean"),
 *              @OA\Property (property="key", type="string"),
 *              @OA\Property (property="entry_year", type="string"),
 *              @OA\Property (property="is_blocking", type="string"),
 *              @OA\Property (property="emp_id", type="string"),
 *              @OA\Property (property="prog_code", type="string"),
 *              @OA\Property (property="dep_code", type="string"),
 *              @OA\Property (property="classof", type="string"),
 *              @OA\Property (property="edu_level", type="string"),
 *          ),
 *      ),
 * )
 */
final class SystemCalendarRecordsResource extends BaseResource
{
    /**
     * @return array
     */
    #[ArrayShape(['terms' => 'null|object', 'records' => 'null|object'])]
    public function getResponseArray(): array
    {
        return [
            'terms' => $this->terms,
            'records' =>$this->records,
        ];
    }
}
