<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Resources;

use Domains\Administrative\v100\Cooker\DTO\RulesAndRegulations\RulesAndRegulationsResponseDTO;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @mixin RulesAndRegulationsResponseDTO
 * @OA\Schema(
 *     @OA\Property(
 *          property="records",
 *          type="array",
 *          @OA\Items (
 *              @OA\Property(property="uuid", type="string"),
 *              @OA\Property(property="title", type="string"),
 *              @OA\Property(property="description", type="string"),
 *              @OA\Property(property="category_name", type="string"),
 *              @OA\Property(property="meta", type="string"),
 *              @OA\Property(property="created_at", type="string"),
 *              @OA\Property(property="updated_at", type="string")
 *          )
 *      ),
 *     @OA\Property(
 *          property="categories",
 *          type="array",
 *          @OA\Items (
 *              @OA\Property(property="uuid", type="string"),
 *              @OA\Property(property="title", type="string"),
 *              @OA\Property(property="meta", type="string"),
 *              @OA\Property(property="created_at", type="string"),
 *              @OA\Property(property="updated_at", type="string"),
 *          )
 *      )
 * )
 */
final class RulesAndRegulationsRecordsResource extends BaseResource
{
    /**
     * @return array
     */
    #[ArrayShape(['records' => "\Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations\RulesAndRegulationsRecordsCollection", 'categories' => "\Domains\Administrative\v100\Cooker\Collections\RulesAndRegulations\RulesAndRegulationsCategoriesCollection"])]
    public function getResponseArray(): array
    {
        return [
            'records' => $this->records,
            'categories' => $this->categories,
        ];
    }
}
