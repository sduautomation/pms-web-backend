<?php

declare(strict_types=1);

namespace Domains\Administrative\v100\Resources;

use Domains\Administrative\v100\Cooker\Attributes\PageResourceAttribute;
use Support\Resources\BaseResource;

/**
 * Class PageResourcesResource.
 * @mixin PageResourceAttribute
 * @OA\Schema(
 *     schema="PageResourcesResource",
 *     @OA\Property(
 *          property="departments",
 *          type="array",
 *          @OA\Items (
 *              @OA\Property (property="id", type="int"),
 *              @OA\Property (property="title", type="string"),
 *              @OA\Property (property="code", type="string"),
 *              @OA\Property (property="level", type="int")
 *          )
 *      ),
 *     @OA\Property(
 *          property="positions",
 *          type="array",
 *          @OA\Items(
 *              @OA\Property (property="id", type="int"),
 *              @OA\Property (property="title", type="string")
 *          )
 *      ),
 *     @OA\Property(
 *          property="employees",
 *          type="object",
 *          @OA\Property(
 *              property="departments", type="array",
 *              @OA\Items(
 *                  @OA\Property (property="id", type="int"),
 *                  @OA\Property (property="title", type="string"),
 *                  @OA\Property (property="code", type="string"),
 *                  @OA\Property (property="level", type="int")
 *              )
 *          ),
 *          @OA\Property(
 *              property="types",
 *              type="array",
 *              @OA\Items(
 *                  @OA\Property (property="id", type="int"),
 *                  @OA\Property (property="title", type="string")
 *              )
 *          )
 *      ),
 *      @OA\Property(
 *          property="students",
 *          type="object",
 *          @OA\Property(
 *              property="departments",
 *              type="array",
 *              @OA\Items(
 *                  @OA\Property (property="id", type="int"),
 *                  @OA\Property (property="title", type="string"),
 *                  @OA\Property (property="code", type="string"),
 *                  @OA\Property (property="level", type="int")
 *              )
 *          ),
 *          @OA\Property(
 *              property="types",
 *              type="array",
 *              @OA\Items(
 *                  @OA\Property(property="id", type="int"),
 *                  @OA\Property(property="title", type="string")
 *              )
 *         )
 *      )
 * )
 */
final class PageResourcesResource extends BaseResource
{
    /**
     * @return array
     */
    public function getResponseArray(): array
    {
        return [$this->resource];
    }
}
