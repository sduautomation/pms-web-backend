<?php

declare(strict_types=1);

namespace Domains\Release\V100\Core\Factories;

use Domains\Release\V100\Models\Release;
use Illuminate\Support\Str;

final class CreateReleaseFactory
{
    /**
     * @param object $data
     * @return void
     */
    public static function createRelease(object $data): void
    {
        $release = new Release();
        $release->long_id = (string) Str::uuid();
        $release->version = $data->version;
        $release->title = $data->title;
        $release->content = $data->content;
        $release->author = $data->author;
        $release->contributors = $data->contributors;
        $release->save();
    }

    /**
     * @param string $version
     * @return bool
     */
    public static function checkReleaseByVersion(string $version): bool
    {
        return Release::where('version', $version)->count() > 0;
    }
}
