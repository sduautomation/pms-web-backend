<?php

declare(strict_types=1);

namespace Domains\Release\V100\Core\Factories;

use Domains\Release\V100\Models\Release;

final class ShowReleaseFactory
{
    /**
     * @param string $uuid
     * @return object
     */
    public static function makeRelease(string $uuid): object|null
    {
        return Release::where('long_id', $uuid)->first();
    }
}
