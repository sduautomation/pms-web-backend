<?php

declare(strict_types=1);

namespace Domains\Release\V100\Core\Factories;

use Domains\Release\V100\Models\Release;

final class DeleteReleaseFactory
{
    /**
     * @param string $uuid
     * @return void
     */
    public static function deleteRelease(string $uuid): void
    {
        $release = Release::firstWhere('long_id', $uuid)->delete();
    }

    public static function checkIfReleaseExists(string $uuid): bool
    {
        return Release::where('long_id', $uuid)->count() > 0;
    }
}
