<?php

declare(strict_types=1);

namespace Domains\Release\V100\Core\Factories;

use Domains\Release\V100\Models\Release;
use Illuminate\Database\Eloquent\Collection;

final class ReleaseFactory
{
    public static function makeReleases(): Collection
    {
        return Release::all();
    }
}
