<?php

declare(strict_types=1);

namespace Domains\Release\V100\Requests;

use Domains\Release\V100\Cooker\DTO\StoreReleaseRequestDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Requests\BaseFormRequest;

final class StoreReleaseRequest extends BaseFormRequest
{
    #[ArrayShape(['version' => 'string', 'title' => 'string', 'content' => 'string', 'author' => 'string', 'contributors' => 'string'])]
    public function rules(): array
    {
        return [
            'version' => 'required|string',
            'title' => 'required|string',
            'content' => 'required|string',
            'author' => 'required|string',
            'contributors' => 'required|string',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): StoreReleaseRequestDTO
    {
        $validated = $this->validated();

        return new StoreReleaseRequestDTO(
            version: Arr::get($validated, 'version'),
            title: Arr::get($validated, 'title'),
            content: Arr::get($validated, 'content'),
            author: Arr::get($validated, 'author'),
            contributors: Arr::get($validated, 'contributors'),
        );
    }
}
