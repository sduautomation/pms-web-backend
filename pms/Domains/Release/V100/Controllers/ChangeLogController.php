<?php

declare(strict_types=1);

namespace Domains\Release\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Release\V100\Cooker\Services\GetReleasesPipelineService;
use Illuminate\Http\JsonResponse;

final class ChangeLogController extends Controller
{
    /**
     * @OA\Get(
     *     summary="Get releases in changelog endpoint",
     *     path="/api/V100/changelog",
     *     operationId="ChangeLogController",
     *     tags={"release", "V100"},
     *     description="Get all releases",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Releases are successfully returned",
     *          @OA\JsonContent(ref="#/components/schemas/GetReleasesResource"),
     *     )
     * )
     */
    public function __invoke(GetReleasesPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData([])
            ->handle()
            ->resource();

        return $this->response(
            message: 'Resources are loaded',
            data: $resource
        );
    }
}
