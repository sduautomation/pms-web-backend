<?php

declare(strict_types=1);

namespace Domains\Release\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Release\V100\Cooker\Services\CreateReleaseService;
use Domains\Release\V100\Cooker\Services\DeleteReleaseActionService;
use Domains\Release\V100\Cooker\Services\GetReleasesPipelineService;
use Domains\Release\V100\Cooker\Services\ShowReleasePipelineService;
use Domains\Release\V100\Requests\StoreReleaseRequest;
use Illuminate\Http\JsonResponse;

final class ReleaseResourceController extends Controller
{
    /**
     * @OA\Get(
     *     summary="Get releases",
     *     path="/api/V100/release",
     *     operationId="ReleasesIndexController",
     *     tags={"release", "V100"},
     *     description="Get all releases",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Releases are successfully returned",
     *          @OA\JsonContent(ref="#/components/schemas/GetReleasesResource"),
     *     )
     * )
     */
    public function index(GetReleasesPipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData([])
            ->handle()
            ->resource();

        return $this->response(
            message: 'Resources are loaded',
            data: $resource
        );
    }

    /**
     * @OA\Post  (
     *     summary="Cretae release",
     *     path="/api/V100/release/{uuid}",
     *     operationId="ReleasesCreateController",
     *     tags={"release", "V100"},
     *     description="Create release",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Release is created",
     *     )
     * )
     */
    public function store(CreateReleaseService $service, StoreReleaseRequest $request): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Resource is inserted',
            data: $resource
        );
    }

    /**
     * @OA\Get(
     *     summary="Get single release",
     *     path="/api/V100/release/{uuid}",
     *     operationId="ReleasesShowController",
     *     tags={"release", "V100"},
     *     description="Get single release",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Release are successfully returned",
     *          @OA\JsonContent(ref="#/components/schemas/ShowReleaseResource"),
     *     )
     * )
     */
    public function show(string $uuid, ShowReleasePipelineService $service): JsonResponse
    {
        $resource = $service
            ->setAttributeFromData($uuid)
            ->handle()
            ->resource();

        return $this->response(
            message: 'Resources are loaded',
            data: $resource
        );
    }

    /**
     * @OA\Delete (
     *     summary="Delete release",
     *     path="/api/V100/release/{uuid}",
     *     operationId="ReleasesDeleteController",
     *     tags={"release", "V100"},
     *     description="Delete release",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Resource is deleted",
     *     )
     * )
     */
    public function delete(string $uuid, DeleteReleaseActionService $service): JsonResponse
    {
        $service
            ->setAttributeFromData($uuid)
            ->handle();

        return $this->response(
            message: 'Resource is deleted',
            data: []
        );
    }
}
