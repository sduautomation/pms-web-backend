<?php

declare(strict_types=1);

namespace Domains\Release\V100\Resources;

use Domains\Release\V100\Cooker\DTO\ReleaseResponseDTO;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="releases",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/ReleaseResource")
 *      )
 * )
 * @mixin ReleaseResponseDTO
 */
final class GetReleasesResource extends BaseResource
{
    #[ArrayShape(['releases' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection"])]
    public function getResponseArray(): array
    {
        return [
            'releases' => ReleaseResource::collection($this->releases),
        ];
    }
}
