<?php

declare(strict_types=1);

namespace Domains\Release\V100\Resources;

use Domains\Release\V100\Models\Release;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="id",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="uuid",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="version",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="title",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="content",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="author",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="contributors",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="created_at",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="updated_at",
 *          type="string"
 *      )
 * )
 * @mixin Release
 */
final class ReleaseResource extends BaseResource
{
    #[ArrayShape(['id' => 'int', 'uuid' => 'string', 'version' => 'string', 'title' => 'string', 'content' => 'string', 'author' => 'string', 'contributors' => 'string', 'created_at' => 'string', 'updated_at' => 'string'])]
    public function getResponseArray(): array
    {
        return [
            'id' => $this->id,
            'uuid' => $this->long_id,
            'version' => $this->version,
            'title' => $this->title,
            'content' => $this->content,
            'author' => $this->author,
            'contributors' => $this->contributors,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
