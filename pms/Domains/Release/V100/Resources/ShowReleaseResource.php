<?php

declare(strict_types=1);

namespace Domains\Release\V100\Resources;

use Domains\Release\V100\Cooker\DTO\ShowReleaseResponseDTO;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="record",
 *          type="object",
 *          @OA\Property(ref="#/components/schemas/ReleaseResource")
 *      )
 * )
 * @mixin ShowReleaseResponseDTO
 */
final class ShowReleaseResource extends BaseResource
{
    #[ArrayShape(['record' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection"])]
    public function getResponseArray(): array
    {
        return [
            'record' => new ReleaseResource($this->record),
        ];
    }
}
