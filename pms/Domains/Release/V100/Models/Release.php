<?php

declare(strict_types=1);

namespace Domains\Release\V100\Models;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $long_id
 * @property string $version
 * @property string $title
 * @property string $content
 * @property string $author
 * @property string $contributors
 * @property string $created_at
 * @property string $updated_at
 * @mixin Eloquent
 */
final class Release extends Model
{
    /**
     * @var string
     */
    protected $connection = 'sqlite';

    /**
     * @var string
     */
    protected $table = 'releases';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'long_id',
        'version',
        'title',
        'content',
        'author',
        'contributors',
        'created_at',
        'updated_at',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'id' => 'integer',
    ];
}
