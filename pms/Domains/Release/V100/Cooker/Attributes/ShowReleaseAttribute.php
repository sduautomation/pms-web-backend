<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\Attributes;

use Domains\Release\V100\Cooker\DTO\ShowReleaseRequestDTO;
use Domains\Release\V100\Cooker\DTO\ShowReleaseResponseDTO;

final class ShowReleaseAttribute
{
    public ShowReleaseRequestDTO $requestDTO;
    public ShowReleaseResponseDTO $responseDTO;
    public object $release;
}
