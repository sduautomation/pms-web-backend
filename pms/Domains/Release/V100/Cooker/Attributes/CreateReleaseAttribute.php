<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\Attributes;

use Domains\Release\V100\Cooker\DTO\StoreReleaseRequestDTO;

final class CreateReleaseAttribute
{
    public StoreReleaseRequestDTO $requestDTO;
}
