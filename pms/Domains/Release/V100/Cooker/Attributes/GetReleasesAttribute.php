<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\Attributes;

use Domains\Release\V100\Cooker\DTO\ReleaseResponseDTO;
use Illuminate\Database\Eloquent\Collection;

final class GetReleasesAttribute
{
    public ReleaseResponseDTO $responseDTO;
    public Collection $releases;
}
