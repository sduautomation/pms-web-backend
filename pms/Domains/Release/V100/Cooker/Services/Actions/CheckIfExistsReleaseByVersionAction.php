<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\Services\Actions;

use Closure;
use Domains\Release\V100\Cooker\Attributes\CreateReleaseAttribute;
use Domains\Release\V100\Core\Factories\CreateReleaseFactory;
use Support\Core\Exceptions\ErrorResponseException;

final class CheckIfExistsReleaseByVersionAction
{
    /**
     * @param CreateReleaseAttribute $attribute
     * @param Closure $next
     * @return mixed
     * @throws ErrorResponseException
     */
    public function handle(CreateReleaseAttribute $attribute, Closure $next): mixed
    {
        $exists = CreateReleaseFactory::checkReleaseByVersion($attribute->requestDTO->version);

        if ($exists) {
            throw new ErrorResponseException('Resource is already exists');
        }

        return $next($attribute);
    }
}
