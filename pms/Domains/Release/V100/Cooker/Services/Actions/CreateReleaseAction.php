<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\Services\Actions;

use Closure;
use Domains\Release\V100\Cooker\Attributes\CreateReleaseAttribute;
use Domains\Release\V100\Core\Factories\CreateReleaseFactory;

final class CreateReleaseAction
{
    /**
     * @param CreateReleaseAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public function handle(CreateReleaseAttribute $attribute, Closure $next): mixed
    {
        CreateReleaseFactory::createRelease($attribute->requestDTO);

        return $next($attribute);
    }
}
