<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\Services\Actions;

use Closure;
use Domains\Release\V100\Cooker\Attributes\DeleteReleaseAttribute;
use Domains\Release\V100\Core\Factories\DeleteReleaseFactory;

final class DeleteReleaseAction
{
    /**
     * @param DeleteReleaseAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public function handle(DeleteReleaseAttribute $attribute, Closure $next): mixed
    {
        DeleteReleaseFactory::deleteRelease($attribute->requestDTO->uuid);

        return $next($attribute);
    }
}
