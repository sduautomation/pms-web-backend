<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\Services\Actions;

use Closure;
use Domains\Release\V100\Cooker\Attributes\DeleteReleaseAttribute;
use Domains\Release\V100\Core\Factories\DeleteReleaseFactory;
use Support\Core\Exceptions\ErrorResponseException;

final class CheckIfExistsReleaseAction
{
    /**
     * @param DeleteReleaseAttribute $attribute
     * @param Closure $next
     * @return mixed
     * @throws ErrorResponseException
     */
    public function handle(DeleteReleaseAttribute $attribute, Closure $next): mixed
    {
        $exists = DeleteReleaseFactory::checkIfReleaseExists($attribute->requestDTO->uuid);

        if (! $exists) {
            throw new ErrorResponseException('Resource is not found');
        }

        return $next($attribute);
    }
}
