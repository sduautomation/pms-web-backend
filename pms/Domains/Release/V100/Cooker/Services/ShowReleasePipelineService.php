<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\Services;

use Domains\Release\V100\Cooker\Attributes\ShowReleaseAttribute;
use Domains\Release\V100\Cooker\DTO\ShowReleaseRequestDTO;
use Domains\Release\V100\Cooker\Services\Pipelines\GenerateShowReleaseRecordAction;
use Domains\Release\V100\Cooker\Services\Pipelines\GetReleaseByUuidAction;
use Domains\Release\V100\Resources\ShowReleaseResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class ShowReleasePipelineService implements PipelineServiceInterface
{
    public ShowReleaseAttribute $attribute;

    public function __construct(
        public PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            GetReleaseByUuidAction::class,
            GenerateShowReleaseRecordAction::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
           ->send($this->attribute)
           ->through($this->loadPipes())
           ->then(function () {
               return $this;
           });
    }

    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ShowReleaseAttribute();
        $attribute->requestDTO = new ShowReleaseRequestDTO($data);

        return $this->setAttribute($attribute);
    }

    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new ShowReleaseResource($this->attribute->responseDTO);
    }
}
