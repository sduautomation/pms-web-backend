<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\Services\Pipelines;

use Closure;
use Domains\Release\V100\Cooker\Attributes\GetReleasesAttribute;
use Domains\Release\V100\Core\Factories\ReleaseFactory;

final class GetAllReleaseRecordsAction
{
    public function handle(GetReleasesAttribute $attribute, Closure $next): mixed
    {
        $attribute->releases = ReleaseFactory::makeReleases();

        return $next($attribute);
    }
}
