<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\Services\Pipelines;

use Closure;
use Domains\Release\V100\Cooker\Attributes\ShowReleaseAttribute;
use Domains\Release\V100\Cooker\DTO\ShowReleaseResponseDTO;

final class GenerateShowReleaseRecordAction
{
    public function handle(ShowReleaseAttribute $attribute, Closure $next): mixed
    {
        $attribute->responseDTO = new ShowReleaseResponseDTO(
            record: $attribute->release
        );

        return $next($attribute);
    }
}
