<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\Services\Pipelines;

use Closure;
use Domains\Release\V100\Cooker\Attributes\GetReleasesAttribute;
use Domains\Release\V100\Cooker\DTO\ReleaseResponseDTO;

final class GenerateAllReleaseRecordsAction
{
    public function handle(GetReleasesAttribute $attribute, Closure $next): mixed
    {
        $attribute->responseDTO = new ReleaseResponseDTO(
            releases: $attribute->releases
        );

        return $next($attribute);
    }
}
