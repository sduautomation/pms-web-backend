<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\Services\Pipelines;

use Closure;
use Domains\Release\V100\Cooker\Attributes\ShowReleaseAttribute;
use Domains\Release\V100\Core\Factories\ShowReleaseFactory;
use Support\Core\Exceptions\ErrorResponseException;

final class GetReleaseByUuidAction
{
    /**
     * @param ShowReleaseAttribute $attribute
     * @return mixed
     * @throws ErrorResponseException
     */
    public function handle(ShowReleaseAttribute $attribute, Closure $next): mixed
    {
        $release = ShowReleaseFactory::makeRelease($attribute->requestDTO->uuid);

        if ($release == null) {
            throw new ErrorResponseException('Resource is not found');
        }

        $attribute->release = $release;

        return $next($attribute);
    }
}
