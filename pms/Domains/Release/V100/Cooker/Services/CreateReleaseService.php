<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\Services;

use Domains\Release\V100\Cooker\Attributes\CreateReleaseAttribute;
use Domains\Release\V100\Cooker\Services\Actions\CheckIfExistsReleaseByVersionAction;
use Domains\Release\V100\Cooker\Services\Actions\CreateReleaseAction;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class CreateReleaseService implements PipelineServiceInterface
{
    public CreateReleaseAttribute $attribute;

    public function __construct(
        public PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            CheckIfExistsReleaseByVersionAction::class,
            CreateReleaseAction::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new CreateReleaseAttribute();
        $attribute->requestDTO = $data;

        return $this->setAttribute($attribute);
    }

    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return [];
    }
}
