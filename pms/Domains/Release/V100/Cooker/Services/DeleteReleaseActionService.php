<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\Services;

use Domains\Release\V100\Cooker\Attributes\DeleteReleaseAttribute;
use Domains\Release\V100\Cooker\DTO\DeleteReleaseRequestDTO;
use Domains\Release\V100\Cooker\Services\Actions\CheckIfExistsReleaseAction;
use Domains\Release\V100\Cooker\Services\Actions\DeleteReleaseAction;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class DeleteReleaseActionService implements PipelineServiceInterface
{
    private DeleteReleaseAttribute $attribute;

    public function __construct(
        public PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            CheckIfExistsReleaseAction::class,
            DeleteReleaseAction::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new DeleteReleaseAttribute();
        $attribute->requestDTO = new DeleteReleaseRequestDTO($data);

        return $this->setAttribute($attribute);
    }

    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return [];
    }
}
