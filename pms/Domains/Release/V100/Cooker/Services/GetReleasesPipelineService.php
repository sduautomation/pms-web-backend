<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\Services;

use Domains\Release\V100\Cooker\Attributes\GetReleasesAttribute;
use Domains\Release\V100\Cooker\Services\Pipelines\GenerateAllReleaseRecordsAction;
use Domains\Release\V100\Cooker\Services\Pipelines\GetAllReleaseRecordsAction;
use Domains\Release\V100\Resources\GetReleasesResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class GetReleasesPipelineService implements PipelineServiceInterface
{
    private GetReleasesAttribute $attribute;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            GetAllReleaseRecordsAction::class,
            GenerateAllReleaseRecordsAction::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new GetReleasesAttribute();

        return $this->setAttribute($attribute);
    }

    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new GetReleasesResource($this->attribute->responseDTO);
    }
}
