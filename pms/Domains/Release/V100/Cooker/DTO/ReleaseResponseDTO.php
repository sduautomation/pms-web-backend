<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\DTO;

use Illuminate\Database\Eloquent\Collection;

final class ReleaseResponseDTO
{
    public function __construct(
        public Collection $releases
    ) {
    }
}
