<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\DTO;

final class ShowReleaseRequestDTO
{
    public function __construct(
        public string $uuid
    ) {
    }
}
