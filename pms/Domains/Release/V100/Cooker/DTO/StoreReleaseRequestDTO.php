<?php

declare(strict_types=1);

namespace Domains\Release\V100\Cooker\DTO;

final class StoreReleaseRequestDTO
{
    public function __construct(
        public string $version,
        public string $title,
        public string $content,
        public string $author,
        public string $contributors,
    ) {
    }
}
