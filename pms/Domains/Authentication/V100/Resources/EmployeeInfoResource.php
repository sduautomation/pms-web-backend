<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Resources;

use Domains\Authentication\V100\Cooker\DTO\EmployeeInfoResponseDTO;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * Class EmployeeInfoResource.
 * @mixin EmployeeInfoResponseDTO
 * @OA\Schema(
 *     @OA\Property(
 *          type="array",
 *          @OA\Items(
 *              type="array",
 *              @OA\Items()
 *          )
 *      )
 * )
 */
final class EmployeeInfoResource extends BaseResource
{
    /**
     * @return array
     */
    #[ArrayShape(['info' => 'array', 'messages' => 'array', 'events' => 'array', 'permissions' => 'array'])]
    public function getResponseArray(): array
    {
        return [
            'info' => $this->info,
            'messages' => $this->messages,
            'events' => $this->events,
            'permissions' => $this->permissions,
        ];
    }
}
