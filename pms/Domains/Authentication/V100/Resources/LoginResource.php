<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Resources;

use Domains\Authentication\V100\Cooker\DTO\LoginResponseDTO;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * Class LoginResource.
 * @mixin LoginResponseDTO
 * @OA\Schema(
 *     @OA\Property(
 *          property="token",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="token_type",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="expires_in",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="log",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="access",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="roles",
 *          type="array",
 *          @OA\Items(
 *            @OA\Property(property="roles", type="string"),
 *          )
 *      )
 * )
 */
final class LoginResource extends BaseResource
{
    /**
     * @return array
     */
    #[ArrayShape(['token' => 'string', 'token_type' => 'string', 'expires_in' => 'int', 'log' => 'int', 'access' => 'string', 'roles' => 'array'])]
    public function getResponseArray(): array
    {
        return [
            'token' => $this->token,
            'token_type' => $this->tokenType,
            'expires_in' => $this->expiresIn,
            'log' => $this->log,
            'access' => $this->access,
            'roles' => $this->roles,
        ];
    }
}
