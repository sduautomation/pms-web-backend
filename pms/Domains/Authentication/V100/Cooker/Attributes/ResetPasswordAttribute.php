<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Attributes;

use Domains\Authentication\V100\Cooker\DTO\ResetPasswordRequestDto;
use Domains\Authentication\V100\Cooker\ValueObjects\ResetPasswordTokenValueObject;
use Domains\Authentication\V100\Cooker\ValueObjects\ResetPasswordValueObject;

/**
 * Class ResetPasswordAttribute.
 */
final class ResetPasswordAttribute
{
    public ResetPasswordRequestDto $requestDto;
    public ResetPasswordValueObject $resetPassword;
    public ResetPasswordTokenValueObject $resetPasswordToken;
}
