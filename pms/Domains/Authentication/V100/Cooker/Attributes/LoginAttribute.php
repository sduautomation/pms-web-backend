<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Attributes;

use Domains\Authentication\V100\Cooker\DTO\LoginRequestDTO;
use Domains\Authentication\V100\Cooker\DTO\LoginResponseDTO;
use Domains\Authentication\V100\Cooker\ValueObjects\LoginResultValueObject;
use Illuminate\Contracts\Auth\Authenticatable;
use Support\Models\Employee;

/**
 * Class LoginAttribute.
 */
final class LoginAttribute
{
    public LoginRequestDTO $loginRequestDto;
    public LoginResultValueObject $loginProcedureResult;
    public Employee $employee;
    public LoginResponseDTO $loginResponseDto;
    public Authenticatable $user;
}
