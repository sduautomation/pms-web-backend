<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Attributes;

use Domains\Authentication\V100\Cooker\DTO\EmployeeInfoResponseDTO;
use Domains\Authentication\V100\Cooker\ValueObjects\ProfileInfoValueObject;
use Illuminate\Contracts\Auth\Authenticatable;

/**
 * @property string|null photo
 */
final class EmployeeInfoAttribute
{
    public ProfileInfoValueObject $employee;
    public Authenticatable $user;
    public mixed $parameters;

    public array $messages;
    public array $events;
    public mixed $permissions;

    public EmployeeInfoResponseDTO $responseDTO;
}
