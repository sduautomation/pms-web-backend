<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Attributes;

use Domains\Authentication\V100\Cooker\ValueObjects\ResetPasswordTokenValueObject;
use Support\Models\Employee;

/**
 * Class ForgotPasswordAttribute.
 */
final class ForgotPasswordAttribute
{
    public string $resetPasswordToken;
    public ResetPasswordTokenValueObject $decryptedResetPasswordToken;
    public Employee $user;
}
