<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\ValueObjects;

use Carbon\CarbonImmutable;

/**
 * Class ResetPasswordTokenValueObject.
 */
final class ResetPasswordTokenValueObject
{
    /**
     * @param string $username
     * @param CarbonImmutable $tokenExpiresAt
     * @param CarbonImmutable $tokenStartsAt
     * @param string $resetCode
     */
    public function __construct(
        public string $username,
        public CarbonImmutable $tokenExpiresAt,
        public CarbonImmutable $tokenStartsAt,
        public string $resetCode,
    ) {
    }
}
