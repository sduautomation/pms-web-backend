<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\ValueObjects;

final class ProfileInfoValueObject
{
    /**
     * @param string $language,
     * @param int $status,
     * @param string $name,
     * @param string $surname,
     * @param string $nativeName,
     * @param string $nativeSurname,
     * @param string $username,
     * @param string $wageRate,
     * @param string $email,
     * @param string $birthDate,
     * @param string $lastIP,
     * @param string $lastDevice,
     * @param string $lastLogin,
     * @param int $isReliable,
     * @param string $degreeTitle
     */
    public function __construct(
        public string $language,
        public int $status,
        public string $name,
        public string $surname,
        public string $nativeName,
        public string $nativeSurname,
        public string $username,
        public string $wageRate,
        public string $email,
        public string $birthDate,
        public string $lastIP,
        public string $lastDevice,
        public string $lastLogin,
        public int $isReliable,
        public string $degreeTitle,
        public string|null $photo
    ) {
    }
}
