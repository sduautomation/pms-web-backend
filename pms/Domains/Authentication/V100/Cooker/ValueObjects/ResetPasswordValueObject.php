<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\ValueObjects;

/**
 * Class ResetPasswordValueObject.
 */
final class ResetPasswordValueObject
{
    /**
     * @param string $registerCode
     * @param string $newPassword
     * @param int $webLogId
     */
    public function __construct(
        public string $registerCode,
        public string $newPassword,
        public int $webLogId
    ) {
    }
}
