<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\DTO;

/**
 * Class ResetPasswordProcedureDTO.
 */
final class ResetPasswordProcedureDTO
{
    /**
     * @param int $employeeId
     * @param string $resetCode
     * @param int $webLogId
     */
    public function __construct(
        public int $employeeId,
        public string $resetCode,
        public int $webLogId
    ) {
    }
}
