<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\DTO;

/**
 * Class ForgotPasswordMailDTO.
 */
final class ForgotPasswordMailDTO
{
    /**
     * @param string $resetPasswordToken
     * @param string $userName
     * @param string $userSurname
     */
    public function __construct(
        public string $resetPasswordToken,
        public string $userName,
        public string $userSurname,
    ) {
    }
}
