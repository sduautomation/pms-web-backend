<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\DTO;

/**
 * Class LoginResponseDTO.
 */
final class LoginResponseDTO
{
    /**
     * @param string $token
     * @param string $tokenType
     * @param int $expiresIn
     * @param int $log
     * @param string $access
     * @param array $roles
     */
    public function __construct(
        public string $token,
        public string $tokenType,
        public int $expiresIn,
        public int $log,
        public string $access,
        public array $roles
    ) {
    }
}
