<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\DTO;

/**
 * Class LoginRequestDTO.
 */
final class LoginRequestDTO
{
    /**
     * @param string $username
     * @param string $password
     * @param string $ipAddress
     * @param string $deviceInfo
     */
    public function __construct(
        public string $username,
        public string $password,
        public string $ipAddress,
        public string $deviceInfo,
    ) {
    }
}
