<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\DTO;

/**
 * Class ResetPasswordRequestDto.
 */
final class ResetPasswordRequestDto
{
    /**
     * @param string $token
     * @param string $newPassword
     * @param string $confirmPassword
     * @param string $ipAddress
     * @param string $deviceInfo
     */
    public function __construct(
        public string $token,
        public string $newPassword,
        public string $confirmPassword,
        public string $ipAddress,
        public string $deviceInfo
    ) {
    }
}
