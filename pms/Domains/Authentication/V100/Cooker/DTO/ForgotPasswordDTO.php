<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\DTO;

/**
 * Class ForgotPasswordDTO.
 */
final class ForgotPasswordDTO
{
    /**
     * @param string $username
     * @param string $captcha
     */
    public function __construct(
        public string $username,
        public string $captcha
    ) {
    }
}
