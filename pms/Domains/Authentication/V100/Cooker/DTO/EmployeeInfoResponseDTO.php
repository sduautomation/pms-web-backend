<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\DTO;

use Domains\Authentication\V100\Cooker\ValueObjects\ProfileInfoValueObject;

final class EmployeeInfoResponseDTO
{
    /**
     * @param ProfileInfoValueObject $info
     * @param array $messages
     * @param array $events
     * @param mixed $permissions
     */
    public function __construct(
        public ProfileInfoValueObject $info,
        public array $messages,
        public array $events,
        public mixed $permissions
    ) {
    }
}
