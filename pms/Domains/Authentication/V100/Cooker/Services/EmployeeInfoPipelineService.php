<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services;

use Domains\Authentication\V100\Cooker\Attributes\EmployeeInfoAttribute;
use Domains\Authentication\V100\Cooker\Services\Pipelines\EmployeeInfo\AttachParameters;
use Domains\Authentication\V100\Cooker\Services\Pipelines\EmployeeInfo\EmployeeInfo;
use Domains\Authentication\V100\Cooker\Services\Pipelines\EmployeeInfo\EmployeePermissions;
use Domains\Authentication\V100\Cooker\Services\Pipelines\EmployeeInfo\GenerateResource;
use Domains\Authentication\V100\Cooker\Services\Pipelines\EmployeeInfo\MessageBoardLastMessages;
use Domains\Authentication\V100\Cooker\Services\Pipelines\EmployeeInfo\SystemCalendarLastEvents;
use Domains\Authentication\V100\Resources\EmployeeInfoResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class EmployeeInfoPipelineService implements PipelineServiceInterface
{
    private EmployeeInfoAttribute $attribute;

    /**
     * @param PipelineService $service
     */
    public function __construct(
        private PipelineService $service
    ) {
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @return array
     */
    public function loadPipes(): array
    {
        return [
            EmployeeInfo::class,
            AttachParameters::class,

            MessageBoardLastMessages::class,
            SystemCalendarLastEvents::class,
            EmployeePermissions::class,

            GenerateResource::class,
        ];
    }

    /**
     * @param mixed $data
     * @return PipelineServiceInterface
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new EmployeeInfoAttribute();
        $attribute->user = Auth::user();

        return $this->setAttribute($attribute);
    }

    /**
     * @param mixed $attribute
     * @return PipelineServiceInterface
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * @return BaseResource|AnonymousResourceCollection|array|null
     */
    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new EmployeeInfoResource($this->attribute->responseDTO);
    }
}
