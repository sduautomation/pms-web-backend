<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services;

use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Domains\Authentication\V100\Cooker\ValueObjects\ResetPasswordTokenValueObject;
use Domains\Authentication\V100\Core\Exceptions\ResetPasswordTokenException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Crypt;
use Support\Core\Interfaces\EncryptorServiceInterface;
use Support\Core\Traits\RandomTrait;

/**
 * Class ResetPasswordTokenEncryptorService.
 */
final class ResetPasswordTokenEncryptorService implements EncryptorServiceInterface
{
    use RandomTrait;

    private const TOKEN_EXPLODER = '$';
    private const TOKEN_LIFE_TIME = 5;
    private const RESET_CODE_LENGTH = 10;

    /**
     * @param string $value
     * @return string
     */
    public function encrypt(string $value): string
    {
        $encrypting = $value;
        $encrypting .= self::TOKEN_EXPLODER;
        $encrypting .= Carbon::now()->addHours(self::TOKEN_LIFE_TIME)->toDateTimeString();
        $encrypting .= self::TOKEN_EXPLODER;
        $encrypting .= CarbonImmutable::now()->toDateTimeString();
        $encrypting .= self::TOKEN_EXPLODER;
        $encrypting .= $this->generateRandomString(self::RESET_CODE_LENGTH);

        return Crypt::encryptString($encrypting);
    }

    /**
     * @param string $value
     * @return string
     */
    public function decrypt(string $value): string
    {
        return Crypt::decryptString($value);
    }

    /**
     * @param string $token
     * @return ResetPasswordTokenValueObject
     * @throws ResetPasswordTokenException
     */
    public function getDecryptedTokenData(string $token): ResetPasswordTokenValueObject
    {
        $decryptedToken = $this->decrypt($token);
        $tokenData = explode(self::TOKEN_EXPLODER, $decryptedToken);

        if (count($tokenData) !== 4) {
            throw ResetPasswordTokenException::invalidToken();
        }

        $expiresAt = CarbonImmutable::parse(Arr::get($tokenData, 1));
        $startsAt = CarbonImmutable::parse(Arr::get($tokenData, 2));

        return new ResetPasswordTokenValueObject(
            username: Arr::get($tokenData, 0),
            tokenExpiresAt: $expiresAt,
            tokenStartsAt: $startsAt,
            resetCode: Arr::get($tokenData, 3),
        );
    }
}
