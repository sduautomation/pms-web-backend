<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services;

use Domains\Authentication\V100\Cooker\Attributes\ResetPasswordAttribute;
use Domains\Authentication\V100\Cooker\DTO\ResetPasswordRequestDto;
use Domains\Authentication\V100\Cooker\Services\Pipelines\ResetPassword\CheckTokenExpiration;
use Domains\Authentication\V100\Cooker\Services\Pipelines\ResetPassword\DecryptToken;
use Domains\Authentication\V100\Cooker\Services\Pipelines\ResetPassword\ResetPasswordViaProcedure;
use Domains\Authentication\V100\Cooker\Services\Pipelines\ResetPassword\SetResetPasswordValueObject;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

/**
 * Class ResetPasswordPipelineService.
 */
final class ResetPasswordPipelineService implements PipelineServiceInterface
{
    private ResetPasswordAttribute $attribute;

    /**
     * @param PipelineService $service
     */
    public function __construct(
        private PipelineService $service
    ) {
    }

    /**
     * @return array
     */
    public function loadPipes(): array
    {
        return [
            DecryptToken::class,
            CheckTokenExpiration::class,
            SetResetPasswordValueObject::class,
            ResetPasswordViaProcedure::class,
        ];
    }

    /**
     * @return PipelineServiceInterface
     */
    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(fn () => $this);
    }

    /**
     * @param ResetPasswordRequestDto $data
     * @return PipelineServiceInterface
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ResetPasswordAttribute();
        $attribute->requestDto = $data;

        return $this->setAttribute($attribute);
    }

    /**
     * @param ResetPasswordAttribute $attribute
     * @return PipelineServiceInterface
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * @return BaseResource|AnonymousResourceCollection|array|null
     */
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return null;
    }
}
