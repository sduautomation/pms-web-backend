<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\ForgotPassword;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\ForgotPasswordAttribute;
use Domains\Authentication\V100\Cooker\DTO\ForgotPasswordMailDTO;
use Domains\Authentication\V100\Core\Mail\ForgotPasswordMail;
use Illuminate\Support\Facades\Mail;

/**
 * Class SendMail.
 */
final class SendMail
{
    /** @psalm-readonly */
    private string $userMailerPostfix;

    public function __construct()
    {
        $this->userMailerPostfix = (string) config('portal.user_mailer_postfix');
    }

    /**
     * @param ForgotPasswordAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public function handle(ForgotPasswordAttribute $attribute, Closure $next): mixed
    {
        $forgotPasswordMailDto = new ForgotPasswordMailDTO(
            resetPasswordToken:  $attribute->resetPasswordToken,
            userName: $attribute->user->name,
            userSurname: $attribute->user->sname,
        );

        $email = $attribute->user->hname.$this->userMailerPostfix;

        Mail::to($email)->send(new ForgotPasswordMail($forgotPasswordMailDto));

        return $next($attribute);
    }
}
