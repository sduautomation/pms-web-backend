<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\ForgotPassword;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\ForgotPasswordAttribute;
use Domains\Authentication\V100\Cooker\DTO\ResetPasswordProcedureDTO;
use Domains\Authentication\V100\Core\Handlers\GetWebLogIdHandler;
use Domains\Authentication\V100\Procedures\RegisterPasswordResetProcedure;
use Illuminate\Http\Request;
use Support\Core\Exceptions\JsonApi\HttpException;
use Throwable;

/**
 * Class RegisterPasswordResetViaProcedure.
 */
final class RegisterPasswordResetViaProcedure
{
    /**
     * @param RegisterPasswordResetProcedure $procedure
     * @param GetWebLogIdHandler $getWebLogIdHandler
     * @param Request $request
     */
    public function __construct(
        private RegisterPasswordResetProcedure $procedure,
        private GetWebLogIdHandler $getWebLogIdHandler,
        private Request $request
    ) {
    }

    /**
     * @param ForgotPasswordAttribute $attribute
     * @param Closure $next
     * @return mixed
     * @throws HttpException
     */
    public function handle(ForgotPasswordAttribute $attribute, Closure $next): mixed
    {
        $webLogId = $this->getWebLogIdHandler->handle(
            ipAddress: $this->request->ip(),
            deviceInfo: (string) $this->request->server('HTTP_USER_AGENT')
        );

        $resetPasswordProcedureDto = new ResetPasswordProcedureDTO(
            employeeId: $attribute->user->emp_id,
            resetCode: $attribute->decryptedResetPasswordToken->resetCode,
            webLogId: $webLogId,
        );

        try {
            $this->procedure->execute($resetPasswordProcedureDto);
        } catch (Throwable $exception) {
            throw new HttpException($exception->getMessage(), $exception->getCode());
        }

        return $next($attribute);
    }
}
