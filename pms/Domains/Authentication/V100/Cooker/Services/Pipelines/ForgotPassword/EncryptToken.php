<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\ForgotPassword;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\ForgotPasswordAttribute;
use Domains\Authentication\V100\Cooker\Services\ResetPasswordTokenEncryptorService;
use Domains\Authentication\V100\Core\Exceptions\ResetPasswordTokenException;

/**
 * Class EncryptToken.
 */
final class EncryptToken
{
    /**
     * @param ResetPasswordTokenEncryptorService $service
     */
    public function __construct(
        private ResetPasswordTokenEncryptorService $service
    ) {
    }

    /**
     * @param ForgotPasswordAttribute $attribute
     * @param Closure $next
     * @return mixed
     * @throws ResetPasswordTokenException
     */
    public function handle(ForgotPasswordAttribute $attribute, Closure $next): mixed
    {
        $resetPasswordToken = $this->service->encrypt($attribute->user->hname);
        $attribute->resetPasswordToken = $resetPasswordToken;
        $attribute->decryptedResetPasswordToken = $this->service->getDecryptedTokenData($resetPasswordToken);

        return $next($attribute);
    }
}
