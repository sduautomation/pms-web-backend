<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\EmployeeInfo;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\EmployeeInfoAttribute;
use Domains\Authentication\V100\Cooker\DTO\EmployeeInfoResponseDTO;

final class GenerateResource
{
    /**
     * @param EmployeeInfoAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public function handle(EmployeeInfoAttribute $attribute, Closure $next): mixed
    {
        $attribute->responseDTO = new EmployeeInfoResponseDTO(
            info: $attribute->employee,
            messages: $attribute->messages,
            events: $attribute->events,
            permissions: $attribute->permissions
        );

        return $next($attribute);
    }
}
