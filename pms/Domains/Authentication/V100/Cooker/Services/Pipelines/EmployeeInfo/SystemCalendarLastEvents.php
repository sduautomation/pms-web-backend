<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\EmployeeInfo;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\EmployeeInfoAttribute;
use Support\Models\Domains\Administrative\SystemCalendar;

final class SystemCalendarLastEvents
{
    /**
     * @param EmployeeInfoAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public function handle(EmployeeInfoAttribute $attribute, Closure $next): mixed
    {
        $attribute->events = SystemCalendar::getEventCalendar(
            year: $attribute->parameters->year,
            term: $attribute->parameters->term
        )->toArray();

        return $next($attribute);
    }
}
