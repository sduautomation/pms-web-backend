<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\EmployeeInfo;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\EmployeeInfoAttribute;
use Support\Models\Domains\Administrative\SystemCalendar;
use Support\Models\PermissionModel;

final class EmployeePermissions
{
    /**
     * @param EmployeeInfoAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public function handle(EmployeeInfoAttribute $attribute, Closure $next): mixed
    {
        if (isset($attribute->user->emp_id)) {
            $attribute->permissions = [
                'individual' => [], /*SystemCalendar::getCalendarIndividualPermissions(
                    year: $attribute->parameters->year,
                    term: $attribute->parameters->term,
                    employeeId: $attribute->user->emp_id
                )->toArray()*/
                'assessment' => [], /*SystemCalendar::getCalendarAssessmentPermissions(
                    year: $attribute->parameters->year,
                    term: $attribute->parameters->term,
                    employeeId: $attribute->user->emp_id
                )->toArray(),*/
                'module' => $this->processModulePermissions($attribute),
            ];
        }

        return $next($attribute);
    }

    private function processModulePermissions(EmployeeInfoAttribute $attribute): array
    {
        if (isset($attribute->user->emp_id)) {
            return PermissionModel::getModulePermissions(
                employeeId: $attribute->user->emp_id,
                id: 1
            )->toArray();
        }

        return [];
    }
}
