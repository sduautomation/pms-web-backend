<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\EmployeeInfo;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\EmployeeInfoAttribute;
use Support\Core\Traits\EmployeeTrait;

final class AttachParameters
{
    use EmployeeTrait;

    /**
     * @param EmployeeInfoAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public function handle(EmployeeInfoAttribute $attribute, Closure $next): mixed
    {
        $attribute->parameters = $this->getConfigurations();

        return $next($attribute);
    }
}
