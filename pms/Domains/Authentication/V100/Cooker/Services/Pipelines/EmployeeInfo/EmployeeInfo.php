<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\EmployeeInfo;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\EmployeeInfoAttribute;
use Domains\Authentication\V100\Core\Exceptions\AuthenticationException;
use Support\Models\Employee;

final class EmployeeInfo
{
    /**
     * @param EmployeeInfoAttribute $attribute
     * @param Closure $next
     * @return mixed
     * @throws AuthenticationException
     */
    public function handle(EmployeeInfoAttribute $attribute, Closure $next): mixed
    {
        if ($attribute->user === null) {
            throw AuthenticationException::canNotAuthenticateUser();
        }

        if (isset($attribute->user->emp_id)) {
            $employee = Employee::getEmployeeInformation(employee_id: $attribute->user->emp_id);

            if (is_null($employee)) {
                throw AuthenticationException::canNotAuthenticateUser();
            }

            $attribute->employee = $employee;
            $attribute->employee->photo = Employee::getProfilePhoto(employee_id: $attribute->user->emp_id);
        }

        return $next($attribute);
    }
}
