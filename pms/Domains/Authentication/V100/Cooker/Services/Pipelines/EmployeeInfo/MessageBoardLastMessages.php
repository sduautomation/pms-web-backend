<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\EmployeeInfo;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\EmployeeInfoAttribute;
use Support\Models\Domains\Administrative\MessageBoard;

final class MessageBoardLastMessages
{
    /**
     * @param EmployeeInfoAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public function handle(EmployeeInfoAttribute $attribute, Closure $next): mixed
    {
        $attribute->messages = MessageBoard::getLastMessages()->toArray();

        return $next($attribute);
    }
}
