<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\ResetPassword;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\ResetPasswordAttribute;
use Domains\Authentication\V100\Cooker\ValueObjects\ResetPasswordValueObject;
use Domains\Authentication\V100\Core\Exceptions\AuthenticationException;
use Domains\Authentication\V100\Procedures\ResetPasswordOracleProcedure;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use Support\Core\Exceptions\JsonApi\HttpException;
use Throwable;

/**
 * Class ResetPasswordViaProcedure.
 */
final class ResetPasswordViaProcedure
{
    private ResetPasswordOracleProcedure $procedure;

    /**
     * @throws BindingResolutionException
     */
    public function __construct()
    {
        $this->procedure = Container::getInstance()->make(ResetPasswordOracleProcedure::class);
    }

    /**
     * @param ResetPasswordAttribute $attribute
     * @param Closure $next
     * @return mixed
     * @throws AuthenticationException
     * @throws HttpException
     */
    public function handle(ResetPasswordAttribute $attribute, Closure $next): mixed
    {
        $this->resetPassword($attribute->resetPassword);

        if (! $this->isPasswordReset()) {
            throw AuthenticationException::couldNotResetPassword();
        }

        return $next($attribute);
    }

    /**
     * @param ResetPasswordValueObject $resetPassword
     * @throws HttpException
     */
    private function resetPassword(ResetPasswordValueObject $resetPassword): void
    {
        try {
            $this->procedure->execute($resetPassword);
        } catch (Throwable $exception) {
            throw new HttpException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @return bool
     */
    private function isPasswordReset(): bool
    {
        return $this->procedure->getResetPasswordResult() === 1;
    }
}
