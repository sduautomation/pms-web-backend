<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\ResetPassword;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\ResetPasswordAttribute;
use Domains\Authentication\V100\Cooker\ValueObjects\ResetPasswordValueObject;
use Domains\Authentication\V100\Core\Handlers\GetWebLogIdHandler;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use Support\Core\Exceptions\JsonApi\HttpException;

/**
 * Class SetResetPasswordValueObject.
 */
final class SetResetPasswordValueObject
{
    private GetWebLogIdHandler $getWebLogIdHandler;

    /**
     * @throws BindingResolutionException
     */
    public function __construct()
    {
        $this->getWebLogIdHandler = Container::getInstance()->make(GetWebLogIdHandler::class);
    }

    /**
     * @param ResetPasswordAttribute $attribute
     * @param Closure $next
     * @return mixed
     * @throws HttpException
     */
    public function handle(ResetPasswordAttribute $attribute, Closure $next): mixed
    {
        $webLogId = $this->getWebLogIdHandler->handle(
            ipAddress: $attribute->requestDto->ipAddress,
            deviceInfo: $attribute->requestDto->deviceInfo
        );

        $attribute->resetPassword = new ResetPasswordValueObject(
            registerCode: $attribute->resetPasswordToken->resetCode,
            newPassword: $attribute->requestDto->newPassword,
            webLogId: $webLogId
        );

        return $next($attribute);
    }
}
