<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\ResetPassword;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\ResetPasswordAttribute;
use Domains\Authentication\V100\Cooker\Services\ResetPasswordTokenEncryptorService;
use Domains\Authentication\V100\Core\Exceptions\ResetPasswordTokenException;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;

/**
 * Class DecryptToken.
 */
final class DecryptToken
{
    private ResetPasswordTokenEncryptorService $service;

    /**
     * @throws BindingResolutionException
     */
    public function __construct()
    {
        $this->service = Container::getInstance()->make(ResetPasswordTokenEncryptorService::class);
    }

    /**
     * @param ResetPasswordAttribute $attribute
     * @param Closure $next
     * @return mixed
     * @throws ResetPasswordTokenException
     */
    public function handle(ResetPasswordAttribute $attribute, Closure $next): mixed
    {
        $decryptedToken = $this->service->getDecryptedTokenData(
            $attribute->requestDto->token
        );

        $attribute->resetPasswordToken = $decryptedToken;

        return $next($attribute);
    }
}
