<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\ResetPassword;

use Carbon\CarbonImmutable;
use Closure;
use Domains\Authentication\V100\Cooker\Attributes\ResetPasswordAttribute;
use Domains\Authentication\V100\Core\Exceptions\AuthenticationException;

/**
 * Class CheckTokenExpiration.
 */
final class CheckTokenExpiration
{
    /**
     * @param ResetPasswordAttribute $attribute
     * @param Closure $next
     * @return mixed
     * @throws AuthenticationException
     */
    public function handle(ResetPasswordAttribute $attribute, Closure $next): mixed
    {
        $isTokenExpired = CarbonImmutable::now()
            ->gt($attribute->resetPasswordToken->tokenExpiresAt);

        if ($isTokenExpired) {
            throw AuthenticationException::tokenExpired();
        }

        return $next($attribute);
    }
}
