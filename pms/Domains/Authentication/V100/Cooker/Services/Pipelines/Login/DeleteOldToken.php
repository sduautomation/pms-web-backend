<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\Login;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\LoginAttribute;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class DeleteOldToken.
 */
final class DeleteOldToken
{
    /**
     * @param LoginAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public function handle(LoginAttribute $attribute, Closure $next): mixed
    {
        $employeeId = $attribute->employee->emp_id;
        $this->deleteOldToken($employeeId);

        return $next($attribute);
    }

    /**
     * @param int $employeeId
     */
    private function deleteOldToken(int $employeeId): void
    {
        try {
            DB::table('oauth_access_token')
                ->where('user_id', $employeeId)
                ->delete();
        } catch (Throwable) {
            // TODO: Report error

            return;
        }
    }
}
