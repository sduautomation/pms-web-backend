<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\Login;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\LoginAttribute;
use Domains\Authentication\V100\Cooker\ValueObjects\LoginResultValueObject;
use Domains\Authentication\V100\Core\Exceptions\AuthenticationException;

/**
 * Class VerifyUser.
 */
final class VerifyUser
{
    /**
     * @param LoginAttribute $attribute
     * @param Closure $next
     * @return mixed
     * @throws AuthenticationException
     */
    public function handle(LoginAttribute $attribute, Closure $next): mixed
    {
        if ($this->isUserNotVerified($attribute->loginProcedureResult)) {
            throw AuthenticationException::incorrectCredentials();
        }

        return $next($attribute);
    }

    /**
     * @param LoginResultValueObject $loginResult
     * @return bool
     */
    private function isUserNotVerified(LoginResultValueObject $loginResult): bool
    {
        return $loginResult->isVerified !== 1 && $loginResult->result < 1;
    }
}
