<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\Login;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\LoginAttribute;
use Support\Models\Employee;

/**
 * Class SetEmployee.
 */
final class SetEmployee
{
    /**
     * @param LoginAttribute $attribute
     * @param Closure $next
     * @return mixed
     */
    public function handle(LoginAttribute $attribute, Closure $next): mixed
    {
        $username = $attribute->loginRequestDto->username;
        $attribute->employee = Employee::byUsername($username);

        return $next($attribute);
    }
}
