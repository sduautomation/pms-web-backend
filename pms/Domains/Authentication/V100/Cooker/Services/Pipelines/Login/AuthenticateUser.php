<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\Login;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\LoginAttribute;
use Domains\Authentication\V100\Core\Exceptions\AuthenticationException;
use Illuminate\Support\Facades\Auth;

/**
 * Class AuthenticateUser.
 */
final class AuthenticateUser
{
    /**
     * @param LoginAttribute $attribute
     * @param Closure $next
     * @return mixed
     * @throws AuthenticationException
     */
    public function handle(LoginAttribute $attribute, Closure $next): mixed
    {
        Auth::login($attribute->employee);
        $user = Auth::user();

        if ($user === null) {
            throw AuthenticationException::canNotAuthenticateUser();
        }

        $attribute->user = $user;

        return $next($attribute);
    }
}
