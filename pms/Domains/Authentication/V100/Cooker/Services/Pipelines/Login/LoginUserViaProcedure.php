<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\Login;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\LoginAttribute;
use Domains\Authentication\V100\Cooker\DTO\LoginRequestDTO;
use Domains\Authentication\V100\Procedures\LoginOracleProcedure;
use Support\Core\Exceptions\JsonApi\HttpException;
use Throwable;

/**
 * Class LoginUserViaProcedure.
 */
final class LoginUserViaProcedure
{
    /**
     * @param LoginOracleProcedure $procedure
     */
    public function __construct(
        private LoginOracleProcedure $procedure
    ) {
    }

    /**
     * @param LoginAttribute $attribute
     * @param Closure $next
     * @return mixed
     * @throws HttpException
     */
    public function handle(LoginAttribute $attribute, Closure $next): mixed
    {
        $this->executeProcedure($attribute->loginRequestDto);
        $this->setProcedureResult($attribute);

        return $next($attribute);
    }

    /**
     * @param LoginRequestDTO $dto
     * @throws HttpException
     */
    private function executeProcedure(LoginRequestDTO $dto): void
    {
        try {
            $this->procedure->execute($dto);
        } catch (Throwable $exception) {
            throw new HttpException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param LoginAttribute $parameter
     */
    private function setProcedureResult(LoginAttribute $parameter): void
    {
        $parameter->loginProcedureResult = $this->procedure->getLoginResult();
    }
}
