<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services\Pipelines\Login;

use Closure;
use Domains\Authentication\V100\Cooker\Attributes\LoginAttribute;
use Domains\Authentication\V100\Cooker\DTO\LoginResponseDTO;
use Domains\Authentication\V100\Core\Exceptions\AuthenticationException;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Crypt;
use Support\Core\Traits\RoleMapperTrait;
use Support\Models\Employee;
use Throwable;

/**
 * Class GenerateResponse.
 */
final class GenerateResponse
{
    use RoleMapperTrait;

    private const TOKEN_TYPE = 'Bearer';
    private const TIME_UNIT = 60;

    /**
     * @param LoginAttribute $attribute
     * @param Closure $next
     * @return mixed
     * @throws AuthenticationException
     */
    public function handle(LoginAttribute $attribute, Closure $next): mixed
    {
        $token = $this->createToken($attribute->user);
        $expiresIn = $this->calculateExpiresIn();
        $access = $this->encryptUserData($attribute->employee);

        $responseDto = new LoginResponseDTO(
            token: $token,
            tokenType: self::TOKEN_TYPE,
            expiresIn: $expiresIn,
            log: $attribute->loginProcedureResult->result,
            access: $access,
            roles:  self::getEmployeeMappedRole($attribute->employee->emp_id)
        );

        $attribute->loginResponseDto = $responseDto;

        return $next($attribute);
    }

    /**
     * @param Authenticatable $user
     * @return string
     * @throws AuthenticationException
     */
    private function createToken(Authenticatable $user): string
    {
        try {
            /** @var string $tokenKey */
            $tokenKey = config('portal.app_passport_key');

            $token = $user->createToken($tokenKey);

            return $token->accessToken;
        } catch (Throwable $exception) {
            throw AuthenticationException::processError($exception->getMessage());
        }
    }

    /**
     * @return int
     */
    private function calculateExpiresIn(): int
    {
        $lifeTimeHours = (int) config('portal.app_token_lifetime_hours');
        $time = time();
        $lifeTime = $lifeTimeHours * self::TIME_UNIT * self::TIME_UNIT;

        return (int) ($time + $lifeTime);
    }

    /**
     * @param Employee $employee
     * @return string
     */
    private function encryptUserData(Employee $employee): string
    {
        return Crypt::encryptString($employee->emp_id.'*'.$employee->name);
    }
}
