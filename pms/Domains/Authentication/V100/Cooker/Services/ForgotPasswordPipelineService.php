<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services;

use Domains\Authentication\V100\Cooker\Attributes\ForgotPasswordAttribute;
use Domains\Authentication\V100\Cooker\Services\Pipelines\ForgotPassword\EncryptToken;
use Domains\Authentication\V100\Cooker\Services\Pipelines\ForgotPassword\RegisterPasswordResetViaProcedure;
use Domains\Authentication\V100\Cooker\Services\Pipelines\ForgotPassword\SendMail;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Models\Employee;
use Support\Resources\BaseResource;

/**
 * Class ForgotPasswordPipelineService.
 */
final class ForgotPasswordPipelineService implements PipelineServiceInterface
{
    private ForgotPasswordAttribute $attribute;

    /**
     * @param PipelineService $service
     */
    public function __construct(
        private PipelineService $service
    ) {
    }

    /**
     * @return array
     */
    public function loadPipes(): array
    {
        return [
            EncryptToken::class,
            RegisterPasswordResetViaProcedure::class,
            SendMail::class,
        ];
    }

    /**
     * @return PipelineServiceInterface
     */
    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(fn () => $this);
    }

    /**
     * @param Employee $data
     * @return PipelineServiceInterface
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new ForgotPasswordAttribute();
        $attribute->user = $data;

        return $this->setAttribute($attribute);
    }

    /**
     * @param ForgotPasswordAttribute $attribute
     * @return PipelineServiceInterface
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * @return BaseResource|AnonymousResourceCollection|array|null
     */
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return null;
    }
}
