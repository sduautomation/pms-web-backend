<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Cooker\Services;

use Domains\Authentication\V100\Cooker\Attributes\LoginAttribute;
use Domains\Authentication\V100\Cooker\DTO\LoginRequestDTO;
use Domains\Authentication\V100\Cooker\Services\Pipelines\Login\AuthenticateUser;
use Domains\Authentication\V100\Cooker\Services\Pipelines\Login\DeleteOldToken;
use Domains\Authentication\V100\Cooker\Services\Pipelines\Login\GenerateResponse;
use Domains\Authentication\V100\Cooker\Services\Pipelines\Login\LoginUserViaProcedure;
use Domains\Authentication\V100\Cooker\Services\Pipelines\Login\SetEmployee;
use Domains\Authentication\V100\Cooker\Services\Pipelines\Login\VerifyUser;
use Domains\Authentication\V100\Resources\LoginResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

/**
 * Class LoginPipelineService.
 */
final class LoginPipelineService implements PipelineServiceInterface
{
    private LoginAttribute $attribute;

    /**
     * @param PipelineService $service
     */
    public function __construct(
        private PipelineService $service
    ) {
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    /**
     * @return array
     */
    public function loadPipes(): array
    {
        return [
            LoginUserViaProcedure::class,
            VerifyUser::class,
            SetEmployee::class,
            DeleteOldToken::class,
            AuthenticateUser::class,
            GenerateResponse::class,
        ];
    }

    /**
     * @param LoginRequestDTO $data
     * @return PipelineServiceInterface
     */
    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new LoginAttribute();
        $attribute->loginRequestDto = $data;

        return $this->setAttribute($attribute);
    }

    /**
     * @param LoginAttribute $attribute
     * @return PipelineServiceInterface
     */
    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * @return BaseResource|AnonymousResourceCollection|array|null
     */
    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new LoginResource($this->attribute->loginResponseDto);
    }
}
