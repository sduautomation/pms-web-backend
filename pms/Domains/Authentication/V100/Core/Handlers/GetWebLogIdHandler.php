<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Core\Handlers;

use Domains\Authentication\V100\Procedures\WebLoginInfoProcedure;
use Support\Core\Exceptions\JsonApi\HttpException;
use Throwable;

/**
 * Class GetWebLogIdHandler.
 */
final class GetWebLogIdHandler
{
    /**
     * @param WebLoginInfoProcedure $procedure
     */
    public function __construct(
        private WebLoginInfoProcedure $procedure
    ) {
    }

    /**
     * @param string $ipAddress
     * @param string $deviceInfo
     * @return int
     * @throws HttpException
     */
    public function handle(string $ipAddress, string $deviceInfo): int
    {
        try {
            $this->procedure->execute(
                ipAddress: $ipAddress,
                deviceInfo: $deviceInfo
            );
        } catch (Throwable $exception) {
            throw new HttpException($exception->getMessage(), $exception->getCode());
        }

        return $this->procedure->getWebLogId();
    }
}
