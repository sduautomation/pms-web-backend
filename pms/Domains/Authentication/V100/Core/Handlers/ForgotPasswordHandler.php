<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Core\Handlers;

use Domains\Authentication\V100\Cooker\DTO\ForgotPasswordDTO;
use Domains\Authentication\V100\Core\Events\ForgotPasswordMailEvent;
use Support\Cooker\Services\CryptoJSEncryptorService;
use Support\Core\Exceptions\EncryptorException;
use Support\Models\Employee;

/**
 * Class ForgotPasswordHandler.
 */
final class ForgotPasswordHandler
{
    /** @psalm-readonly */
    private string $captchaKeyword;

    /**
     * @param CryptoJSEncryptorService $service
     */
    public function __construct(
        private CryptoJSEncryptorService $service
    ) {
        $this->captchaKeyword = (string) config('portal.forgot_password_captcha_keyword');
    }

    /**
     * @param ForgotPasswordDTO $dto
     * @throws EncryptorException
     */
    public function handle(ForgotPasswordDTO $dto): void
    {
        $employee = Employee::byUsername($dto->username);
        $decryptedCaptcha = $this->service->decrypt($dto->captcha);

        if ($decryptedCaptcha !== $this->captchaKeyword) {
            throw EncryptorException::inputDoesNotSatisfiesKeyword('captcha');
        }

        event(new ForgotPasswordMailEvent($employee));
    }
}
