<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Core\Mail;

use Domains\Authentication\V100\Cooker\DTO\ForgotPasswordMailDTO;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class ForgotPasswordMail.
 */
final class ForgotPasswordMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private const SUBJECT_TEXT = 'Reset Password Link - Personnel Management System (https://pms.sdu.edu.kz)';

    /** @psalm-readonly */
    private string $fromMailAddress;

    /**
     * @param ForgotPasswordMailDTO $dto
     */
    public function __construct(
        private ForgotPasswordMailDTO $dto
    ) {
        $this->fromMailAddress = (string) config('portal.no_reply_email');
    }

    public function build(): self
    {
        return $this->from($this->fromMailAddress)
            ->subject(self::SUBJECT_TEXT)
            ->view('mail.reset_password_link', [
                'token' => $this->dto->resetPasswordToken,
                'name' => $this->dto->userName,
                'sname' => $this->dto->userSurname,
            ]);
    }
}
