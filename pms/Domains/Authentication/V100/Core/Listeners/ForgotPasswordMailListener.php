<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Core\Listeners;

use Domains\Authentication\V100\Cooker\Services\ForgotPasswordPipelineService;
use Domains\Authentication\V100\Core\Events\ForgotPasswordMailEvent;

/**
 * Class ForgotPasswordMailListener.
 */
final class ForgotPasswordMailListener
{
    /**
     * @param ForgotPasswordPipelineService $service
     */
    public function __construct(
        private ForgotPasswordPipelineService $service
    ) {
    }

    /**
     * @param ForgotPasswordMailEvent $event
     */
    public function handle(ForgotPasswordMailEvent $event): void
    {
        $this->service
            ->setAttributeFromData($event->user)
            ->handle();
    }
}
