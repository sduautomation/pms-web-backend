<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Core\Exceptions;

use Support\Core\Exceptions\JsonApi\CommonException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AuthenticationException.
 */
final class AuthenticationException extends CommonException
{
    /**
     * @return $this
     */
    public static function incorrectCredentials(): self
    {
        return new self('Incorrect username or password', Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @param string $message
     * @param int $code
     * @return static
     */
    public static function processError(string $message, int $code = Response::HTTP_UNAUTHORIZED): self
    {
        return new self($message, $code);
    }

    /**
     * @return static
     */
    public static function canNotAuthenticateUser(): self
    {
        return new self('Can not authenticate user', Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @return static
     */
    public static function tokenExpired(): self
    {
        return new self('Token is expired', Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @return static
     */
    public static function invalidToken(): self
    {
        return new self('Invalid token', Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @return static
     */
    public static function couldNotResetPassword(): self
    {
        return new self('Could not reset password', Response::HTTP_BAD_REQUEST);
    }
}
