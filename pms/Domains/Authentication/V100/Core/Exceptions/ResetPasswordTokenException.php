<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Core\Exceptions;

use Support\Core\Enums\ErrorCodes;
use Support\Core\Exceptions\JsonApi\CommonException;

/**
 * Class ResetPasswordTokenException.
 */
final class ResetPasswordTokenException extends CommonException
{
    /**
     * @return static
     */
    public static function invalidToken(): self
    {
        return new self('Reset password token is invalid', ErrorCodes::UNPROCESSABLE_ENTITY);
    }
}
