<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Core\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Support\Models\Employee;

/**
 * Class ForgotPasswordMailEvent.
 */
final class ForgotPasswordMailEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private const CHANNEL_NAME = 'channel-name';

    /**
     * @param Employee $user
     */
    public function __construct(
        /** @psalm-readonly */
        public Employee $user
    ) {
    }

    /**
     * @return PrivateChannel
     */
    public function broadcastOn(): PrivateChannel
    {
        return new PrivateChannel(self::CHANNEL_NAME);
    }
}
