<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Procedures;

use Domains\Authentication\V100\Cooker\DTO\ResetPasswordProcedureDTO;
use Support\Core\Exceptions\ProcedureException;
use Support\Procedures\BaseOracleProcedure;

/**
 * Class RegisterPasswordResetProcedure.
 */
final class RegisterPasswordResetProcedure extends BaseOracleProcedure
{
    private const PROCEDURE_NAME = 'dbmaster.ProcPasswResetCodeRegister';

    private const EMPLOYEE_PARAMETER = 'pEmployee';
    private const STUDENT_PARAMETER = 'pStudent';
    private const CODE_PARAMETER = 'pCode';
    private const WEB_LOG_ID_PARAMETER = 'pWeb_log_id';
    private const RESET_TYPE_PARAMETER = 'pReset_type';

    private const STUDENT_PARAMETER_DEFAULT_VALUE = '';
    private const RESET_TYPE_PARAMETER_DEFAULT_VALUE = 1;

    /**
     * @param ResetPasswordProcedureDTO $dto
     * @throws ProcedureException
     */
    public function execute(ResetPasswordProcedureDTO $dto): void
    {
        $parameters = $this->getProcedureParameters($dto);
        $parametersCollection = $this->getProcedureParametersCollection($parameters);

        $this->service->execute(
            procedureName: self::PROCEDURE_NAME,
            parameters: $parametersCollection
        );
    }

    /**
     * @param ResetPasswordProcedureDTO $dto
     * @return array[]
     */
    private function getProcedureParameters(ResetPasswordProcedureDTO $dto): array
    {
        return [
            ['name' => self::EMPLOYEE_PARAMETER, 'value' => $dto->employeeId],
            ['name' => self::STUDENT_PARAMETER, 'value' => self::STUDENT_PARAMETER_DEFAULT_VALUE],
            ['name' => self::CODE_PARAMETER, 'value' => $dto->resetCode],
            ['name' => self::WEB_LOG_ID_PARAMETER, 'value' => $dto->webLogId],
            ['name' => self::RESET_TYPE_PARAMETER, 'value' => self::RESET_TYPE_PARAMETER_DEFAULT_VALUE],
            [
                'name' => self::RESULT_PARAMETER,
                'value' => self::DEFAULT_OUT_PARAMETER_VALUE,
                'is_out' => true,
                'size' => 20,
            ],
        ];
    }
}
