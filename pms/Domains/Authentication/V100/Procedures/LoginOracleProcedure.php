<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Procedures;

use Domains\Authentication\V100\Cooker\DTO\LoginRequestDTO;
use Domains\Authentication\V100\Cooker\ValueObjects\LoginResultValueObject;
use Support\Cooker\ValueObjects\ProcedureParameterValueObject;
use Support\Core\Exceptions\ProcedureException;
use Support\Procedures\BaseOracleProcedure;

/**
 * Class LoginOracleProcedure.
 */
final class LoginOracleProcedure extends BaseOracleProcedure
{
    private const PROCEDURE_NAME = 'dbmaster.LoginEmployee';

    private const USERNAME_PARAMETER = 'pUname';
    private const PASSWORD_PARAMETER = 'pPSW';
    private const IP_ADDRESS_PARAMETER = 'pUserIP';
    private const DEVICE_INFO_PARAMETER = 'pDeviceInfo';
    private const IS_VERIFIED_PARAMETER = 'pIsVerified';

    /**
     * @param LoginRequestDTO $dto
     * @throws ProcedureException
     */
    public function execute(LoginRequestDTO $dto): void
    {
        $parameters = $this->getProcedureParameters($dto);
        $parametersCollection = $this->getProcedureParametersCollection($parameters);

        $this->service->execute(
            procedureName: self::PROCEDURE_NAME,
            parameters: $parametersCollection
        );
    }

    /**
     * @return LoginResultValueObject
     */
    public function getLoginResult(): LoginResultValueObject
    {
        $outputParameters = $this->service->getOutputParameters();

        /** @var ProcedureParameterValueObject $isVerified */
        $isVerified = $outputParameters
            ->where('name', self::IS_VERIFIED_PARAMETER)
            ->first();

        /** @var ProcedureParameterValueObject $result */
        $result = $outputParameters
            ->where('name', self::RESULT_PARAMETER)
            ->first();

        return new LoginResultValueObject(
            isVerified: (int) $isVerified->value,
            result: (int) $result->value
        );
    }

    /**
     * @param LoginRequestDTO $dto
     * @return array[]
     */
    private function getProcedureParameters(LoginRequestDTO $dto): array
    {
        return [
            ['name' => self::USERNAME_PARAMETER, 'value' => $dto->username],
            ['name' => self::PASSWORD_PARAMETER, 'value' => $dto->password],
            ['name' => self::IP_ADDRESS_PARAMETER, 'value' => $dto->ipAddress],
            ['name' => self::DEVICE_INFO_PARAMETER, 'value' => $dto->deviceInfo],
            [
                'name' => self::IS_VERIFIED_PARAMETER,
                'value' => self::DEFAULT_OUT_PARAMETER_VALUE,
                'is_out' => true,
                'size' => 1,
            ],
            [
                'name' => self::RESULT_PARAMETER,
                'value' => self::DEFAULT_OUT_PARAMETER_VALUE,
                'is_out' => true,
                'size' => 20,
            ],
        ];
    }
}
