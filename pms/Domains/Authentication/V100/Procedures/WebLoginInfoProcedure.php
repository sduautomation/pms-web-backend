<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Procedures;

use Support\Cooker\ValueObjects\ProcedureParameterValueObject;
use Support\Core\Exceptions\ProcedureException;
use Support\Procedures\BaseOracleProcedure;

/**
 * Class WebLoginInfoProcedure.
 */
final class WebLoginInfoProcedure extends BaseOracleProcedure
{
    private const PROCEDURE_NAME = 'dbmaster.ProcWebLogInfo';

    private const LOGIN_IP_PARAMETER = 'pLogin_ip';
    private const ID_PARAMETER = 'pId';
    private const DEVICE_INFO_PARAMETER = 'pDeviceInfo';
    protected const RESULT_PARAMETER = self::ID_PARAMETER;

    /**
     * @param string $ipAddress
     * @param string $deviceInfo
     * @throws ProcedureException
     */
    public function execute(string $ipAddress, string $deviceInfo): void
    {
        $parameters = $this->getProcedureParameters($ipAddress, $deviceInfo);
        $parametersCollection = $this->getProcedureParametersCollection($parameters);

        $this->service->execute(
            procedureName: self::PROCEDURE_NAME,
            parameters: $parametersCollection
        );
    }

    /**
     * @return int
     */
    public function getWebLogId(): int
    {
        $outputParameters = $this->service->getOutputParameters();

        /** @var ProcedureParameterValueObject $result */
        $result = $outputParameters
            ->where('name', self::RESULT_PARAMETER)
            ->first();

        return (int) $result->value;
    }

    /**
     * @param string $ipAddress
     * @param string $deviceInfo
     * @return array
     */
    private function getProcedureParameters(string $ipAddress, string $deviceInfo): array
    {
        return [
            ['name' => self::LOGIN_IP_PARAMETER, 'value' => $ipAddress],
            ['name' => self::ID_PARAMETER, 'value' => 0, 'is_out' => true, 'size' => 100],
            ['name' => self::DEVICE_INFO_PARAMETER, 'value' => $deviceInfo],
        ];
    }
}
