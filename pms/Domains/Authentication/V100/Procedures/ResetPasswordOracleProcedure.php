<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Procedures;

use Domains\Authentication\V100\Cooker\ValueObjects\ResetPasswordValueObject;
use Support\Cooker\ValueObjects\ProcedureParameterValueObject;
use Support\Core\Exceptions\ProcedureException;
use Support\Procedures\BaseOracleProcedure;

/**
 * Class ResetPasswordOracleProcedure.
 */
final class ResetPasswordOracleProcedure extends BaseOracleProcedure
{
    private const PROCEDURE_NAME = 'dbmaster.ProcPasswReset';

    private const REGISTER_CODE_PARAMETER = 'pCode';
    private const NEW_PASSWORD_PARAMETER = 'pNew_password';
    private const WEB_LOG_ID_PARAMETER = 'pWeb_log_id';

    /**
     * @param ResetPasswordValueObject $valueObject
     * @throws ProcedureException
     */
    public function execute(ResetPasswordValueObject $valueObject): void
    {
        $parameters = $this->getProcedureParameters($valueObject);
        $parametersCollection = $this->getProcedureParametersCollection($parameters);

        $this->service->execute(
            procedureName: self::PROCEDURE_NAME,
            parameters: $parametersCollection
        );
    }

    /**
     * @return int
     */
    public function getResetPasswordResult(): int
    {
        $outputParameters = $this->service->getOutputParameters();

        /** @var ProcedureParameterValueObject $result */
        $result = $outputParameters
            ->where('name', self::RESULT_PARAMETER)
            ->first();

        return (int) $result->value;
    }

    /**
     * @param ResetPasswordValueObject $valueObject
     * @return array[]
     */
    private function getProcedureParameters(ResetPasswordValueObject $valueObject): array
    {
        return [
            ['name' => self::REGISTER_CODE_PARAMETER, 'value' => $valueObject->registerCode],
            ['name' => self::NEW_PASSWORD_PARAMETER, 'value' => $valueObject->newPassword],
            ['name' => self::WEB_LOG_ID_PARAMETER, 'value' => $valueObject->webLogId],
            [
                'name' => self::RESULT_PARAMETER,
                'value' => self::DEFAULT_OUT_PARAMETER_VALUE,
                'is_out' => true,
                'size' => 100,
            ],
        ];
    }
}
