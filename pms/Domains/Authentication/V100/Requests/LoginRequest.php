<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Requests;

use Domains\Authentication\V100\Cooker\DTO\LoginRequestDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @OA\Schema(
 *     required={"username", "password"},
 *      @OA\Property(
 *         property="username",
 *         type="string"
 *      ),
 *     @OA\Property(
 *         property="password",
 *         type="string"
 *      )
 * )
 * Class LoginRequest.
 */
final class LoginRequest extends FormRequest
{
    /**
     * @return array
     */
    #[ArrayShape(['username' => 'string', 'password' => 'string'])]
    public function rules(): array
    {
        return [
            'username' => 'required|string',
            'password' => 'required|string',
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return LoginRequestDTO
     */
    public function getDto(): LoginRequestDTO
    {
        $validated = $this->validated();
        $username = Arr::get($validated, 'username');
        $password = Arr::get($validated, 'password');
        $ipAddress = $this->ip();
        $deviceInfo = $this->getDeviceInfo();

        return new LoginRequestDTO(
            username: $username,
            password: $password,
            ipAddress: $ipAddress,
            deviceInfo: $deviceInfo
        );
    }
}
