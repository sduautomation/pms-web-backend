<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Requests;

use Illuminate\Contracts\Validation\Validator;
use Support\Core\Exceptions\JsonApi\ValidationException;
use Support\Requests\BaseFormRequest;

/**
 * Class FormRequest.
 */
abstract class FormRequest extends BaseFormRequest
{
    /**
     * @param Validator $validator
     * @throws ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new ValidationException($validator->errors()->first());
    }
}
