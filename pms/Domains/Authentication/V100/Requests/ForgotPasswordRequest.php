<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Requests;

use Domains\Authentication\V100\Cooker\DTO\ForgotPasswordDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @OA\Schema(
 *     required={"employee", "captcha"},
 *      @OA\Property(
 *         property="employee",
 *         type="string"
 *      ),
 *     @OA\Property(
 *         property="captcha",
 *         type="string"
 *      )
 * )
 * Class ForgotPasswordRequest.
 */
final class ForgotPasswordRequest extends FormRequest
{
    /**
     * @return string[]
     */
    #[ArrayShape(['employee' => 'string', 'captcha' => 'string'])]
    public function rules(): array
    {
        return [
            'employee' => 'required|string|min:2',
            'captcha' => 'required|string',
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return ForgotPasswordDTO
     */
    public function getDto(): ForgotPasswordDTO
    {
        $validated = $this->validated();

        return new ForgotPasswordDTO(
            username: Arr::get($validated, 'employee'),
            captcha: Arr::get($validated, 'captcha')
        );
    }
}
