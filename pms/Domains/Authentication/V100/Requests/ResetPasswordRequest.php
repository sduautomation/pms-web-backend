<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Requests;

use Domains\Authentication\V100\Cooker\DTO\ResetPasswordRequestDto;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @OA\Schema(
 *     required={"token", "new_password", "confirm_password"},
 *      @OA\Property(
 *         property="token",
 *         type="string"
 *      ),
 *     @OA\Property(
 *         property="new_password",
 *         type="string"
 *      ),
 *     @OA\Property(
 *         property="confirm_password",
 *         type="string"
 *      )
 * )
 * Class ResetPasswordRequest.
 */
final class ResetPasswordRequest extends FormRequest
{
    /**
     * @return array
     */
    #[ArrayShape(['token' => 'string', 'new_password' => 'string', 'confirm_password' => 'string'])]
    public function rules(): array
    {
        return [
            'token' => 'required|string|min:9',
            'new_password' => 'required|string|min:8|max:30',
            'confirm_password' => 'required|string|same:new_password|min:8|max:30',
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return ResetPasswordRequestDto
     */
    public function getDto(): ResetPasswordRequestDto
    {
        $validated = $this->validated();
        $token = Arr::get($validated, 'token');
        $newPassword = Arr::get($validated, 'new_password');
        $confirmPassword = Arr::get($validated, 'confirm_password');
        $ipAddress = $this->ip();
        $deviceInfo = $this->getDeviceInfo();

        return new ResetPasswordRequestDto(
            token: $token,
            newPassword: $newPassword,
            confirmPassword: $confirmPassword,
            ipAddress: $ipAddress,
            deviceInfo: $deviceInfo
        );
    }
}
