<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Authentication\V100\Cooker\Services\LoginPipelineService;
use Domains\Authentication\V100\Requests\LoginRequest;
use Domains\Authentication\V100\Resources\LoginResource;
use Illuminate\Http\JsonResponse;

/**
 * Class LoginController.
 */
final class LoginController extends Controller
{
    /**
     * @OA\Post(
     *     summary="User authentication",
     *     path="/api/v100/login",
     *     operationId="login",
     *     tags={"authentication", "v100"},
     *     description="User authentication",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/LoginRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="User is successfully authenticated",
     *          @OA\JsonContent(ref="#/components/schemas/LoginResource"),
     *     ),
     * )
     * @param LoginRequest $request
     * @param LoginPipelineService $service
     * @return JsonResponse
     */
    public function __invoke(LoginRequest $request, LoginPipelineService $service): JsonResponse
    {
        /** @var LoginResource $resource */
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'User is successfully authenticated',
            data: $resource
        );
    }
}
