<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Authentication\V100\Core\Handlers\ForgotPasswordHandler;
use Domains\Authentication\V100\Requests\ForgotPasswordRequest;
use Illuminate\Http\JsonResponse;
use Support\Core\Exceptions\JsonApi\HttpException;
use Throwable;

/**
 * Class ForgotPasswordController.
 */
final class ForgotPasswordController extends Controller
{
    /**
     * @OA\Post(
     *     summary="Forgot password",
     *     path="/api/v100/forgot-password",
     *     operationId="forgotPassword",
     *     tags={"authentication", "v100"},
     *     description="Forgot password",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ForgotPasswordRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Link to reset password sent to email",
     *     ),
     * )
     * @param ForgotPasswordRequest $request
     * @param ForgotPasswordHandler $handler
     * @return JsonResponse
     * @throws HttpException
     */
    public function __invoke(ForgotPasswordRequest $request, ForgotPasswordHandler $handler): JsonResponse
    {
        try {
            $handler->handle($request->getDto());
        } catch (Throwable $exception) {
            throw new HttpException($exception->getMessage(), $exception->getCode());
        }

        return $this->response('Link to reset password sent to email');
    }
}
