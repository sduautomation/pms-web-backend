<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Authentication\V100\Cooker\Services\EmployeeInfoPipelineService;
use Domains\Authentication\V100\Resources\EmployeeInfoResource;
use Illuminate\Http\JsonResponse;

final class EmployeeInfoController extends Controller
{
    /**
     * @OA\Post(
     *     summary="User info details",
     *     path="/api/1.0.0/me",
     *     operationId="login",
     *     tags={"employee info", "1.0.0"},
     *     description="This api retrieves information about authed employee",
     *     @OA\Response(
     *          response=200,
     *          description="Employee resource loaded",
     *          @OA\JsonContent(ref="#/components/schemas/EmployeeInfoResource"),
     *     ),
     * )
     * @param EmployeeInfoPipelineService $service
     * @return JsonResponse
     */
    public function __invoke(EmployeeInfoPipelineService $service): JsonResponse
    {
        /** @var EmployeeInfoResource $resource */
        $resource = $service
            ->setAttributeFromData([])
            ->handle()
            ->resource();

        return $this->response(
            message: 'Employee resource loaded',
            data: $resource
        );
    }
}
