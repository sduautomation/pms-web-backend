<?php

declare(strict_types=1);

namespace Domains\Authentication\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Authentication\V100\Cooker\Services\ResetPasswordPipelineService;
use Domains\Authentication\V100\Requests\ResetPasswordRequest;
use Illuminate\Http\JsonResponse;

/**
 * Class ResetPasswordController.
 */
final class ResetPasswordController extends Controller
{
    /**
     * @OA\Post(
     *     summary="User reset password",
     *     path="/api/v100/reset-password",
     *     operationId="resetPassword",
     *     tags={"authentication", "v100"},
     *     description="User reset password",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ResetPasswordRequest")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Password successfully changed"
     *     ),
     * )
     * @param ResetPasswordRequest $request
     * @param ResetPasswordPipelineService $service
     * @return JsonResponse
     */
    public function __invoke(ResetPasswordRequest $request, ResetPasswordPipelineService $service): JsonResponse
    {
        $service
            ->setAttributeFromData($request->getDto())
            ->handle();

        return $this->response('Password successfully changed');
    }
}
