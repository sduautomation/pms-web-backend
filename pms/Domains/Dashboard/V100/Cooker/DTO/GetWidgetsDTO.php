<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\DTO;

final class GetWidgetsDTO
{
    public function __construct(
        public int $employeeId,
        public array $roles
    ) {
    }
}
