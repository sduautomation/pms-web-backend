<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\DTO;

use Domains\Dashboard\V100\Cooker\Collections\AttendanceCollection;
use Domains\Dashboard\V100\Cooker\Collections\CoursesCollection;
use Domains\Dashboard\V100\Cooker\Collections\FacultyStatisticsCollection;
use Domains\Dashboard\V100\Cooker\Collections\GreetingsCollection;
use Domains\Dashboard\V100\Cooker\Collections\RulesAndRegulationsCollection;
use Domains\Dashboard\V100\Cooker\Collections\ScheduleCollection;
use Domains\Dashboard\V100\Cooker\Collections\SyllabusStatisticsCollection;

final class WidgetsResponseDTO
{
    public function __construct(
        public GreetingsCollection $birthdays,
        public ScheduleCollection $schedule,
        public CoursesCollection $courses,
        public AttendanceCollection $attendances,
        public FacultyStatisticsCollection $facultyStatistics,
        public RulesAndRegulationsCollection $rulesAndRegulations,
        public SyllabusStatisticsCollection $syllabusStatistics
    ) {
    }
}
