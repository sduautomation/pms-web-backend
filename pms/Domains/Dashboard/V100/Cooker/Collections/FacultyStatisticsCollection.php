<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\Collections;

use Domains\Dashboard\V100\Cooker\ValueObjects\FacultyStatisticsValueObject;
use http\Exception\RuntimeException;
use Illuminate\Support\Collection;

final class FacultyStatisticsCollection extends Collection
{
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            switch (true) {
                case is_array($item):
                    $self->add(FacultyStatisticsValueObject::fromArray($item));
                    break;
                case is_object($item):
                    $self->add(FacultyStatisticsValueObject::fromObject($item));
                    break;
            }
        }

        return $self;
    }

    public function add($item): self
    {
        if (! $item instanceof FacultyStatisticsValueObject) {
            throw new RuntimeException('Item must be of the type FacultyValueObject');
        }

        return parent::add($item);
    }
}
