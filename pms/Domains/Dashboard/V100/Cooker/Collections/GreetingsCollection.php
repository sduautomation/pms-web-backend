<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\Collections;

use Domains\Dashboard\V100\Cooker\Entities\GreetingEntity;
use http\Exception\RuntimeException;
use Illuminate\Support\Collection;

final class GreetingsCollection extends Collection
{
    /**
     * @param array $items
     * @return static
     */
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            switch (true) {
                case is_array($item):
                    $self->add(GreetingEntity::fromArray($item));
                    break;
                case is_object($item):
                    $self->add(GreetingEntity::fromObject($item));
                    break;
            }
        }

        return $self;
    }

    /**
     * @param mixed $item
     * @return $this
     */
    public function add($item): self
    {
        if (! $item instanceof GreetingEntity) {
            throw new RuntimeException('Item must be of the type GreetingEntity');
        }

        return parent::add($item);
    }
}
