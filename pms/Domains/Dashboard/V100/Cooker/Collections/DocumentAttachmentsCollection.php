<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\Collections;

use Domains\Dashboard\V100\Cooker\Entities\DocumentAttachmentsEntity;
use http\Exception\RuntimeException;
use Illuminate\Support\Collection;

final class DocumentAttachmentsCollection extends Collection
{
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            switch (true) {
                case is_array($item):
                    $self->add(DocumentAttachmentsEntity::fromArray($item));
                    break;
                case is_object($item):
                    $self->add(DocumentAttachmentsEntity::fromObject($item));
                    break;
            }
        }

        return $self;
    }

    /**
     * @param mixed $item
     * @return $this
     */
    public function add($item): self
    {
        if (! $item instanceof DocumentAttachmentsEntity) {
            throw new RuntimeException('Item must be of the type DocumentAttachmentsEntity');
        }

        return parent::add($item);
    }
}
