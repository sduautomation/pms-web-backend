<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\Collections;

use Domains\Dashboard\V100\Cooker\ValueObjects\AttendanceChartValueObject;
use http\Exception\RuntimeException;
use Illuminate\Support\Collection;

final class AttendanceChartCollection extends Collection
{
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            switch (true) {
                case is_array($item):
                    $self->add(AttendanceChartValueObject::fromArray($item));
                    break;
                case is_object($item):
                    $self->add(AttendanceChartValueObject::fromObject($item));
                    break;
            }
        }

        return $self;
    }

    public function add($item): self
    {
        if (! $item instanceof AttendanceChartValueObject) {
            throw new RuntimeException('Item must be of the type AttendanceChartValueObject');
        }

        return parent::add($item);
    }
}
