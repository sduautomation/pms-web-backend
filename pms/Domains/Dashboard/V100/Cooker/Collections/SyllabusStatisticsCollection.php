<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\Collections;

use Domains\Dashboard\V100\Cooker\ValueObjects\SyllabusStatisticsValueObject;
use http\Exception\RuntimeException;
use Illuminate\Support\Collection;

final class SyllabusStatisticsCollection extends Collection
{
    public static function make($items = []): self
    {
        $self = new self();

        foreach ($items as $item) {
            switch (true) {
                case is_array($item):
                    $self->add(SyllabusStatisticsValueObject::fromArray($item));
                    break;
                case is_object($item):
                    $self->add(SyllabusStatisticsValueObject::fromObject($item));
                    break;
            }
        }

        return $self;
    }

    public function add($item): self
    {
        if (! $item instanceof SyllabusStatisticsValueObject) {
            throw new RuntimeException('Item must be of the type SyllabusStatisticsValueObject');
        }

        return parent::add($item);
    }
}
