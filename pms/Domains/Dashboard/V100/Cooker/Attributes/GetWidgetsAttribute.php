<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\Attributes;

use Domains\Dashboard\V100\Cooker\Collections\AttendanceCollection;
use Domains\Dashboard\V100\Cooker\Collections\CoursesCollection;
use Domains\Dashboard\V100\Cooker\Collections\FacultyStatisticsCollection;
use Domains\Dashboard\V100\Cooker\Collections\GreetingsCollection;
use Domains\Dashboard\V100\Cooker\Collections\RulesAndRegulationsCollection;
use Domains\Dashboard\V100\Cooker\Collections\ScheduleCollection;
use Domains\Dashboard\V100\Cooker\Collections\SyllabusStatisticsCollection;
use Domains\Dashboard\V100\Cooker\DTO\GetWidgetsDTO;
use Domains\Dashboard\V100\Cooker\DTO\WidgetsResponseDTO;

final class GetWidgetsAttribute
{
    public GetWidgetsDTO $requestDto;
    public GreetingsCollection $birthdays;
    public ScheduleCollection $schedule;
    public CoursesCollection $courses;
    public AttendanceCollection $attendances;
    public FacultyStatisticsCollection $facultiesStatistics;
    public RulesAndRegulationsCollection $rulesAndRegulations;
    public SyllabusStatisticsCollection $syllabusStatistics;
    public WidgetsResponseDTO $responseDto;
}
