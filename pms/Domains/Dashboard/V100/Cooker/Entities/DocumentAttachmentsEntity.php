<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\Entities;

use Illuminate\Support\Arr;
use JetBrains\PhpStorm\Pure;

final class DocumentAttachmentsEntity
{
    public function __construct(
        public string $title,
        public string $language,
        public string $version,
        public string $path
    ) {
    }

    /**
     * @param array<string, mixed> $data
     */
    public static function fromArray(array $data): self
    {
        return new self(
            title: Arr::get($data, 'title'),
            language: Arr::get($data, 'language'),
            version: Arr::get($data, 'version'),
            path: Arr::get($data, 'path')
        );
    }

    #[Pure]
    public static function fromObject(object $data): self
    {
        return new self(
            title: $data->title,
            language: $data->language,
            version: $data->version,
            path: $data->path
        );
    }
}
