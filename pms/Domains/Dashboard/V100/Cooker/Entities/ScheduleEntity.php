<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\Entities;

use Illuminate\Support\Arr;
use JetBrains\PhpStorm\Pure;

final class ScheduleEntity
{
    public function __construct(
        public int $emp_id,
        public int $weekDay,
        public string $title,
        public string $start_time,
        public string $end_time,
        public string $room
    ) {
    }

    /**
     * @param array<string, mixed> $data
     */
    public static function fromArray(array $data): self
    {
        return new self(
            emp_id: (int) Arr::get($data, 'emp_id'),
            weekDay: (int) Arr::get($data, 'week_d_id'),
            title: Arr::get($data, 'title_en'),
            start_time: Arr::get($data, 'start_time'),
            end_time: Arr::get($data, 'end_time'),
            room: Arr::get($data, 'no')
        );
    }

    #[Pure]
    public static function fromObject(object $data): self
    {
        return new self(
            emp_id: (int) $data->emp_id,
            weekDay: (int) $data->week_d_id,
            title: $data->title_en,
            start_time: $data->start_time,
            end_time: $data->end_time,
            room: $data->no
        );
    }
}
