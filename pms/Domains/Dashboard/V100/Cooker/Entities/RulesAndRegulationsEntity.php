<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\Entities;

use Illuminate\Support\Arr;
use JetBrains\PhpStorm\Pure;

final class RulesAndRegulationsEntity
{
    public function __construct(
        public string $id,
        public string $uuid,
        public string $title,
        public string $description,
        public string $categoryName,
    ) {
    }

    /**
     * @param array<string, mixed> $data
     */
    public static function fromArray(array $data): self
    {
        return new self(
            id: Arr::get($data, 'id'),
            uuid: Arr::get($data, 'uuid'),
            title: Arr::get($data, 'title'),
            description: Arr::get($data, 'description'),
            categoryName: Arr::get($data, 'categoryName'),
        );
    }

    #[Pure]
    public static function fromObject(object $data): self
    {
        return new self(
            id: $data->id,
            uuid: $data->uuid,
            title: $data->title,
            description: $data->description,
            categoryName: $data->categoryName,
        );
    }
}
