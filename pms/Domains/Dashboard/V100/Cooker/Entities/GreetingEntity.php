<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\Entities;

use Illuminate\Support\Arr;
use JetBrains\PhpStorm\Pure;

final class GreetingEntity
{
    public function __construct(
        public int $emp_id,
        public string $name,
        public string $sname,
        public string $birthdate
    ) {
    }

    /**
     * @param array<string, mixed> $data
     */
    public static function fromArray(array $data): self
    {
        return new self(
            emp_id: (int) Arr::get($data, 'emp_id'),
            name: Arr::get($data, 'name'),
            sname: Arr::get($data, 'sname'),
            birthdate: Arr::get($data, 'bithdate')
        );
    }

    #[Pure]
    public static function fromObject(object $data): self
    {
        return new self(
            emp_id: (int) $data->emp_id,
            name: $data->name,
            sname: $data->sname,
            birthdate: $data->birthdate
        );
    }
}
