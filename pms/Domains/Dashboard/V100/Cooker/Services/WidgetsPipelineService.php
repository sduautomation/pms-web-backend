<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\Services;

use Domains\Dashboard\V100\Cooker\Attributes\GetWidgetsAttribute;
use Domains\Dashboard\V100\Cooker\Services\Pipelines\SetAttendances;
use Domains\Dashboard\V100\Cooker\Services\Pipelines\SetCourses;
use Domains\Dashboard\V100\Cooker\Services\Pipelines\SetFacultyStatistics;
use Domains\Dashboard\V100\Cooker\Services\Pipelines\SetGreetings;
use Domains\Dashboard\V100\Cooker\Services\Pipelines\SetResponse;
use Domains\Dashboard\V100\Cooker\Services\Pipelines\SetRulesAndRegulations;
use Domains\Dashboard\V100\Cooker\Services\Pipelines\SetSchedule;
use Domains\Dashboard\V100\Cooker\Services\Pipelines\SetSyllabusStatistics;
use Domains\Dashboard\V100\Resources\GetWidgetsResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Support\Cooker\Services\PipelineService;
use Support\Core\Interfaces\PipelineServiceInterface;
use Support\Resources\BaseResource;

final class WidgetsPipelineService implements PipelineServiceInterface
{
    private GetWidgetsAttribute $attribute;

    public function __construct(
        private PipelineService $service
    ) {
    }

    public function loadPipes(): array
    {
        return [
            SetGreetings::class,
            SetSchedule::class,
            SetCourses::class,
            SetAttendances::class,
            SetFacultyStatistics::class,
            SetRulesAndRegulations::class,
            SetSyllabusStatistics::class,
            SetResponse::class,
        ];
    }

    public function handle(): PipelineServiceInterface
    {
        return $this->service
            ->send($this->attribute)
            ->through($this->loadPipes())
            ->then(function () {
                return $this;
            });
    }

    public function setAttributeFromData(mixed $data): PipelineServiceInterface
    {
        $attribute = new GetWidgetsAttribute();
        $attribute->requestDto = $data;

        return $this->setAttribute($attribute);
    }

    public function setAttribute(mixed $attribute): PipelineServiceInterface
    {
        $this->attribute = $attribute;

        return $this;
    }

    #[Pure]
    public function resource(): BaseResource|AnonymousResourceCollection|array|null
    {
        return new GetWidgetsResource($this->attribute->responseDto);
    }
}
