<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\Services\Pipelines;

use Closure;
use Domains\Dashboard\V100\Cooker\Attributes\GetWidgetsAttribute;
use Domains\Dashboard\V100\Core\Factories\GreetingFactory;

final class SetGreetings
{
    public function handle(GetWidgetsAttribute $attribute, Closure $next): mixed
    {
        $attribute->birthdays = GreetingFactory::makeGreetings();

        return $next($attribute);
    }
}
