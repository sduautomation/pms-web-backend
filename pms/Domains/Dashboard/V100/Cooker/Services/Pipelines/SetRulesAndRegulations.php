<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\Services\Pipelines;

use Closure;
use Domains\Dashboard\V100\Cooker\Attributes\GetWidgetsAttribute;
use Domains\Dashboard\V100\Core\Factories\RulesAndRegulationsFactory;

final class SetRulesAndRegulations
{
    public function handle(GetWidgetsAttribute $attribute, Closure $next)
    {
        $attribute->rulesAndRegulations = RulesAndRegulationsFactory::makeRulesAndRegulations($attribute->requestDto->roles);

        return $next($attribute);
    }
}
