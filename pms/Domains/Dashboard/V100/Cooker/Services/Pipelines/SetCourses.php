<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\Services\Pipelines;

use Closure;
use Domains\Dashboard\V100\Cooker\Attributes\GetWidgetsAttribute;
use Domains\Dashboard\V100\Core\Factories\CourseFactory;

final class SetCourses
{
    public function handle(GetWidgetsAttribute $attribute, Closure $next)
    {
        $attribute->courses = CourseFactory::makeCourses(
            empId: $attribute->requestDto->employeeId,
            roles: $attribute->requestDto->roles
        );

        return $next($attribute);
    }
}
