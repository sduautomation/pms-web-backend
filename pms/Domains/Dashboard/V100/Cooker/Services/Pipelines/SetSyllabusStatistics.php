<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\Services\Pipelines;

use Closure;
use Domains\Dashboard\V100\Cooker\Attributes\GetWidgetsAttribute;
use Domains\Dashboard\V100\Core\Factories\SyllabusStatisticsFactory;

final class SetSyllabusStatistics
{
    public function handle(GetWidgetsAttribute $attribute, Closure $next): mixed
    {
        $attribute->syllabusStatistics = SyllabusStatisticsFactory::makeSyllabuses($attribute->requestDto->roles);

        return $next($attribute);
    }
}
