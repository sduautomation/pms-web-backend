<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\Services\Pipelines;

use Closure;
use Domains\Dashboard\V100\Cooker\Attributes\GetWidgetsAttribute;
use Domains\Dashboard\V100\Cooker\DTO\WidgetsResponseDTO;

final class SetResponse
{
    public function handle(GetWidgetsAttribute $attribute, Closure $next): mixed
    {
        $attribute->responseDto = new WidgetsResponseDTO(
            birthdays: $attribute->birthdays,
            schedule: $attribute->schedule,
            courses:  $attribute->courses,
            attendances: $attribute->attendances,
            facultyStatistics: $attribute->facultiesStatistics,
            rulesAndRegulations: $attribute->rulesAndRegulations,
            syllabusStatistics: $attribute->syllabusStatistics
        );

        return $next($attribute);
    }
}
