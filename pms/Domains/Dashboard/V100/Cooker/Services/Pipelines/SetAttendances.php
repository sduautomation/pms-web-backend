<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\Services\Pipelines;

use Closure;
use Domains\Dashboard\V100\Cooker\Attributes\GetWidgetsAttribute;
use Domains\Dashboard\V100\Core\Factories\AttendanceFactory;

final class SetAttendances
{
    public function handle(GetWidgetsAttribute $attribute, Closure $next): mixed
    {
        $attribute->attendances = AttendanceFactory::makeAttendance(
            empId: $attribute->requestDto->employeeId,
            roles: $attribute->requestDto->roles
        );

        return $next($attribute);
    }
}
