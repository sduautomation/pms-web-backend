<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\ValueObjects;

use Illuminate\Support\Arr;
use JetBrains\PhpStorm\Pure;

final class SyllabusStatisticsValueObject
{
    public function __construct(
        public string $departmentCode,
        public string $title,
        public int $status,
        public string $statusTitle,
        public int $courses
    ) {
    }

    #[Pure]
    public static function fromObject(object $data): self
    {
        return new self(
            departmentCode: $data->dep_code,
            title: $data->title,
            status: (int) $data->status,
            statusTitle: $data->status_title,
            courses: (int) $data->courses
        );
    }

    /**
     * @param array<string, mixed> $data
     */
    public static function fromArray(array $data): self
    {
        return new self(
            departmentCode: Arr::get($data, 'dep_code'),
            title: Arr::get($data, 'title'),
            status: (int) Arr::get($data, 'status'),
            statusTitle: Arr::get($data, 'status_title'),
            courses: (int) Arr::get($data, 'courses')
        );
    }
}
