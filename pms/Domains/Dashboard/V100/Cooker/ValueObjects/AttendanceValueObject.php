<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\ValueObjects;

use Domains\Dashboard\V100\Core\Traits\LoggedUserTrait;
use Illuminate\Support\Arr;

final class AttendanceValueObject
{
    use LoggedUserTrait;

    public function __construct(
        public int $employeeId,
        public string $title,
        public string $courseCode,
        public int $weekDay,
        public string $startTime
    ) {
    }

    /**
     * @param array<string, mixed> $data
     */
    public static function fromArray(array $data): self
    {
        return new self(
            employeeId: self::getCurrentlyLoggedUserId(),
            title: Arr::get($data, 'title_en'),
            courseCode: Arr::get($data, 'ders_kod'),
            weekDay: (int) Arr::get($data, 'week_d_id'),
            startTime: Arr::get($data, 'start_time')
        );
    }

    public static function fromObject(object $data): self
    {
        return new self(
            employeeId: self::getCurrentlyLoggedUserId(),
            title: $data->title_en,
            courseCode: $data->ders_kod,
            weekDay: (int) $data->week_d_id,
            startTime: $data->start_time
        );
    }
}
