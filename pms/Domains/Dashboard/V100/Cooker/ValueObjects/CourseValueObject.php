<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\ValueObjects;

use Illuminate\Support\Arr;
use JetBrains\PhpStorm\Pure;

final class CourseValueObject
{
    public function __construct(
        public string $courseCode,
        public string $title,
        public string $section,
        public string $type,
        public int $credits
    ) {
    }

    #[Pure]
    public static function fromObject(object $data): self
    {
        return new self(
            courseCode: $data->ders_kod,
            title: $data->title,
            section: $data->section,
            type: $data->type,
            credits: (int) $data->credits
        );
    }

    /**
     * @param array<string, mixed> $data
     */
    public static function fromArray(array $data): self
    {
        return new self(
            courseCode: Arr::get($data, 'ders_kod'),
            title: Arr::get($data, 'title'),
            section: Arr::get($data, 'section'),
            type: Arr::get($data, 'type'),
            credits: (int) Arr::get($data, 'credits')
        );
    }
}
