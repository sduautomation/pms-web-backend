<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\ValueObjects;

use Illuminate\Support\Arr;
use JetBrains\PhpStorm\Pure;

final class FacultyStatisticsValueObject
{
    public function __construct(
        public string $title,
        public int $programs,
        public int $courses,
        public int $students,
        public int $employee
    ) {
    }

    #[Pure]
    public static function fromObject(object $data): self
    {
        return new self(
            title: $data->title,
            programs: (int) $data->programs,
            courses: (int) $data->courses,
            students: (int) $data->students,
            employee: (int) $data->employee
        );
    }

    /**
     * @param array<string, mixed> $data
     */
    public static function fromArray(array $data): self
    {
        return new self(
            title: Arr::get($data, 'title'),
            programs: (int) Arr::get($data, 'programs'),
            courses: (int) Arr::get($data, 'courses'),
            students: (int) Arr::get($data, 'students'),
            employee: (int) Arr::get($data, 'employee')
        );
    }
}
