<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Cooker\ValueObjects;

use Illuminate\Support\Arr;
use JetBrains\PhpStorm\Pure;

final class AttendanceChartValueObject
{
    public function __construct(
        public string $lessonDate,
        public bool $conducted,
        public int $absent,
        public int $attended,
        public int $permitted
    ) {
    }

    /**
     * @param array<string, mixed> $data
     */
    public static function fromArray(array $data): self
    {
        return new self(
            lessonDate: Arr::get($data, 'lesson_date'),
            conducted: (bool) Arr::get($data, 'conducted'),
            absent: (int) Arr::get($data, 'absent'),
            attended: (int) Arr::get($data, 'attended'),
            permitted: (int) Arr::get($data, 'permitted')
        );
    }

    #[Pure]
    public static function fromObject(object $data): self
    {
        return new self(
            lessonDate: $data->lesson_date,
            conducted: (bool) $data->conducted,
            absent: (int) $data->absent,
            attended: (int) $data->attended,
            permitted: (int) $data->permitted
        );
    }
}
