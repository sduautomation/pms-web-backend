<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Requests;

use Domains\Dashboard\V100\Cooker\DTO\GetWidgetsDTO;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Support\Core\Enums\RoleTypes;
use Support\Models\Employee;
use Support\Requests\BaseFormRequest;

/**
 * @OA\Schema(
 *     required={"roles"},
 *     @OA\Property(
 *         property="roles",
 *         type="string"
 *     )
 * )
 */
final class GetWidgetsRequest extends BaseFormRequest
{
    #[ArrayShape(['roles' => 'string'])]
    public function rules(): array
    {
        return [
            'roles' => 'required|string',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function getDto(): GetWidgetsDTO
    {
        $validated = $this->validated();
        /** @var Employee $user */
        $user = $this->user();

        $roles = explode(',', Arr::get($validated, 'roles', RoleTypes::ADMINISTRATOR));

        return new GetWidgetsDTO(
            employeeId: $user->emp_id,
            roles: $roles
        );
    }
}
