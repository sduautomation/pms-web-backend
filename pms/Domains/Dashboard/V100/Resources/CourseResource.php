<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Resources;

use Domains\Dashboard\V100\Cooker\ValueObjects\CourseValueObject;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="course_code",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="title",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="section",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="type",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="credits",
 *          type="int"
 *      )
 * )
 * @mixin CourseValueObject
 */
final class CourseResource extends BaseResource
{
    #[ArrayShape(['course_code' => 'string', 'title' => 'string', 'section' => 'string', 'type' => 'string', 'credits' => 'int'])]
    public function getResponseArray(): array
    {
        return [
            'course_code' => $this->courseCode,
            'title' => $this->title,
            'section' => $this->section,
            'type' => $this->type,
            'credits' => $this->credits,
        ];
    }
}
