<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Resources;

use Domains\Dashboard\V100\Cooker\DTO\WidgetsResponseDTO;
use Domains\Dashboard\V100\Core\Factories\MoodleFactory;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="greeting",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/GreetingResource")
 *      ),
 *     @OA\Property(
 *          property="schedule",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/ScheduleResource")
 *      ),
 *     @OA\Property(
 *          property="courses",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/CourseResource")
 *      ),
 *     @OA\Property(
 *          property="attendances",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/AttendanceResource")
 *      ),
 *     @OA\Property(
 *          property="facultyStatistics",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/FacultyStatisticsResource")
 *      ),
 *     @OA\Property(
 *          property="syllabusStatistics",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/SyllabusStatisticsResource")
 *      ),
 *     @OA\Property(
 *          property="rulesAndRegulations",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/RulesAndRegulationsResource")
 *      ),
 *     @OA\Property(
 *          property="moodleActivities",
 *          type="array",
 *          @OA\Items()
 *      )
 * )
 * @mixin WidgetsResponseDTO
 */
final class GetWidgetsResource extends BaseResource
{
    #[ArrayShape(['greeting' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'schedule' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'courses' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'attendances' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'facultyStatistics' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'rulesAndRegulations' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'syllabusStatistics' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'moodleActivities' => "\Illuminate\Database\Eloquent\Collection"])]
    public function getResponseArray(): array
    {
        return [
            'greeting' => GreetingResource::collection($this->birthdays),
            'schedule' => ScheduleResource::collection($this->schedule),
            'courses' => CourseResource::collection($this->courses),
            'attendances' => AttendanceResource::collection($this->attendances),
            'facultyStatistics' => FacultyStatisticsResource::collection($this->facultyStatistics),
            'rulesAndRegulations' => RulesAndRegulationsResource::collection($this->rulesAndRegulations),
            'syllabusStatistics' => SyllabusStatisticsResource::collection($this->syllabusStatistics),
            'moodleActivities' => MoodleFactory::makeMoodle(),
        ];
    }
}
