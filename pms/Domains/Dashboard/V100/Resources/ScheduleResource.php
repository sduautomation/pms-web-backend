<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Resources;

use Domains\Dashboard\V100\Cooker\Entities\ScheduleEntity;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="week_day",
 *          type="integer"
 *      ),
 *     @OA\Property(
 *          property="title",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="start_time",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="end_time",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="room",
 *          type="string"
 *      )
 * )
 * @mixin ScheduleEntity
 */
final class ScheduleResource extends BaseResource
{
    #[ArrayShape(['week_day' => 'int', 'title' => 'string', 'start_time' => 'string', 'end_time' => 'string', 'room' => 'string'])]
    public function getResponseArray(): array
    {
        return [
            'week_day' => $this->weekDay,
            'title' => $this->title,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            'room' => $this->room,
        ];
    }
}
