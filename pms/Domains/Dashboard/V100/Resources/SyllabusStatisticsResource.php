<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Resources;

use Domains\Dashboard\V100\Cooker\ValueObjects\SyllabusStatisticsValueObject;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="departmentCode",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="title",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="status",
 *          type="int"
 *      ),
 *     @OA\Property(
 *          property="statusTitle",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="courses",
 *          type="int"
 *      )
 * )
 * @mixin SyllabusStatisticsValueObject
 */
final class SyllabusStatisticsResource extends BaseResource
{
    #[ArrayShape(['departmentCode' => 'string', 'title' => 'string', 'status' => 'int', 'statusTitle' => 'string', 'courses' => 'int'])]
    public function getResponseArray(): array
    {
        return [
            'departmentCode' => $this->departmentCode,
            'title' => $this->title,
            'status' => $this->status,
            'statusTitle' => $this->statusTitle,
            'courses' => $this->courses,
        ];
    }
}
