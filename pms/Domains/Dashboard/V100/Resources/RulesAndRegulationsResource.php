<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Resources;

use Domains\Dashboard\V100\Cooker\Entities\RulesAndRegulationsEntity;
use Domains\Dashboard\V100\Core\Factories\RulesAndRegulationsDocumentAttachmentsFactory;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="uuid",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="title",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="category_name",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="description",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="attachments",
 *          type="array",
 *          @OA\Items(
 *              @OA\Property(property="title", type="string"),
 *              @OA\Property(property="language", type="string"),
 *              @OA\Property(property="version", type="string"),
 *              @OA\Property(property="path", type="string")
 *          )
 *      )
 * )
 * @mixin RulesAndRegulationsEntity
 */
final class RulesAndRegulationsResource extends BaseResource
{
    #[ArrayShape(['uuid' => 'string', 'title' => 'string', 'description' => 'string', 'category_name' => 'string', 'attachments' => 'array'])]
    public function getResponseArray(): array
    {
        return [
            'uuid' => $this->uuid,
            'title' => $this->title,
            'description' => $this->description,
            'category_name' => $this->categoryName,
            'attachments' => RulesAndRegulationsDocumentAttachmentsFactory::makeDocumentAttachments($this->id),
        ];
    }
}
