<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Resources;

use Domains\Dashboard\V100\Cooker\ValueObjects\FacultyStatisticsValueObject;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="programs",
 *          type="int"
 *      ),
 *     @OA\Property(
 *          property="courses",
 *          type="int"
 *      ),
 *     @OA\Property(
 *          property="students",
 *          type="int"
 *      ),
 *     @OA\Property(
 *          property="employee",
 *          type="int"
 *      )
 * )
 * @mixin FacultyStatisticsValueObject
 */
final class FacultyStatisticsResource extends BaseResource
{
    #[ArrayShape(['title' => 'string', 'programs' => 'int', 'courses' => 'int', 'students' => 'int', 'employee' => 'int'])]
    public function getResponseArray(): array
    {
        return [
            'title' => $this->title,
            'programs' => $this->programs,
            'courses' => $this->courses,
            'students' => $this->students,
            'employee' => $this->employee,
        ];
    }
}
