<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Resources;

use Domains\Dashboard\V100\Cooker\ValueObjects\AttendanceValueObject;
use Domains\Dashboard\V100\Core\Factories\AttendanceChartFactory;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="title",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="course_code",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="week_day",
 *          type="int"
 *      ),
 *     @OA\Property(
 *          property="start_time",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="attendanceChart",
 *          type="array",
 *          @OA\Items(
 *              @OA\Property(property="lessonDate", type="string"),
 *              @OA\Property(property="conducted", type="boolean"),
 *              @OA\Property(property="absent", type="int"),
 *              @OA\Property(property="attended", type="int"),
 *              @OA\Property(property="permitted", type="int")
 *          )
 *      )
 * )
 * @mixin AttendanceValueObject
 */
final class AttendanceResource extends BaseResource
{
    #[ArrayShape(['title' => 'string', 'course_code' => 'string', 'week_day' => 'int', 'start_time' => 'string', 'attendanceChart' => "\Domains\Dashboard\V100\Cooker\Collections\AttendanceChartCollection"])]
    public function getResponseArray(): array
    {
        return [
            'title' => $this->title,
            'course_code' => $this->courseCode,
            'week_day' => $this->weekDay,
            'start_time' => $this->startTime,
            'attendanceChart' => AttendanceChartFactory::makeAttendanceChart($this->employeeId, $this->weekDay, $this->startTime),
        ];
    }
}
