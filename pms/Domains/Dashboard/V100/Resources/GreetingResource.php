<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Resources;

use Domains\Dashboard\V100\Cooker\Entities\GreetingEntity;
use JetBrains\PhpStorm\ArrayShape;
use Support\Resources\BaseResource;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="name",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="surname",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          property="birthdate",
 *          type="string"
 *      )
 * )
 * @mixin GreetingEntity
 */
final class GreetingResource extends BaseResource
{
    #[ArrayShape(['name' => 'string', 'surname' => 'string', 'birthdate' => 'string'])]
    public function getResponseArray(): array
    {
        return [
            'name' => $this->name,
            'surname' => $this->sname,
            'birthdate' => $this->birthdate,
        ];
    }
}
