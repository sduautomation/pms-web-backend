<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Controllers;

use App\Http\Controllers\Controller;
use Domains\Dashboard\V100\Cooker\Services\WidgetsPipelineService;
use Domains\Dashboard\V100\Requests\GetWidgetsRequest;
use Illuminate\Http\JsonResponse;

final class WidgetsController extends Controller
{
    /**
     * @OA\Get(
     *     summary="Get all widgets",
     *     path="/api/V100/dashboard",
     *     operationId="index",
     *     tags={"profile", "V100"},
     *     description="Get all widgets",
     *     parameters={
     *      {"name": "Authorization", "in":"header", "type":"string", "required":true, "description":"Bearer token"}
     *     },
     *     security={{"passport": {}}},
     *     @OA\Response(
     *          response=200,
     *          description="Widgets are successfully returned",
     *          @OA\JsonContent(ref="#/components/schemas/GetWidgetsResource"),
     *     )
     * )
     */
    public function index(GetWidgetsRequest $request, WidgetsPipelineService $service): JsonResponse
    {
        // TODO: Написать тесты для всех виджетов
        $resource = $service
            ->setAttributeFromData($request->getDto())
            ->handle()
            ->resource();

        return $this->response(
            message: 'Resources are loaded',
            data: $resource
        );
    }
}
