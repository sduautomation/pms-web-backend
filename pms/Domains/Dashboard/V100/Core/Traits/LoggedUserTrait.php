<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Core\Traits;

use Support\Models\Employee;

trait LoggedUserTrait
{
    public static function getCurrentlyLoggedUser(): Employee
    {
        return request()->user();
    }

    public static function getCurrentlyLoggedUserId(): int
    {
        return request()->user()->emp_id;
    }
}
