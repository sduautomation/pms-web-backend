<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Core\Factories;

use Domains\Dashboard\V100\Cooker\Collections\GreetingsCollection;
use Domains\Dashboard\V100\Models\EmployeeInfo;

final class GreetingFactory
{
    public static function makeGreetings(): GreetingsCollection
    {
        return EmployeeInfo::getEmployeeBirthdays();
    }
}
