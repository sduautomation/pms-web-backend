<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Core\Factories;

use Domains\Dashboard\V100\Cooker\Collections\AttendanceCollection;
use Domains\Dashboard\V100\Models\EmployeeInfo;
use Support\Core\Enums\RoleTypes;

final class AttendanceFactory
{
    public static function makeAttendance(int $empId, array $roles): AttendanceCollection
    {
        if (in_array(RoleTypes::TEACHER, $roles)) {
            return EmployeeInfo::getEmployeeAttendance($empId);
        } else {
            return AttendanceCollection::make();
        }
    }
}
