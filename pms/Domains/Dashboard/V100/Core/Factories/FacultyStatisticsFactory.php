<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Core\Factories;

use Domains\Dashboard\V100\Cooker\Collections\FacultyStatisticsCollection;
use Domains\Dashboard\V100\Models\EmployeeInfo;
use Support\Core\Enums\RoleTypes;

final class FacultyStatisticsFactory
{
    public static function makeFaculties(array $roles): FacultyStatisticsCollection
    {
        if (in_array(RoleTypes::COORDINATOR, $roles) ||
            in_array(RoleTypes::EXPERT, $roles) ||
            in_array(RoleTypes::ADMINISTRATOR, $roles)) {
            return EmployeeInfo::getFacultyStatistics();
        } else {
            return FacultyStatisticsCollection::make();
        }
    }
}
