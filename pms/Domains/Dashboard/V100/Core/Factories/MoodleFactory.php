<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Core\Factories;

use Illuminate\Database\Eloquent\Collection;

final class MoodleFactory
{
    public static function makeMoodle(): Collection
    {
        return Collection::make();
    }
}
