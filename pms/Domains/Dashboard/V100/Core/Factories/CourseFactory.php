<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Core\Factories;

use Domains\Dashboard\V100\Cooker\Collections\CoursesCollection;
use Domains\Dashboard\V100\Models\EmployeeInfo;
use Support\Core\Enums\RoleTypes;

final class CourseFactory
{
    public static function makeCourses(int $empId, array $roles): CoursesCollection
    {
        if (in_array(RoleTypes::TEACHER, $roles) || in_array(RoleTypes::COORDINATOR, $roles)) {
            return EmployeeInfo::getEmployeeCourses($empId);
        } else {
            return CoursesCollection::make();
        }
    }
}
