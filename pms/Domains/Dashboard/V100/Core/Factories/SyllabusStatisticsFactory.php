<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Core\Factories;

use Domains\Dashboard\V100\Cooker\Collections\SyllabusStatisticsCollection;
use Domains\Dashboard\V100\Models\EmployeeInfo;
use Support\Core\Enums\RoleTypes;

final class SyllabusStatisticsFactory
{
    public static function makeSyllabuses(array $roles): SyllabusStatisticsCollection
    {
        if (in_array(RoleTypes::COORDINATOR, $roles)) {
            return EmployeeInfo::getSyllabusStatistics();
        } else {
            return SyllabusStatisticsCollection::make();
        }
    }
}
