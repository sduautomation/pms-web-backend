<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Core\Factories;

use Domains\Dashboard\V100\Cooker\Collections\RulesAndRegulationsCollection;
use Domains\Dashboard\V100\Models\EmployeeInfo;
use Support\Core\Enums\RoleTypes;

final class RulesAndRegulationsFactory
{
    public static function makeRulesAndRegulations(array $roles): RulesAndRegulationsCollection
    {
        if (in_array(RoleTypes::COORDINATOR, $roles) ||
            in_array(RoleTypes::EXPERT, $roles) ||
            in_array(RoleTypes::ADMINISTRATOR, $roles)) {
            return EmployeeInfo::getRulesAndRegulations();
        } else {
            return RulesAndRegulationsCollection::make();
        }
    }
}
