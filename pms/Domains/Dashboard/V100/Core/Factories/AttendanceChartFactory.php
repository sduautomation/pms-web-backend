<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Core\Factories;

use Domains\Dashboard\V100\Cooker\Collections\AttendanceChartCollection;
use Domains\Dashboard\V100\Models\EmployeeInfo;

final class AttendanceChartFactory
{
    public static function makeAttendanceChart(int $employeeId, int $weekDay, string $startTime): AttendanceChartCollection
    {
        return EmployeeInfo::getAttendanceChart($employeeId, $weekDay, $startTime);
    }
}
