<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Core\Factories;

use Domains\Dashboard\V100\Cooker\Collections\DocumentAttachmentsCollection;
use Domains\Dashboard\V100\Models\EmployeeInfo;

final class RulesAndRegulationsDocumentAttachmentsFactory
{
    public static function makeDocumentAttachments(string $doc_id): DocumentAttachmentsCollection
    {
        return EmployeeInfo::getDocumentAttachmentsByDocID((int) $doc_id);
    }
}
