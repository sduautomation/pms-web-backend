<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Core\Factories;

use Domains\Dashboard\V100\Cooker\Collections\ScheduleCollection;
use Domains\Dashboard\V100\Models\EmployeeInfo;
use Support\Core\Enums\RoleTypes;

final class ScheduleFactory
{
    public static function makeSchedule(int $empId, array $roles): ScheduleCollection
    {
        if (in_array(RoleTypes::TEACHER, $roles) || in_array(RoleTypes::COORDINATOR, $roles)) {
            return EmployeeInfo::getEmployeeSchedule($empId);
        } else {
            return ScheduleCollection::make();
        }
    }
}
