<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string $prog_code
 * @property int $year
 * @property string|null $title_az
 * @property string|null $title_tr
 * @property string|null $title_en
 * @property string|null $dep_code
 * @property string|null $dep_code_f
 * @property string $prog_type
 * @property string|null $edu_lang
 * @property string $edu_level
 * @property int|null $edu_type_id
 * @property int|null $haz_var
 * @property int|null $tez_var
 * @property string|null $period_kod
 * @property int $period_count
 * @property int|null $period_count_max
 * @property int|null $ds_say
 * @property int|null $tqdk_kont
 * @property string|null $ixtisas_kod
 * @property string|null $istiqamet_az
 * @property string|null $istiqamet_tr
 * @property string|null $istiqamet_en
 * @property string|null $derece_az
 * @property string|null $derece_tr
 * @property string|null $derece_en
 * @property int $son
 * @property string|null $pg_type
 * @property string $dep_last
 * @property Carbon|null $last_modified
 * @property string|null $group_code
 * @property string|null $doc_targets
 * @property-read SectionRequest[]|Collection $sectionRequests
 * @property-read DepartmentProgram $departmentProgram
 * @property-read Language $language
 * @property-read EduLevel $eduLevel
 */
final class DepartmentProgram extends Model
{
    /**
     * @var string
     */
    protected $table = 'dbmaster.dep_programs';

    /**
     * @var string[]
     */
    protected $primaryKey = 'prog_code';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'year', // TODO: if auto increment, then we don't need to fill
        'title_az',
        'title_tr',
        'title_en',
        'dep_code',
        'dep_code_f',
        'prog_type',
        'edu_lang',
        'edu_level',
        'edu_type_id',
        'haz_var',
        'tez_var',
        'period_kod',
        'period_count',
        'period_count_max',
        'ds_say',
        'tqdk_kont',
        'ixtisas_kod',
        'istiqamet_az',
        'istiqamet_tr',
        'istiqamet_en',
        'derece_az',
        'derece_tr',
        'derece_en',
        'son',
        'pg_type',
        'dep_last',
        'last_modified',
        'group_code',
        'doc_targets',
    ];

    protected $casts = [
        'year' => 'integer',
        'edu_type_id' => 'integer',
        'haz_var' => 'integer',
        'tez_var' => 'integer',
        'period_count' => 'integer',
        'period_count_max' => 'integer',
        'ds_say' => 'integer',
        'tqdk_kont' => 'integer',
        'son' => 'integer',
        'last_modified' => 'date',
    ];

    public function sectionRequests(): HasMany
    {
        return $this->hasMany(SectionRequest::class, 'prog_code', 'prog_code');
    }

    public function department(): BelongsTo
    {
        return $this->belongsTo(self::class, 'dep_code_f', 'dep_id');
    }

    public function language(): BelongsTo
    {
        return $this->belongsTo(Language::class, 'edu_lang', 'lang_code');
    }

    public function eduLevel(): BelongsTo
    {
        return $this->belongsTo(EduLevel::class, 'edu_level', 'level_code');
    }
}
