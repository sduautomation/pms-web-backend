<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Models\Scopes;

use Domains\Dashboard\V100\Cooker\Collections\GreetingsCollection;
use Illuminate\Support\Facades\DB;
use Support\Core\Traits\LanguageTrait;

/**
 * @mixin LanguageTrait
 */
trait GreetingScopes
{
    /**
     * @return GreetingsCollection
     */
    public static function getEmployeeBirthdays(): GreetingsCollection
    {
        $records = DB::select(
            "select e.emp_id, e.name, e.sname, e.hname, decode(to_char(ei.birth_date, 'MM.DD'), to_char(sysdate, 'MM.DD'), 1, 0) as is_birthday, ei.gender_id as gender, TO_CHAR(ei.birth_date, 'DD Month', 'NLS_DATE_LANGUAGE='||decode('".LanguageTrait::getLocale()."', 'AZ', 'RUSSIAN', 'TR', 'RUSSIAN', 'ENGLISH')) as BIRTHDATE from view_employee e, employee_info ei where e.emp_id = ei.emp_id and ei.bday_announce = 1
       and (extract(year from sysdate)||to_char(ei.birth_date, 'MMDD') between to_char(sysdate, 'YYYYMMDD') and to_char(sysdate + 6, 'YYYYMMDD')
       or extract(year from sysdate) + 1||to_char(ei.birth_date, 'MMDD') between to_char(sysdate, 'YYYYMMDD') and to_char(sysdate + 6, 'YYYYMMDD'))
       order by case when extract(month from sysdate) = extract(month from ei.birth_date) and extract(month from sysdate) = 12
       then extract(year from sysdate) else extract(year from sysdate) + 1 end||to_char(ei.birth_date, 'MMDD'), e.name, e.sname"
        );

        return GreetingsCollection::make($records);
    }
}
