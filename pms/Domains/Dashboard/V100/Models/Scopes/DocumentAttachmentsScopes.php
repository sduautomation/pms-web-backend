<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Models\Scopes;

use Domains\Dashboard\V100\Cooker\Collections\DocumentAttachmentsCollection;
use Illuminate\Support\Facades\DB;

trait DocumentAttachmentsScopes
{
    public static function getDocumentAttachmentsByDocID(int $doc_id): DocumentAttachmentsCollection
    {
        $records = DB::connection('sqlite')->select(
            'select
                        da.title,
                        l.long as language,
                        r.version as version,
                        da.path as path
                    from document_attachments da
                    join languages l on l.id = da.language_id
                    join releases r on r.id = da.version_id
                    where da.doc_id = :document_id
                    ',
            ['document_id' => $doc_id]
        );

        return DocumentAttachmentsCollection::make($records);
    }
}
