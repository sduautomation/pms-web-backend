<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Models\Scopes;

use Domains\Dashboard\V100\Cooker\Collections\SyllabusStatisticsCollection;
use Illuminate\Support\Facades\DB;

trait SyllabusScopes
{
    public static function getSyllabusStatistics(): SyllabusStatisticsCollection
    {
        $records = DB::select(
            "select decode(p.type, 'F', p.dep_code, dp.dep_code) as dep_code, decode(p.type, 'F', p.title_en, dp.title_en) as title, nvl(sc.status, 0) as status,
                    (select s.title_en from application_status s where s.id = nvl(sc.status, 0)) as status_title, count(*) as courses
                    from ders d
                    left outer join cfg_active_period c on c.cfg_key = 'CARI_IL_SEM'
                    left outer join departments dp on dp.dep_code = d.dep_code and dp.son = 1
                    left outer join departments p on p.dep_id = dp.p_id
                    left outer join SYLLABUS_CONFIRM sc on d.ders_kod = sc.course_code and d.year = sc.year
                    where d.year = c.year
                    group by p.type, p.dep_code, decode(p.type, 'F', p.dep_code, dp.dep_code), decode(p.type, 'F', p.title_en, dp.title_en), nvl(sc.status, 0)
                    order by p.dep_code, nvl(sc.status, 0)"
        );

        return SyllabusStatisticsCollection::make($records);
    }
}
