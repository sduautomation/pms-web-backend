<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Models\Scopes;

use Domains\Dashboard\V100\Cooker\Collections\FacultyStatisticsCollection;
use Illuminate\Support\Facades\DB;

trait FacultyScopes
{
    public static function getFacultyStatistics(): FacultyStatisticsCollection
    {
        $records = DB::select(
            "select
                    t.dep_code,
                    t.title_en as title,
                        (select count(*) from dep_programs dp where dp.dep_code_f = t.dep_code and dp.son = 1) as programs,
                        (select count(*) from ders d left outer join departments dd on dd.dep_code = d.dep_code and dd.son = 1
                        left outer join departments pd on pd.dep_id = dd.p_id where pd.dep_code = t.dep_code and dd.year = t.year) as courses,
                        (select count(*) from stud_prog sp left outer join dep_programs dp on dp.prog_code = sp.prog_code and dp.son = 1
                        left outer join students s on s.stud_id = sp.stud_id where s.status = 1 and dp.dep_code_f = t.dep_code) as students,
                        (select count(e.emp_id) from employee e where instr((select listagg(tt.emp, ',') from
                        (select (select listagg(eg.emp_id, ',') from emp_gorev eg left outer join dep_gorev dg on dg.dep_gorev_id = eg.dep_gorev_id
                        where dg.dep_code = d.dep_code and eg.state > 0) as emp from departments d where d.son = 1
                        start with d.dep_code = t.dep_code connect by prior d.dep_id = d.p_id) tt), e.emp_id) > 0) as employee
                        from DEPARTMENTS t
                        where t.son = 1 and t.type = 'F'"
        );

        return FacultyStatisticsCollection::make($records);
    }
}
