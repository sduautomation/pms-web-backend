<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Models\Scopes;

use Domains\Dashboard\V100\Cooker\Collections\RulesAndRegulationsCollection;
use Illuminate\Support\Facades\DB;

trait RulesAndRegulationsScopes
{
    public static function getRulesAndRegulations(): RulesAndRegulationsCollection
    {
        $records = DB::connection('sqlite')->select(
            'select
                    doc.id as id,
                    doc.long_id as uuid,
                    doc.title,
                    doc.description,
                    dc.title as categoryName
                from documents doc
                         join document_attachments da on da.doc_id = doc.id
                         join document_categories dc on doc.category_id = dc.id
                where da.active = 1
                group by da.doc_id'
        );

        return RulesAndRegulationsCollection::make($records);
    }
}
