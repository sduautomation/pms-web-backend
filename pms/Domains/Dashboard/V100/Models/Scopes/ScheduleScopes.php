<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Models\Scopes;

use Domains\Dashboard\V100\Cooker\Collections\ScheduleCollection;
use Illuminate\Support\Facades\DB;

trait ScheduleScopes
{
    public static function getEmployeeSchedule(int $empId): ScheduleCollection
    {
        $records = DB::select(
            "select nvl(sr.prog_code, sq.prog_code) as prog_code,
                        ws.ders_kod,
                        d.title_az,
                        d.title_en,
                        d.title_tr,
                        d.k_teor,
                        d.k_prat,
                        d.k_lab,
                        d.k_qu,
                        d.k_ects,
                        ws.section,
                        sq.quota,
                        ds.type,
                        st.title_az as section_az,
                        st.title_tr as section_tr,
                        st.title_en as section_en,
                        ws.year,
                        ws.term,
                        ds.emp_id,
                        getempfullname(ds.emp_id) as instructor_native,
                        getempfullname(ds.emp_id, 'en') as instructor,
                        s.week_d_id,
                        to_char(s.start_time, 'HH24:MI') as start_time,
                        to_char(s.start_time + s.duration, 'HH24:MI') as end_time,
                        o.bina,
                        o.no,
                        o.description,
                        b.title_az as bina_az,
                        b.title_tr as bina_tr,
                        b.title_en as bina_en
                    from COURSE_W_SCHEDULE ws
                    left outer join ders_sobe ds on ds.ders_kod = ws.ders_kod and ds.year = ws.year and ds.term = ws.term and ds.section = ws.section
                    left outer join ders_saat s on s.ders_s_id = ws.ders_s_id
                    left outer join oda_main o on o.oda_id = ws.oda_id
                    left outer join ders d on d.ders_kod = ws.ders_kod and d.year = ws.year
                    left outer join bina_main b on b.bina_no = o.bina
                    left outer join section_types st on st.key = ds.type
                    left outer join section_quota sq on ds.ders_sobe_id in (sq.normal_sid, sq.lab_sid, sq.practice)
                    left outer join section_requests sr on sq.req_id = sr.req_id
                    left outer join cfg_active_period c on c.cfg_key = 'CARI_IL_SEM'
                    where ws.year = c.year
                    and ws.term = c.term
                    and ds.emp_id = :emp_id",
            ['emp_id' => $empId]
        );

        return ScheduleCollection::make($records);
    }
}
