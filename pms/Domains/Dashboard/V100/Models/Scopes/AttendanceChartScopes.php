<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Models\Scopes;

use Domains\Dashboard\V100\Cooker\Collections\AttendanceChartCollection;
use Illuminate\Support\Facades\DB;

trait AttendanceChartScopes
{
    public static function getAttendanceChart(int $employeeId, int $weekDay, string $startTime): AttendanceChartCollection
    {
        $records = DB::select(
            "select ws.start_time as lesson_date, ws.attended as conducted,
       (select sum(decode(ja.attend, 0, 1, 0)) from JOURN_ATTEND ja where ja.sch_id = ws.id) as absent,
       (select sum(decode(ja.attend, 1, 1, 0)) from JOURN_ATTEND ja where ja.sch_id = ws.id) as attended,
       (select sum(decode(ja.attend, 2, 1, 0)) from JOURN_ATTEND ja where ja.sch_id = ws.id) as permitted,
            ws.ders_kod,
            d.title_az,
            d.title_en,
            d.title_tr,
            d.k_teor,
            d.k_prat,
            d.k_lab,
            d.k_qu,
            d.k_ects,
            ws.section,
            ds.type,
            st.title_az as section_az,
            st.title_tr as section_tr,
            st.title_en as section_en,
            ws.year,
            ws.term,
            ds.emp_id,
            getempfullname(ds.emp_id) as instructor_native,
            getempfullname(ds.emp_id, 'en') as instructor,
            s.week_d_id,
            to_char(s.start_time, 'HH24:MI') as start_time,
            to_char(s.start_time + s.duration, 'HH24:MI') as end_time,
            o.bina,
            o.no,
            o.description,
            b.title_az as bina_az,
            b.title_tr as bina_tr,
            b.title_en as bina_en
         from course_schedule ws
         left outer join ders_sobe ds on ds.ders_kod = ws.ders_kod and ds.year = ws.year and ds.term = ws.term and ds.section = ws.section
         left outer join ders_saat s on s.ders_s_id = ws.ders_s_id
         left outer join oda_main o on o.oda_id = ws.oda_id
         left outer join ders d on d.ders_kod = ws.ders_kod and d.year = ws.year
         left outer join bina_main b on b.bina_no = o.bina
         left outer join section_types st on st.key = ds.type
         left outer join cfg_active_period c on c.cfg_key = 'DERS_QEYDIYYAT'
         where ws.year = c.year
            and ws.term = c.term
            and to_char(s.start_time, 'HH24:MI') = :start_time
            and week_d_id = :week_d_id
            and ds.emp_id = :emp_id
            ",
            ['emp_id' => $employeeId, 'week_d_id' => $weekDay, 'start_time' => $startTime]
        );

        return AttendanceChartCollection::make($records);
    }
}
