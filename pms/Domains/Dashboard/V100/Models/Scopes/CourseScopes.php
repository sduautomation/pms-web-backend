<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Models\Scopes;

use Domains\Dashboard\V100\Cooker\Collections\CoursesCollection;
use Illuminate\Support\Facades\DB;

trait CourseScopes
{
    public static function getEmployeeCourses(int $empId): CoursesCollection
    {
        $records = DB::select(
            "select
                        ds.ders_kod,
                        d.title_en as title,
                        ds.section,
                        ds.type,
                        decode(ds.type, 'N', d.k_teor, 'P', d.k_prat, 'L', d.k_lab) as credits
                    from ders_sobe ds
                    left outer join ders d on d.ders_kod = ds.ders_kod and d.year = ds.year
                    where ds.year = (select to_char(sysdate, 'YYYY') from dual) - 1
                      and ds.term = 2
                      and credits > 0
                      and ds.emp_id_ent = :emp_id",
            ['emp_id' => $empId]
        );

        return CoursesCollection::make($records);
    }
}
