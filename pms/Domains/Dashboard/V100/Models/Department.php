<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $dep_id
 * @property string $dep_code
 * @property string $title_az
 * @property string|null $title_tr
 * @property string $title_en
 * @property string|null $title_short_az
 * @property string|null $title_short_tr
 * @property string|null $title__short_en
 * @property int $year
 * @property int|null $p_id
 * @property string|null $dep_url
 * @property string|null $type
 * @property int $son
 * @property-read DepartmentProgram[]|Collection $departmentPrograms
 */
final class Department extends Model
{
    /**
     * @var string
     */
    protected $table = 'dbmaster.departments';

    /**
     * @var string
     */
    protected $primaryKey = 'dep_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'dep_code',
        'year',
        'p_id',
        'title_az',
        'title_tr',
        'title_en',
        'title_short_az',
        'title_short_tr',
        'title_short_en',
        'dep_url',
        'type',
        'son',
    ];

    protected $casts = [
        'dep_id' => 'integer',
        'year' => 'integer',
        'p_id' => 'integer',
        'son' => 'integer',
    ];

    public function departmentPrograms(): HasMany
    {
        return $this->hasMany(DepartmentProgram::class, 'dep_code_f', 'dep_id');
    }
}
