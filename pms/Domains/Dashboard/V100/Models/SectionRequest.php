<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Models\Employee;

/**
 * @property int $req_id
 * @property string $ders_kod
 * @property int $ders_year
 * @property int $term
 * @property string $prog_code
 * @property int|null $prog_year
 * @property int|null $period_no
 * @property int|null $muf_id
 * @property int $quota
 * @property string|null $req_note
 * @property int|null $req_emp_id
 * @property Carbon|null $req_date
 * @property int|null $status
 * @property string|null $cause_of_reject
 * @property-read Employee $employee
 * @property-read DepartmentProgram $departmentProgram
 * @property-read Lesson $lesson
 */
final class SectionRequest extends Model
{
    /**
     * @var string
     */
    protected $table = 'dbmaster.section_requests';

    /**
     * @var string
     */
    protected $primaryKey = 'req_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'ders_kod',
        'ders_year',
        'term',
        'prog_code',
        'prog_year',
        'period_no',
        'muf_id',
        'quota',
        'req_note',
        'req_emp_id',
        'req_date',
        'status',
        'cause_of_reject',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'req_id' => 'integer',
        'ders_year' => 'integer',
        'term' => 'integer',
        'prog_year' => 'integer',
        'period_no' => 'integer',
        'muf_id' => 'integer',
        'quota' => 'integer',
        'req_emp_id' => 'integer',
        'req_date' => 'date',
        'status' => 'integer',
    ];

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'req_emp_id', 'emp_id');
    }

    public function departmentProgram(): BelongsTo
    {
        return $this->belongsTo(DepartmentProgram::class, 'prog_code', 'prog_code');
    }

    public function lesson(): BelongsTo
    {
        return $this->belongsTo(Lesson::class, 'ders_year', 'year');
    }
}
