<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Support\Models\Employee;

/**
 * @property string $ders_kod
 * @property int $year
 * @property string|null $dep_code
 * @property string|null $title_az
 * @property string|null $title_tr
 * @property string|null $title_en
 * @property string|null $period_code
 * @property int|null $term
 * @property int $k_teor
 * @property int $k_prat
 * @property int $k_lab
 * @property int $k_qu
 * @property int $k_ects
 * @property int $equ_code
 * @property string $lang_code
 * @property string $type
 * @property Carbon|null $open_date
 * @property Carbon|null $close_date
 * @property string|null $locked
 * @property int|null $emp_id
 * @property string $grading_type
 * @property string|null $local_code
 * @property int|null $external_id
 * @property int|null $log_id
 * @property-read Employee $employee
 * @property-read SectionRequest[]|Collection $sectionRequests
 */
final class Lesson extends Model
{
    /**
     * @var string
     */
    protected $table = 'dbmaster.ders';

    /**
     * @var string[]
     */
    protected $primaryKey = ['ders_kod', 'year'];

    /**
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'title_az',
        'title_tr',
        'title_en',
        'dep_code',
        'period_kod',
        'term',
        'k_teor',
        'k_prat',
        'k_lab',
        'k_qu',
        'k_ects',
        'equ_code',
        'lang_code',
        'type',
        'open_date',
        'close_date',
        'locked',
        'emp_id',
        'grading_type',
        'local_code',
        'external_id',
        'log_id',
    ];

    protected $casts = [
        'year' => 'integer',
        'term' => 'integer',
        'k_teor' => 'integer',
        'k_prat' => 'integer',
        'k_lab' => 'integer',
        'k_qu' => 'integer',
        'k_ects' => 'integer',
        'equ_code' => 'integer',
        'open_date' => 'date',
        'close_date' => 'date',
        'emp_id' => 'integer',
        'external_id' => 'integer',
        'log_id' => 'integer',
    ];

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public function sectionRequests(): HasMany
    {
        return $this->hasMany(SectionRequest::class, 'ders_year', 'year');
    }
}
