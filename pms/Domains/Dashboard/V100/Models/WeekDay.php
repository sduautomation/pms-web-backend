<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $week_d_id
 * @property int $weight
 * @property string $title_az
 * @property string $title_tr
 * @property string $title_en
 * @property string|null $title_short_az
 * @property string|null $title_short_tr
 * @property string|null $title__short_en
 * @property-read LessonHour[]|Collection $lessonHours
 */
final class WeekDay extends Model
{
    /**
     * @var string
     */
    protected $table = 'dbmaster.week_days';

    /**
     * @var string
     */
    protected $primaryKey = 'week_d_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'title_az',
        'title_tr',
        'title_en',
        'weight',
        'title_short_az',
        'title_short_en',
        'title_short_tr',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'week_d_id' => 'integer',
        'weight' => 'integer',
    ];

    public function lessonHours(): HasMany
    {
        return $this->hasMany(LessonHour::class, 'week_d_id', 'week_d_id');
    }
}
