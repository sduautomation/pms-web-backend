<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string $lang_code
 * @property string|null $lang_full_name
 * @property string|null $lang_name_az
 * @property string|null $lang_name_tr
 * @property string|null $lang_name_en
 * @property-read DepartmentProgram[]|Collection $departmentPrograms
 */
final class Language extends Model
{
    /**
     * @var string
     */
    protected $table = 'dbmaster.language';

    /**
     * @var string
     */
    protected $primaryKey = 'lang_code';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'lang_full_name',
        'lang_name_az',
        'lang_name_en',
        'lang_name_tr',
    ];

    public function departmentPrograms(): HasMany
    {
        return $this->hasMany(DepartmentProgram::class, 'edu_lang', 'lang_code');
    }
}
