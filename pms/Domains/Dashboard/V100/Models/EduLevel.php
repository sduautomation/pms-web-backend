<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string $level_code
 * @property int|null $weight
 * @property string|null $title_az
 * @property string|null $title_tr
 * @property string|null $title_en
 * @property int|null $is_active
 * @property-read DepartmentProgram[]|Collection $departmentPrograms
 */
final class EduLevel extends Model
{
    /**
     * @var string
     */
    protected $table = 'dbmaster.edu_levels';

    /**
     * @var string
     */
    protected $primaryKey = 'level_code';

    /**
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'title_az',
        'title_tr',
        'title_en',
        'weight',
        'is_active',
    ];

    protected $casts = [
        'weight' => 'integer',
        'is_active' => 'integer',
    ];

    public function departmentPrograms(): HasMany
    {
        return $this->hasMany(DepartmentProgram::class, 'edu_level', 'level_code');
    }
}
