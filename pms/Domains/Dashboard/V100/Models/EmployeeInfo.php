<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Models;

use Carbon\Carbon;
use Domains\Dashboard\V100\Models\Scopes\AttendanceChartScopes;
use Domains\Dashboard\V100\Models\Scopes\AttendanceScopes;
use Domains\Dashboard\V100\Models\Scopes\CourseScopes;
use Domains\Dashboard\V100\Models\Scopes\DocumentAttachmentsScopes;
use Domains\Dashboard\V100\Models\Scopes\FacultyScopes;
use Domains\Dashboard\V100\Models\Scopes\GreetingScopes;
use Domains\Dashboard\V100\Models\Scopes\RulesAndRegulationsScopes;
use Domains\Dashboard\V100\Models\Scopes\ScheduleScopes;
use Domains\Dashboard\V100\Models\Scopes\SyllabusScopes;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $emp_id
 * @property int|null $blood_type_id
 * @property Carbon|null $entrance_date
 * @property string $passport_no
 * @property string|null $card_no
 * @property string|null $inner_telephone
 * @property int $country_id
 * @property Carbon $birth_date
 * @property string|null $inner_email
 * @property string|null $outer_email
 * @property int|null $marriage_status_id
 * @property string|null $insurance_no
 * @property int|null $sec_answ_id
 * @property string|null $website
 * @property string|null $workbook_no
 * @property Carbon|null $pass_issue_date
 * @property int|null $bday_announce
 * @property string|null $pass_authority
 * @property string|null $pass_pinkod
 * @property int|null $toefl
 * @property int|null $ielts
 * @property int|null $school_id
 * @property int $attestat_distinguished
 * @property int|null $attestat_gpa
 * @property Carbon|null $attestat_issue_date
 * @property string|null $attestat_no
 * @property int|null $permission_letter
 * @property int|null $main_work_place
 * @property string|null $iin_no
 * @property string|null $iban_no
 * @property int $gender_id
 * @property int|null $nationality
 */
final class EmployeeInfo extends Model
{
    use GreetingScopes;
    use ScheduleScopes;
    use CourseScopes;
    use AttendanceScopes;
    use FacultyScopes;
    use SyllabusScopes;
    use RulesAndRegulationsScopes;
    use DocumentAttachmentsScopes;
    use AttendanceChartScopes;
    /**
     * @var string
     */
    protected $table = 'dbmaster.emp_id';

    /**
     * @var string
     */
    protected $primaryKey = 'emp_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'blood_type_id',
        'entrance_date',
        'passport_no',
        'card_no',
        'inner_telephone',
        'country_id',
        'birth_date',
        'inner_email',
        'outer_email',
        'marriage_status_id',
        'insurance_no',
        'sec_answ_id',
        'website',
        'workbook_no',
        'pass_issue_date',
        'bday_announce',
        'pass_authority',
        'pass_pinkod',
        'toefl',
        'ielts',
        'school_id',
        'attestat_distinguished',
        'attestat_gpa',
        'attestat_issue_date',
        'attestat_no',
        'permission_letter',
        'main_work_place',
        'iin_no',
        'iban_no',
        'gender_id',
        'nationality',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'blood_type_id' => 'integer',
        'entrance_date' => 'date',
        'country_id' => 'integer',
        'birth_date' => 'date',
        'marriage_status_id' => 'integer',
        'sec_answ_id' => 'integer',
        'pass_issue_date' => 'date',
        'bday_announce' => 'integer',
        'toefl' => 'integer',
        'ielts' => 'integer',
        'school_id' => 'integer',
        'attestat_distinguished' => 'integer',
        'attestat_gpa' => 'integer',
        'attestat_issue_date' => 'date',
        'permission_letter' => 'integer',
        'main_work_place' => 'integer',
        'gender_id' => 'integer',
        'nationality' => 'integer',
    ];
}
