<?php

declare(strict_types=1);

namespace Domains\Dashboard\V100\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $ders_s_id
 * @property int $week_d_id
 * @property Carbon $start_time
 * @property int $duration
 * @property string $bina_no
 * @property int $year
 * @property int $donem
 * @property int $son
 * @property-read WeekDay $weekDay
 * @method static Builder getSchedule()
 */
final class LessonHour extends Model
{
    /**
     * @var string
     */
    protected $table = 'dbmaster.ders_saat';

    /**
     * @var string
     */
    protected $primaryKey = 'ders_s_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'week_d_id',
        'start_time',
        'duration',
        'bina_no',
        'year',
        'donem',
        'son',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'ders_s_id' => 'integer',
        'week_d_id' => 'integer',
        'start_time' => 'date',
        'duration' => 'date',
        'year' => 'integer',
        'donem' => 'integer',
        'son' => 'integer',
    ];

    public function weekDay(): BelongsTo
    {
        return $this->belongsTo(WeekDay::class, 'week_d_id', 'week_d_id');
    }
}
