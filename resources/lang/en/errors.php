<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error messages
    |--------------------------------------------------------------------------
    */

    4001 => 'Please, initiate cors configurations in request header. (Error #4001: Request has CORS conflict.)',
    4002 => 'Please, initiate application language in request header. (Error #4002: Request has LANGUAGE conflict. Content-Language: Not Defined)',
    4003 => 'Invalid request parameters. Some parameters are missing or invalid. (Error #4003)',
    4004 => 'Too many attempts. Please try again in :seconds seconds. (Error #4004)',
    
    4100 => 'These credentials do not match our records.',
    
    4050 => 'Record is not inserted. (Error #4050)',
    4051 => 'Record is not found in our database. (Error #4051)',
    4052 => 'Record is not updated. (Error #4052)',
    4053 => 'Record is not deleted. Database error. (Error #4053)',

    4010 => 'These credentials do not match our records.',
    4011 => 'Username do not match our records. Or captcha is not valid.',
];
