<?php

return [
	'reset_welcome_text' => 'You have requested an email to reset your password',
    'password_forgot_text' => 'We cannot simply send you your old password. A unique link to reset your password has been generated for you. To reset your password, click the following link and follow the instructions.',
    'reset_button_text' => 'Reset Password',
    'password_forgot_button_does_not_work' => 'If the button doesn\'t work click on the following link:',
    'hi' => 'Hi, ',
];
