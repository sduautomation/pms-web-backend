<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Gate Entry Records Request</title>

        <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://getbootstrap.com/docs/4.0/examples/sticky-footer/sticky-footer.css" rel="stylesheet">
    </head>
    <body>
        <main role="main" class="container">
            <h4 class="mt-5">Gate Entry Records Request</h4>
            <p class="lead">Request generated from https://pms.sdu.edu.kz and sent to your email. For more details:</p>
            <p><strong>Username: </strong> {{ $username }}</p>
            <p><strong>Department: </strong> {{ $department }}</p>
            <p><strong>From date: </strong> {{ $from }}</p>
            <p><strong>Due date: </strong> {{ $due }}</p>

            <table class="mt-4 table table-striped" style="width:100%; padding-bottom: 30px;">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Full name</th>
                    <th scope="col">Enter date</th>
                    <th scope="col">Leave date</th>
                    <th scope="col">Enter time</th>
                    <th scope="col">Leave time</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    $counter = 1;
                    foreach ($records as $record) {
                ?>
                <tr>
                    <th scope="row">{{ $counter++ }}</th>
                    <td>{{ $record->fullName }}</td>
                    <td style="text-align: center;">{{ $record->accessDateIn !== null ? $record->accessDateIn->toDateString() : $record->accessDateIn }}</td>
                    <td style="text-align: center;">{{ $record->accessDateOut !== null ? $record->accessDateOut->toDateString() : $record->accessDateOut }}</td>
                    <td style="text-align: center;">{{ $record->accessTimeIn }}</td>
                    <td style="text-align: center;">{{ $record->accessTimeOut }}</td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </main>

        <footer class="footer">
            <div class="container">
                <span class="text-muted">pms.sdu.edu.kz. {{ date('Y') }}</span>
            </div>
        </footer>
    </body>
</html>
