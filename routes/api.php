<?php

use App\Http\Controllers\IndexController;
use Domains\Administrative\v100\Controllers\GateEntryRecordsController;
use Domains\Administrative\v100\Controllers\RulesAndRegulationsController;
use Domains\Administrative\v100\Controllers\SystemCalendarRecordsController;
use Domains\Administrative\v100\Controllers\StaffListController;
use Domains\Authentication\V100\Controllers\EmployeeInfoController;
use Domains\Authentication\V100\Controllers\ForgotPasswordController;
use Domains\Authentication\V100\Controllers\LoginController;
use Domains\Authentication\V100\Controllers\ResetPasswordController;
use Domains\Dashboard\V100\Controllers\WidgetsController;
use Domains\Profile\V100\Controllers\AchievementsController;
use Domains\Profile\V100\Controllers\ArticleController;
use Domains\Profile\V100\Controllers\BiographyController;
use Domains\Profile\V100\Controllers\BookController;
use Domains\Profile\V100\Controllers\ConductedCourseController;
use Domains\Profile\V100\Controllers\ConferenceController;
use Domains\Profile\V100\Controllers\ContactsController;
use Domains\Profile\V100\Controllers\EducationController;
use Domains\Profile\V100\Controllers\ScientificOrganizationController;
use Domains\Profile\V100\Controllers\InfoVisibilitiesController;
use Domains\Profile\V100\Controllers\SkillController;
use Domains\Profile\V100\Controllers\ThesisSupervisionController;
use Domains\Administrative\v100\Controllers\PageResourcesController;
use Domains\Release\V100\Controllers\ReleaseResourceController;
use Domains\Release\V100\Controllers\ChangeLogController;
use Illuminate\Support\Facades\Route;

Route::get('/', [IndexController::class, 'index'])->name('base-index');

Route::prefix('v100')->name('v100-')->group(function () {
    Route::name('auth-')->group(function () {
        Route::post('login', LoginController::class)->name('login');
        Route::post('reset-password', ResetPasswordController::class)->name('reset-password');
        Route::post('forgot-password', ForgotPasswordController::class)->name('forgot-password');
    });

     Route::middleware(['auth:employee-api'])->group(function () {
         Route::get('me', EmployeeInfoController::class)->name('employee-info');

         Route::prefix('administrative')->name('administrative-')->group(function () {
             Route::get('resources', PageResourcesController::class)->name('page-resources');

             Route::get('system-calendar', SystemCalendarRecordsController::class)->name('system-calendar');
             Route::get('staff-list', StaffListController::class)->name('staff-list');
             Route::get('gate-entry', GateEntryRecordsController::class)->name('gate-entry-records');
             Route::prefix('rules-and-regulations')->name('rules-and-regulaions-')->group(function () {
                 Route::get('/{uuid}', [RulesAndRegulationsController::class, 'show'])->name('show');
                 Route::post('', [RulesAndRegulationsController::class ,'store'])->name('create');
                 Route::delete('/{uuid}', [RulesAndRegulationsController::class ,'delete'])->name('delete');
                 Route::get('', [RulesAndRegulationsController::class, 'index'])->name('index');
             });
         });

         Route::prefix('profile')->name('profile-')->group(function () {
             Route::prefix('biographies')->name('biographies-')->group(function () {
                 Route::get('', [BiographyController::class, 'index'])->name('index');
                 Route::post('', [BiographyController::class, 'createBiography'])->name('create');
                 Route::put('{biography}', [BiographyController::class, 'editBiography'])->name('edit');
                 Route::delete('{biography}', [BiographyController::class, 'deleteBiography'])->name('delete');
                 Route::get('{biography}', [BiographyController::class, 'showBiography'])->name('show');
             });

             Route::prefix('children')->name('children-')->group(function () {
                 Route::get('{child}', [BiographyController::class, 'showChild'])->name('show');
                 Route::post('', [BiographyController::class, 'createChild'])->name('create');
                 Route::put('{child}', [BiographyController::class, 'editChild'])->name('edit');
                 Route::delete('{child}', [BiographyController::class, 'deleteChild'])->name('delete');
             });

             Route::prefix('skills')->name('skills-')->group(function () {
                 Route::get('', [SkillController::class, 'index'])->name('index');
                 Route::prefix('languageSkills')->name('languageSkills-')->group(function () {
                     Route::post('', [SkillController::class, 'createLanguageSkill'])->name('create');
                     Route::put('{languageSkill}', [SkillController::class, 'editLanguageSkill'])->name('edit');
                     Route::delete('{languageSkill}', [SkillController::class, 'deleteLanguageSkill'])->name('delete');
                 });

                 Route::prefix('hardSkills')->name('hardSkills-')->group(function () {
                     Route::post('', [SkillController::class, 'createHardSkill'])->name('create');
                     Route::put('{hardSkill}', [SkillController::class, 'editHardSkill'])->name('edit');
                     Route::delete('{hardSkill}', [SkillController::class, 'deleteHardSkill'])->name('delete');
                 });

                 Route::prefix('interests')->name('interests-')->group(function () {
                     Route::put('{interest}', [SkillController::class, 'editInterest'])->name('edit');
                 });
             });

             Route::prefix('achievements')->name('achievements-')->group(function () {
                 Route::get('', [AchievementsController::class, 'index'])->name('index');

                 Route::prefix('projects')->name('projects-')->group(function () {
                     Route::post('', [AchievementsController::class, 'createProject'])->name('create');
                     Route::put('{project}', [AchievementsController::class, 'editProject'])->name('edit');
                     Route::delete('{project}', [AchievementsController::class, 'deleteProject'])->name('delete');
                 });

                 Route::prefix('experience')->name('experience-')->group(function () {
                     Route::post('', [AchievementsController::class, 'createWorkExperience'])->name('create');
                     Route::put('{experience}', [AchievementsController::class, 'editWorkExperience'])->name('edit');
                     Route::delete('{experience}', [AchievementsController::class, 'deleteWorkExperience'])->name('delete');
                 });

                 Route::prefix('awards')->name('awards-')->group(function () {
                     Route::post('', [AchievementsController::class, 'createAward'])->name('create');
                     Route::put('{award}', [AchievementsController::class, 'editAward'])->name('edit');
                     Route::delete('{award}', [AchievementsController::class, 'deleteAward'])->name('delete');
                 });
             });

             Route::prefix('settings')->name('settings-')->group(function () {
                 Route::get('', [InfoVisibilitiesController::class, 'index'])->name('index');
                 Route::post('', [InfoVisibilitiesController::class, 'manage'])->name('edit');
             });

             Route::prefix('contacts')->name('contacts-')->group(function () {
                 Route::get('', [ContactsController::class, 'index'])->name('index');
                 Route::post('', [ContactsController::class, 'create'])->name('create');
                 Route::put('{contact}', [ContactsController::class, 'edit'])->name('edit');
                 Route::delete('{contact}', [ContactsController::class, 'delete'])->name('delete');
             });

             Route::prefix('educations')->name('educations-')->group(function () {
                 Route::get('', [EducationController::class, 'index'])->name('index');
                 Route::post('', [EducationController::class, 'create'])->name('create');
                 Route::put('{education}', [EducationController::class, 'edit'])->name('edit');
                 Route::delete('{education}', [EducationController::class, 'delete'])->name('delete');

                 Route::prefix('thesis')->name('thesis-')->group(function () {
                     Route::post('', [ThesisSupervisionController::class, 'create'])->name('create');
                     Route::put('{thesis}', [ThesisSupervisionController::class, 'edit'])->name('edit');
                     Route::delete('{thesis}', [ThesisSupervisionController::class, 'delete'])->name('delete');
                 });

                 Route::prefix('organizations')->name('organizations-')->group(function () {
                     Route::post('', [ScientificOrganizationController::class, 'create'])->name('create');
                     Route::put('{organization}', [ScientificOrganizationController::class, 'edit'])->name('edit');
                     Route::delete('{organization}', [ScientificOrganizationController::class, 'delete'])->name('delete');
                 });

                 Route::prefix('courses')->name('courses-')->group(function () {
                     Route::post('', [ConductedCourseController::class, 'create'])->name('create');
                     Route::put('{lecture}', [ConductedCourseController::class, 'edit'])->name('edit');
                     Route::delete('{lecture}', [ConductedCourseController::class, 'delete'])->name('delete');
                 });

                 Route::prefix('books')->name('books-')->group(function () {
                     Route::post('', [BookController::class, 'create'])->name('create');
                     Route::put('{book}', [BookController::class, 'edit'])->name('edit');
                     Route::delete('{book}', [BookController::class, 'delete'])->name('delete');
                 });

                 Route::prefix('articles')->name('articles-')->group(function () {
                     Route::post('', [ArticleController::class, 'create'])->name('create');
                     Route::put('{article}', [ArticleController::class, 'edit'])->name('edit');
                     Route::delete('{article}', [ArticleController::class, 'delete'])->name('delete');
                 });

                 Route::prefix('conferences')->name('conferences-')->group(function () {
                     Route::post('', [ConferenceController::class, 'create'])->name('create');
                     Route::put('{conference}', [ConferenceController::class, 'edit'])->name('edit');
                     Route::delete('{conference}', [ConferenceController::class, 'delete'])->name('delete');
                 });
             });
         });

         Route::prefix('releases')->name('release-')->group(function () {
             Route::get('/{uuid}', [ReleaseResourceController::class, 'show'])->name('show');
             Route::delete('/{uuid}', [ReleaseResourceController::class, 'delete'])->name('delete');
             Route::post('', [ReleaseResourceController::class, 'store'])->name('create');
             Route::get('', [ReleaseResourceController::class, 'index'])->name('index');
         });

         Route::prefix('dashboard')->name('dashboard-')->group(function () {
             Route::get('', [WidgetsController::class, 'index'])->name('index');
         });
     });

    Route::get('changelog', ChangeLogController::class)->name('releases-changelog');
});
